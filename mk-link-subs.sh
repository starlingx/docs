#!/bin/bash

l_pattern='\.\.\s+_([a-zA-Z0-9_-]+):\s*$'
out_file="doc/source/_vendor/rl-strings.txt"

# rewrite from scratch
if [[ $1 == "-t" ]] || [[ $1 == "-l" ]]; then
   echo "" > $out_file
fi

for f in $(find . -name "*.rst"); do 

   label=""
   title=""
   in_title_block=0

   while IFS= read -r line || [[ -n "$line" ]]; do

     if [[ "$line" =~ $l_pattern ]]; then
        label="${BASH_REMATCH[1]}"
     fi


     if [[ "$in_title_block" -eq 1 ]]; then
     
        if [[ "$line" == "$title_marker" ]]; then
           break
        else
           in_title_block=0
           title=""
        fi
     fi

     if [[ "$line" =~ ^=+$ ]]; then
     
        prev_line=""
        IFS= read -r prev_line
     
        if [[ "$prev_line" =~ ^[^=]+$ && "${#line}" -eq "${#prev_line}" ]]; then
        
           title="$prev_line"
           title_marker="$line"
           in_title_block=1
        
        fi
     fi

   done <$f

   if [[ $title != "" ]] && [[ $label != "" ]]; then
      if [[ $1 == "-l" ]]; then
         echo ".. |${label}| replace:: :ref:\`${title} <${label}>\`" >> $out_file
      elif [[ $1 == "-t" ]]; then
         echo ".. |${label}| replace:: *${title}*" >> $out_file
      else
         echo -e ".. |${label}| replace:: :ref:\`${label}\`\n.. |${label}| replace:: *${title}*"
      fi
   fi
 
done