import sys
from bs4 import BeautifulSoup

def remove_empty_rows_from_tables(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        soup = BeautifulSoup(file, 'lxml')

    # Find all tables in the document
    tables = soup.find_all('table')

    for table in tables:
        # Find all rows in the table
        rows = table.find_all('tr')
        for row in rows:
            # Check if the row is empty (contains no visible text)
            if not row.get_text(strip=True):
                row.decompose()  # Remove the empty row

    # Save the modified HTML back to the file
    with open(file_path, 'w', encoding='utf-8') as file:
        file.write(str(soup))

if __name__ == '__main__':
    for html_file in sys.argv[1:]:
        remove_empty_rows_from_tables(html_file)
        print(f"Processed {html_file}")

