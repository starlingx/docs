.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. meta::
   :remove-column-from-html-table: Images
   :remove-column-emptied-row: 1
   :docs-build-context: starlingx

|hide-empty-rows|

.. _application-reference-8916dfe370cd:

=====================
Application Reference
=====================

Below is a list of Applications that are supported in |prod|. Use the links in
the table to navigate to the applications based on your requirement.

.. only:: partner

    .. include:: /_includes/application-reference-8916dfe370cd.rest
        :start-after: note-begin
        :end-before: note-end

.. note::

    Applications are automatically updated on patch application and
    automatically downgraded on patch removal.

    Applications on uploaded status will have their old version excluded and
    the new version will be uploaded.

.. list-table::
   :header-rows: 1

   * - Application name
     - Charts
     - Mandatory / Optional
     - Underlying Opensource Project version
     - Link to more detailed application section
     - Images
   * - intel-device-plugins-operator
     - intel-device-plugins-operator-0.30.3

       intel-device-plugins-qat-0.30.1

       intel-device-plugins-gpu-0.30.0

       intel-device-plugins-dsa-0.30.1

       secret-observer-0.1.1
     - Optional
     - v.0.30.0
     - :ref:`intel-device-plugins-operator-application-overview-c5de2a6212ae`
     - docker.io/intel/intel-deviceplugin-operator:0.30.0

       docker.io/intel/intel-qat-plugin:0.30.0

       docker.io/intel/intel-gpu-initcontainer:0.30.0

       docker.io/intel/intel-gpu-plugin:0.30.0

       gcr.io/kubebuilder/kube-rbac-proxy:v0.16.0

       docker.io/intel/intel-dsa-plugin:0.30.0

       docker.io/intel/intel-idxd-config-initcontainer:0.30.0
   * - kubevirt-app
     - kubevirt-app-1.1.0
     - Optional
     - v1.1.0
     - :ref:`introduction-bb3a04279bf5`
     - docker.io/starlingx/stx-kubevirt-app:stx.8.0-v1.1.0

       quay.io/kubevirt/cdi-apiserver:v1.58.0

       quay.io/kubevirt/cdi-cloner:v1.58.0

       quay.io/kubevirt/cdi-controller:v1.58.0

       quay.io/kubevirt/cdi-importer:v1.58.0

       quay.io/kubevirt/cdi-operator:v1.58.0

       quay.io/kubevirt/cdi-uploadproxy:v1.58.0

       quay.io/kubevirt/cdi-uploadserver:v1.58.0

       quay.io/kubevirt/virt-api:v1.1.0

       quay.io/kubevirt/virt-controller:v1.1.0

       quay.io/kubevirt/virt-handler:v1.1.0

       quay.io/kubevirt/virt-launcher:v1.1.0

       quay.io/kubevirt/virt-operator:v1.1.0
   * - power-metrics
     - cadvisor-v0.50.0

       telegraf-v1.1.30
     - Optional
     - cadvisor: v0.50.0

       telegraf: v1.1.30
     - :ref:`install-power-metrics-application-a12de3db7478`
     - docker.io/starlingx/telegraf:stx.10.0-v1.30.2

       gcr.io/cadvisor/cadvisor:v0.49.1
   * - dell-storage
     - karavi-observability-1.8.0

       csi-powermax-2.10.0

       csi-powerstore-2.10.0

       csm-replication-1.8.0

       csi-isilon-2.10.0

       csi-vxflexos-2.10.1

       csi-unity-2.10.0
     - Optional
     - v2.10.0
     - :ref:`dell-storage-backend-28f5771e6d9b`
     - docker.io/dellemc/csi-isilon:v2.10.0

       docker.io/dellemc/csi-metadata-retriever:v1.7.2

       docker.io/dellemc/csi-powermax:v2.10.0

       docker.io/dellemc/csi-powerstore:v2.10.0

       docker.io/dellemc/csi-unity:v2.10.0

       docker.io/dellemc/csi-volumegroup-snapshotter:v1.5.0

       docker.io/dellemc/csi-vxflexos:v2.10.0

       docker.io/dellemc/csipowermax-reverseproxy:v2.9.0

       docker.io/dellemc/csm-authorization-sidecar:v1.10.0

       docker.io/dellemc/csm-encryption:v0.3.0

       docker.io/dellemc/csm-metrics-powerflex:v1.8.0

       docker.io/dellemc/csm-metrics-powermax:v1.3.0

       docker.io/dellemc/csm-metrics-powerscale:v1.5.0

       docker.io/dellemc/csm-metrics-powerstore:v1.8.0

       docker.io/dellemc/csm-topology:v1.8.0

       docker.io/dellemc/dell-csi-migrator:v1.3.0

       docker.io/dellemc/dell-csi-node-rescanner:v1.2.0

       docker.io/dellemc/dell-csi-replicator:v1.8.0

       docker.io/dellemc/dell-replication-controller:v1.8.0

       docker.io/dellemc/podmon:v1.9.0

       docker.io/dellemc/sdc:4.5.1

       docker.io/nginxinc/nginx-unprivileged:1.20

       docker.io/otel/opentelemetry-collector:0.42.0
   * - security-profiles-operator
     - security-profiles-operator-0.8.7
     - Optional
     - v0.8.3
     - :ref:`install-security-profiles-operator-1b2f9a0f0108`
     - registry.k8s.io/security-profiles-operator/security-profiles-operator:v0.8.3

       gcr.io/kubebuilder/kube-rbac-proxy:v0.15.0
   * - ptp-notification
     - ptp-notification-2.0.55
     -
     - v2.0.55
     - :ref:`install-ptp-notifications`
     - docker.io/rabbitmq:3.13.4-management

       docker.io/starlingx/locationservice-base:stx.10.0-v2.2.3

       docker.io/starlingx/notificationclient-base:stx.10.0-v2.2.3

       docker.io/starlingx/notificationservice-base-v2:stx.10.0-v2.2.4

       docker.io/starlingx/notificationservice-base:stx.10.0-v2.2.4
   * - snmp
     - snmp-1.0.3
     - Optional
     - v1.0-36
     - :ref:`snmp-overview`
     - docker.io/starlingx/stx-fm-subagent:stx.9.0-v1.0.1

       docker.io/starlingx/stx-fm-trap-subagent:stx.9.0-v1.0.2

       docker.io/starlingx/stx-snmp:stx.9.0-v1.0.2
   * - auditd
     - auditd-1.0.5
     - Optional
     - N/A
     - :ref:`auditd-support-339a51d8ce16`
     - docker.io/starlingx/stx-audit:stx.10.0-v1.0.5
   * - oidc-auth-apps
     - oidc-auth-secret-observer secret-observer-0.1.6 1.0

       oidc-dex dex-0.18.0+STX.4 2.40.0

       oidc-oidc-client oidc-client-0.1.22 1.0
     - Optional
     - helm-charts/dex: 0.18.0
     - :ref:`configure-oidc-auth-applications`
     - ghcr.io/dexidp/dex:v2.40.0

       docker.io/curlimages/curl:8.8.0

       docker.io/starlingx/stx-oidc-client:stx.9.0-v1.0.7
   * - node-interface-metrics-exporter
     - node-interface-metrics-exporter-0.1.3
     - Optional
     - N/A
     - :ref:`node-interface-metrics-exporter-application-d98b2707c7e9`
     - registry.k8s.io/starlingx/metrics-exporter-api:v1.0.2
   * - nginx-ingress-controller
     - ingress-nginx-4.11.1

       secret-observer-0.1.1
     - Optional
     - v1.11.1
     - :ref:`the-cert-manager-bootstrap-process`
     - registry.k8s.io/defaultbackend-amd64:1.5

       registry.k8s.io/ingress-nginx/controller:v1.11.1

       registry.k8s.io/ingress-nginx/kube-webhook-certgen:v1.4.1

       registry.k8s.io/ingress-nginx/opentelemetry:v20230721-3e2062ee5
   * - metrics-server
     - metrics-server-3.12.1
     - Optional
     - v3.12.1
     - :ref:`kubernetes-admin-tutorials-metrics-server`
     - docker.io/starlingx/stx-metrics-server-sample-app:stx.7.0-v1.0.0

       registry.k8s.io/metrics-server/metrics-server:v0.7.1

       registry.k8s.io/autoscaling/addon-resizer:1.8.20
   * - harbor
     - harbor-1.12.4
     - Optional
     - v2.8.0
     - :ref:`harbor-as-system-app-1d1e3ec59823`
     - docker.io/goharbor/harbor-core:v2.8.0

       docker.io/goharbor/harbor-db:v2.8.0

       docker.io/goharbor/harbor-exporter:v2.8.0

       docker.io/goharbor/harbor-jobservice:v2.8.0

       docker.io/goharbor/nginx-photon:v2.8.0

       docker.io/goharbor/notary-server-photon:v2.8.0

       docker.io/goharbor/notary-signer-photon:v2.8.0

       docker.io/goharbor/harbor-portal:v2.8.0

       docker.io/goharbor/redis-photon:v2.8.0

       docker.io/goharbor/harbor-registryctl:v2.8.0

       docker.io/goharbor/registry-photon:v2.8.0

       docker.io/goharbor/trivy-adapter-photon:v2.8.0
   * - vault
     - vault-0.25.0

       vault-manager-1.0.1
     - Optional
     - Hashicorp Vault 1.14.0,

       Hashicorp vault-helm chart 0.25.0
     - :ref:`security-vault-overview`
     - docker.io/hashicorp/vault:1.14.0

       docker.io/hashicorp/vault-csi-provider:1.4.0

       docker.io/hashicorp/vault-k8s:1.2.1

       docker.io/starlingx/stx-vault-manager:stx.10.0-v1.29.6-1
   * - oran-o2
     - oran-o2-v2.0.4
     - Optional
     - v2.0.4
     - :ref:`oran-o2-application-b50a0c899e66`
     - docker.io/library/postgres:9.6

       docker.io/library/redis:7.0.5-alpine3.16

       docker.io/oranscinf/pti-o2imsdms:2.0.0

       docker.io/oranscinf/pti-o2imsdms:2.0.3

       docker.io/oranscinf/pti-o2imsdms:2.0.4
   * - portieris
     - portieris-certs-0.1.2

       portieris-0.13
     - Optional
     - IBM portieris v0.13.16

       IBM portieris-helm chart v0.13.16
     - :ref:`install-portieris`
     - icr.io/portieris/portieris:v0.13.16
   * - sts-silicom
     - sts-silicom-v0.0.15
     - Optional
     - 727b7ca0afee06c1b93fec8ad518c6b43095e49d
     - :ref:`configure-silicom-sts-ptp-application-1bc4a8d07aad`
     - quay.io/silicom/gpsd:3.23.1

       quay.io/silicom/grpc-tsyncd:2.1.2.18

       quay.io/silicom/phc2sys:3.1-00193-g6bac465

       quay.io/silicom/tsyncd:2.1.3.6
   * - cert-manager
     - cert-manager-v1.15.3
     - Optional
     - v1.15.3
     - :ref:`the-cert-manager-bootstrap-process`
     - quay.io/jetstack/cert-manager-controller v1.15.3

       quay.io/jetstack/cert-manager-webhook:v1.15.3

       quay.io/jetstack/cert-manager-cainjector:v1.15.3

       quay.io/jetstack/cert-manager-acmesolver:v1.15.3

       quay.io/jetstack/cert-manager-startupapicheck:v1.15.3
   * - platform-integ-apps
     - ceph-csi-rbd-3.11.0

       ceph-csi-cephfs-3.11.0

       ceph-pools-audit-0.2.0
     - Optional
     - v3.11.0
     - :ref:`default-behavior-of-the-cephfs-provisioner`
     - registry.k8s.io/sig-storage/csi-provisioner:v4.0.0

       registry.k8s.io/sig-storage/csi-resizer:v1.10.0

       registry.k8s.io/sig-storage/csi-snapshotter:v7.0.1

       registry.k8s.io/sig-storage/csi-attacher:v4.5.0

       registry.k8s.io/sig-storage/csi-node-driver-registrar:v2.10.0

       quay.io/cephcsi/cephcsi:v3.11.0

       docker.io/openstackhelm/ceph-config-helper:ubuntu_jammy_18.2.2-1-20240312
   * - node-feature-discovery
     - node-feature-discovery-v0.16.4
     - Optional
     - v0.16.4
     - :ref:`install-node-feature-discovery-nfd-starlingx-application-70f6f940bb4a`
     - registry.k8s.io/nfd/node-feature-discovery:v0.15.4
   * - sriov-fec-operator
     - sriov-fec-operator-1.0.5
     - Optional
     - v2.9.0
     - :ref:`configure-sriov-fec-operator-to-enable-hw-accelerators-for-hosted-vran-containarized-workloads`
     - docker.io/starlingx/sriov-fec-daemon:stx.10.0-v2.9.0

       docker.io/starlingx/sriov-fec-labeler:stx.10.0-v2.9.0

       docker.io/starlingx/sriov-fec-operator:stx.10.0-v2.9.0

       ghcr.io/k8snetworkplumbingwg/sriov-cni:v2.6.3

       ghcr.io/k8snetworkplumbingwg/sriov-cni:v2.8.1

       ghcr.io/k8snetworkplumbingwg/sriov-network-device-plugin:v3.5.1

       ghcr.io/k8snetworkplumbingwg/sriov-network-device-plugin:v3.6.2

       gcr.io/kubebuilder/kube-rbac-proxy:v0.15.0
   * - istio
     - istio-operator-1.22.1

       kiali-server-1.85.0
     - Optional
     - Istio - v1.22.1

       Kiali - v1.85.0
     - :ref:`istio-service-mesh-application-eee5ebb3d3c4`
     - docker.io/istio/install-cni:1.22.1

       docker.io/istio/operator:1.22.1

       docker.io/istio/pilot:1.22.1

       docker.io/istio/proxyv2:1.22.1

       quay.io/kiali/kiali:v1.85.0
   * - kubernetes-power-manager
     - kubernetes-power-manager-v2.5.1
     - Optional
     - v2.5.1
     - :ref:`configurable-power-manager-04c24b536696`
     - docker.io/intel/power-node-agent:v2.5.0

       docker.io/intel/power-operator:v2.5.0
   * - app-netapp-storage
     - N/A
     - Optional
     - v24.02
     - :ref:`configure-an-external-netapp-deployment-as-the-storage-backend`
     - docker.io/netapp/trident-autosupport:24.02

       docker.io/netapp/trident:24.02.0

       registry.k8s.io/sig-storage/csi-provisioner:v4.0.0

       registry.k8s.io/sig-storage/csi-attacher:v4.5.0

       registry.k8s.io/sig-storage/csi-resizer:v1.9.3

       registry.k8s.io/sig-storage/csi-snapshotter:v6.3.3

       registry.k8s.io/sig-storage/csi-node-driver-registrar:v2.10.0
   * - rook-ceph
     - rook-ceph-1.13.7

       rook-ceph-cluster-1.13.7

       rook-ceph-provisioner-2.0.0

       rook-ceph-floating-monitor-1.0.0
     - Optional
     - v1.13.7
     - :ref:`install-rook-ceph-a7926a1f9b70`
     - registry.local:9001/quay.io/ceph/ceph:v18.2.2

       registry.local:9001/quay.io/cephcsi/cephcsi:v3.10.2

       registry.local:9001/registry.k8s.io/sig-storage/csi-provisioner:v4.0.0

       registry.local:9001/registry.k8s.io/sig-storage/csi-node-driver-registrar:v2.10.0

       registry.local:9001/registry.k8s.io/sig-storage/csi-resizer:v1.10.0

       registry.local:9001/registry.k8s.io/sig-storage/csi-snapshotter:v7.0.1

       registry.local:9001/docker.io/openstackhelm/ceph-config-helper:ubuntu_jammy_18.2.2-1-20240312

       docker.io/rook/ceph:v1.13.7

       registry.k8s.io/sig-storage/csi-attacher:v4.5.0

       quay.io/airshipit/kubernetes-entrypoint:v1.0.0

       docker.io/bitnami/kubectl:1.29

       docker.io/starlingx/stx-ceph-manager:stx.10.0-v18.2.2-0
   * - stx-openstack
     - `starlingx/openstack-armada-app <https://opendev.org/starlingx/openstack-armada-app/src/branch/master/stx-openstack-helm-fluxcd/stx-openstack-helm-fluxcd>`__
     - Optional
     - OpenStack 2024.1 Caracal
     - |_os-install-guide|
     - N/A
   * - .. only:: starlingx

          intel-ethernet-operator
     - .. only:: starlingx

          sriov-network-operator

          intel-ethernet-operator-1.0.0
     - .. only:: starlingx

          Optional
     - .. only:: starlingx

          v1.0.0
     - .. only:: starlingx

          :ref:`configure-intel-e810-nics-using-intel-ethernet-operator`
     - .. only:: starlingx

          N/A
   * - helm
     - N/A
     - Optional
     - v3.12.2
     - :ref:`kubernetes-user-tutorials-helm-package-manager`
     - N/A
   * - FluxCD helm-controller
     - N/A
     - Optional
     - v1.0.1
     - :ref:`kubernetes-user-tutorials-fluxcd-deploy-00d5706c3358`
     - docker.io/fluxcd/helm-controller:v1.0.1

       docker.io/fluxcd/source-controller:v1.3.0
   * - .. only:: partner

          deployment-manager
     - .. only:: partner

          .. include:: /_includes/application-reference-8916dfe370cd.rest
            :start-after: deployment-manager-chart-begin
            :end-before: deployment-manager-chart-end
     - .. only:: partner

          .. include:: /_includes/application-reference-8916dfe370cd.rest
            :start-after: deployment-manager-optional-begin
            :end-before: deployment-manager-optional-end
     - .. only:: partner

          .. include:: /_includes/application-reference-8916dfe370cd.rest
            :start-after: deployment-manager-version-begin
            :end-before: deployment-manager-version-end
     - .. only:: partner

          .. include:: /_includes/application-reference-8916dfe370cd.rest
            :start-after: deployment-manager-ref-begin
            :end-before: deployment-manager-ref-end
     - .. only:: partner

          .. include:: /_includes/application-reference-8916dfe370cd.rest
            :start-after: deployment-manager-image-begin
            :end-before: deployment-manager-image-end
