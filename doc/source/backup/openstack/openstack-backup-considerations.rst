
.. include:: /_stx-related-links/openstack-backup-considerations.rln
.. RL strings
.. include:: /_vendor/rl-strings.txt

.. tye1591106946243
.. _openstack-backup-considerations:

=============================================
Containerized OpenStack Backup Considerations
=============================================

Backup of the containerized OpenStack application is performed as part of the
|prod-long| backup procedures.

See |backing-up-starlingx-system-data|.

