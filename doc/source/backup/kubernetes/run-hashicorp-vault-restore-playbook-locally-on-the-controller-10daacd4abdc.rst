.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _run-hashicorp-vault-restore-playbook-locally-on-the-controller-10daacd4abdc:

==============================================================
Run Hashicorp Vault Restore Playbook Locally on the Controller
==============================================================

Similar to platform restore, Hashicorp Vault restore backup files must be on
the active controller.

You can use an external storage device, for example, a USB drive. Use the
following command to run the Hashicorp Vault Restore playbook:

.. code-block:: none

    ~(keystone_admin)]$ ansible-playbook /usr/share/ansible/stx-ansible/playbooks/vault_restore.yml -e "initial_backup_dir=<location_of_tarball> backup_filename=<backup_filename> on_box_data=<true/false>"

Below you can find other ``-e`` command line options:

**Common**

``-e "initial_backup_dir=/home/sysadmin"``
    Where the backup .tgz files are located on the box.

``-e backup_filename=localhost_hc_vault_backup.tgz``
    The basename of the platform backup tgz. The full path will be a
    combination ``{{initial_backup_dir}/{backup_filename}}}``

``-e backup_encryption_enabled=false``
    If ``backup encryption`` is selected during platform backup, then this option
    is also required when you run the Vault restore procedure. The archive file
    of Vault backup contains metadata which indicates whether the snapshot was
    encrypted with a user supplied passphrase, for example:

    .. code-block:: none

        [sysadmin@controller-0 ~(keystone_admin)]$ VAULT_ARCHIVE=/opt/platform-backup/localhost_hc_vault_backup_2024_07_17_18_00_24.tgz

        [sysadmin@controller-0 ~(keystone_admin)]$ METAF="$( tar tf $VAULT_ARCHIVE | grep "tar.metadata$" )"

        [sysadmin@controller-0 ~(keystone_admin)]$ tar xf "$VAULT_ARCHIVE" -O "$METAF" | jsonpath.py 'user_encrypted'

    Select the ``backup_encryption_enabled`` option if user_encrypted is 'true'
    in the metadata.

``-e backup_encryption_passphrase=<encryption_password>``
    When ``backup_encryption_enabled`` is set to true, then the
    ``backup_encryption_passphrase`` option is also required. Consider using
    ansible-vault to store and specify passwords.

(Optional): You can set these options to specify the backup file location:

-  To define a convenient place to store the backup files, defined by
   ``initial-backup_dir``, on the system (such as the home folder for sysadmin,
   or /tmp, or even a mounted USB device), use the ``on_box_data=true/false``
   parameter.

   If this parameter is set to true, Hashicorp Vault playbook will look
   for the backup file provided on the target server. The parameter
   ``initial_backup_dir`` can be omitted from the command line. In this
   case, the backup file will be under ``/opt/platform-backup`` directory.

   If this parameter is set to false, the Hashicorp Vault playbook will
   look for backup files provided on the Ansible controller. In this
   case, both the ``initial_backup_dir`` and ``backup_filename`` must be
   specified in the command.

   Example of a backup file in ``/home/sysadmin``:

   .. code-block:: none

      ~(keystone_admin)]$ ansible-playbook /usr/share/ansible/stx-ansible/playbooks/vault_restore.yml -e "initial_backup_dir=/home/sysadmin backup_filename=localhost_hc_vault_backup_2020_07_27_07_48_48.tgz"



