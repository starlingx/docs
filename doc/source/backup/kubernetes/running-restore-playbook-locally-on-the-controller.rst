
.. rmy1571265233932
.. _running-restore-playbook-locally-on-the-controller:

==============================================
Run Restore Playbook Locally on the Controller
==============================================

To run restore on the controller, you need to upload the backup to the
active controller.

You can use an external storage device, for example, a USB drive. Use the
following commands to run the Ansible Restore playbook:

**Optimized**: Optmized restore must be used on |AIO-SX|.

.. code-block:: none

    ~(keystone_admin)]$ ansible-playbook /usr/share/ansible/stx-ansible/playbooks/restore_platform.yml -e "restore_mode=optimized initial_backup_dir=<location_of_tarball ansible_become_pass=<admin_password> admin_password=<admin_password backup_filename=<backup_filename> wipe_ceph_osds=<true/false> restore_registry_filesystem=true ssl_ca_certificate_file=<complete path>/<ssl_ca certificates file>"

**Legacy**:  Legacy restore must be used on systems that are not |AIO-SX|.

.. code-block:: none

    ~(keystone_admin)]$ ansible-playbook /usr/share/ansible/stx-ansible/playbooks/restore_platform.yml -e "initial_backup_dir=<location_of_tarball ansible_become_pass=<admin_password> admin_password=<admin_password backup_filename=<backup_filename> wipe_ceph_osds=<true/false> ssl_ca_certificate_file=<complete path>/<ssl_ca certificates file>"

.. note::
    If there are any expired ``ssl_ca`` certificates in the backup, the restore
    (both, legacy and optimized) filters out the expired certificates and
    restores only the valid ones. 

  
Below you can find other ``-e`` command line options:

**Common**

``-e restore_mode=optimized``
    Enable optimized restore mode

    .. note::

        Optimized restore is currently supported only on |AIO-SX| systems.

``-e "initial_backup_dir=/home/sysadmin"``
    Where the backup tgz files are located on box.

``-e backup_filename=localhost_platform_backup.tgz``
    The basename of the platform backup tgz.  The full path will be a
    combination ``{initial_backup_dir}/{backup_filename}``

``-e backup_encryption_enabled=false``
    If ``backup encryption`` is selected during platform backup, then this option
    is also required when you run the platform restore procedure. Select this
    option if the platform backup archive file includes the '.gpg' extension.

``-e backup_encryption_passphrase=<encryption_password>``
    When ``backup_encryption_enabled`` is set to true, then the
    ``backup_encryption_passphrase`` option is also required. Consider using
    ansible-vault to store and specify passwords.

``-e exclude_sw_deployments={true,false}``
    Setting this to true causes the restore to skip any restoring of software
    deployments. By default, this is true for |AIO-SX| and subclouds and false
    for |AIO-DX| systems (except subclouds). Software deployments can only be
    restored using ``exclude_sw_deployments=false`` during backup.

``-e verify_base_commit={true,false}``
    When restoring software deployments, the base commit will be checked to ensure
    that the restore is being done with the original ISO. Set this to false
    to ignore the base commit.

.. _running-restore-playbook-locally-on-the-controller-steps-usl-2c3-pmb:

(Optional): You can select one of the following restore modes:

-   To keep the Ceph cluster data intact (false - default option), use the
    following parameter, when passing the extra arguments to the Ansible
    Restore playbook command:

    .. code-block:: none

       wipe_ceph_osds=false


    To wipe the Ceph cluster entirely (true), where the Ceph cluster will need
    to be recreated, or if the Ceph partition was previously wiped, such as
    during a fresh install between backup and restore or during reinstall, use
    the following parameter:

    .. code-block:: none

       wipe_ceph_osds=true

-   To define a convenient place to store the backup files, defined by
    ``initial-backup_dir``, on the system (such as the home folder for
    sysadmin, or /tmp, or even a mounted USB device), use the
    ``on_box_data=true/false`` parameter.

    If this parameter is set to true, Ansible Restore playbook will look
    for the backup file provided on the target server. The parameter
    ``initial_backup_dir`` can be ommited from the command line. In this
    case, the backup file will be under ``/opt/platform-backup`` directory.

    If this parameter is set to false, the Ansible Restore playbook will
    look for backup file provided on the Ansible controller. In this
    case, both the ``initial_backup_dir`` and ``backup_filename`` must be
    specified in the command.

    Example of a backup file in ``/home/sysadmin``:

    .. code-block:: none

        ~(keystone_admin)]$ ansible-playbook /usr/share/ansible/stx-ansible/playbooks/restore_platform.yml -e "initial_backup_dir=/home/sysadmin ansible_become_pass=St8rlingXCloud* admin_password=St8rlingXCloud* backup_filename=localhost_platform_backup_2020_07_27_07_48_48.tgz wipe_ceph_osds=true"

    .. note::

        If the backup contains patches and ``exclude_sw_deployments=false`` is
        used, Ansible Restore playbook will apply the patches and automatically
        reboot the system if required. Then you will need to re-run Ansible
        Restore playbook.

        The flag ``wipe_ceph_osds=true`` is required for a restore in a new
        hardware. For more details, see :ref:`node-replacement-for-aiominussx-using-optimized-backup-and-restore-6603c650c80d`.


-   ``ssl_ca_certificate_file`` defines a single certificate or a bundle that
    contains all the ``ssl_ca`` certificates that will be installed during the
    restore. 

    .. note::

      The ``ssl_ca`` certificates are not automatically renewed, you MUST renew
      the soon-to-expire certificates before the backup operation. The expired
      ``ssl_ca`` certificates are not restored.
      For more details, see :ref:`Recommended Backup and Retention Policies<recommended-backup-and-retention-policies>`.

    For example:

    .. code-block:: none

        ssl_ca_certificate_file=<complete path>/<ssl_ca certificates file>

        E.g.:

        -e "ssl_ca_certificate_file=/home/sysadmin/new_ca-cert.pem"

    .. note::

        In **legacy** restore, when this option is used, it replaces all
        ``ssl_ca`` certificates in the backup {{
        with the one specified in ``ssl_ca_certificate_file``.

        In the **optimized** restore, when this option is used, it adds certificates
        from ``ssl_ca_certificate_file`` to the existing ``ssl_ca`` certificates in
        the backup” }}.

    This parameter depends on ``on_box_data`` value.

    When ``on_box_data=true`` or not defined, ``ssl_ca_certificate_file``
    will be the location of the ``ssl_ca`` certificate file on the target host.
    This is the default case.

    When ``on_box_data=false``, ``ssl_ca_certificate_file`` will be the
    location of the ``ssl_ca`` certificate file where the Ansible controller is
    running. This is useful for remote play.

    .. note::

        To use this option on local restore mode, you need to download the
        ``ssl_ca`` certificate file to the active controller.

-   The backup contains the bootstrap override values (``localhost.yml``) from
    the time of the backup; for example the registry credentials. There may be
    scenarios where you need to change these values during the restore.

    You can choose from the two options of overrides below:

    *   ``restore_overrides`` used to override parts of the ``localhost.yml``
        found in backup. By default, entire sections will be modified.

        For example:

        To override entire docker registry section:

        #.  Create file ``restore_overrides.yaml``.

            .. code-block:: none

                ---
                restore_overrides:
                  docker_registries:
                    defaults:
                      type: docker
                      username: admin
                      password: Hello123!
                    docker.elastic.co:
                      url: example.com/stx/docker.elastic.co
                    docker.io:
                      url: example.com/stx/docker.io
                    gcr.io:
                      url: example.com/stx/gcr.io
                    ghcr.io:
                      url: example.com/stx/ghcr.io
                    icr.io:
                      url: example.com/stx/icr.io
                    k8s.gcr.io:
                      url: example.com/stx/k8s.gcr.io
                    quay.io:
                      url: example.com/stx/quay.io
                    registry.k8s.io:
                      url: example.com/stx/registry.k8s.io

        #.  Run restore.

            .. code-block:: none

                nsible-playbook restore_platform.yml \
                -e restore_mode=optimized \
                -e "@/path/to/restore_overrides.yaml"


    *   ``restore_overrides_recursive`` used to override specific fields as
        opposed to entire sections.

        An example is to override only the registry password without changing
        any of the other values as opposed to the previous example.

        To override only password:

        #.  Create file ``restore_overrides.yaml``.

            .. code-block:: none

                ---
                restore_overrides_recursive: true
                restore_overrides:
                  docker_registries:
                    defaults:
                      password: Hello123!


        #.  Run restore.

            .. code-block:: none

                ansible-playbook restore_platform.yml \
                -e restore_mode=optimized \
                -e "@/path/to/restore_overrides.yaml"

``-e restore_extra_items``
    Enables the restore playbook to extract additional items from the backup
    during its execution. This is useful when the user has unmanaged files that
    are not included in a normal restore operation.

    The following is an example of restoring some files that had been manually
    modified on the system pre-backup:

    .. code-block::

        ansible-playbook /usr/share/ansible/stx-ansible/playbooks/restore_platform.yml \
          -e "initial_backup_dir=/home/sysadmin" \
          -e "backup_filename=localhost_platform_backup_2024_08_12_22_58_21.tgz"
          -e '{"restore_extra_items": ["etc/pam.d/common-auth", "etc/security/group.conf"]}'

    .. note::

        The ``restore_extra_items`` option will restore files only on
        controller-0. The user must restore files on other nodes after they
        are reinstalled, if required.

**Legacy**

``-e restore_user_images=true``
    Restores the user images created during backup when ``backup_user_images`` was
    true. If the user images are not restored, the images must be pulled from
    upstream or ``registry.central``.

**Optimized**

``-e restore_registry_filesystem=true``
    Restores the registry images created during backup when
    ``backup_registry_filesystem`` was true.  If the registry filesystem is not
    restored the images must be pulled from upstream or registry.central.

``-e registry_backup_filename=custom_name_registry_filesystem_backup.tgz``
    By default this override is not required.  When
    ``restore_registry_filesystem`` is true and a custom name was used during
    backup, ``registry_backup_filename`` needs to be set to match. The full
    path will be a combination
    ``{initial_backup_dir}/{registry_backup_filename}``

.. note::

    After restore is completed it is not possible to restart (or rerun) the
    restore playbook.
