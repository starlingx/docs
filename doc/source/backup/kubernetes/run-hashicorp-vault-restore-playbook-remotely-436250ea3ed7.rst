.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _run-hashicorp-vault-restore-playbook-remotely-436250ea3ed7:

=============================================
Run Hashicorp Vault Restore Playbook Remotely
=============================================

In this method you can run Ansible Hashicorp Vault playbook and point to controller-0.

.. rubric:: |prereq|

.. _system-backup-running-ansible-restore-playbook-remotely-ul-ylm-g44-bkb:

-   It is recommended that you have Ansible version 2.7.5 or higher installed
    on your remote workstation. Copy the Ansible Backup/Restore playbooks
    from directory ``/usr/share/ansible/stx-ansible/playbooks/``.

-   Your network has IPv6 connectivity before running Ansible Playbook, if
    the system configuration is IPv6.

.. rubric:: |proc|

.. _system-backup-running-ansible-restore-playbook-remotely-steps-sgp-jjc-ljb:

#.  Log in to the remote workstation.

    You can log in directly on the console or remotely using :command:`ssh`.

#.  Provide an inventory file, either a customized one that is specified
    using the ``-i`` option, or the default one that is in the Ansible
    configuration directory (that is, /etc/ansible/hosts). You must
    specify the floating |OAM| IP of the controller host. For example, if the
    host name is |prefix|\_Cluster, the inventory file should have an entry
    called |prefix|\_Cluster.

    .. parsed-literal::

        ---
        all:
          hosts:
            wc68:
              ansible_host: 128.222.100.02
            |prefix|\_Cluster:
              ansible_host: 128.224.141.74

#.  Run the Ansible Hashicorp Vault playbook.

    .. code-block:: none

        ~(keystone_admin)]$ ansible-playbook path-to-hashicorp-vault-restore-playbook-entry-file --limit host-name -i inventory-file -e optional-extra-vars

    where ``optional-extra-vars`` can be:

    -   To set a convenient place to store the backup files defined by
        initial-backup_dir on the system (such as the home folder for sysadmin,
        or /tmp, or even a mounted USB device), use the following parameter:

        .. code-block::

           on_box_data=true/false

        If this parameter is set to true, Ansible Hashicorp Vault playbook will look
        for the backup file provided on the target server. The parameter
        ``initial_backup_dir`` can be omitted from the command line. In this
        case, the backup file will be under ``/opt/platform-backup`` directory.

        If this parameter is set to false, the Ansible Hashicorp Vault playbook will
        look for a backup file provided on the Ansible controller. In this
        case, both the ``initial_backup_dir`` and ``backup_filename`` must be
        specified in the command.

    -   ``backup_filename`` is the platform backup tar file. It must be
        provided using the ``-e`` option on the command line, for example:

        .. code-block:: none

           -e backup_filename= localhost_hc_vault_backup_2019_07_15_14_46_37.tgz

    -   The ``initial_backup_dir`` is the location where the platform backup
        tar file is placed to restore the platform. It must be provided using
        ``-e`` option on the command line.

        .. note::

            When ``on_box_data=false``, ``initial_backup_dir`` must be defined.

    -   The :command:`admin_password`, :command:`ansible_become_pass`,
        and :command:`ansible_ssh_pass` need to be set correctly using
        the ``-e`` option on the command line or in the Ansible secret file.
        :command:`ansible_ssh_pass` is the password to the sysadmin user
        on controller-0.

    -   If ``backup encryption`` was enabled when the Vault backup was taken
        then the options ``backup_encryption_enabled=true`` and
        ``backup_encryption_passphrase="<encryption_password>`` are also
        required when restoring the Vault snapshot. Consider storing the
        ``backup_encryption_passphrase`` in the Ansible secret file.

        The archive file of the Vault backup contains metadata which indicates
        whether the snapshot was encrypted with user supplied passphrase,
        for example:

        .. code-block:: none

            [sysadmin@controller-0 ~(keystone_admin)]$ VAULT_ARCHIVE=/opt/platform-backup/localhost_hc_vault_backup_2024_07_17_18_00_24.tgz

            [sysadmin@controller-0 ~(keystone_admin)]$ METAF="$( tar tf $VAULT_ARCHIVE | grep "tar.metadata$" )"

            [sysadmin@controller-0 ~(keystone_admin)]$ tar xf "$VAULT_ARCHIVE" -O "$METAF" | jsonpath.py 'user_encrypted'

        Select the ``backup_encryption_enabled`` option if ``user_encrypted``
        is 'true' in the metadata.
