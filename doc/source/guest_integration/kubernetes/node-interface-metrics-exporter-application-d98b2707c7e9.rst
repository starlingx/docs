.. _node-interface-metrics-exporter-application-d98b2707c7e9:

===========================================
Node Interface Metrics Exporter Application
===========================================

.. rubric:: |context|

The Node Interface Metrics Exporter application is designed to fetch and
display node statistics in a Kubernetes environment. It deploys an Interface
Metrics Exporter DaemonSet on all nodes with the
``starlingx.io/interface-metrics=true node`` label. It uses the Netlink library
to gather data directly from the kernel, offering real-time insights into node
performance.

Node Interface Metrics Exporter provides the following details in the Open
Metrics API format using REST APIs.

#.  Physical Function Interface Device information and Statistics.

    #.  Device information contains device name, hardware address, broadcast,
        duplex, ifalias, operation state and pci-address.

    #.  Statistics contains.

        -   RX/TX packets, RX/TX bytes, multicast, RX/TX errors, RX/TX dropped,
            RX/TX compressed.

        -   RX detailed errors (length, over, crc, frame, missed, fifo).

        -   TX detailed errors (aborted, carrier, heartbeat, window, fifo).

#.  Virtual Function Interface Device information, Statistics and POD
    information.

    #.  Device information contains device name, Virtual Function id,
        pci-adddress, hardware address, vlan, spoofcheck, trust.

    #.  Statistics contains RX/TX packets, RX/TX bytes, RX/TX dropped, TX rate,
        TX rate max, TX rate min.

    #.  POD information contains namespace, POD name, container name, resource
        name, device name, Virtual Function id, hardware address.

---------------------------------------------------
Install Node Interface Metrics Exporter Application
---------------------------------------------------

Follow the steps below to install the Node Interface Metrics Exporter
application.

#.  Locate the application tarball in the ``/usr/local/share/applications/helm``
    directory.

    For example:

    .. code-block:: none

        /usr/local/share/applications/helm/node-interface-metrics-exporter-<version>.tgz

#.  Upload the application using the following command, replacing ``<version>``
    with the latest version number.

    .. code-block:: none

        ~(keystone_admin)]$ system application-upload /usr/local/share/applications/helm node-interface-metrics-exporter-<version>.tgz

#.  Verify that the application has been uploaded successfully.

    .. code-block:: none

        ~(keystone_admin)]$ system application-list

#.  Apply the application using the following command.

    .. code-block:: none

        ~(keystone_admin)]$ system application-apply node-interface-metrics-exporter

    The application should be in applied state.

#.  Monitor the status of the application using one of the following commands.

    .. code-block:: none

        ~(keystone_admin)]$ watch -n 5 system application-list

    OR

    .. code-block:: none

        ~(keystone_admin)]$ watch kubectl get pods -n node-interface-metrics-exporter


-----------------------------------------------------
Uninstall Node Interface Metrics Exporter Application
-----------------------------------------------------

Follow the steps below to uninstall the Node Interface Metrics Exporter
application.

#.  Remove the application using the following command:

    .. code-block:: none

        ~(keystone_admin)]$ system application-remove node-interface-metrics-exporter

#.  Delete application using the following command:

    .. code-block:: none

        ~(keystone_admin)]$ system application-delete node-interface-metrics-exporter

Once uninstallation is complete, all related resources and configurations will
be removed from the system.


-----------------------------------
Node Interface Metrics Exporter API
-----------------------------------

.. note::

    The following APIs are expected to be accessed only from applications
    running internal to the cluster and should not be exported.


`Node Interface Metrics Exporter API <https://docs.starlingx.io/api-ref/app-node-interface-metrics-exporter/index.html>`__ 
section contains the API details