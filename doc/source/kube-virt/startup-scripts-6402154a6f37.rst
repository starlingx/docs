.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _startup-scripts-6402154a6f37:

===============
Startup Scripts
===============

KubeVirt supports the ability to assign a startup script to a |VMI| instance
which is executed automatically when the |VM| initializes.

These scripts are commonly used to automate injection of users and |SSH| keys
into |VMs| in order to provide remote access to the machine. For example, a
startup script can be used to inject credentials into a |VM| that allows an
Ansible job running on a remote host to access and provision the |VM|.

Startup scripts are not limited to any specific use case though. They can be
used to run any arbitrary script in a |VM| on boot.

Example of use:

Assign static IP using ``cloudInitNoCloud``.

.. code-block:: none

    apiVersion: kubevirt.io/v1
    kind: VirtualMachine
    metadata:
      labels:
        special: vmi-sriov-network
      name: vmi-sriov-network
    spec:
      running: true
      template:
        metadata:
          labels:
            kubevirt.io/size: small
            kubevirt.io/domain: fedora
        spec:
          domain:
            cpu:
              cores: 4
            devices:
              disks:
              - name: containerdisk
                disk:
                  bus: virtio
              - name: cloudinitdisk
                disk:
                  bus: virtio
              interfaces:
              - masquerade: {}
                name: default
              - macAddress: "02:00:00:00:00:01"
                name: sriov-net1
                sriov: {}
              rng: {}
            resources:
              requests:
                memory: 1024M
          networks:
          - name: default
            pod: {}
          - multus:
              networkName: sriov-net1
            name: sriov-net1
          volumes:
          - containerDisk:
              image: docker.io/kubevirt/fedora-cloud-container-disk-demo:devel
            name: containerdisk
          - cloudInitNoCloud:
              networkData: |
                ethernets:
                  sriov-net1:
                    addresses:
                    - 10.10.10.14/24
                    gateway: 10.10.10.1
                    match:
                      macAddress: "02:00:00:00:00:01"
                    nameservers:
                      addresses:
                      - 10.96.0.10
                      search:
                      - default.svc.cluster.local
                      - svc.cluster.local
                    set-name: sriov-link-enabled
                                       version: 2
                  #          userData: '#!/bin/bash echo "fedora" | passwd fedora --stdin'
              userData: |-
                #!/bin/bash
                echo "fedora" |passwd fedora --stdin
            name: cloudinitdisk
