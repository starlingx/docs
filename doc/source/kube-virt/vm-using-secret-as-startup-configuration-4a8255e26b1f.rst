.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _vm-using-secret-as-startup-configuration-4a8255e26b1f:

========================================
VM Using Secret as Startup Configuration
========================================

A secret can be presented to the |VM| as disk or as a filesystem.

The disk method does not support dynamic change propagation and the filesystem
method does not support live migration. Therefore, depending on the use-case,
one or the other may be more suitable.

Example of the creation of a Secret:

.. code-block:: none

    apiVersion: v1
    kind: Secret
    metadata:
      name: app-secret
    type: Opaque
    data:
      username: YWxheA==
      password: TGk2OW51eCo=

Example of a |VM| using secret as filesystem:

.. code-block:: none

    apiVersion: kubevirt.io/v1
    kind: VirtualMachineInstance
    metadata:
      labels:
        special: vmi-fedora
      name: vmi-fedora-secret
    spec:
      domain:
        devices:
          filesystems:
            - name: app-secret-fs
              virtiofs: {}
          disks:
            - disk:
                bus: virtio
              name: containerdisk
        machine:
          type: ""
        resources:
          requests:
            memory: 1024M
      terminationGracePeriodSeconds: 0
      volumes:
        - name: containerdisk
          containerDisk:
            image: quay.io/containerdisks/fedora:latest
        - cloudInitNoCloud:
            userData: |-
              #cloud-config
              chpasswd:
                expire: false
              password: fedora
              user: fedora
              bootcmd:
                # mount the Secret
                - "sudo mkdir /mnt/app-secret"
                - "sudo mount -t virtiofs app-secret-fs /mnt/app-secret"
          name: cloudinitdisk
        - secret:
            secretName: app-secret
          name: app-secret-fs


Then, you can login to |VM| to verify:

.. code-block:: none

    [fedora@vmi-fedora-secret ~]$ df -h
    Filesystem      Size  Used Avail Use% Mounted on
    /dev/vda5       4.0G  453M  3.1G  13% /
    devtmpfs        4.0M     0  4.0M   0% /dev
    tmpfs           450M     0  450M   0% /dev/shm
    tmpfs           180M  720K  179M   1% /run
    tmpfs           450M     0  450M   0% /tmp
    /dev/vda2       966M   61M  840M   7% /boot
    /dev/vda3       100M   12M   89M  12% /boot/efi
    /dev/vda5       4.0G  453M  3.1G  13% /home
    app-secret-fs    94G  8.0K   94G   1% /mnt/app-secret
    tmpfs            90M  4.0K   90M   1% /run/user/1000
    [fedora@vmi-fedora-secret ~]$ ls -lrth /mnt/app-secret
    total 0
    lrwxrwxrwx. 1 root 107 15 Jan 15 16:43 username -> ..data/username
    lrwxrwxrwx. 1 root 107 15 Jan 15 16:43 password -> ..data/password
