.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _persistent-storage-for-vms-8ddc8fa611aa:

==========================
Persistent Storage for VMs
==========================

Allows connecting a ``PersistentVolumeClaim`` to a |VM| disk.

Use a ``PersistentVolumeClaim`` when the ``VirtualMachineInstance`` disk needs
to persist after the |VM| terminates. This allows for the |VM|'s data to remain
persistent between restarts.

A ``PersistentVolume`` can be in ``filesystem`` or ``block`` mode.

Below is a simple example which attaches a ``PersistentVolumeClaim`` as a disk
to a VM:

.. code-block:: none

    apiVersion: kubevirt.io/v1
    kind: VirtualMachineInstance
    metadata:
      name: testvmi-pvc
    spec:
      domain:
        resources:
          requests:
            memory: 64M
        devices:
          disks:
          - name: mypvcdisk
            lun: {}
      volumes:
        - name: mypvcdisk
          persistentVolumeClaim:
            claimName: mypvc


Upload VM Image to CDI
----------------------

You can upload/import a VM image to |CDI|, creating a |PVC| on system, so you
can use the |PVC| to boot or create the |VM|.

#.  Get the upload proxy Server IP.

    .. code-block:: none

        kubectl -n cdi get svc |grep cdi-uploadproxy
        cdi-uploadproxy          ClusterIP   10.111.132.43    <none>        443/TCP    85m

#.  Use ``virtctl`` client upload the image to |CDI|.

    .. code-block:: none

        virtctl image-upload --pvc-name=cirros-vm-disk-test-2 --pvc-size=500Mi --image-path=/home/sysadmin/kubevirt-valid/cirros-0.5.1-x86_64-disk.img --uploadproxy-url=https://<uploadproxy-svc-ip> --insecure

.. note::

    Now you can use ``pvc cirros-vm-disk-test-2`` in |VM| specs.

    .. code-block:: none

        apiVersion: kubevirt.io/v1
        kind: VirtualMachineInstance
        metadata:
          name: testvmi-pvc-1
        spec:
          domain:
            resources:
              requests:
                memory: 64M
        devices:
          disks:
          - name: mypvcdisk
            lun: {}
          volumes:
          - name: mypvcdisk
            persistentVolumeClaim:
              claimName: cirros-vm-disk-test-2 

    If you want to upload an image located outside the cluster, the cluster
    admin should expose the |CDI| upload proxy to the outside, for example with
    NodePort. 

    For more details see :ref:`set-up-cdi-proxy-ad165d884417`.