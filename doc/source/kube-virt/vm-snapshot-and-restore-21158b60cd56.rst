.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _vm-snapshot-and-restore-21158b60cd56:

=======================
VM Snapshot and Restore
=======================

|VM| snapshot allows you to snapshot the running |VM| with existing
configuration and restore back to configuration point.


Snapshot a VM
-------------

Snapshotting a |VM| is supported for online and offline |VMs|.

When snapshotting a running |VM| the controller will check for the qemu guest
agent in the |VM|. If the agent exists it will freeze the |VM| filesystems
before taking the snapshot and unfreeze after the snapshot. It is recommended
to take online snapshots with the guest agent for a better snapshot, if not
present, a best effort snapshot will be taken.

.. rubric:: |proc|

To enable snapshot functionality system does require snapshot |CRD| and
snapshot controller to be created on system. Follow the steps below:

#.  Run to install snapshot |CRD| and snapshot controller on Kubernets:

    -   ``kubectl apply -f`` https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/master/client/config/crd/snapshot.storage.k8s.io_volumesnapshots.yaml

    -   ``kubectl apply -f`` https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/master/client/config/crd/snapshot.storage.k8s.io_volumesnapshotcontents.yaml

    -   ``kubectl apply -f`` https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/master/client/config/crd/snapshot.storage.k8s.io_volumesnapshotclasses.yaml

    -   ``kubectl apply -f`` https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/master/deploy/kubernetes/snapshot-controller/rbac-snapshot-controller.yaml

    -   ``kubectl apply -f`` https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/master/deploy/kubernetes/snapshot-controller/setup-snapshot-controller.yaml


#.  Create ``VolumeSnapshotClass`` for ``cephfs`` and ``rbd``:

    .. code-block:: none

        cat <<EOF>cephfs-storageclass.yaml
        —
        apiVersion: snapshot.storage.k8s.io/v1
        kind: VolumeSnapshotClass
        metadata:
          name: csi-cephfsplugin-snapclass
        driver: cephfs.csi.ceph.com
        parameters:
          clusterID: 60ee9439-6204-4b11-9b02-3f2c2f0a4344
          csi.storage.k8s.io/snapshotter-secret-name: ceph-pool-kube-cephfs-data
          csi.storage.k8s.io/snapshotter-secret-namespace: default
        deletionPolicy: Delete
        EOF
        cat <<EOF>rbd-storageclass.yaml
        —
        apiVersion: snapshot.storage.k8s.io/v1
        kind: VolumeSnapshotClass
        metadata:
          name: csi-rbdplugin-snapclass
        driver: rbd.csi.ceph.com
        parameters:
          clusterID: 60ee9439-6204-4b11-9b02-3f2c2f0a4344
          csi.storage.k8s.io/snapshotter-secret-name: ceph-pool-kube-rbd
          csi.storage.k8s.io/snapshotter-secret-namespace: default
        deletionPolicy: Delete
       EOF

    .. note::

        Get the cluster ID from: ``kubectl describe sc cephfs, rbd``.

#.  Create snapshot manifest of running |VM| using the example yaml below:

    .. code-block:: none

        cat<<EOF>cirros-snapshot.yaml
        apiVersion: snapshot.kubevirt.io/v1alpha1
        kind: VirtualMachineSnapshot
        metadata:
          name: snap-cirros
        spec:
          source:
            apiGroup: kubevirt.io
            kind: VirtualMachine
            name: pvc-test-vm
          failureDeadline: 3m
        EOF

#.  Apply the snapshot manifest and verify if the snapshot is successfully
    created.

    .. code-block:: none

        kubectl apply -f cirros-snapshot.yaml
        [sysadmin@controller-0 kubevirt-GA-testing(keystone_admin)]$ kubectl get VirtualMachineSnapshot
        NAME          SOURCEKIND       SOURCENAME    PHASE       READYTOUSE   CREATIONTIME   ERROR
        snap-cirros   VirtualMachine   pvc-test-vm   Succeeded   true         28m

Example manifest to restore the snapshot:

.. code-block:: none

    <<EOF>cirros-restore.yaml
    apiVersion: snapshot.kubevirt.io/v1alpha1
    kind: VirtualMachineRestore
    metadata:
      name: restore-cirros
    spec:
      target:
        apiGroup: kubevirt.io
        kind: VirtualMachine
        name: pvc-test-vm
      virtualMachineSnapshotName: snap-cirros
    EOF
    kubectl apply -f cirros-restore.yaml

Verify the snapshot restore:

.. code-block:: none

    [sysadmin@controller-0 kubevirt-GA-testing(keystone_admin)]$ kubectl get VirtualMachineRestore
    NAME               TARGETKIND       TARGETNAME    COMPLETE   RESTORETIME   ERROR
    restore-cirros     VirtualMachine   pvc-test-vm   true       34m
