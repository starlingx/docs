.. include:: /_includes/toc-title-kubevirt.rest

.. _index-kubevirt-f1bfd2a21152:

========
KubeVirt
========

.. toctree::
   :maxdepth: 1

   introduction-bb3a04279bf5
   kubevirt-architecture-c92c8908e775
   installation-66477d7646db
   kubevirt-removal-97cc897941bc

Usage Examples
==============

.. toctree::
   :maxdepth: 1

   hello-world-kubevirt-vm-05503659173c
   set-up-cdi-proxy-ad165d884417
   virtual-machine-dd561f6db3fd
   use-shared-resource-cpu-ad295227aa0c
   live-migration-support-for-vms-bea635bacc50
   interface-and-networks-7cadb7bdb80b
   startup-scripts-6402154a6f37
   persistent-storage-for-vms-8ddc8fa611aa
   host-device-assignment-a6feb2f0c3bc
   vm-snapshot-and-restore-21158b60cd56
   virtualmachineinstancereplicaset-8518d55de52b
   static-ip-assignment-via-cloudinit-configuration-d053375e78fa
   configuration-as-filesystem-0a5023c8386e
   vm-using-secret-as-startup-configuration-4a8255e26b1f
   vm-using-service-account-as-filesystem-5fd4deb7339a
   node-assignment-using-nodeselector-affinity-and-antiminusaffi-06ad222ceb13
   create-an-ubuntu-vm-fafb82ec424b
   set-up-remote-management-of-vms-a082461d660e
   create-a-windows-vm-82957181df02
