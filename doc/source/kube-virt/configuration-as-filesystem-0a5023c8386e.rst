.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _configuration-as-filesystem-0a5023c8386e:

===========================
Configuration as Filesystem
===========================

By using filesystem, ``configMaps`` are shared through ``virtiofs``. In
contrast with using disks for sharing ``configMaps``, filesystem allows you to
dynamically propagate changes on ``configMaps`` to |VMIs| (i.e. the VM does not
need to be rebooted).

.. rubric:: Limitation

Currently, |VMIs| cannot be live migrated since ``virtiofs`` does not support
live migration.

Example of ``configMap``:

.. code-block:: none

    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: app-config
    data:
      DATABASE: staging
      USERNAME: admin
      PASSWORD: secret

Example of VM using ``configMap`` as a filesystem:

.. code-block:: none

    apiVersion: kubevirt.io/v1
    kind: VirtualMachineInstance
    metadata:
      labels:
        special: vmi-fedora
      name: vmi-fedora
    spec:
      domain:
        devices:
          filesystems:
            - name: config-fs
              virtiofs: {}
          disks:
          - disk:
              bus: virtio
            name: containerdisk
        machine:
          type: ""
        resources:
          requests:
            memory: 1024M
      terminationGracePeriodSeconds: 0
      volumes:
      - name: containerdisk
        containerDisk:
          image: quay.io/containerdisks/fedora:latest
      - cloudInitNoCloud:
          userData: |-
            #cloud-config
            chpasswd:
              expire: false
            password: fedora
            user: fedora
            bootcmd:
              # mount the ConfigMap
              - "sudo mkdir /mnt/app-config"
              - "sudo mount -t virtiofs config-fs /mnt/app-config"
        name: cloudinitdisk
      - configMap:
          name: app-config
        name: config-fs

To login and verify the VM run:

.. code-block:: none

    [fedora@vmi-fedora ~]$ df -h
    Filesystem      Size  Used Avail Use% Mounted on
    /dev/vda5       4.0G  461M  3.1G  13% /
    devtmpfs        4.0M     0  4.0M   0% /dev
    tmpfs           450M     0  450M   0% /dev/shm
    tmpfs           180M  720K  179M   1% /run
    tmpfs           450M     0  450M   0% /tmp
    /dev/vda2       966M   61M  840M   7% /boot
    /dev/vda3       100M   12M   89M  12% /boot/efi
    /dev/vda5       4.0G  461M  3.1G  13% /home
    config-fs       9.8G  910M  8.4G  10% /mnt/app-config
    tmpfs            90M  4.0K   90M   1% /run/user/1000
    [fedora@vmi-fedora ~]$ ls -lrth /mnt/app-config
    total 0
    lrwxrwxrwx. 1 root 107 15 Jan 15 16:20 USERNAME -> ..data/USERNAME
    lrwxrwxrwx. 1 root 107 15 Jan 15 16:20 PASSWORD -> ..data/PASSWORD
    lrwxrwxrwx. 1 root 107 15 Jan 15 16:20 DATABASE -> ..data/DATABASE



