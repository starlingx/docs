.. _kubevirt-architecture-c92c8908e775:

=====================
KubeVirt Architecture
=====================

The following diagram describes the KubeVirt architecture:

.. figure:: figures/kubevirt-architecture.png
    :width: 800

.. rubric:: ``virt-api-server``

HTTP API server serves as the entry point for all virtualization related flows.

The API server updates the virtualization related custom resource definition
(see below).

As the main entry point to KubeVirt it is responsible for defaulting and
validation of the provided VMI |CRDs|.

.. rubric:: VMI (CRD)

|VMI| definitions are kept as custom resources inside the Kubernetes API
server.

The |VMI| definition defines all properties of the |VM| itself, for example:

-   Machine type

-   CPU type

-   Amount of RAM and |vCPUs|

-   Number and type of |NICs|


.. rubric:: ``virt-controller``

The ``virt-controller`` has all the cluster wide virtualization functionality
and it is responsible for monitoring the |VMI| (|CRs|) and managing the
associated pods. Currently, the controller ensure the creation and management
of the life-cycle of the pods associated with the |VMI| objects.

A |VMI| object will always be associated with a pod during its life-time.
However, migration of a |VMI| the pod instance might change over time.


.. rubric:: ``virt-launcher``

For every |VMI| object one pod is created. This pod's primary container runs
the ``virt-launcher`` KubeVirt component.

Kubernetes or kubelet is not running the |VMIs|. Instead a daemon on every host
in the cluster launches a |VMI| process for every pod which is associated with
a |VMI| object whenever it is scheduled on a host.

The main purpose of the ``virt-launcher`` pod is to provide ``cgroups`` and
``namespaces``, used to host the |VMI| process.

``virt-handler`` signals ``virt-launcher`` to start a |VMI| by passing the
|VMI|'s |CRD| object to ``virt-launcher``. ``virt-launcher`` uses a local
``libvirtd`` instance within its container to start the |VMI|. The
``virt-launcher`` monitors the |VMI| process and terminates once the |VMI| has
exited.

If the Kubernetes runtime attempts to shutdown the ``virt-launcher`` pod before
|VMI| has exited, ``virt-launcher`` forwards signals from Kubernetes to the
|VMI| process and attempts to hold off the termination of the pod until the
|VMI| has shutdown successfully.


.. rubric:: ``virt-handler``

Every host needs a single instance of ``virt-handler``. It can be delivered as
a DaemonSet.

Like the virt-controller, the ``virt-handler`` is also reactive and watches
for changes of the |VMI| object, once detected it will perform all necessary
operations to change a |VMI| to meet the required state.

This behavior is similar to the choreography between the Kubernetes |VMI|
server and the kubelet.

The functions of the ``virt-handler`` are:

-   Keeps a cluster-level |VMI| specification in sync with a corresponding
    ``libvirt`` domain.

-   Reports domain state and spec changes to the cluster.

-   Invokes node-centric plugins which can fulfill networking and storage
    requirements defined in |VMI| specs.

.. rubric:: ``Libvirtd``

An instance of ``libvirtd`` is present in every |VMI| pod. ``virt-launcher``
uses ``libvirtd`` to manage the life-cycle of the |VMI| process.

Additional components from |prod| Kubernetes are:

-   Storage

-   Networking

-   |RBAC|

.. rubric:: Simplified Diagram

.. figure:: figures/kubrvirt-simplified-diagram.png
    :width: 800

---------------------------
Containerized Data Importer
---------------------------

The |CDI| project provides facilities for enabling |PVCs| to be used as disks
for KubeVirt |VMs| by way of DataVolumes. The three main |CDI| use cases are:

-   Import a disk image from a web server or container registry to a DataVolume.

-   Clone an existing |PVC| to a DataVolume.

-   Upload a local disk image to a DataVolume.