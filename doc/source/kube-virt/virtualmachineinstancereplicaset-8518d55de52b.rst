.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _virtualmachineinstancereplicaset-8518d55de52b:

====================================
Virtual Machine Instance Replica Set
====================================

A |VMIRS| tries to ensure that a specified number of |VMI| replicas are
running at any time. Meaning, a |VMIRS| makes sure that a |VMI| or a
homogeneous set of |VMIs| is always up and ready.


Use VMIRS
---------

The |VMIRS| allows you to specify a |VMI| Template in ``spec.template``. It
consists of ``ObjectMetadata`` in ``spec.template.metadata``, and a |VMI| spec
in ``spec.template.spec``. The specification of the |VM| is equal to the
specification of the |VM| in the |VMI| workload.

``spec.replicas`` can be used to specify how many replicas are wanted. If
unspecified, the default value is 1. This value can be updated anytime. The
controller reacts to the changes.

Example manifest for ``replicaset``:

.. code-block:: none

    apiVersion: kubevirt.io/v1
    kind: VirtualMachineInstanceReplicaSet
    metadata:
      name: testreplicaset
    spec:
      replicas: 3
      selector:
        matchLabels:
          myvmi: myvmi
      template:
        metadata:
          name: test
          labels:
            myvmi: myvmi
        spec:
          domain:
            devices:
              disks:
              - disk:
                name: containerdisk
            resources:
              requests:
                memory: 64M
          volumes:
          - name: containerdisk
            containerDisk:
              image: kubevirt/cirros-container-disk-demo:latest


Configure Horizontal VM Autoscaler
----------------------------------

.. code-block:: none

    [sysadmin@controller-0 ravi-test(keystone_admin)]$ cat hpa.yaml
    ---
    apiVersion: autoscaling/v1
    kind: HorizontalPodAutoscaler
    metadata:
      name: test-hpa
    spec:
      scaleTargetRef:
        kind: VirtualMachineInstanceReplicaSet
        name: testreplicaset
        apiVersion: kubevirt.io/v1
      minReplicas: 3
      maxReplicas: 5
      targetCPUUtilizationPercentage: 50
    ---

    Verify :
    [sysadmin@controller-0 (keystone_admin)]$ kubectl describe horizontalpodautoscaler.autoscaling
    Name:                                   test-hpa
    Namespace:                              default
    Labels:                                 <none>
    Annotations:                            autoscaling.alpha.kubernetes.io/conditions:
                                              [{"type":"AbleToScale","status":"True","lastTransitionTime":"2023-11-30T18:17:34Z","reason":"ScaleDownStabilized","message":"recent recomm...
                                            autoscaling.alpha.kubernetes.io/current-metrics:
                                              [\{"type":"Resource","resource":{"name":"cpu","currentAverageUtilization":1,"currentAverageValue":"2m"}}]
    CreationTimestamp:                      Thu, 30 Nov 2023 19:17:19 +0100
    Reference:                              VirtualMachineInstanceReplicaSet/testreplicaset
    Target CPU utilization:                 50%
    Current CPU utilization:                1%
    Min replicas:                           3
    Max replicas:                           5
    VirtualMachineInstanceReplicaSet pods:  3 current / 3 desired
    Events:                                 <none>

.. note::

    Based on CPU usage in above example the autoscaler will scale up and down
    the ``replicaset``.
