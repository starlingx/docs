.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _static-ip-assignment-via-cloudinit-configuration-d053375e78fa:

================================================
Static IP Assignment via Cloudinit Configuration
================================================

KubeVirt supports cloud-init's NoCloud and ConfigDrive datasources, which
involve injecting startup scripts into a |VM| instance using of an ephemeral
disk. |VMs| with the cloud-init package installed detect the ephemeral disk and
execute custom userdata scripts at boot.

In the example below, the |VM| is using NoCloud for assigning the static IP and
MAC address on |SRIOV| interface.

.. code-block:: none

    cat <<EOF>cloud-NoCloud-test.yaml
    apiVersion: kubevirt.io/v1
    kind: VirtualMachine
    metadata:
      labels:
        special: vmi-sriov-network
      name: vmi-sriov-test-vm-1
    spec:
      running: true
      template:
        metadata:
          labels:
            kubevirt.io/size: small
            kubevirt.io/domain: fedora
        spec:
          domain:
            cpu:
              cores: 4
            devices:
              disks:
              - name: containerdisk
                disk:
                  bus: virtio
              - name: cloudinitdisk
                disk:
                  bus: virtio
              interfaces:
              - masquerade: {}
                name: default
              - macAddress: "02:00:00:00:00:03"
                name: sriov-net1
                sriov: {}
              rng: {}
            resources:
              requests:
                memory: 1024M
          networks:
          - name: default
            pod: {}
          - multus:
              networkName: sriov-net1
            name: sriov-net1
          volumes:
          - containerDisk:
              image: docker.io/kubevirt/fedora-cloud-container-disk-demo:devel
            name: containerdisk
          - cloudInitNoCloud:
              networkData: |
                ethernets:
                  sriov-net1:
                    addresses:
                    - 10.10.10.42/24
                    gateway: 10.10.10.1
                    match:
                      macAddress: "02:00:00:00:00:03"
                    nameservers:
                      addresses:
                      - 10.96.0.10
                      search:
                      - default.svc.cluster.local
                      - svc.cluster.local
                      - cluster.local
                    set-name: sriov-link-enabled
                version: 2
              userData: |-
                #!/bin/bash
                echo "fedora" |passwd fedora --stdin
            name: cloudinitdisk
    EOF

    kubectl apply -f cloud-NoCloud-test.yaml


    [sysadmin@controller-0 kubevirt-GA-testing(keystone_admin)]$ kubectl get vmi
    NAME                  AGE     PHASE     IP               NODENAME       READY
    vmi-sriov-test-vm-1   3h17m   Running   172.16.166.152   controller-1   True

    [sysadmin@controller-0 kubevirt-GA-testing(keystone_admin)]$ virtctl console vmi-sriov-test-vm-1
    Successfully connected to vmi-sriov-test-vm-1 console. The escape sequence is ^]vmi-sriov-test-vm-1 login: fedora
    Password:
    [fedora@vmi-sriov-test-vm-1 ~]$ ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
           valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
           valid_lft forever preferred_lft forever
    2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 52:54:00:90:b4:d7 brd ff:ff:ff:ff:ff:ff
        altname enp1s0
        inet 10.0.2.2/24 brd 10.0.2.255 scope global dynamic noprefixroute eth0
           valid_lft 86301690sec preferred_lft 86301690sec
        inet6 fe80::5054:ff:fe90:b4d7/64 scope link
           valid_lft forever preferred_lft forever
    3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
        link/ether 02:00:00:00:00:03 brd ff:ff:ff:ff:ff:ff
        altname enp8s0
        inet 10.10.10.42/24 brd 10.10.10.10.255 scope global noprefixroute eth1
           valid_lft forever preferred_lft forever
        inet6 fe80::ff:fe00:3/64 scope link
           valid_lft forever preferred_lft forever
    [fedora@vmi-sriov-test-vm-1 ~]$
