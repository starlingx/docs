.. _installation-66477d7646db:

=====================
KubeVirt Installation
=====================

.. rubric:: |prereq|

-  |prod| is up and running.

-  System is Alarm free.

   .. code-block:: none

         $ fm alarm-list

.. rubric:: |proc|

Complete the following steps to install KubeVirt.

#. Upload and apply the KubeVirt package (Helm).

   .. code-block:: none

         # system application-upload /usr/local/share/applications/helm/kubevirt-app-24.09-30.tgz
         # system application-apply kubevirt-app
         # system application-show kubevirt-app
         # system application-list

#. Check all the objects are up and running.

   .. code-block:: none

         # kubectl get all -n cdi
         # kubectl get all -n kubevirt

   .. note::

      When installing the containerized KubeVirt application, the virtctl
      client is installed on the host. You can test the client by running
      virtctl in an SSH / Local Console shell of the controller.

#. Optionally, Install noVNC in order to access a VM's graphical console thru a
   browser.

   .. note::

      noVNC is used to access a VM's graphical console in a browser, where
      virtctl provides access to the VM's serial console.

      Create  and apply ``vnc-install.yaml``.

      .. code-block:: yaml

            apiVersion: v1
            kind: ServiceAccount
            metadata:
              name: virtvnc
              namespace: kubevirt
            ---
            kind: ClusterRoleBinding
            apiVersion: rbac.authorization.k8s.io/v1
            metadata:
              name: virtvnc
            subjects:
            - kind: ServiceAccount
              name: virtvnc
              namespace: kubevirt
            roleRef:
              kind: ClusterRole
              name: virtvnc
              apiGroup: rbac.authorization.k8s.io
            ---
            kind: ClusterRole
            apiVersion: rbac.authorization.k8s.io/v1
            metadata:
              name: virtvnc
            rules:
            - apiGroups:
              - subresources.kubevirt.io
              resources:
              - virtualmachineinstances/console
              - virtualmachineinstances/vnc
              verbs:
              - get
            - apiGroups:
              - kubevirt.io
              resources:
              - virtualmachines
              - virtualmachineinstances
              - virtualmachineinstancepresets
              - virtualmachineinstancereplicasets
              - virtualmachineinstancemigrations
              verbs:
              - get
              - list
              - watch
            ---
            apiVersion: v1
            kind: Service
            metadata:
              labels:
               app: virtvnc
              name: virtvnc
              namespace: kubevirt
            spec:
              ports:
              - port: 8001
                protocol: TCP
                targetPort: 8001
                nodePort: 31002
              selector:
               app: virtvnc
              type: NodePort
            ---
            apiVersion: apps/v1
            kind: Deployment
            metadata:
              name: virtvnc
              namespace: kubevirt
            spec:
              replicas: 1
              selector:
               matchLabels:
                  app: virtvnc
            template:
               metadata:
                  labels:
                  app: virtvnc
               spec:
                  serviceAccountName: virtvnc
                  nodeSelector:
                    node-role.kubernetes.io/master: ''
                  tolerations:
                  - key: "node-role.kubernetes.io/master"
                    operator: "Equal"
                    value: ""
                    effect: "NoSchedule"
                  containers:
                  - name: virtvnc
                    image: quay.io/samblade/virtvnc:latest
                    livenessProbe:
                     httpGet:
                        port: 8001
                        path: /
                        scheme: HTTP
                     failureThreshold: 30
                     initialDelaySeconds: 30
                     periodSeconds: 10
                     successThreshold: 1
                     timeoutSeconds: 5


   .. code-block:: none

         kubectl apply -f vnc-install.yaml

   To access the VNC: http://<OAM FLOATING IP>:31002

.. rubric:: |result|

KubeVirt has been installed on the system.
