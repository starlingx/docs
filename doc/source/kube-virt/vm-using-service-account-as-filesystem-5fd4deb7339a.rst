.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _vm-using-service-account-as-filesystem-5fd4deb7339a:

======================================
VM Using Service Account as Filesystem
======================================

A ``serviceaccount`` volume references a Kubernetes ``serviceaccount``. A
``serviceaccount`` can be presented to the |VM| as disk or as a filesystem.

The disk method does not support dynamic change propagation and the filesystem
method does not support live migration. Therefore, depending on the use-case,
one or the other may be more suitable.

By using filesystem, ``serviceaccounts`` are shared through ``virtiofs``. In
contrast with using disk for sharing ``serviceaccounts``, filesystem allows you
to dynamically propagate changes on ``serviceaccounts`` to |VMIs| (i.e. the
|VM| does not need to be rebooted).

.. rubric:: Limitation

Currently, |VMIs| cannot be live migrated since ``virtiofs`` does not support
live migration.


Example of a |VM| creation using default service account:

.. code-block:: none

    apiVersion: kubevirt.io/v1
    kind: VirtualMachineInstance
    metadata:
      labels:
        special: vmi-fedora-sa
      name: vmi-fedora
    spec:
      domain:
        devices:
          filesystems:
            - name: serviceaccount-fs
              virtiofs: {}
          disks:
            - disk:
                bus: virtio
              name: containerdisk
        machine:
          type: ""
        resources:
          requests:
            memory: 1024M
      terminationGracePeriodSeconds: 0
      volumes:
        - name: containerdisk
          containerDisk:
            image: quay.io/containerdisks/fedora:latest
        - cloudInitNoCloud:
            userData: |-
              #cloud-config
              chpasswd:
                expire: false
              password: fedora
              user: fedora
              bootcmd:
                # mount the ConfigMap
                - "sudo mkdir /mnt/serviceaccount"
                - "sudo mount -t virtiofs serviceaccount-fs /mnt/serviceaccount"
          name: cloudinitdisk
        - name: serviceaccount-fs
          serviceAccount:
            serviceAccountName: default
