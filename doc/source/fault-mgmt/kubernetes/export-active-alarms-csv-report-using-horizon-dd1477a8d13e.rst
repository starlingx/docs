.. _export-active-alarms-csv-report-using-horizon-dd1477a8d13e:

=============================================
Export Active Alarms CSV Report Using Horizon
=============================================

You can export reports of Active Alarms using the Horizon Dashboard. A download
button is located at the bottom left of the display table. Data is exported in
|CSV| format. All properties retrieved from the API are exported; pre-applied
filters do not affect the export.

.. rubric:: |proc|

#.  Select **Admin** > **Fault Management** > **Active Alarms** in the left
    pane. The current Active Alarms are displayed in a table.

    .. image:: figures/active-alarms-table.png
        :width: 800

#.  Click the **Export CSV Report** icon located at bottom left of the display
    table.