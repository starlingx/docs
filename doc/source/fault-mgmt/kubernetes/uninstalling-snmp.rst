==============
Uninstall SNMP
==============

Use the following procedure to uninstall |SNMP|:

.. rubric:: |proc|

#.  Run the following command to check if the SNMP application is installed
    (status "applied").

    .. only:: starlingx

        .. code-block:: none

            ~(keystone)admin)$ system application-list
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
            | application              | version   | manifest name                             | manifest file    | status   | progress  |
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
            | cert-manager             | 24.09-79  | cert-manager-fluxcd-manifests             | fluxcd-manifests | applied  | completed |
            | dell-storage             | 24.09-25  | dell-storage-fluxcd-manifests             | fluxcd-manifests | uploaded | completed |
            | nginx-ingress-controller | 24.09-64  | nginx-ingress-controller-fluxcd-manifests | fluxcd-manifests | applied  | completed |
            | oidc-auth-apps           | 24.09-59  | oidc-auth-apps-fluxcd-manifests           | fluxcd-manifests | uploaded | completed |
            | platform-integ-apps      | 24.09-141 | platform-integ-apps-fluxcd-manifests      | fluxcd-manifests | applied  | completed |
            | rook-ceph                | 24.09-48  | rook-ceph-fluxcd-manifests                | fluxcd-manifests | uploaded | completed |
            | snmp                     | 24.09-89  | snmp-fluxcd-manifests                     | fluxcd-manifests | uploaded | completed |
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+

    .. only:: partner

        .. include:: /_includes/system-application-list.rest
           :start-after: system-application-list-begin
           :end-before: system-application-list-end

#.  Uninstall |SNMP|.

    .. code-block:: none

       ~(keystone_admin)]$ system application-remove snmp
       +---------------+----------------------------------+
       | Property      | Value                            |
       +---------------+----------------------------------+
       | active        | False                            |
       | app_version   | 24.09-89                         |
       | created_at    | 2024-06-27T10:45:42.733267+00:00 |
       | manifest_file | fluxcd-manifests                 |
       | manifest_name | snmp-fluxcd-manifests            |
       | name          | snmp                             |
       | progress      | None                             |
       | status        | removing                         |
       | updated_at    | 2022-06-27T10:45:51.253949+00:00 |
       +---------------+----------------------------------+

    The |SNMP| application is removed, but still shows as "Uploaded".

    .. only:: starlingx

        .. code-block:: none

            ~(keystone)admin)$ system application-list
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
            | application              | version   | manifest name                             | manifest file    | status   | progress  |
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
            | cert-manager             | 24.09-79  | cert-manager-fluxcd-manifests             | fluxcd-manifests | applied  | completed |
            | dell-storage             | 24.09-25  | dell-storage-fluxcd-manifests             | fluxcd-manifests | uploaded | completed |
            | nginx-ingress-controller | 24.09-64  | nginx-ingress-controller-fluxcd-manifests | fluxcd-manifests | applied  | completed |
            | oidc-auth-apps           | 24.09-59  | oidc-auth-apps-fluxcd-manifests           | fluxcd-manifests | uploaded | completed |
            | platform-integ-apps      | 24.09-141 | platform-integ-apps-fluxcd-manifests      | fluxcd-manifests | applied  | completed |
            | rook-ceph                | 24.09-48  | rook-ceph-fluxcd-manifests                | fluxcd-manifests | uploaded | completed |
            | snmp                     | 24.09-89  | snmp-fluxcd-manifests                     | fluxcd-manifests | uploaded | completed |
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+

    .. only:: partner

        .. include:: /_includes/system-application-list.rest
           :start-after: system-application-list-begin
           :end-before: system-application-list-end

#.  Delete the uninstalled |SNMP| application definitions from the system.

    .. code-block:: none

       ~(keystone_admin)]$ system application-delete snmp

    The following message is displayed when the |SNMP| application is deleted
    "Application SNMP deleted".

#.  Run the following command to check if the |SNMP| application is deleted.

    .. only:: starlingx

        .. code-block:: none

            ~(keystone)admin)$ system application-list
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
            | application              | version   | manifest name                             | manifest file    | status   | progress  |
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
            | cert-manager             | 24.09-79  | cert-manager-fluxcd-manifests             | fluxcd-manifests | applied  | completed |
            | dell-storage             | 24.09-25  | dell-storage-fluxcd-manifests             | fluxcd-manifests | uploaded | completed |
            | nginx-ingress-controller | 24.09-64  | nginx-ingress-controller-fluxcd-manifests | fluxcd-manifests | applied  | completed |
            | oidc-auth-apps           | 24.09-59  | oidc-auth-apps-fluxcd-manifests           | fluxcd-manifests | uploaded | completed |
            | platform-integ-apps      | 24.09-141 | platform-integ-apps-fluxcd-manifests      | fluxcd-manifests | applied  | completed |
            | rook-ceph                | 24.09-48  | rook-ceph-fluxcd-manifests                | fluxcd-manifests | uploaded | completed |
            | snmp                     | 24.09-89  | snmp-fluxcd-manifests                     | fluxcd-manifests | uploaded | completed |
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+

    .. only:: partner

        .. include:: /_includes/system-application-list.rest
           :start-after: system-application-list-begin
           :end-before: system-application-list-end
