.. _export-event-log-csv-report-using-horizon-1c58a29d7fb6:

=========================================
Export Event Log CSV Report Using Horizon
=========================================

You can export reports of Events using the Horizon Dashboard. A download button
is located at the bottom left of the display table. Data is exported in |CSV|
format. All properties retrieved from the API are exported; pre-applied filters
do not affect the export.

.. rubric:: |proc|

#.  Select **Admin** > **Fault Management** > **Events** in the left pane. The
    current Events are displayed in a table.

    .. image:: figures/events-table.png
        :width: 800

#.  Click the **Export CSV Report** icon located at bottom left of the display
    table.