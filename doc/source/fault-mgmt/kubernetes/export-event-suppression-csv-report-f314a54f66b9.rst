.. _export-event-suppression-csv-report-f314a54f66b9:

===================================
Export Event Suppression CSV Report
===================================

You can export reports of Events Suppression using the Horizon Dashboard. A
download button is located at the bottom left of the display table. Data is
exported in |CSV| format. All properties retrieved from the API are exported;
pre-applied filters do not affect the export.

.. rubric:: |proc|

#.  Select **Admin** > **Fault Management** > **Events Suppression** in the
    left pane. The current Events Suppression are displayed in a table.

    .. image:: figures/events-supression-table.png
        :width: 800

#.  Click the **Export CSV Report** icon located at bottom left of the display
    table.