
.. yxz1511555520499
.. _vxlan-data-network-setup-completion:

===================================
VXLAN Data Network Setup Completion
===================================

You can complete the |VXLAN| data network setup by using the |os-prod-hor-long|
or the |CLI|.

For more information on setting up |VXLAN| Data Networks, see :ref:`VXLAN Data Networks <vxlan-data-networks>`.

-   :ref:`Adding a Static IP Address to a Data Interface <adding-a-static-ip-address-to-a-data-interface>`

-   :ref:`Using IP Address Pools for Data Interfaces <using-ip-address-pools-for-data-interfaces>`

-   :ref:`Adding and Maintaining Routes for a VXLAN Network <adding-and-maintaining-routes-for-a-vxlan-network>`


