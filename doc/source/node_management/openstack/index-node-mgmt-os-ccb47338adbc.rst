.. _index-node-mgmt-os-ccb47338adbc:

.. include:: /_includes/toc-title-node-os.rest

.. only:: partner

   .. include:: /node_management/index-node-mgmt-cd0f9d8eaee6.rst
      :start-after: os-begin
      :end-before: os-end

.. toctree::
   :maxdepth: 1

   node-management-overview
   adding-compute-nodes-to-an-existing-duplex-system
   using-labels-to-identify-openstack-nodes

-------------------------
PCI Device Access for VMs
-------------------------

.. toctree::
   :maxdepth: 1

   pci-passthrough-ethernet-interface-devices
   configuring-pci-passthrough-ethernet-interfaces
   generic-pci-passthrough
   pci-sr-iov-ethernet-interface-devices
   sr-iov-encryption-acceleration
   pci-device-access-for-vms
   configuring-a-flavor-to-use-a-generic-pci-device
   exposing-a-generic-pci-device-for-use-by-vms
   exposing-a-generic-pci-device-using-the-cli
