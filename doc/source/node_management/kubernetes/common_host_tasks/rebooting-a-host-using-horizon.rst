
.. qfl1579716777827
.. _rebooting-a-host-using-horizon:

===========================
Reboot a Host Using Horizon
===========================

Rebooting gracefully restarts a locked host, ensuring that all system
processes are properly shut off first.

The host then reboots automatically.

.. note::
    This function is not available on a |prod| Simplex system.

.. rubric:: |proc|

.. _rebooting-a-host-using-horizon-steps-wlz-ksx-lkb:

#.  From **Admin** \> **Platform** \> **Host Inventory**, select the
    **Hosts** tab.

#.  From the **Edit** menu for the host, select **Reboot Host**.

    .. figure:: /node_management/kubernetes/figures/mwl1579716430090.png
        :scale: 100%
