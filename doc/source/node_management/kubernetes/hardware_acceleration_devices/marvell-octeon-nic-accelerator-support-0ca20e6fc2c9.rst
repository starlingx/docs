.. _marvell-octeon-nic-accelerator-support-0ca20e6fc2c9:

======================================
Marvell Octeon NIC Accelerator Support
======================================

Marvell supports Octeon line of system-on-chips (SoCs) to be used as a RAN |NIC|
when configured in Endpoint mode. Marvell developed PCI |SRIOV| |NIC| drivers
for these products. The |NIC| drivers on host create a netdev interface for PF
device and supports creating VFs. Octeon |SoCs| feature multiple ARM v9 cores
along with Ethernet, PCIe and baseband PHY (RAN). This enables users to offload
L1 processing to the Octeon |SoCs|.

To support this line of accelerator cards, the Marvell Octeon |SRIOV| PF and VF
host drivers have been integrated into |prod|.

Each PF and VF can be configured to run |NIC| functionality or RAN
functionality based on the operator's choice.

The drivers enable netdev communication between L2/L3 running on the host cpu
and L1 running on the accelerator.

.. note::

    The devices are seen as a PCIe |SRIOV| capable PF device.

The drivers for real-time and standard Linux kernels can be found in the
location: ``/lib/modules/<release-kernel-version>/updates/octeon_ep/``.

--------------------------------
Dell Open RAN Accelerator (DORA)
--------------------------------

The Dell Open RAN Accelerator (DORA) utilizes the OCTEON Fusion CNF95xx chipset
from Marvell and its design deviates from that of a standard network interface
card (NIC), which requires specialized configuration. Unlike conventional NICs,
the |DORA| requires the deployment of dedicated application software prior to
its operational functionality. This application software assumes responsibility
for:

-   loading the octeon kernel module

-   bringing up the |DORA| card on every boot

-   making the VFs available as resources to Kubernetes

-   specifying number of VFs

-   bringing the |DORA| card down

-   back up if a driver upgrade is desired

Any attempt to use the 'system' CLI to add ``pci-sriov`` VF interfaces will not
succeed as drivers are not being loaded by default at boot time, although it is
possible to configure it as a PCI passthrough PF interface. After loading the
drivers, VFs can be created using raw Linux commands once the PF associated
with the |DORA| becomes detected by the system.

.. note::

    Using sysinv to configure the card as a PCI passthrough interface should
    not be used in production because it will break the next reboot, as the
    kernel module is not loaded by default.

.. rubric:: Identify the DORA Hardware

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ lspci -mm | grep Cavium
    51:00.0 "Network controller" "Cavium, Inc." "Device ba00" "Dell" "Device 0074"
    51:00.1 "Network controller" "Cavium, Inc." "Device ef00" "Dell" "Device 0074"
