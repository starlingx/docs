.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _gpu-device-plugin-configuration-615e2f6edfba:

===============================
GPU Device Plugin Configuration
===============================

Intel |GPU| plugin enables Kubernetes clusters to utilize Intel GPUs for
hardware acceleration of various workloads.

This section describes how to enable and use the Intel |GPU| device plugin
in |prod|.

.. _prerequisites-1:

.. rubric:: |prereq|

-  The host should have Intel |GPU| hardware. For supported |GPU| devices,
   refer to Intel |GPU| plugin documentation for more details: `Intel GPU
   device plugin for Kubernetes
   <https://github.com/intel/intel-device-plugins-for-kubernetes/blob/main/cmd/gpu_plugin/README.md>`__.

-  Node Feature Discovery application must be installed using the following
   commands:

   .. code-block::

      ~(keystone_admin)]$ system application-upload /usr/local/share/applications/helm/node-feature-discovery-<version>.tgz
      ~(keystone_admin)]$ system application-apply node-feature-discovery

   Replace ``<version>`` with the latest version number.

Enable Intel GPU Device Plugin
------------------------------

#. Locate the application tarball in the ``/usr/local/share/applications/helm``
   directory. For example:

   ``/usr/local/share/applications/helm/intel-device-plugins-operator-<version>.tgz``

#. Upload the application using the following command.

   .. code-block::

      ~(keystone_admin)]$ system application-upload intel-device-plugins-operator-<version>.tgz

   Replace ``<version>`` with the latest version number.

#. Verify that the application has been uploaded successfully.

   .. code-block::

      ~(keystone_admin)]$ system application-list

#. Check the helm chart status using the following command:

   .. code-block::

      ~(keystone_admin)]$ system helm-override-list intel-device-plugins-operator --long

#. Enable |GPU| Helm chart using the following command:

   .. code-block::

      ~(keystone_admin)]$ system helm-chart-attribute-modify --enabled true intel-device-plugins-operator intel-device-plugins-gpu intel-device-plugins-operator

#. Apply the application using the following command:

   .. code-block::

      ~(keystone_admin)]$ system application-apply intel-device-plugins-operator

#. Monitor the status of the application using one of the following commands.

   .. code-block::

      ~(keystone_admin)]$ watch -n 5 system application-list

   OR

   .. code-block::

      ~(keystone_admin)]$ watch kubectl get pods -n intel-device-plugins-operator

#. Pods can be checked using the following command:

   .. code-block::

      $ kubectl get pods -n intel-device-plugins-operator

Use Intel GPU Device Plugin
---------------------------

For information related to using |GPU| device plugin, see `Testing and Demos
<https://github.com/intel/intel-device-plugins-for-kubernetes/blob/main/cmd/gpu_plugin/README.md#testing-and-demos>`__.
