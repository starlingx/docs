.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _intel-device-plugins-operator-application-overview-c5de2a6212ae:

=========================================
Intel Device Plugins Operator Application
=========================================

This application provides a set of plugins developed by Intel to facilitate the
use of Intel hardware features in Kubernetes clusters. These plugins are
designed to enable and optimize the use of Intel-specific hardware capabilities
in a Kubernetes environment.

The following plugins are supported:

* Intel |QAT| device plugin 

* Intel |GPU| device plugin 

* Intel |DSA| device plugin


Install Intel Device Plugins Operator Application
-------------------------------------------------

Intel device plugin Operator application is required to be installed for
configuring the Intel |QAT| device plugin, the Intel |GPU| device plugin and
the Intel |DSA| device plugin. Installation steps are mentioned in the
respective device plugin configuration sections below.

-  :ref:`qat-device-plugin-configuration-616551306371`

-  :ref:`gpu-device-plugin-configuration-615e2f6edfba`

-  :ref:`data-streaming-accelerator-db88a67c930c`


Uninstall Intel Device Plugins Operator Application
---------------------------------------------------
 
Use the following steps to uninstall the Intel Device Plugins operator
application:

#. Remove the application using the following command:

   .. code-block::

      ~(keystone_admin)]$ system application-remove intel-device-plugins-operator

#. Delete application using the following command:

   .. code-block::

      ~(keystone_admin)]$ system application-delete intel-device-plugins-operator