.. _configuring-cpu-core-assignments-using-cli-96ee12bdfc83:

==========================================
Configuring CPU Core Assignments Using CLI
==========================================

You can improve the performance and capacity of specific functions by assigning
more CPU cores using the following command:

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ system host-cpu-modify [--nowrap] -f <function> [-c <cpulist>] [-p0 <num_cores_on_processor0>] [-p1 <num_cores_on_processor1>] [-p2 <num_cores_on_processor2>] [-p3 <num_cores_on_processor3>] <hostnameorid>

``function``

Use this command with the ``-f`` argument, which specifies the function, such
as **platform** and **application-isolated** whose core count is to be modified.

.. note::

    Any core that is not specified as one of these functions will be considered
    as an application core.

**Platform**
    You can reserve one or more cores per |NUMA| node for platform use.
    One core on each host is required to run the operating system and
    associated services. For a combined controller and worker node in a
    |prod| |AIO-SX| or |AIO-DX| configuration, two cores are required.

    The ability to assign platform cores to specific |NUMA| nodes offers
    increased flexibility for high-performance configurations. For example, you
    can dedicate certain |NUMA| nodes for platform use such that other |NUMA|
    nodes that service |IRQ| requests are available for the containers (hosted
    applications) that require high-performance |IRQ| servicing.

    .. note::

        If you plan on running the |prefix|-openstack application on an
        |AIO-SX| or |AIO-DX| deployment, at least 4 platform cores are required
        for adequate Horizon Web Interface and CLI performance and 6 platform
        cores are recommended.

**Application-isolated**
    You can isolate a core from the host process scheduler by specifying the
    **application-isolated** function. This minimizes interruptions by other tasks to ensure
    more predictable latency.

    .. note::

        The tasks on the isolated cores will not be automatically load-balanced
        by the Linux task scheduler. Thus, it is up to the application to affine
        tasks appropriately.

``cpulist``

Use this option with the ``-c`` argument. This option specifies an explicit list of CPUs.

.. note::

    Only one CPU function can be specified with the ``-c`` option. The ``c`` option
    must be used in the final call to :command:`system host-cpu-modify`. Unlike specifying
    by core counts, when using the ``-c`` option, sysinv will not automatically add
    in any unspecified |SMT| hyperthreads if the host has hyperthreading enabled.
    It is up to the caller to ensure that all sibling threads are included in the CPU list.

``num_cores_on_processor``

Use this option with the ``-pX`` argument where ``X`` represents the NUMA
node whose count you want to modify. Multiple unique ``-pX`` arguments may be
specified, with a core count for each NUMA node.

``hostnameorid``

This is the name or ID of a host.





