.. _user-host-entries-configuration-9ad4c060eb15:

=======================================
User-Defined Host Entries Configuration
=======================================

You can configure user-defined host entries for external resources that are not
maintained by |DNS| records resolvable by the external |DNS| server(s) (i.e.
``nameservers`` in ``system dns-show/dns-modify``). This functionality enables
the configuration of local host records, supplementing hosts resolvable by
external |DNS| server(s).

.. only:: starlingx

   User-defined host entries can be configured at bootstrap time and/or
   post-deployment.

.. only:: partner

   .. include:: /_includes/user-host-entries-configuration-9ad4c060eb15.rest
      :start-after: host-entries-start
      :end-before: host-entries-end

The following methods are enabled to configure user host records/entries.

-  :ref:`Configure User-Defined Host Entries During Bootstrap <bootstrap-user-host-entries-configuration-9ad4c060eb15>`

-  :ref:`Configure User-Defined Host Entries via System Inventory API/CLI <CLI-user-host-entries-configuration-9ad4c060eb15>`

.. only:: partner

   .. include:: /_includes/user-host-entries-configuration-9ad4c060eb15.rest
      :start-after: deploy-manager-ref-start
      :end-before: deploy-manager-ref-end


.. _bootstrap-user-host-entries-configuration-9ad4c060eb15:

Configure User-Defined Host Entries During Bootstrap
----------------------------------------------------

During the bootstrap phase, update the Ansible playbook override file,
``localhost.yaml <hostname.yaml>``, with the following data within the
``user_dns_host_records`` section.

To add host records, use the following command syntax:

.. code-block:: none

    user_dns_host_records:
      <host-record-name>: <fqdn>[,<fqdn>...],[<IPv4-address>],[<IPv6-address>][,<TTL>]

Where, |TTL| can be optionally provided and fqdn = fully qualified domain
name.

For example:

.. code-block:: none

    user_dns_host_records:
      test-server: example.com,192.168.0.1,1234::100,300
      host1: host,example2.com,127.0.0.1

.. note::

    Upon system initialization, you can observe that data has been updated
    within the system database, and it can be listed using the system CLI.

.. _CLI-user-host-entries-configuration-9ad4c060eb15:

Configure User-Defined Host Entries via System Inventory API/CLI
----------------------------------------------------------------

After system deployment, update host records using the CLI. Source the script
``/etc/platform/openrc`` to obtain administrative privileges and use the
following procedure:

.. rubric:: |proc|

#.  Use the following command syntax to add the parameter.

    .. code-block:: none

        system service-parameter-add dns host-record <host-record-name>=<fqdn>[,<fqdn>...],[<IPv4-address>],[<IPv6-address>][,<TTL>]
        system service-parameter-apply dns

    Where fqdn = fully qualified domain name.

    For example:

    .. code-block:: none

        system service-parameter-add dns host-record test-server1=host1,testserver1.com,127.0.0.1

    .. image:: figures/add-parameter.png
         :width: 800

#.  Use the following command syntax to modify the parameter.

    .. code-block:: none

        system service-parameter-modify dns host-record <host-record-name>:<fqdn>[,<fqdn>...],[<IPv4-address>],[<IPv6-address>][,<TTL>]
        system service-parameter-apply dns

    Where fqdn = fully qualified domain name.

    The following example shows the command syntax to add another |DNS| name
    for the existing host entry.

    Example:

    .. code-block:: none

        system service-parameter-modify dns host-record test-server1=host1,host,testserver1.com,127.0.0.1

    .. image:: figures/modify-parameter.png
         :width: 800

#.  Follow the steps below to delete the parameter.

    #.  Run the command to display the service parameter list for ``host-record
        service-parameter`` type.

        .. code-block:: none

            system service-parameter-list | grep host-record

        Copy the |UUID| of the parameter that needs to be deleted.

    #.  Run the command to delete the entry.

        .. code-block:: none

            system service-parameter-delete <uuid>

        For example:

        .. code-block:: none

            system service-parameter-delete  a24e147f-2d13-4d7b-a8e7-47a1d77b95b5


.. _deployment_manager:

.. only:: partner

   .. include:: /_includes/user-host-entries-configuration-9ad4c060eb15.rest
      :start-after: deploy-manager-start
      :end-before: deploy-manager-end
