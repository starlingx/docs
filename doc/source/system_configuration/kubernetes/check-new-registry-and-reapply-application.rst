
..
.. _check-new-registry-and-reapply-application:

==========================================
Check New Registry and Reapply Application
==========================================

Perform these steps on both controllers. Use controller-0 first and then swact
to controller-1 and perform the same steps.

.. note::
    The private docker registry name must not end with ".local" to ensure that
    the domain name is properly resolved using external DNS.

.. rubric:: |proc|

#. Run this command to login:

   .. code-block:: none

       $ sudo docker login new-registry.domain.com:9001

#. Run this command to do a test pull of the image:

   .. code-block:: none

       $ sudo docker image pull new-registry.domain.com:9001/product-abc/starlingx/docker.io/alpine:latest
       $ crictl pull --creds docker:****** new-registry.domain.com:9001/product-abc/starlingx/docker.io/alpine:latest

       $ sudo docker image rm new-registry.domain.com:9001/product-abc/starlingx/docker.io/alpine:latest
       $ crictl rmi new-registry.domain.com:9001/product-abc/starlingx/docker.io/alpine:latest

#. Check if an application re-apply will now properly pull from the registries.
   First, remove the images for an application, such as
   nginx-ingress-controller, from the registry.local and the local image cache
   for all nodes (assuming AIO-SX), in order to force the next re-apply of
   the application to re-pull these images.

   .. code-block:: none

       ~(keystone_admin)]$ system registry-image-tags registry.k8s.io/ingress-nginx/controller
       +-----------+
       | Image Tag |
       +-----------+
       | v1.11.1   |
       +-----------+

       system registry-image-delete registry.k8s.io/ingress-nginx/controller:v1.11.1
       system registry-garbage-collect
       crictl images ls | grep registry.k8s.io/ingress-nginx/controller
       registry.local:9001/registry.k8s.io/ingress-nginx/controller v1.11.1 5a3c471280784 105MB                                                   42d47fe0c78f5       242MB

       crictl rmi registry.local:9001/registry.k8s.io/ingress-nginx/controller:v1.11.1
       # Note an error on this step means there is no image in the cache

       # SSH to controller-1 (or the standby controller)
       crictl rmi registry.local:9001/registry.k8s.io/ingress-nginx/controller:v1.11.1

   .. note::

        The :command:`registry-garbage-collect` command executes background
        tasks that may affect access to the docker registry. It is recommended
        to wait a few minutes before executing other registry related commands.

#. To reapply the application run the following command:

   .. code-block:: none

       ~(keystone_admin)]$ system application-apply nginx-ingress-controller

#. Then, debug tail ``-f /var/log/sysinv.log`` and look for the following information:

   .. parsed-literal::

       sysinv 2024-10-09 23:42:23.476 14930 INFO sysinv.conductor.kube_app [-] Image registry.local:9001/registry.k8s.io/ingress-nginx/controller:v1.11.1 is not available in local registry, download started from public/private registry
       sysinv 2024-10-09 23:42:23.526 14930 INFO sysinv.conductor.kube_app [-] Image registry.local:9001/registry.k8s.io/defaultbackend-amd64:1.5 download succeeded in 0 seconds
       sysinv 2024-10-09 23:43:10.226 14930 INFO sysinv.conductor.kube_app [-] Remove image <hostname>:5001/registry.k8s.io/ingress-nginx/controller:v1.11.1 after push to local registry.
       sysinv 2024-10-09 23:43:10.595 14930 INFO sysinv.conductor.kube_app [-] Image registry.local:9001/registry.k8s.io/ingress-nginx/controller:v1.11.1 download succeeded in 47 seconds
       sysinv 2024-10-09 23:43:10.596 14930 INFO sysinv.conductor.kube_app [-] All docker images for application nginx-ingress-controller were successfully downloaded in 50 seconds

#. Validate that the application is running:

   .. begin-eg-system-aplication-list

   .. only:: starlingx

       .. code-block:: none

           ~(keystone)admin)$ system application-list
           +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
           | application              | version   | manifest name                             | manifest file    | status   | progress  |
           +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
           | cert-manager             | 24.09-79  | cert-manager-fluxcd-manifests             | fluxcd-manifests | applied  | completed |
           | dell-storage             | 24.09-25  | dell-storage-fluxcd-manifests             | fluxcd-manifests | uploaded | completed |
           | nginx-ingress-controller | 24.09-64  | nginx-ingress-controller-fluxcd-manifests | fluxcd-manifests | applied  | completed |
           | oidc-auth-apps           | 24.09-59  | oidc-auth-apps-fluxcd-manifests           | fluxcd-manifests | uploaded | completed |
           | platform-integ-apps      | 24.09-141 | platform-integ-apps-fluxcd-manifests      | fluxcd-manifests | applied  | completed |
           | rook-ceph                | 24.09-40  | rook-ceph-fluxcd-manifests                | fluxcd-manifests | uploaded | completed |
           | snmp                     | 24.09-88  | snmp-fluxcd-manifests                     | fluxcd-manifests | applied  | completed |
           +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+

   .. only:: partner

       .. include:: /_includes/system-application-list.rest
          :start-after: system-application-list-begin
          :end-before: system-application-list-end

   .. end-eg-system-aplication-list

#. Validate that the image is in the local registry:

   .. code-block:: none

       ~(keystone_admin)]$ system registry-image-tags registry.k8s.io/ingress-nginx/controller
        +-----------+
        | Image Tag |
        +-----------+
        | v1.11.1   |
        +-----------+


