
.. hby1568295041837
.. _sysconf-application-commands-and-helm-overrides:

=======================================
Application Commands and Helm Overrides
=======================================

Use |prod| :command:`system application` and :command:`system helm-override`
commands to manage containerized applications provided as part of |prod|.

.. note::
    Application commands and Helm overrides apply to **user overrides** only
    and take precedence over system overrides.


``application-list``
    Use the following command to list all containerized applications provided
    as part of |prod|.

    .. code-block:: none

        ~(keystone_admin)]$ system application-list [--nowrap]

    where:

    ``--nowrap``
        prevents line wrapping of the output.

    For example:

    .. only:: partner

        .. include:: /_includes/application-commands-and-helm-overrides.rest
           :start-after: app-version-begin
           :end-before: app-version-end

    .. only:: starlingx

        .. parsed-literal::

            ~(keystone_admin)]$ system application-list
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
            | application              | version   | manifest name                             | manifest file    | status   | progress  |
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
            | cert-manager             | 24.09-79  | cert-manager-fluxcd-manifests             | fluxcd-manifests | applied  | completed |
            | dell-storage             | 24.09-25  | dell-storage-fluxcd-manifests             | fluxcd-manifests | uploaded | completed |
            | nginx-ingress-controller | 24.09-64  | nginx-ingress-controller-fluxcd-manifests | fluxcd-manifests | applied  | completed |
            | oidc-auth-apps           | 24.09-59  | oidc-auth-apps-fluxcd-manifests           | fluxcd-manifests | uploaded | completed |
            | platform-integ-apps      | 24.09-141 | platform-integ-apps-fluxcd-manifests      | fluxcd-manifests | applied  | completed |
            | rook-ceph                | 24.09-48  | rook-ceph-fluxcd-manifests                | fluxcd-manifests | uploaded | completed |
            | snmp                     | 24.09-89  | snmp-fluxcd-manifests                     | fluxcd-manifests | applied  | completed |
            +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+

    .. only:: partner

        .. include:: /_includes/system-application-list.rest
           :start-after: system-application-list-begin
           :end-before: system-application-list-end

``application-show``
    Use the following command to show details for |prod|.

    .. code-block:: none

        ~(keystone_admin)]$ system application-show <app_name>

    where:

    **<app_name>**
        is the name of the application to show details for.

    For example, run the following command to show details of the ``cert-manager``
    application.

    .. code-block:: none

        ~(keystone_admin)]$ system application-show cert-manager

``application-upload``
    Use the following command to upload application Helm chart\(s) and
    manifest.

    .. code-block:: none

        ~(keystone_admin)]$ system application-upload [-n | --app-name] <app_name> [-v | --version] <version> <tar_file>

    where the following are optional arguments:

    **<app_name>**
        assigns a custom name for application. You can use this name to
        interact with the application in the future.

    **<version>**
        is the version of the application.

    and the following is a positional argument:

    **<tar_file>**
        is the path to the tar file containing the application to be uploaded.


    Run the following command to upload the application contained in the tar file.

    .. code-block:: none

        ~(keystone_admin)]$ system application-upload metrics-server-*.tgz

``helm-override-list``
    To list the Helm chart overrides for the |prod|, use the following
    command:

    .. code-block:: none

        ~(keystone_admin)]$ system helm-override-list
        usage: system helm-override-list [--nowrap] [-l | --long] <app_name>

    where the following is a positional argument:

    **<app_name>**
        The name of the application.

    and the following are optional arguments:

    ``--nowrap``
        No word-wrapping of output.

    ``--long``
        List additional fields in output.

    For example:

    .. parsed-literal::

        ~(keystone_admin)]$ system helm-override-list |prefix|-openstack --long
        +---------------------+--------------------------------+---------------+
        | chart name          | overrides namespaces           | chart enabled |
        +---------------------+--------------------------------+---------------+
        | barbican            | [u'openstack']                 | [False]       |
        | ceph-rgw            | [u'openstack']                 | [False]       |
        | cinder              | [u'openstack']                 | [True]        |
        | garbd               | [u'openstack']                 | [True]        |
        | glance              | [u'openstack']                 | [True]        |
        | heat                | [u'openstack']                 | [True]        |
        | helm-toolkit        | []                             | []            |
        | horizon             | [u'openstack']                 | [True]        |
        | ingress             | [u'kube-system', u'openstack'] | [True, True]  |
        | ironic              | [u'openstack']                 | [False]       |
        | keystone            | [u'openstack']                 | [True]        |
        | keystone-api-proxy  | [u'openstack']                 | [True]        |
        | libvirt             | [u'openstack']                 | [True]        |
        | mariadb             | [u'openstack']                 | [True]        |
        | memcached           | [u'openstack']                 | [True]        |
        | neutron             | [u'openstack']                 | [True]        |
        | nginx-ports-control | []                             | []            |
        | nova                | [u'openstack']                 | [True]        |
        | nova-api-proxy      | [u'openstack']                 | [True]        |
        | openvswitch         | [u'openstack']                 | [True]        |
        | placement           | [u'openstack']                 | [True]        |
        | rabbitmq            | [u'openstack']                 | [True]        |
        | version_check       | []                             | []            |
        +---------------------+--------------------------------+---------------+

    ..  lines below will be temporarily removed from table:
        | aodh                | [u'openstack']                 | [False]       |
        | ceilometer          | [u'openstack']                 | [False]       |
        | gnocchi             | [u'openstack']                 | [False]       |
        | panko               | [u'openstack']                 | [False]       |

``helm-override-show``
    To show the overrides for a particular chart, use the following command.
    System overrides are displayed in the **system_overrides** section of
    the **Property** column.

    .. code-block:: none

        ~(keystone_admin)]$ system helm-override-show
        usage: system helm-override-show <app_name> <chart_name> <namespace>

    where the following are positional arguments:

    **<app_name>**
        The name of the application.

    **< chart_name>**
        The name of the chart.

    **<namespace>**
        The namespace for chart overrides.

    For example:

    .. parsed-literal::

        ~(keystone_admin)]$ system helm-override-show |prefix|-openstack glance openstack

``helm-override-update``
    To modify service configuration parameters using user-specified overrides,
    use the following command. To update a single configuration parameter, you
    can use ``--set``. To update multiple configuration parameters, use
    the ``--values`` option with a ``yaml`` file.

    .. code-block:: none

        ~(keystone_admin)]$ system helm-override-update
        usage: system helm-override-update <app_name> <chart_name> <namespace> --reuse-values --reset-values --values <file_name> --set <commandline_overrides>

    where the following are positional arguments:

    **<app_name>**
        The name of the application.

    **<chart_name>**
        The name of the chart.

    **<namespace>**
        The namespace for chart overrides.

    and the following are optional arguments:

    ``--reuse-values``
        Reuse existing Helm chart user override values. If reset-values is
        used, reuse-values is ignored.

    ``--reset-values``
        Replace any existing Helm chart overrides with the ones specified.

    ``--values``
        Specify a **yaml** file containing Helm chart override values. You can
        specify this value multiple times.

    ``--set``
        Set Helm chart override values using the command line. Multiple
        override values can be specified with multiple :command:`set`
        arguments. These are processed after files passed through the
        values argument.

    For example, to enable the glance debugging log, use the following
    command:

    .. parsed-literal::

        ~(keystone_admin)]$ system helm-override-update |prefix|-openstack glance openstack --set conf.glance.DEFAULT.DEBUG=true
        +----------------+-------------------+
        | Property       | Value             |
        +----------------+-------------------+
        | name           | glance            |
        | namespace      | openstack         |
        | user_overrides | conf:             |
        |                |   glance:         |
        |                |     DEFAULT:      |
        |                |       DEBUG: true |
        +----------------+-------------------+

    The user overrides are shown in the **user_overrides** section of the
    **Property** column.

    .. note::
        To apply the updated Helm chart ovverrides to the running application,
        use the :command:`system application-apply` command.

``helm-chart-attribute-modify``
    To enable or disable the installation of a particular Helm chart within an
    application manifest, use the :command:`helm-chart-attribute-modify`
    command. This command does not modify a chart or modify chart overrides,
    which are managed through the :command:`helm-override-update` command.

    .. code-block:: none

        ~(keystone_admin)]$ system helm-chart-attribute-modify [--enabled <true/false>] <app_name> <chart_name> <namespace>

    where the following is an optional argument:

    ``--enabled``
        determines whether the chart is enabled.

    and the following are positional arguments:

    **<app_name>**
        The name of the application.

    **<chart_name>**
        The name of the chart.

    **<namespace>**
        The namespace for chart overrides.

    .. note::
        To apply the updated helm chart attribute to the running application,
        use the :command:`system application-apply` command.

``helm-override-delete``
    To delete all the user overrides for a chart, use the following command:

    .. code-block:: none

        ~(keystone_admin)]$ system helm-override-delete
        usage: system helm-override-delete <app_name> <chart_name> <namespace>

    where the following are positional arguments:

    **<app_name>**
        The name of the application.

    **<chart_name>**
        The name of the chart.

    **<namespace>**
        The namespace for chart overrides.

    For example:

    .. parsed-literal::

        ~(keystone_admin)]$ system helm-override-delete |prefix|-openstack glance openstack
        Deleted chart overrides glance:openstack for application |prefix|-openstack

``application-apply``
    Use the following command to apply or reapply an application, making it
    available for service.

    .. code-block:: none

        ~(keystone_admin)]$ system application-apply [-m | --mode] <mode> <app_name>

    where the following is an optional argument:

    **mode**
        An application-specific mode controlling how the manifest is
        applied. This option is used to back-up and restore the
        |prefix|-openstack application.

    and the following is a positional argument:

    **<app_name>**
        is the name of the application to apply.

    For example, run the following command to apply the ``metrics-server`` application.

    .. code-block:: none

        ~(keystone_admin)]$ system application-apply metrics-server

``application-abort``
    Use the following command to abort the current application.

    .. code-block:: none

        ~(keystone_admin)]$ system application-abort <app_name>

    where:

    **<app_name>**
        is the name of the application to abort.

    For example:

    .. parsed-literal::

        ~(keystone_admin)]$ system application-abort metrics-server
        Application abort request has been accepted. If the previous operation has not
        completed/failed, it will be cancelled shortly.

    Use :command:`application-list` to confirm that the application has been
    aborted.

``application-update``
    Use the following command to update the deployed application to a different
    version.

    .. code-block:: none

        ~(keystone_admin)]$ system application-update [-n | --app-name] <app_name> [-v | --app-version] <version> [ --reuse-attributes <true/false> ] [ --reuse-user-overrides ` <true/false> ] <tar_file>

    where the following are optional arguments:

    **<app_name>**
        The name of the application to update.

        You can look up the name of an application using the :command:`application-list` command:

        .. only:: partner

            .. include:: /_includes/application-commands-and-helm-overrides.rest
               :start-after: application-version-begin
               :end-before: application-version-end

        .. only:: starlingx

            .. code-block:: none

                ~(keystone_admin)]$ system application-list
                    +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
                    | application              | version   | manifest name                             | manifest file    | status   | progress  |
                    +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
                    | cert-manager             | 24.09-79  | cert-manager-fluxcd-manifests             | fluxcd-manifests | applied  | completed |
                    | dell-storage             | 24.09-25  | dell-storage-fluxcd-manifests             | fluxcd-manifests | uploaded | completed |
                    | nginx-ingress-controller | 24.09-64  | nginx-ingress-controller-fluxcd-manifests | fluxcd-manifests | applied  | completed |
                    | oidc-auth-apps           | 24.09-59  | oidc-auth-apps-fluxcd-manifests           | fluxcd-manifests | uploaded | completed |
                    | platform-integ-apps      | 24.09-141 | platform-integ-apps-fluxcd-manifests      | fluxcd-manifests | applied  | completed |
                    | rook-ceph                | 24.09-48  | rook-ceph-fluxcd-manifests                | fluxcd-manifests | uploaded | completed |
                    | snmp                     | 24.09-89  | snmp-fluxcd-manifests                     | fluxcd-manifests | applied  | completed |
                    +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+

        .. only:: partner

            .. include:: /_includes/system-application-list.rest
               :start-after: system-application-list-begin
               :end-before: system-application-list-end

        The output indicates that the currently installed version of **cert-manager** is 1.0-34.

    **<version>**
        The version to update the application to.

    **reuse-attributes**
        If the user disabled some Helm charts (using :command:`system helm-chart-attribute-modify --enabled false`)
        and wants to keep them disabled during an app-update then the user
        should use ``--reuse-attributes true``.

    **reuse-user-overrides**
        If the user added some Helm chart overrides
        (using :command:`system helm-override-update`) and wants to keep them
        over an app-update then the user should use ``--reuse-user-overrides true``.

    and the following is a positional argument which must come last:

    **<tar_file>**
        The tar file containing the application manifest, Helm charts and
        configuration file.

``application-remove``
    Use the following command to remove an application from service. Removing
    an application will clean up related Kubernetes resources and delete all
    of its installed helm charts.

    .. code-block:: none

        ~(keystone_admin)]$ system application-remove <app_name>

    where:

    **<app_name>**
        is the name of the application to remove.

    For example, run the following command to remove the ``metric-server`` application.

    .. code-block:: none

        ~(keystone_admin)]$ system application-remove metrics-server

    This command places the application in the uploaded state.

``application-delete``
    Use the following command to completely delete an application from the
    system.

    .. code-block:: none

        ~(keystone_admin)]$ system application-delete <app_name>

    where:

    **<app_name>**
        is the name of the application to delete.

    You must run :command:`application-remove` before deleting an application.

    For example:

    .. parsed-literal::

        ~(keystone_admin)]$ system application-delete metrics-server
        Application metrics-server deleted.


