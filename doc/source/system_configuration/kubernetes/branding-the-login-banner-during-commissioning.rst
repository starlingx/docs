
.. xjc1559744910969
.. _branding-the-login-banner-during-commissioning:

===========================================
Brand the Login Banner During Commissioning
===========================================

You can customize the pre-login message (issue) and post-login |MOTD| across
the entire |prod| cluster during system commissioning and installation.

The following Linux files controlling pre-login (issue) and post-login (motd)
messaging can be customized with the procedure below.

.. _branding-the-login-banner-during-commissioning-d665e16:

.. table::
    :widths: auto

    +---------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------+
    | /etc/issue                                                                                  | console login banner                                                                        |
    +---------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------+
    | /etc/issue.net                                                                              | ssh login banner                                                                            |
    +---------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------+
    | /etc/motd.head                                                                              | message of the day header                                                                   |
    +---------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------+
    | /etc/motd.tail                                                                              | message of the day footer                                                                   |
    |                                                                                             |                                                                                             |
    |                                                                                             | This file is not present by default. You must first create it to apply your customizations. |
    +---------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------+

``issue`` and ``issue.net`` are free-standing files. ``/etc/motd`` is generated
from the following sources in the order presented:

.. _branding-the-login-banner-during-commissioning-d665e97:

#.  ``/etc/motd.head``

#.  ``/etc/sysinv/motd.system``

#.  ``/etc/platform/motd.license``

#.  ``/etc/motd.tail``

Complete the following procedure to customize the login banner during
installation and commissioning:

.. rubric:: |proc|

#.  Define customized banners. To configure the issue and |MOTD| messages,
    there are two available options:

    -   Define banner variable in the Ansible Overrides file for the Bootstrap
        Playbook (``$HOME/localhost.yml``)

        It is possible to configure the issue and |MOTD| messages by defining
        the "banner" variable in the ``ansible-override`` YAML file. Messages
        should be formatted as multiline string entries.  For each category, it
        is possible to define either one or both parameters, or leave them
        undefined. If parameters are left undefined, default values will be
        applied for each message.

        .. code-block:: none

            banner: {}

            e.g.
            banner:
               issue:
                  network: |
                     ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                     ::::::::::::::::::::: EXAMPLE SSH WELCOME MESSAGE ::::::::::::::::::::
                     ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                  console: |
                     :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                     ::::::::::::::::::: EXAMPLE CONSOLE WELCOME MESSAGE ::::::::::::::::::
                     :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
               motd:
                  head: |
                     -----------------------------------------------------------------------------------------------
                     ---------------------- EXAMPLE MOTD HEAD MESSAGE ---------------------
                     -----------------------------------------------------------------------------------------------
                  tail: |
                     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                     ++++++++++++++++++++++ EXAMPLE MOTD TAIL MESSAGE +++++++++++++++++++++
                     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -   Provide customization files.

        Alternatively, to customize any of the four customizable banner files
        listed above, it is possible to provide the new files directly in the
        following locations:

        -   ``/opt/banner/issue``

        -   ``/opt/banner/issue.net``

        -   ``/opt/banner/motd.head``

        -   ``/opt/banner/motd.tail``

    See the :command:`issue` and :command:`motd` man pages for details on
    file syntax.

#.  Run the Ansible Bootstrap playbook.

    When the Ansible Bootstrap playbook is run, either the messages defined in
    the Ansible Overrides or the files specified in ``/opt/banner are moved``
    to configuration storage and are applied to the controller node as it is
    initialized. All nodes in the cluster which are subsequently configured
    will retrieve these custom banners as well.

    .. note::

        To update the pre-login message (issue) or the post-login |MOTD| after
        the system has been installed and commissioned, see :ref:`Brand the
        Login Banner on a Commissioned System
        <branding-the-login-banner-on-a-commissioned-system>` for more
        information.