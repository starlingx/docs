
.. jpu1552672927783
.. _changing-the-oam-ip-configuration-using-the-cli:

=============================================
Change the OAM IP Configuration Using the CLI
=============================================

If you prefer, you can use the CLI to view or change the |OAM| IP Configuration.

.. rubric:: |context|

During installation, |prod-long| is configured with an |OAM| network subnet and
related IP addresses. You can change these addresses using the the CLI or the
Horizon Web Interface. You can use IPv4 or IPv6 addresses.

.. caution::

    Access to the |OAM| network is interrupted during this procedure. When a
    :command:`swact` is performed on the controllers, the newly active
    controller uses the changed |OAM| IP addresses. The existing |OAM| IP
    addresses are no longer valid, and you must use the new OAM IP addresses
    to reconnect to the controller. Changes to external |OAM| access routing
    settings may also be required. In addition, |VNC| console access to
    worker-node hosts is interrupted until the hosts are locked and unlocked.

    The registry and SSL certificates will automatically update the |OAM|
    addresses in their |SANs|. However, any optional applications must have
    their certificate |SANs| updated manually.

------------------------------
OAM Address Pool Configuration
------------------------------

The system supports the dual-stack (IPv4 and IPv6) values for the affected pools.
To modify the values, use the following command:

.. code-block:: none

    system addrpool-modify [--name <name>] [--network <network address>] \
                           [--prefix <network prefix>] [--ranges <ranges>] \
                           [--order <sequential | random>] \
                           [--floating-address <floating address>] \
                           [--controller0-address <controller0 address>] \
                           [--controller1-address <controller1 address>] \
                           [--gateway-address <gateway address>] \
                            <address_pool uuid>

To view the existing |OAM| pools, use the following command.

.. code-block:: none

    ~(keystone_admin)]# system network-addrpool-list
    +-------...----------------+-----------------+-----------------------------+
    | uuid  ...                | network_name    | addrpool_name               |
    +-------...----------------+-----------------+-----------------------------+
    | 54911d...b6-62266582a8f4 | admin           | admin-ipv6                  |
    | 7a54f0...61-55eb6b12b1c5 | cluster-host    | cluster-host-subnet-ipv6    |
    | 12204f...24-1ea6400da629 | cluster-pod     | cluster-pod-subnet-ipv6     |
    | 6d6795...3c-2506fdf386b1 | cluster-service | cluster-service-subnet-ipv6 |
    | f82081...48-662f8c48466c | mgmt            | management-ipv6             |
    | 7fbd42...4a-4807cf039a0e | multicast       | multicast-subnet-ipv6       |
    | 0f1ab4...0c-bca519d752b6 | oam             | oam-ipv4                    |
    | a1d33e...f1-a5523c2bf079 | oam             | oam-ipv6                    |
    | 55a0d2...a0-eff1e72ff05a | pxeboot         | pxeboot                     |
    +-------...----------------+-----------------+-----------------------------+

Get the |OAM| pool uuid with the following command:

.. code-block:: none

    ~(keystone_admin)]# system addrpool-list --nowrap
    +--------------------------------------+-----------------------------+---------------+--------+-...-+------------------+---------------------+---------------------+-----------------+
    | uuid                                 | name                        | network       | prefix | ... | floating_address | controller0_address | controller1_address | gateway_address |
    +--------------------------------------+-----------------------------+---------------+--------+-...-+------------------+---------------------+---------------------+-----------------+
    | 7ac2e794-184f-4b40-8529-e07b52bfd2c2 | admin-ipv6                  | 3333::        | 64     | ... | 3333::2          | 3333::3             | 3333::4             | None            |
    | 6250edb8-15f5-4204-80f1-8e54b9e28a5a | cluster-host-subnet-ipv6    | fd02::        | 64     | ... | fd02::1          | fd02::2             | fd02::3             | None            |
    | f4c9560c-47e5-46bd-aff5-18642831b1da | cluster-pod-subnet-ipv6     | fd03::        | 64     | ... | None             | None                | None                | None            |
    | a6366aab-b3c1-4947-97e5-f5171e0e2f3e | cluster-service-subnet-ipv6 | fd04::        | 112    | ... | None             | None                | None                | None            |
    | 412aebff-9a86-40b1-a379-752f00a0c3a0 | management-ipv6             | fd01::        | 64     | ... | fd01::1          | fd01::2             | fd01::3             | None            |
    | 7c445f38-067c-4b3c-a511-d8e00da5791c | multicast-subnet-ipv6       | ff08::1:1:0   | 124    | ... | None             | None                | None                | None            |
    | 4affb8fb-6b67-467a-9674-cfa6743ca5c5 | oam-ipv4                    | 20.20.20.0    | 24     | ... | 20.20.20.1       | 20.20.20.2          | 20.20.20.3          | None            |
    | b46512d7-5404-4daa-a64d-fc510e0c5864 | oam-ipv6                    | fd00::        | 64     | ... | fd00::a14:103    | fd00::a14:104       | fd00::a14:105       | fd00::1         |
    | 05fde56d-f26a-4ea4-8b32-1ebf868743e2 | pxeboot                     | 169.254.202.0 | 24     | ... | 169.254.202.1    | 169.254.202.2       | 169.254.202.3       | None            |
    +--------------------------------------+-----------------------------+---------------+--------+-...-+------------------+---------------------+---------------------+-----------------+
    ~(keystone_admin)]# system addrpool-show b46512d7-5404-4daa-a64d-fc510e0c5864
    +---------------------+---------------------------------------+
    | Property            | Value                                 |
    +---------------------+---------------------------------------+
    | uuid                | b46512d7-5404-4daa-a64d-fc510e0c5864  |
    | name                | oam-ipv6                              |
    | network             | fd00::                                |
    | prefix              | 64                                    |
    | order               | random                                |
    | ranges              | ['fd00::1-fd00::ffff:ffff:ffff:fffe'] |
    | floating_address    | fd00::a14:103                         |
    | controller0_address | fd00::a14:104                         |
    | controller1_address | fd00::a14:105                         |
    | gateway_address     | fd00::1                               |
    +---------------------+---------------------------------------+


Modify the pool, in this example ``oam-ipv6``, using the following parameters:

.. note::
    If the system is dual-stack open a ssh session via the other |OAM| network
    configured before proceeding, to not loose the connection.

.. code-block:: none

    system addrpool-modify b46512d7-5404-4daa-a64d-fc510e0c5864 \
        --floating-address fd00::3 \
        --controller0-address fd00::4 \
        --controller1-address fd00::5 \
        --gateway-address fd00::2

For |AIO-SX| ``controller0-address`` and ``controller1-address`` do not take effect.

-------------------------
Legacy OAM Configuration
-------------------------

The set of commands below can also be used instead of ``addrpool`` commands. It
affects only the |OAM| primary address pool. If using CLI a warning is displayed
showing the command is deprecated, but can still be used.

To view the existing |OAM| IP configuration, use the following command.

.. code-block:: none

    ~(keystone_admin)]$ system oam-show
    +-----------------+--------------------------------------+
    | Property        | Value                                |
    +-----------------+--------------------------------------+
    | created_at      | 2018-05-16T20:06:25.523495+00:00     |
    | isystem_uuid    | b0380a56-697c-42f7-97bc-f1e407111416 |
    | oam_c0_ip       | 10.10.10.3                           |
    | oam_c1_ip       | 10.10.10.4                           |
    | oam_floating_ip | 10.10.10.2                           |
    | oam_gateway_ip  | 10.10.10.1                           |
    | oam_subnet      | 10.10.10.0/24                        |
    | updated_at      | None                                 |
    | uuid            | 2818e7c4-f730-43bd-b33d-eaff53a92ee1 |
    +-----------------+--------------------------------------+

To change the |OAM| IP subnet, floating IP address, gateway IP address, or
controller IP addresses, use the following command syntax.

.. code-block:: none

    ~(keystone_admin)]$ system oam-modify oam_subnet=<subnet>/<netmask> \
    oam_gateway_ip=<gateway_ip_address> \
    oam_floating_ip=<floating_IP_address> \
    oam_c0_ip=<controller-0_IP_address> \
    oam_c1_ip=<controller-1_ip_address>

For example:

.. code-block:: none

    ~(keystone_admin)]$ system oam-modify oam_subnet=10.10.10.0/24 \
    oam_gateway_ip=10.10.10.1 \
    oam_floating_ip=10.10.10.2 \
    oam_c0_ip=10.10.10.3 \
    oam_c1_ip=10.10.10.4

.. note::
    On |AIO-SX| systems, the
    oam_floating_ip, oam_c0\_ip and oam_c0\_ip parameters are not
    supported. To change the |OAM| IP address of a Simplex System, the parameter
    oam_ip must be used in combination with oam_gateway_ip and oam_subnet.

    For example:

    .. code-block:: none

        ~(keystone_admin)]$ system oam-modify oam_subnet=10.10.10.0/24 oam_gateway_ip=10.10.10.1 oam_ip=10.10.10.2

.. note::
    If you change the IP address version (IPv4 or IPv6), ensure that the
    same version is used for the DNS and NTP servers.


.. rubric:: |postreq|

After changing the |OAM| server configuration, you must lock and unlock the
controllers. This process requires a swact on the controllers. Then you must
lock and unlock the worker nodes one at a time, ensuring that sufficient
resources are available to migrate any running instances.

.. note::
   On |AIO-SX| systems you do not need to lock and unlock the host. The
   changes are applied automatically.

In the |prod-dc| system, if the system controller |OAM| address is changed
(the network itself or the floating address), subclouds need to update the
address pool ``system-controller-oam-subnet`` so that the registry.central domain
points to the new value. Run the following command on all subclouds:

.. code-block::

    ~(keystone_admin)]$ system addrpool-modify [system-controller-oam-subnet's uuid] \
                          --network [new system-controller OAM network address] \
                          --prefix [new system-controller OAM network prefix] \
                          --ranges [new system-controller OAM network address range] \
                          --floating-address [new system-controller OAM floating address]

Adjust the necessary parameters based on the altered values in the system controller.

(Optional) If you are running Docker proxy on your system, you need to
change the **no_proxy** service parameter using the CLI to append the new
OAM IP, using the following steps:

#.  To list the service parameters, use the :command:`system service-parameter-list`
    command. Note the old **no_proxy** values.

#.  To append the new OAM IP to the no_proxy parameter, use the
    :command:`system serviceparameter-modify docker proxy no_proxy=old_value, new_oam_ip(s)`
    command.

    Where, *<new_oam_ip(s)>* is the new OAM IP(s) that needs to be appended
    to the **no_proxy** parameter. Ensure you include new values for the
    floating OAM, controller-0 OAM, and controller-1 if they are changed.

#.  To apply the changes, use the :command:`system service-parameter-apply docker`
    command.

#.  If it is not |AIO-SX|, then lock and unlock the controller(s), otherwise the
    |OAM| modification will be applied immediately.

.. seealso::

    :ref:`Default Firewall Rules <security-default-firewall-rules>`

    :ref:`Modify Firewall Options <security-firewall-options>`

    :ref:`IPv4/IPv6 Dual Stack Networking <dual-stack-support-318550fd91b5>`
