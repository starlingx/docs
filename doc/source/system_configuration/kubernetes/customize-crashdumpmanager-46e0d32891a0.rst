.. _customize-crashdumpmanager-46e0d32891a0:

============================
Customize Crash-dump-manager
============================

The Linux kernel can be configured to perform a crash dump and reboot in
response to specific serious events. A crash dump event produces a
crash dump report with bundle of files that represent the state of the kernel at the
time of the event, which is useful for post-event root cause analysis.

The kernel directs the new crash dump bundles to ``/var/crash/<dated dump bundle>``.
Crash dump bundles are quite large and they can fill up their target filesystem.

The purpose of crash-dump-manager service is to prevent crash dumps from
filling up the root filesystem by compressing the crash bundles found in
``/var/crash`` and moving it to ``/var/log/crash``. The first bundle is compressed as
``vmcore_first.tar`` and preserved. Subsequent crash bundles are saved as
``vmcore_<date>.tar``.

The status of the crash-dump-manager can be checked by running the following command:

.. code-block:: none

    % sudo systemctl status crash-dump-manager
      crash-dump-manager.service - Crash Dump Manager
         Loaded: loaded (/lib/systemd/system/crash-dump-manager.service; enabled; vendor preset: enabled)
         Active: inactive (dead) since Mon 2023-09-18 23:08:10 UTC; 14s ago
       Main PID: 1328880 (code=exited, status=0/SUCCESS)
            CPU: 22ms

---------------------------------------------------
Change the Default Crash-dump-manager Configuration
---------------------------------------------------

.. rubric:: |context|

The crash-dump-manager service uses four parameters in order to customize its
behavior. You can change the parameter values using service-parameters. The editable
parameters and their default values are as follows:

**Maximum Files**

Number of saved crash dump files in crash dump file system (``/var/log/crash``).
Default: 4

**Maximum Size**

Limits the maximum size of an individual crash dump file. Support for ``unlimited``.
Default: 5GiB

**Maximum Used**

Limits the maximum storage used by saved crash dump files. Support for ``unlimited``.
Default: Unlimited

**Minimum Available**

Limits the minimum available storage on the crash dump file system.
Default: 10% of crash dump file system size
Minimum: 1GiB

**Configuration Parameters Units**

When configuring the **Maximum Size**, **Maximum Used**, and **Minimum Available**
parameters, you have the flexibility to specify the units of measurement. These
parameters accept ``integer/float values`` followed by the unit of measurement,
that can be written in two primary forms: units based on the metric system
(decimal) and units based on the binary system (binary prefixes).

- Decimal units (metric system)

  K: Represents kilobytes, where 1 KB is equal to 1,000 bytes.

  M: Represents megabytes, where 1 MB is equal to 1,000,000 bytes.

  G: Represents gigabytes, where 1 GB is equal to 1,000,000,000 bytes.

  T: Represents terabytes, where 1 TB is equal to 1,000,000,000,000 bytes.

  Example:

  5K represents 5 kilobytes (1 KB = 1,000 bytes).

  5M represents 5 megabytes (1 MB = 1,000,000 bytes).

  5G represents 5 gigabytes (1 GB = 1,000,000,000 bytes).

  5T represents 5 terabytes (1 TB = 1,000,000,000,000 bytes).

- Binary units (binary prefixes)

  Ki: Represents kibibytes, where 1 KiB is equal to 1,024 bytes.

  Mi: Represents mebibytes, where 1 MiB is equal to 1,048,576 bytes.

  Gi: Represents gibibytes, where 1 GiB is equal to 1,073,741,824 bytes.

  Ti: Represents tebibytes, where 1 TiB is equal to 1,099,511,627,776 bytes.

  Example:

  5Ki represents 5 kibibytes (1 KiB = 1,024 bytes).

  5Mi represents 5 mebibytes (1 MiB = 1,048,576 bytes).

  5Gi represents 5 gibibytes (1 GiB = 1,073,741,824 bytes).

  5Ti represents 5 tebibytes (1 TiB = 1,099,511,627,776 bytes).

- Unlimited

  Additionally, the **Maximum Size** and **Maximum Used** parameters support
  the special value ``unlimited``. When the parameter value is set to
  ``unlimited``, there are no imposed restrictions on the size or storage usage.

.. rubric:: |prereq|

Ensure that you have sufficient storage available on the host’s log filesystem.
For more information about adjusting the storage size, see :ref:`resizing-filesystems-on-a-host`.

.. rubric:: |proc|

#. Add a crash dump service parameter.

   .. code-block:: none

       ~(keystone_admin)]$ system service-parameter-add platform crash dump <parameter>=<value>

#. Modify an existing crash dump service parameter.

   .. code-block:: none

       ~(keystone_admin)]$ system service-parameter-modify platform crashdump <parameter>=<value>

#. Delete an existing crash dump service parameter.

   .. code-block:: none

       ~(keystone_admin)]$ system service-parameter-delete <uuid>

   .. note::

       When a parameter is deleted, its value will be reset to the default value.

Where, <parameter> can be one of these: ``max_files``, ``max_size``, ``max_used``, ``min_available``.

Example:

The following example adds **Maximum Files**, **Maximum Size**, **Maximum
Used**, and **Minimum Available**.

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ system service-parameter-add platform crashdump max_files=6

    +-------------+--------------------------------------+
    | Property    | Value                                |
    +-------------+--------------------------------------+
    | uuid        | 3f5d8bc0-26cc-43b5-aeb6-d6a5b5b27035 |
    | service     | platform                             |
    | section     | crashdump                            |
    | name        | max_files                            |
    | value       | 6                                    |
    | personality | None                                 |
    | resource    | None                                 |
    +-------------+--------------------------------------+

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ system service-parameter-add platform crashdump max_size=unlimited
    +-------------+--------------------------------------+
    | Property    | Value                                |
    +-------------+--------------------------------------+
    | uuid        | 164b5630-9125-4482-b08d-5b5b80d25e14 |
    | service     | platform                             |
    | section     | crashdump                            |
    | name        | max_size                             |
    | value       | unlimited                            |
    | personality | None                                 |
    | resource    | None                                 |
    +-------------+--------------------------------------+

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ system service-parameter-add platform crashdump max_used=10G
    +-------------+--------------------------------------+
    | Property    | Value                                |
    +-------------+--------------------------------------+
    | uuid        | f2a6d3b4-0b9f-40d9-89b1-4e06d425cf79 |
    | service     | platform                             |
    | section     | crashdump                            |
    | name        | max_used                             |
    | value       | 10G                                  |
    | personality | None                                 |
    | resource    | None                                 |
    +-------------+--------------------------------------+

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ system service-parameter-add platform crashdump min_available=4,5Gi
    +-------------+--------------------------------------+
    | Property    | Value                                |
    +-------------+--------------------------------------+
    | uuid        | da4af8a9-0a19-41d7-9f70-e803e312925c |
    | service     | platform                             |
    | section     | crashdump                            |
    | name        | min_available                        |
    | value       | 4,5Gi                                |
    | personality | None                                 |
    | resource    | None                                 |
    +-------------+--------------------------------------+


If you need to modify or delete any parameter, perform the following:

#. List the currently configured service parameters.

   .. code-block:: none

       [sysadmin@controller-0 ~(keystone_admin)]$ system service-parameter-list | grep crashdump

       | 3f5d8bc0-26cc-43b5-aeb6-d6a5b5b27035 | platform   | crashdump   | max_files     | 6           | None        | None    |
       | 164b5630-9125-4482-b08d-5b5b80d25e14 | platform   | crashdump   | max_size      | unlimited   | None        | None    |
       | f2a6d3b4-0b9f-40d9-89b1-4e06d425cf79 | platform   | crashdump   | max_used      | 10G         | None        | None    |
       | da4af8a9-0a19-41d7-9f70-e803e312925c | platform   | crashdump   | min_available | 4,5Gi       | None        | None    |

#. Update it or delete it using the UUID.

   .. code-block:: none

       [sysadmin@controller-0 ~(keystone_admin)]$ system service-parameter-modify platform crashdump max_used='unlimited'

       +-------------+--------------------------------------+
       | Property    | Value                                |
       +-------------+--------------------------------------+
       | uuid        | f2a6d3b4-0b9f-40d9-89b1-4e06d425cf79 |
       | service     | platform                             |
       | section     | crashdump                            |
       | name        | max_used                             |
       | value       | unlimited                            |
       | personality | None                                 |
       | resource    | None                                 |
       +-------------+--------------------------------------+

   .. code-block:: none

       [sysadmin@controller-0 ~(keystone_admin)]$ system service-parameter-delete f2a6d3b4-0b9f-40d9-89b1-4e06d425cf79

       Deleted service parameter: f2a6d3b4-0b9f-40d9-89b1-4e06d425cf79

#. List the service-parameter to confirm the new values.

   .. code-block:: none

       [sysadmin@controller-0 ~(keystone_admin)]$ system service-parameter-list | grep crashdump

       | 164b5630-9125-4482-b08d-5b5b80d25e14 | platform   | crashdump    | max_size         | unlimited  | None        | None     |
       | 3f5d8bc0-26cc-43b5-aeb6-d6a5b5b27035 | platform   | crashdump    | max_files        | 6          | None        | None     |
       | f2a6d3b4-0b9f-40d9-89b1-4e06d425cf79 | platform   | crashdump    | max_used         | 10G        | None
       | da4af8a9-0a19-41d7-9f70-e803e312925c | platform   | crashdump    | min_available    | 4,5Gi      | None        | None     |

