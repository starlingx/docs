.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _intel-driver-version-c6e3fa384ff7:

============================
Switch Intel Driver Versions
============================

.. contents:: |minitoc|
   :local:
   :depth: 2

------------
Introduction
------------

The system supports both in-tree and out-of-tree versions of the Intel ``ice``, ``i40e``,
and ``iavf`` drivers. On initial installation, the system uses the default out-of-tree
driver version. This sections describes how to switch between the in-tree and
out-of-tree driver versions.

.. warning::

    The |NIC| firmware (NVM) has to align with one of the selected driver
    listed below.

-   Out-of-tree drivers (default)
      - ice: 1.14.9.2 / Required NVM/firmware: 4.20 (Intel E810)
      - i40e: 2.25.7 / Required NVM/firmware: 9.00
      - iavf: 4.11.1

-   In-tree drivers
      - ice: Same as 6.6.7 kernel in-tree driver / Required NVM/firmware: 4.20
      - i40e: Same as 6.6.7 kernel in-tree driver / Required NVM/firmware: 9.00
      - iavf: Same as 6.6.7 kernel in-tree driver

----------------------------------------------------------
Switch between the In-tree and Out-of-tree Driver Versions
----------------------------------------------------------

The ``out_of_tree_drivers`` service parameter can be used to switch between the
in-tree and out-of-tree versions of a given driver.

Switch to In-tree Driver Versions
---------------------------------

The system can be switched to the in-tree versions of ``ice``, ``i40e``, and ``iavf``
drivers using any of the following two mechanisms.

-   Before installation, specify the ``out_of_tree_drivers`` service parameter
    to none in the deployment config file.

    .. code-block:: none

        serviceParameters:
                - service: platform
                  section: kernel
                  paramname: out_of_tree_drivers
                  paramvalue: "none"

-   After installation, modify the ``out_of_tree_drivers`` service parameter to
    none and apply the configuration using the steps mentioned in :ref:`apply-driver-configuration-0342c0220631`.

    .. code-block:: none

        ~(keystone_admin)$ system service-parameter-modify platform kernel out_of_tree_drivers=none


Switch to Out-of-tree Driver Versions
-------------------------------------

To use the out-of-tree drivers, set the out-of-tree drivers you want to load in
the ``out_of_tree_drivers`` service parameter and apply the configuration using
the steps mentioned in :ref:`apply-driver-configuration-0342c0220631`. You can set all
the supported drivers, or only specific ones.

-   Set all out-of-tree drivers:

    .. code-block:: none

        ~(keystone_admin)$ system service-parameter-modify platform kernel out_of_tree_drivers=ice,i40e,iavf

-   Set specified out-of-tree drivers:

    To set only ``ice`` and ``i40e``, but not ``iavf`` (in this case, the iavf is in the in-tree driver):

    .. code-block:: none

        ~(keystone_admin)$ system service-parameter-modify platform kernel out_of_tree_drivers=ice,i40e

    To set ``iavf`` and ``ice``, but not ``i40e``:

    .. code-block:: none

        ~(keystone_admin)$ system service-parameter-modify platform kernel out_of_tree_drivers=ice,iavf

    You can set any permutation or combination of ``ice``, ``i40e``, and ``iavf``
    in the ``out_of_tree_drivers`` service parameter. Any driver not explicitly set
    using the ``out_of_tree_drivers`` service parameter will use its in-tree version.

.. _apply-driver-configuration-0342c0220631:

Apply Driver Configuration
--------------------------

To apply the service parameter change, all the hosts need to be locked and unlocked.
Use the following commands for each host depending on the deployed configuration:

For |AIO-SX| deployments:

.. code-block:: none

    ~(keystone_admin)$ system host-lock controller-0
    ~(keystone_admin)$ system host-unlock controller-0

For |AIO-DX| and standards deployments, after ``controller-1`` is locked/unlocked,
swact ``controller-0`` to make ``controller-1`` the active node. The next set of
commands are executed on the ``controller-0`` node:

.. code-block:: none

    ~(keystone_admin)$ system host-lock controller-1
    ~(keystone_admin)$ system host-unlock controller-1
    ~(keystone_admin)$ system host-swact controller-0

On ``controller-1``, after ``controller-0`` is locked/unlocked swact ``controller-1`` to go
back to ``controller-0`` as the active node. The next set of commands are executed
on the ``controller-1`` node:

.. code-block:: none

    ~(keystone_admin)$ system host-lock controller-0
    ~(keystone_admin)$ system host-unlock controller-0
    ~(keystone_admin)$ system host-swact controller-1

All the remaining nodes in the deployment need to be locked/unlocked, one at a time.
Use the following commands (replace ``worker-0`` with the name of the node being
locked/unlocked):

.. code-block:: none

    ~(keystone_admin)$ system host-lock worker-0
    ~(keystone_admin)$ system host-unlock worker-0

.. note::

    You must wait for the node to enter the locked state prior to unlocking.

To verify the current Intel driver version use ``ethtool -i`` on the desired
Intel network interface. For example:

.. code-block:: none

    ~(keystone_admin)$ ethtool -i ens785f0 | egrep '^(driver|version):'
    driver: i40e
    version: 6.6.40-stx.3


Backup and Restore
------------------

If a backup and restore is performed, after unlocking the host during a restore
operation, the system is configured to the driver version at the time of the backup.

.. only:: partner

    .. include:: /_includes/intel-driver-version-c6e3fa384ff7.rest
        :start-after: intel-driver-version-begin
        :end-before: intel-driver-version-end
