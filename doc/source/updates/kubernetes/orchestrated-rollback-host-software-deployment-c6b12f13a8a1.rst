.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _orchestrated-rollback-host-software-deployment-c6b12f13a8a1:

==============================================
Orchestrated Rollback Host Software Deployment
==============================================

For an orchestrated software deployment, the
:ref:`orchestrated-deployment-host-software-deployment-d234754c7d20` procedure
can be aborted and rolled back as long as the postrequisite :command:`software
deploy delete` has not been done either manually or via orchestrated upgrade.
After the postrequisite :command:`software deploy delete` step, an orchestrated
abort and rollback of the deployment is not possible.

.. note::

    This section also covers the orchestrated abort and rollback of an updated
    release deployment.

.. note::

    Currently, software deployments cannot be rolled back after the activation
    stage. Orchestrated rollback is supported only for |AIO-SX|.

.. rubric:: |prereq|

You have done either of the following:

- Aborted an :ref:`orchestrated-deployment-host-software-deployment-d234754c7d20` procedure.

  .. code-block::

      ~(keystone_admin)]$ sw-manager sw-deploy-strategy abort
      Strategy Software Deploy Strategy:
        strategy-uuid:                          63f48dfc-f833-479b-b597-d11f9219baf5
        release-id:                             <software-release-id>
        controller-apply-type:                  serial
        storage-apply-type:                     serial
        worker-apply-type:                      serial
        default-instance-action:                stop-start
        alarm-restrictions:                     strict
        current-phase:                          apply
        current-phase-completion:               7%
        state:                                  aborting
        inprogress:                             true

  and waited for the orchestration to finish its current stage/step and then
  abort.

  .. code-block::

      ~(keystone_admin)]$ sw-manager sw-deploy-strategy show
      Strategy Software Deploy Strategy:
        strategy-uuid:                          63f48dfc-f833-479b-b597-d11f9219baf5
        release-id:                             <software-release-id>
        controller-apply-type:                  serial
        storage-apply-type:                     serial
        worker-apply-type:                      serial
        default-instance-action:                stop-start
        alarm-restrictions:                     strict
        current-phase:                          abort
        current-phase-completion:               100%
        state:                                  aborted
        apply-result:                           aborted
        apply-reason:
        abort-result:                           success
        abort-reason:

- Or completed an :ref:`orchestrated-deployment-host-software-deployment-d234754c7d20` procedure:

  .. code-block::

      ~(keystone_admin)]$ sw-manager sw-deploy-strategy show
      Strategy Software Deploy Strategy:
        strategy-uuid:                          6282f049-bb9e-46f0-9ca8-97bf626884e0
        release-id:                             <software-release-id>
        controller-apply-type:                  serial
        torage-apply-type:                     serial
        worker-apply-type:                      serial
        default-instance-action:                stop-start
        alarm-restrictions:                     strict
        current-phase:                          apply
        current-phase-completion:               100%
        state:                                  applied
        apply-result:                           success
        apply-reason:

But, you have not executed the postrequisite :command:`software deploy delete` step.

.. rubric:: |proc|

#. Delete any old software deployment strategy.

   .. code-block::

       ~(keystone_admin)]$ sw-manager sw-deploy-strategy delete
       Strategy deleted

#. Create a software deployment rollback orchestration strategy with desired
   policies to abort and rollback the current software deployment.

   .. code-block::

       ~(keystone_admin)]$ sw-manager sw-deploy-strategy create [--controller-apply-type {serial,ignore}]
                                                                 [--storage-apply-type {serial,parallel,ignore}]
                                                                 [--worker-apply-type {serial,parallel,ignore}]
                                                                 [--max-parallel-worker-hosts {2,3,4,5,6,7,8,9,10}]
                                                                 [--instance-action {stop-start,migrate}]
                                                                 [--alarm-restrictions {strict,relaxed}]
                                                                 --rollback

       strategy-uuid:                          5435e049-7002-4403-acfb-7886f6da14af
       release-id:                             <software-release-id>
       controller-apply-type:                  serial
       storage-apply-type:                     serial
       worker-apply-type:                      serial
       default-instance-action:                migrate
       alarm-restrictions:                     strict
       current-phase:                          build
       current-phase-completion:               0%
       state:                                  building
       inprogress:                             true

   where,

   ``--rollback``
       Specifies that this is a software deployment rollback orchestration
       strategy for the current software deployment.

   ``[--controller-apply-type {serial,ignore}]``
       (Optional) Specifies whether software should be deployed to the
       controller hosts in serial or ignored. The default is serial.
       ``ignore`` should only be used when re-creating and applying a strategy
       after an abort or failure.

   ``[--storage-apply-type {serial,parallel,ignore}]``
       (Optional) Specifies whether software should be deployed to the storage
       hosts in serial, in parallel, or ignored. The default is serial.
       Software is deployed to the storage hosts in parallel by software
       deploying a storage host from each storage redundancy group at the same
       time. ``ignore`` should only be used when recreating and applying a
       strategy after an abort or failure.

       .. note::

           If parallel apply for storage is used, it will be automatically
           replaced with the serial apply for ``--storage-apply-type``.

   ``[--worker-apply-type {serial,parallel,ignore}]``
       (Optional) Specifies whether software should be deployed to the worker
       hosts in serial, in parallel, or ignored. The default is serial. The
       number of worker hosts that are software deployed in parallel is
       specified by ``[--max-parallel-worker-hosts {2,3,4,5,6,7,8,9,10}]``.
       The default is 2. ``ignore`` should only be used when recreating and
       applying a strategy after an abort or failure.

   ``[--max-parallel-worker-hosts {2,3,4,5,6,7,8,9,10}]``
       Specifies the number of worker hosts that are software deployed in
       parallel specified by ``[--max-parallel-worker-hosts
       {2,3,4,5,6,7,8,9,10}]``. The default is 2.

   ``[--instance-action {stop-start,migrate}]``
       This option only applies to OpenStack |VM| hosted guests. It specifies
       the action done to hosted OpenStack |VMs| on a worker host (|AIO|
       controller) prior to deploying the new software to the host. The default
       is ``stop-start``.

       - ``stop-start``
         Before deploying the software release to the host, all hosted
         OpenStack |VMs| are stopped or shutdown.

         After deploying the software release to the host, all hosted
         OpenStack |VMs| are restarted.

       - ``migrate``
         Before deploying the software release to the host, all hosted
         OpenStack |VMs| are migrated to another host that is capable of hosting
         the hosted OpenStack |VM| and that is not part of the current stage.

         Hosts whose software is already updated are preferred over
         hosts whose software is not yet updated. Live migration is
         attempted first, if not available for the OpenStack |VM|, then cold
         migration is performed.

   ``[--alarm-restrictions {strict,relaxed}]``
       This option lets you determine how to handle alarm restrictions based on
       the management affecting statuses of any existing alarms, which takes
       into account the alarm type as well as the alarm's current severity.

       The default is strict.

       If set to relaxed, orchestration will be allowed to proceed if there are
       no management affecting alarms present.

       Performing management actions without specifically relaxing the alarm
       checks will still fail if there are any alarms present in the system
       (except for a small list of basic alarms for the orchestration actions
       such as an upgrade operation in progress alarm not impeding upgrade
       orchestration).

       You can use the CLI command :command:`fm alarm-list --mgmt_affecting` to
       view the alarms that are management affecting.

       - ``Strict``

         Maintains alarm restrictions.

       - ``Relaxed``

         Relaxes the usual alarm restrictions and allows the action to proceed
         if there are no alarms present in the system with a severity equal to
         or greater than its management affecting severity.

#. Apply the software deployment rollback orchestration:

   .. code-block::

       ~(keystone_admin)]$ sw-manager sw-deploy-strategy apply
       Strategy Software Deploy Strategy:
         strategy-uuid:                          52873771-fc1a-48cd-b322-ab921d34d01c
         release-id:                             <software-release-id>
         controller-apply-type:                  serial
         storage-apply-type:                     serial
         worker-apply-type:                      serial
         default-instance-action:                stop-start
         alarm-restrictions:                     strict
         current-phase:                          apply
         current-phase-completion:               0%
         state:                                  applying
         inprogress:                             true

#. Wait for all the steps of all stages of the software deployment rollback
   orchestration strategy to complete.

   .. code-block::

       ~(keystone_admin)]$ sw-manager sw-deploy-strategy show
       Strategy Software Deploy Strategy:
         strategy-uuid:                          6282f049-bb9e-46f0-9ca8-97bf626884e0
         release-id:                             <software-release-id>
         controller-apply-type:                  serial
         storage-apply-type:                     serial
         worker-apply-type:                      serial
         default-instance-action:                stop-start
         alarm-restrictions:                     strict
         current-phase:                          apply
         current-phase-completion:               100%
         state:                                  done
         apply-result:                           success
         apply-reason:

#. Delete the completed software deployment rollback orchestration strategy.

   .. code-block::

       ~(keystone_admin)]$ sw-manager sw-deploy-strategy delete
       Strategy deleted.
