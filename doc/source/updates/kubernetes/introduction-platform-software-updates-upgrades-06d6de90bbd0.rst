.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _introduction-platform-software-updates-upgrades-06d6de90bbd0:

============
Introduction
============

|prod| software management enables you to upversion your |prod| software to a
new Patch Release or a new Major Release.

**Major Releases** (versioned as 'X.Y.0', for example, starlingx-10.0.0)

-  deliver new and enhanced feature content,

-  are packaged and delivered as Install ISOs containing all software packages.

**Patch Releases** (versioned as 'X.Y.P', for example, starlingx-10.0.3)

-  deliver fixes for known bugs and CVE vulnerabilities,

-  are packaged and delivered as patch archive files,

   -  containing only new and changed software packages,

   -  with meta data to indicate dependencies on previously released Patch
      Releases or the associated Major Release.

Both Major Releases and Patch Releases are **cryptographically signed** to
ensure integrity and authenticity, and the StarlingX REST APIs, CLIs, and GUI
validate the signature of software releases before loading them into the
system.

Both Major Releases and Patch Releases can be deployed using either:

-  :ref:`manual-host-software-deployment-ee17ec6f71a4`

or

-  :ref:`orchestrated-deployment-host-software-deployment-d234754c7d20`

Both manual and orchestrated procedures use a '**rolling deployment**'
procedure for deploying the software of the new release. |prod| hosts are
updated/upgraded one (or more) at a time such that |prod| can continue to
provide hosting services to its hosted applications on other |prod| hosts.

Specifically:

-  Controllers are updated/upgraded one at a time,

-  then Storage hosts are updated/upgraded one (or more) at a time,
   respecting Storage host redundancy,

-  then Worker hosts are updated/upgraded one (or more) at a time.

For a Major Release deployment, the upgrading of a new Major Release will
result in a **reboot of each host**, as the host is upgraded with the new Major
Release, in order for the host to boot into the new software's root filesystem.

For a Patch Release deployment, depending on the type of software changes in
the Patch Release, one of two deployment modes will be used:

In-Service
   in this mode, the upversioning to a new Patch Release will only result in
   the install of new software and the restart of the required services, as
   each host is upversioned with the new Patch Release.

Reboot-Required (RR)
   in this mode, the upversioning to a new Patch Release will result in a
   reboot of each host, as the host is upversioned to the new Patch Release.

For either a Patch Release or a Major Release, an **Abort and Rollback** of the
active software deployment is supported.  The deployment of a Release can be
aborted and rolled back at any step of the deployment process, as long as the
active deployment has not been both completed and deleted.

For a Patch Release only, a **removal or un-deployment of a release** is
supported. One or more Patch Releases can be removed/un-deployed by deploying a
previous Patch Release.