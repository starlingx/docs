.. _index-updates-kub-03d4d10fa0be:

.. include:: /_includes/toc-title-updates-kub.rest

.. only:: partner

   .. include:: /_includes/upgrade-note.rest
      :start-after: important-begin
      :end-before: important-end

.. only:: partner

   .. include:: /updates/index-updates-e3b970bb69ce.rst
      :start-after: kub-begin
      :end-before: kub-end


------------
Introduction
------------

.. toctree::
   :maxdepth: 1

   introduction-platform-software-updates-upgrades-06d6de90bbd0

------------------------
Host software deployment
------------------------

.. toctree::
   :maxdepth: 1

   manual-host-software-deployment-ee17ec6f71a4
   manual-rollback-host-software-deployment-9295ce1e6e29
   manual-removal-host-software-deployment-24f47e80e518
   orchestrated-deployment-host-software-deployment-d234754c7d20
   orchestrated-rollback-host-software-deployment-c6b12f13a8a1
   orchestrated-removal-host-software-deployment-3f542895daf8

--------------------------
Kubernetes Version Upgrade
--------------------------

******
Manual
******

.. toctree::
   :maxdepth: 1

   manual-kubernetes-components-upgrade
   manual-kubernetes-multi-version-upgrade-in-aio-sx-13e05ba19840

************
Orchestrated
************

.. toctree::
   :maxdepth: 1

   about-kubernetes-orchestrated-upgrades
   the-kubernetes-update-orchestration-process
   configuring-kubernetes-update-orchestration
   configuring-kubernetes-multi-version-upgrade-orchestration-aio-b0b59a346466
   handling-kubernetes-update-orchestration-failures

--------
Appendix
--------

.. toctree::
   :maxdepth: 1

**********
Deprecated
**********

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
N3000 FPGA Firmware Update Orchestration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 1

   overview-of-firmware-update-orchestration
   the-firmware-update-orchestration-process
   firmware-update-operations-requiring-manual-migration
   configure-firmware-update-orchestration
   firmware-update-orchestration-using-the-cli
   handle-firmware-update-orchestration-failures


------------------------------
Appendix - Kubernetes Platform
------------------------------

.. toctree::
   :maxdepth: 1

   patch-release-deployment-before-bootstrap-and-commissioning-of-7d0a97144db8

------------------------------------------------------
N3000 FPGA Firmware Update Orchestration is deprecated
------------------------------------------------------

.. toctree::
   :maxdepth: 1

   overview-of-firmware-update-orchestration
   the-firmware-update-orchestration-process
   firmware-update-operations-requiring-manual-migration
   configure-firmware-update-orchestration
   firmware-update-orchestration-using-the-cli
   handle-firmware-update-orchestration-failures
