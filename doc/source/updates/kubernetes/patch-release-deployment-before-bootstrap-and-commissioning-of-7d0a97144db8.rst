.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _patch-release-deployment-before-bootstrap-and-commissioning-of-7d0a97144db8:

=======================================================================================
Deployment of Patch Releases before Bootstrap and Commissioning of initial Installation
=======================================================================================

.. rubric:: |context|

When initially installing a |prod-long| system, occasionally it is required
to deploy the latest available patch release for the major release
being deployed.

The patch releases should be deployed using the procedure below.

-  *AFTER* the initial install of software on the first host
   (controller-0), and

-  *BEFORE* running the Ansible Bootstrap Playbook and configuring and
   installing software on any other hosts.

This ensures that:

-  The software on controller-0, and all other hosts, is up to date when
   the cluster first becomes active.

-  You reduce installation time by avoiding having to patch the system
   right after an out-of-date software installation is completed.

.. rubric:: |prereq|

-  The software patch releases that are required to be deployed are available
   on a USB flash drive, or from a server reachable by controller-0.

-  You are at the beginning of a |prod-long| system installation. Specifically,

   -  You have used the |prod-long| bootable ISO image (for a major
      release) to install the |prod-long| software on controller-0.

   -  You have logged into the console port as user sysadmin, **BUT you have NOT
      bootstrapped the system yet.**

.. rubric:: |proc|

#. Upload the patch releases.

   Upload the updates from the USB flash drive using the :command:`software
   upload <patch-release.patch file>` command.

   .. note::

       ``sudo`` is required for authentication due to |prod|
       authentication not currently running.

       .. code-block::

          $ sudo software upload --local <patch-release>.patch**
          <release-id> is now uploaded
          +-------------------------------+-------------------+
          | Uploaded File                  | Release          |
          +-------------------------------+-------------------+
          | <new-release>.patch            | <new-release-id> |
          +-------------------------------+-------------------+

   .. note:: Repeat the above command for all patch releases to be deployed.

#. If you copied the patch release file(s) to /home/sysadmin/, then
   remove these files.

   After the patch release files are uploaded to the system, the
   original files are no longer required.

   You should delete them to ensure enough disk space to complete the
   installation.

   .. Caution::

       If the original files are not deleted before the updates are applied,
       the installation may fail due to a full disk.

#. Deploy the patch releases.

   .. code-block::

      $ sudo software deploy start
      <highest-patch-release-id-being-deployed>

      $ sudo software deploy localhost

      Patch Release deployment complete.
      Please reboot before continuing.

#. Reboot controller-0.

   You must reboot the controller to ensure that it is running with
   the software fully updated.

   .. code-block::

      $ sudo reboot

.. rubric:: |postreq|

Continue with bootstrapping and configuring your system.

.. rubric:: |result|

Once all hosts in the cluster are initialized, they will all be running
fully patched software.
