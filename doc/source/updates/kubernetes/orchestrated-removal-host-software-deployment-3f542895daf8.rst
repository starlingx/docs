.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _orchestrated-removal-host-software-deployment-3f542895daf8:

=============================================
Orchestrated Removal Host Software Deployment
=============================================

A fully deployed patch release can be removed (or un-deployed) by using the
:ref:`orchestrated-deployment-host-software-deployment-d234754c7d20` procedure and deploying a
previous patch release or the major release.

.. note::

    A fully deployed major release cannot be removed (or un-deployed).

For example, the following shows the current software releases deployed:

.. code-block::

    ~(keystone_admin)]$ software list
     +--------------------------+-------+-----------+
     | Release                  | RR    |   State   |
     +--------------------------+-------+-----------+
     | starlingx-10.0.0         | True  | deployed  |
     | starlingx-10.0.1         | False | deployed  |
     | starlingx-10.0.2         | True  | deployed  |
     | starlingx-10.0.3         | False | deployed  |
     +--------------------------+-------+-----------+

To remove patch releases starlingx-10.0.3 and starlingx-10.0.2, follow the
:ref:`orchestrated-deployment-host-software-deployment-d234754c7d20` procedure,
and deploy (or go back to) the starlingx-10.0.1 software release in step :ref:`<1 <orchestrated-deployment-host-software-deployment-d234754c7d20-step>`.

.. code-block::

    ~(keystone_admin)]$ sw-manager sw-deploy-strategy create [--controller-apply-type {serial,ignore}]
                                                                [--storage-apply-type {serial,parallel,ignore}]
                                                                [--worker-apply-type {serial,parallel,ignore}]
                                                                [--max-parallel-worker-hosts {2,3,4,5,6,7,8,9,10}]
                                                                [--instance-action {stop-start,migrate}]
                                                                [--alarm-restrictions {strict,relaxed}]
                                                                starlingx-10.0.1

    strategy-uuid:                          5435e049-7002-4403-acfb-7886f6da14af
    release-id:                             starlingx-10.0.1
    controller-apply-type:                  serial
    storage-apply-type:                     serial
    worker-apply-type:                      serial
    default-instance-action:                stop-start
    alarm-restrictions:                     strict
    current-phase:                          build
    current-phase-completion:               0%
    state:                                  building
    inprogress:                             true

.. code-block::

    ~(keystone_admin)]$ sw-manager sw-deploy-strategy show
    Strategy Software Deploy Strategy:
      strategy-uuid:                          6282f049-bb9e-46f0-9ca8-97bf626884e0
      release-id:                             starlingx-10.0.1
      controller-apply-type:                  serial
      storage-apply-type:                     serial
      worker-apply-type:                      serial
      default-instance-action:                stop-start
      alarm-restrictions:                     strict
      current-phase:                          build
      current-phase-completion:               100%
      state:                                  ready-to-apply
      build-result:                           success
      build-reason:

.. note::

    If parallel apply for storage is used, it will be automatically replaced
    with the serial apply for ``--storage-apply-type``.

Apply the software orchestration deployment strategy.

.. code-block::

    ~(keystone_admin)]$ sw-manager sw-deploy-strategy apply
    Strategy Software Deploy Strategy:
    strategy-uuid: 92e69661-2ef2-4d7d-baf2-272b8886d95a
    release-id: starlingx-10.0.1
    controller-apply-type: serial
    storage-apply-type: serial
    worker-apply-type: serial
    default-instance-action: stop-start
    alarm-restrictions: relaxed
    current-phase: apply
    current-phase-completion: 0%
    state: applying
    inprogress: true

On completion of the
:ref:`orchestrated-deployment-host-software-deployment-d234754c7d20` procedure,
run the following command:

.. code-block::

    ~(keystone_admin)]$ software list
    +--------------------------+-------+-----------+
    | Release                  | RR    |   State   |
    +--------------------------+-------+-----------+
    | starlingx-10.0.0         | True  | deployed  |
    | starlingx-10.0.1         | False | deployed  |
    | starlingx-10.0.2         | True  | available |
    | starlingx-10.0.3         | False | available |
    +--------------------------+-------+-----------+

If the removed patches are no longer required on the system, you can delete them.

.. code-block::

    ~(keystone_admin)]$ software delete starlingx-10.0.3
    starlingx-10.0.3 has been deleted.

.. code-block::

    ~(keystone_admin)]$ software delete starlingx-10.0.2
    starlingx-10.0.2 has been deleted.

.. code-block::

    ~(keystone_admin)]$ software list
    +--------------------------+-------+-----------+
    | Release                  | RR    |   State   |
    +--------------------------+-------+-----------+
    | starlingx-10.0.0         | True  | deployed  |
    | starlingx-10.0.1         | False | deployed  |
    +--------------------------+-------+-----------+

