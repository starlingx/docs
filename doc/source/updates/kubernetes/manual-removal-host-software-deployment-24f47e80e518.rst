.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _manual-removal-host-software-deployment-24f47e80e518:

=======================================
Manual Removal Host Software Deployment
=======================================

A fully deployed patch release can be removed (un-deployed) by using the
the :ref:`manual-host-software-deployment-ee17ec6f71a4` procedure and deploying a
previous patch release or the major release.

.. note::

    A fully deployed major release cannot be removed (un-deployed).

For example, the following shows the current software releases deployed:

.. code-block::

    ~(keystone_admin)]$ software list
    +--------------------------+-------+-----------+
    | Release                  | RR    |    State  |
    +--------------------------+-------+-----------+
    | starlingx-10.0.0         | True  | deployed  |
    | starlingx-10.0.1         | False | deployed  |
    | starlingx-10.0.2         | True  | deployed  |
    | starlingx-10.0.3         | False | deployed  |
    +--------------------------+-------+-----------+

To remove patch releases `starlingx-10.0.3` and `starlingx-10.0.2`, follow the
:ref:`manual-host-software-deployment-ee17ec6f71a4` procedure, and deploy (or
go back to) the `starlingx-10.0.1` software release in the software deployment
start step.

.. code-block::

    ~(keystone_admin)]$ software deploy start starlingx-10.0.1
    Deployment for starlingx-10.0.1 started

.. code-block::

    ~(keystone_admin)]$ software deploy show
    +--------------+------------------+------+-------+
    | From Release | To Release       |   RR | State |
    +--------------+------------------+------+-------+
    | 10.0.3       | 10.0.1           | True | start |
    +--------------+------------------+------+-------+

On the completion of the :ref:`manual-host-software-deployment-ee17ec6f71a4`
procedure, run the following command:

.. code-block::

    ~(keystone_admin)]$ software list
    +--------------------------+-------+-----------+
    | Release                  | RR    |    State  |
    +--------------------------+-------+-----------+
    | starlingx-10.0.0         | True  | deployed  |
    | starlingx-10.0.1         | False | deployed  |
    | starlingx-10.0.2         | True  | available |
    | starlingx-10.0.3         | False | available |
    +--------------------------+-------+-----------+

If the removed patches are no longer required on the system, you can delete them.

.. code-block::

    ~(keystone_admin)]$ software delete starlingx-10.0.3
    starlingx-10.0.3 has been deleted.

.. code-block::

    ~(keystone_admin)]$ software delete starlingx-10.0.2
    starlingx-10.0.2 has been deleted.

.. code-block::

    ~(keystone_admin)]$ software list
    +--------------------------+-------+-----------+
    | Release                  | RR    |    State  |
    +--------------------------+-------+-----------+
    | starlingx-10.0.0         | True  | deployed  |
    | starlingx-10.0.1         | False | deployed  |
    +--------------------------+-------+-----------+
