
.. dqn1590002648435
.. _software-updates-and-upgrades-overview:

========
Overview
========

The ``system application-update -n |prefix|-openstack -v <new-app-version>``
command is used for:

- up-versioning to a new patch version of the system application that contains corrective content (bug fixes) or

- up-versioning to a new major version of the system application that contains new feature content and possibly a new SLURP release of OpenStack itself (e.g. Antelope to Caracal).

The system application-update -n |prefix|-openstack effectively performs a
helm upgrade of one or more of the OpenStack Helm chart releases within the
FluxCD Application. One or all of the containerized OpenStack deployments will
be updated according to their deployment specification.

.. note::
    Compute nodes do not need to be reset, and hosted application |VMs| are not impacted.

