.. _index-intro-27197f27ad41:

============
Introduction
============

StarlingX is a fully integrated edge cloud software stack that provides
everything needed to deploy an edge cloud on one, two, or up to 100 servers.

Key features of StarlingX include:

* Provided as a single, easy to install package that includes an operating system,
  storage and networking components, and all the cloud infrastructure needed to
  run edge workloads.
* Optimized software that meets edge application requirements.
* Designed with pre-defined configurations to meet a variety of edge cloud deployment
  needs.
* Tested and released as a complete stack, ensuring compatibility among open
  source components.
* Included fault management and service management capabilities, which provide
  high availability for user applications.
* Optimized by the community for security, ultra-low latency, extremely high
  service uptime, and streamlined operation.

Download the StarlingX ISO image from the
`StarlingX mirror <https://mirror.starlingx.windriver.com/mirror/starlingx/>`_.

Learn more about StarlingX:

.. toctree::
   :maxdepth: 1

   functional_overview
   deploy_config_overview
   deploy_options
   fault-and-performance-management-940c6f6b3f6e
   terms
   consuming
   StarlingX Project Overview (PDF) <https://www.starlingx.io/collateral/StarlingX_Onboarding_Deck_for_Web.pdf>

.. _stx_projects:

--------
Projects
--------

StarlingX contains multiple sub-projects that include additional edge cloud
support services and clients. API documentation and release notes for each
project are found on the specific project page:

* `Bare Metal </metal/index.html>`__
* `Clients </clients/index.html>`__
* `Config </config/index.html>`__
* `Distributed Cloud </distcloud/index.html>`__
* `Distributed Cloud Client </distcloud-client/index.html>`__
* `Fault Management </fault/index.html>`__
* `High Availability </ha/index.html>`__
* `Horizon Plugin (GUI) </gui/index.html>`__
* `Integration </integ/index.html>`__
* `NFV </nfv/index.html>`__
* `Software Updates </update/index.html>`__
* `Upstream </upstream/index.html>`__

Supporting projects and repositories:

* `Tools </tools/index.html>`__

For additional information about project teams, refer to the
`StarlingX wiki <https://wiki.openstack.org/wiki/StarlingX>`_.

------------------------------
New features in StarlingX 10.0
------------------------------

.. include:: /releasenotes/index.rst
   :start-after: start-new-features-r10
   :end-before: end-new-features-r10

-----------------------------
New features in StarlingX 9.0
-----------------------------

**See**: https://docs.starlingx.io/r/stx.9.0/releasenotes/index.html#release-notes

-----------------------------
New features in StarlingX 8.0
-----------------------------

**See**: https://docs.starlingx.io/r/stx.8.0/releasenotes/index.html#release-notes

-----------------------------
New features in StarlingX 7.0
-----------------------------

**See**: https://docs.starlingx.io/r/stx.7.0/releasenotes/index.html#new-features-and-enhancements

