.. _website_contribute_guide:

===================================
StarlingX Website Contributor Guide
===================================

This section describes the guidelines for contributing changes and updates
to the StarlingX website.

.. contents::
   :local:
   :depth: 1

Unlike the rest of the StarlingX artifacts, the StarlingX website code and
content is stored in a `GitHub repository
<https://github.com/StarlingXWeb/starlingx-website>`_.

If you found any typos, outdated content or have further thoughts and ideas
what to add or update on the website, [submit an issue](https://github.com/StarlingXWeb/starlingx-website/issues)
or [submit a pull request](https://github.com/StarlingXWeb/starlingx-website/pulls).

If you need help with either submitting an issue or creating your PR to
propose fixes or updates to the website on GitHub, please reach out to
Ildiko Vancsa (ildiko@openinfra.dev) for help.
