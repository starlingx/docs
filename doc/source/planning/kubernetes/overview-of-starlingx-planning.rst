
.. sii1465846708497
.. _overview-of-starlingx-planning:

===================================================
Overview of Installation and Configuration Planning
===================================================

Fully planning your |prod-long| installation and configuration helps to
expedite the process and ensure that you have everything required.

Planning helps ensure that the requirements of your containers, and the
requirements of your cloud administration and operations teams can be met. It
ensures proper integration of a |prod| into the target data center or telecom
office, and helps you plan up front for future cloud growth.

.. xbooklink This planning guide assumes that you have read both the |intro-doc|: :ref:`<introduction-to-starlingx>` and the |deploy-doc|: :ref:`<deployment-options>` guides in order to understand general concepts and assist you in choosing a particular deployment configuration.

The |planning-doc| guide is intended to help you plan for your installation. It
discusses detailed planning topics for the following areas:


.. _overview-of-starlingx-planning-ul-v2m-t5h-hw:

-   Network Planning

-   Storage Planning

-   Node Installation Planning

-   Node Resource Planning
