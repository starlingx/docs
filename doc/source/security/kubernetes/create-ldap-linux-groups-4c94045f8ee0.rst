
.. vaq1552681912484
.. _create-ldap-linux-groups-4c94045f8ee0:

==========================
Create LDAP Linux Groups
==========================

|prod| offers |LDAP| commands to create and manage |LDAP| Linux groups as part of the  `ldapscripts` library.

.. rubric:: |context|

.. note::
    For security reasons, it is recommended that ONLY admin level users be
    allowed to |SSH| to the nodes of the |prod|. Non-admin level users should
    strictly use remote CLIs or remote web GUIs.

The main commands that manage |LDAP| Linux groups are: ``ldapaddgroup`` ,
``ldapaddusertogroup``, ``ldapdeletegroup``, ``ldapdeleteuserfromgroup``.

To list all the commands in the  `ldapscripts` library, the following command
can be used:

.. code-block:: none

    sysadmin@controller-0:~$ ls /usr/sbin/ldap*
    /usr/sbin/ldapaddgroup             /usr/sbin/ldapid
    /usr/sbin/ldapaddmachine           /usr/sbin/ldapinit
    /usr/sbin/ldapaddsudo              /usr/sbin/ldapmodifygroup
    /usr/sbin/ldapadduser              /usr/sbin/ldapmodifymachine
    /usr/sbin/ldapaddusertogroup       /usr/sbin/ldapmodifysudo
    /usr/sbin/ldapdeletegroup          /usr/sbin/ldapmodifyuser
    /usr/sbin/ldapdeletemachine        /usr/sbin/ldaprenamegroup
    /usr/sbin/ldapdeletesudo           /usr/sbin/ldaprenamemachine
    /usr/sbin/ldapdeleteuser           /usr/sbin/ldaprenameuser
    /usr/sbin/ldapdeleteuserfromgroup  /usr/sbin/ldapsetpasswd
    /usr/sbin/ldapfinger               /usr/sbin/ldapsetprimarygroup
    /usr/sbin/ldapgid                  /usr/sbin/ldapusersetup

The |LDAP| commands usage information can be found from man pages or using
the "--help" option. For example, this is the usage information for creating
or adding a |LDAP| Linux group.

.. code-block:: none

    sysadmin@controller-0:~$ ldapaddgroup --help
    Usage : /usr/sbin/ldapaddgroup <groupname> [gid]

    sysadmin@controller-0:~$ man ldapaddgroup
    ldapaddgroup(1)             General Commands Manual            
    ldapaddgroup(1)
    NAME
           ldapaddgroup - adds a POSIX group entry to LDAP.

    SYNOPSIS
           ldapaddgroup <groupname> [gid]

    OPTIONS
           <groupname>
                  The name of the group to add.
           [gid]  The gid of the group to add. Automatically computed if
           not specified.

|LDAP| Linux group command examples:

Create a group

.. code-block:: none

    $ sudo ldapaddgroup group-test
    Successfully added group group-test to LDAP

Add a user to the group

.. code-block:: none

    $ sudo ldapaddusertogroup user-test group-test
    Successfully added user user-test to group cn=group-test,ou=Group,
    dc=cgcs,dc=local

Delete a user membership from the group

.. code-block:: none

    sysadmin@controller-0:~$ ldapdeleteuserfromgroup --help
    Usage : /usr/sbin/ldapdeleteuserfromgroup <username | dn> <groupname | gid>
    $ sudo ldapdeleteuserfromgroup user-test group-test
    Successfully deleted user user-test from group cn=group-test,ou=Group,
    dc=cgcs,dc=local

Delete a group

.. code-block:: none

    sysadmin@controller-0:~$ ldapdeletegroup --help
    Usage : /usr/sbin/ldapdeletegroup <groupname | gid>
    $ sudo ldapdeletegroup group-test
    Successfully deleted group cn=group-test,ou=Group,dc=cgcs,dc=local
    from LDAP

After the execution of a |LDAP| Linux group command, the command prompt is
displayed.

.. code-block:: none

    controller-0: ~$

