
.. afi1590692698424
.. _centralized-vs-distributed-oidc-auth-setup:

====================================================
Centralized vs Distributed OIDC Authentication Setup
====================================================

In a |prod-dc| configuration, you can configure |OIDC| authentication in a
distributed or centralized setup. For other configurations, like |AIO-SX|,
|AIO-DX| or Standard Cloud, follow the instructions in the distributed setup
documented below.


.. _centralized-vs-distributed-oidc-auth-setup-section-ugc-xr5-wlb:

-----------------
Distributed Setup
-----------------

For a distributed setup, configure the **kube-apiserver** and the
**oidc-auth-apps** independently for each cloud, System Controller, and all
subclouds. The **oidc-auth-apps** runs on each active controller of the setup
and the **kube-apiserver** is configured to point to the local instance of
**oidc-auth-apps**. For more information, see:


.. _centralized-vs-distributed-oidc-auth-setup-ul-gjs-ds5-wlb:

-   Configure Kubernetes for |OIDC| Token Validation


    -   :ref:`Configure Kubernetes for OIDC Token Validation while
        Bootstrapping the System
        <configure-kubernetes-for-oidc-token-validation-while-bootstrapping-the-system>`

        **or**

    -   :ref:`Configure Kubernetes for OIDC Token Validation after
        Bootstrapping the System
        <configure-kubernetes-for-oidc-token-validation-after-bootstrapping-the-system>`


-   :ref:`Configure OIDC Auth Applications <configure-oidc-auth-applications>`


All clouds **oidc-auth-apps** can be configured to communicate to the same
or different authentication servers (Windows Active Directory and/or |LDAP|).
However, each cloud manages |OIDC| tokens individually. A user must login,
authenticate, and get an |OIDC| token for each cloud independently.


.. _centralized-vs-distributed-oidc-auth-setup-section-yqz-yr5-wlb:

-----------------
Centralized Setup
-----------------

For a centralized setup, the **oidc-auth-apps** is configured '**only**' on
the System Controller. The **kube-apiserver** must be configured on all
clouds, System Controller, and all subclouds, to point to the centralized
**oidc-auth-apps** running on the System Controller. In the centralized
setup, a user logs in, authenticates, and gets an |OIDC| token from the
Central System Controller's |OIDC| identity provider, and uses the |OIDC| token
with '**any**' of the subclouds as well as the System Controller cloud.

For a centralized |OIDC| authentication setup, use the following procedure:

.. rubric:: |proc|

#.  Configure the **kube-apiserver** parameters on the System Controller and
    each subcloud either during bootstrapping or by using the **system
    service-parameter-add kubernetes kube_apiserver** command after
    bootstrapping the system, using the System Controller's floating OAM IP
    address as the oidc-issuer-url for all clouds.

    For example,
    ``oidc-issuer-url=https://<central-cloud-floating-ip>:<oidc-auth-apps-dex
    -service-NodePort>/dex`` on the subcloud.

    For more information, see:

    -   :ref:`Configure Kubernetes for OIDC Token Validation while
        Bootstrapping the System
        <configure-kubernetes-for-oidc-token-validation-while-bootstrapping-the-system>`

        **or**

    -   :ref:`Configure Kubernetes for OIDC Token Validation after
        Bootstrapping the System
        <configure-kubernetes-for-oidc-token-validation-after-bootstrapping-the-system>`


#.  Configure the **oidc-auth-apps** only on the System Controller. For more
    information, see :ref:`Configure OIDC Auth Applications
    <configure-oidc-auth-applications>`

    .. note::
        For IPv6 deployments, ensure that the IPv6 OAM floating address is,
        ``https://\[<central-cloud-floating-ip>\]:30556/dex`` (that is, in
        lower case, and wrapped in square brackets).


.. rubric:: |postreq|

For more information on configuring Users, Groups, Authorization, and
**kubectl** for the user and retrieving the token on subclouds, see:


.. _centralized-vs-distributed-oidc-auth-setup-ul-vf3-jnl-vlb:

-   :ref:`Configure Users, Groups, and Authorization <configure-users-groups-and-authorization>`

-   :ref:`Configure Kubernetes Client Access <configure-kubernetes-client-access>`
