.. _partial-disk-transparent-encryption-support-via-software-enc-27a570f3142c:

============================================================================
Partial Disk (Transparent) Encryption Support via Software Encryption (LUKS)
============================================================================

.. rubric:: |context|

A new encrypted filesystem using Linux Unified Key Setup (LUKS) is created
automatically on all hosts to store security-sensitive files. This is mounted
at '/var/luks/stx/luks_fs' and the files kept in '/var/luks/stx/luks_fs/controller'
directory are replicated between the controllers.
