.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _system-administrator-collect-system-information-for-user-8502c985343d:

========================================================================
System Administrator - Collect System Information for Remote User Access
========================================================================

This procedure collects up a variety of data requried for a user to
remotely interface with |prod| system.

The following data needs to be collected:

-  The public certificate of the Root |CA| that signed the certificates of
   the |prod| system.

   -  the remote user needs to update the remote system to trust this certificate.

-  Kubernetes environment data for the |prod| system.

-  |prod| environment data for the |prod| system.

.. rubric:: |proc|

#.   Login to the active controller as a ``Level1SystemAdmin`` system
     administrator, ``joefulladmin`` in this example.

     Use either a local console or |SSH|.

#.   Use ``local_starlingxrc`` to setup |prod| environment variables
     and to setup your keystone user's authentication credentials.

     .. code-block::

        $ source local_starlingxrc
        Enter the password to be used with Keystone user joefulladmin:
        Created file /home/joefulladmin/joefulladmin-openrc

#.   Use ``kubeconfig-setup`` to setup ``KUBECONFIG`` for local environment
     and use ``oidc-auth`` to setup |OIDC|/|LDAP| authentication credentials.

     .. code-block::

        $ kubeconfig-setup
        $ source ~/.profile

        $ oidc-auth
        Using "joefulladmin" as username.
        Password:
        Successful authentication.
        Updated /home/joefulladmin/.kube/config .

#.   Create a directory for storing information for remote users.

     .. code-block::

        $ mkdir ~/stx-remote-access-info

#.   Get public certificate of the Root |CA| that signed the certificates of the
     |prod| system.

     .. code-block::

        $ kubectl get secret system-local-ca -n cert-manager -o=jsonpath='{.data.tls\.crt}' | base64 --decode > ~/stx-remote-access-info/stx.ca.crt

#.   Get the kubernetes environment data for the |prod| system.

     .. code-block::

         $ OAMIP=$(system oam-show | egrep "(oam_ip|oam_floating_ip)" | awk '{print $4}')

         $ touch ~/stx-remote-access-info/kubeconfig
         $ kubectl config --kubeconfig ~/stx-remote-access-info/kubeconfig set-cluster stx-cluster --server=https://${OAMIP}:6443 --embed-certs --certificate-authority=/etc/kubernetes/pki/ca.crt
         $ kubectl config --kubeconfig ~/stx-remote-access-info/kubeconfig set-context YOURUSERNAMEHERE@stx-cluster --cluster=stx-cluster --user YOURUSERNAMEHERE
         $ kubectl config --kubeconfig ~/stx-remote-access-info/kubeconfig use-context YOURUSERNAMEHERE@stx-cluster

#.   Get the |prod| environment data for the |prod| system.

     .. code-block::

         $ OAMIP=$(system oam-show | egrep "(oam_ip|oam_floating_ip)" | awk '{print $4}')
         $ PROJECTNAME="admin"
         $ PROJECTID=`openstack project list | grep ${PROJECTNAME} | awk '{print $2}'`

         $ cat <<EOF > ~/stx-remote-access-info/starlingxrc
         #!/usr/bin/env bash
         #
         export OS_AUTH_URL=https://${OAMIP}:5000/v3
         export OS_PROJECT_ID=${PROJECTID}
         export OS_PROJECT_NAME=${PROJECTNAME}
         export OS_USER_DOMAIN_NAME="Default"
         export OS_PROJECT_DOMAIN_ID="default"
         export OS_PROJECT_DOMAIN_NAME=""
         export OS_USERNAME=YOURUSERNAMEHERE
         echo "Please enter your OpenStack Password for project \$OS_PROJECT_NAME as user \$OS_USERNAME: "
         read -sr OS_PASSWORD_INPUT
         export OS_PASSWORD=\$OS_PASSWORD_INPUT
         export OS_REGION_NAME=${OS_REGION_NAME}
         export OS_INTERFACE=public
         export OS_IDENTITY_API_VERSION=3
         export OS_CACERT=./stx-remote-access-info/stx.ca.crt
         EOF

#.   Package up the following files for a remote user to use when
     setting up his remote access on his system.

     .. code-block::

        $ cd ~
        $ tar cvf stx-remote-access-info.tar ./stx-remote-access-info

.. rubric:: |postreq|

For any user requiring remote access:

-  securely send them the ``stx-remote-access-info.tar`` file.

-  have them follow the procedures for setting up remote access. See :ref:`remote-access-2209661be417`.
