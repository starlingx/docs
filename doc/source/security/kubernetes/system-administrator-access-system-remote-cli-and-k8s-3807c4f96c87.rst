.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _system-administrator-access-system-remote-cli-and-k8s-3807c4f96c87:

==========================================================================
System Administrator - Access System Remote CLI & Kubernetes Remote CLI
==========================================================================

Access your |prod| system through the |prod| remote |CLI| and kubernetes
remote |CLI| on your Linux-based system.

.. rubric:: |prereq|

You need to have a Linux-based system that has configured the |prod| remote |CLI|
and kubernetes remote |CLI|. See section:
:ref:`system-administrator-configure-system-remote-cli-and-7b814d8937df`.

.. rubric:: |proc|

#.   Source the remote client for the |prod| platform.

     .. code-block::

        $ cd ~/remote_cli
        
        $ source ./remote_client_platform.sh

#.   Test the |prod| remote CLI commands.

     .. code-block::

        $ cd ~/remote_cli

        $ system host-list

        $ fm alarm-list

#.   Test kubernetes remote CLI commands.

     .. code-block::

        $ cd ~/remote_cli

        $ oidc-auth -u <LDAP-USERNAME> -p <LDAP-PASSWORD> -c <OAM-FLOATING-IP>

        $ kubectl get all
