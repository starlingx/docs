.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _system-administrator-local-access-using-ssh-linux-shell-and-st-69213db2a936:

============================================================================================
System Administrator - Test Local Access using SSH/Linux Shell and System and Kubernetes CLI
============================================================================================

After installing your first system administrator, with full privileges,
test access to Linux, |prod| and Kubernetes commands and resources.

.. rubric:: |prereq|

- You must have created your first system administrator;

- You need to perform this procedure using the first system administrator.

.. rubric:: |proc|

#.   Login to active controller as the first system administrator,
     ``joefulladmin`` in these examples.

     Use either local console or |SSH|.

     .. note::

      If this is the first time logging in with your Local |LDAP| account, the
      password configured is your username. You will be forced to update your
      password.

#.   Test access to linux commands (admin and non-admin).

     .. code-block::

         # Creating user requires sudo

         $ sudo ldapusersetup -u johnsmith
         Successfully added user johnsmith to LDAP
         Successfully set password for user johnsmith
         Warning : password is reset, user will be asked to change password at login
         Successfully modified user entry uid=johnsmith,ou=People,dc=cgcs,dc=local in LDAP
         Updating password expiry to 90 days
         Successfully modified user entry uid=johnsmith,ou=People,dc=cgcs,dc=local in LDAP
         Updating password expiry to 2 days

         # Listing IP interfaces does not require admin privileges

         $ ip link show
         1: lo: <LOOPBACK,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
            link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
         2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
            link/ether 08:00:27:39:06:4e brd ff:ff:ff:ff:ff:ff
         3: enp0s8: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
            link/ether 08:00:27:38:8b:7c brd ff:ff:ff:ff:ff:ff
         ...

#.   Test access to Kubernetes commands / resources.

     #.   Use ``kubeconfig-setup`` to setup ``KUBECONFIG`` for local environment.

          .. code-block::

             $ kubeconfig-setup
             $ source ~/.profile

     #.   Use ``oidc-auth`` to authenticate via |OIDC|/|LDAP|.

          .. code-block::

            $ oidc-auth
            Using "joefulladmin" as username.
            Password:
            Successful authentication.
            Updated /home/joefulladmin/.kube/config .

     #.   Use ``kubectl`` to test access to kubernetes commands / resources
          (admin and non-admin).

          .. code-block::

            # Displaying anything in 'kube-system' namespace requires 'cluster-admin' privileges
            $ kubectl -n kube-system get secrets
            NAME                        TYPE                    DATA  AGE
            ceph-admin                  Opaque                  2     3d8h
            ceph-pool-kube-cephfs-data  kubernetes.io/cephfs    4     3d8h
            ceph-pool-kube-rbd          kubernetes.io/rbd       2     3d8h


            # Anyone can display resources in 'default' namespace
            $ kubectl -n default get all
            NAME                TYPE       CLUSTER-IP  EXTERNAL-IP  PORT(S)  AGE
            service/kubernetes  ClusterIP  10.96.0.1   <none>       443/TCP  3d9h

#. Test access to |prod| commands / resources.

   #.   Use ``local_starlingxrc`` to setup |prod| environment variables
        and to setup your keystone user's authentication credentials.

        .. code-block::

         $ source local_starlingxrc
         Enter the password to be used with Keystone user joefulladmin:
         Created file /home/joefulladmin/joefulladmin-openrc


   #.   Test keystone commands  (admin and non-admin).

        .. code-block:: none

         # Making changes to the system requires 'admin' role
         $ system modify -l Ottawa

         +----------------------+--------------------------------------+
         | Property             | Value                                |
         +----------------------+--------------------------------------+
         | contact              | None                                 |
         | created_at           | 2024-07-12T10:52:40.609006+00:00     |
         | description          | None                                 |
         | https_enabled        | True                                 |
         | latitude             | None                                 |
         | location             | Ottawa                               |
         | longitude            | None                                 |
         ...

         # Any member of 'admin' project  can display system parameters
         $ system host-if-list controller-0

         +--------------------------------------+--------+----------+----------+---------+------------+----------+-------------+------------+
         | uuid                                 | name   | class    | type     | vlan id | ports      | uses i/f | used by i/f | attributes |
         +--------------------------------------+--------+----------+----------+---------+------------+----------+-------------+------------+
         | 287eca5a-8721-4422-b73a-bf24805eac4c | enp0s3 | platform | ethernet | None    | ['enp0s3'] | []       | []          |  MTU=1500  |
         | 325c32b9-fe40-4900-a0ff-59062190ce80 | lo     | platform | virtual  | None    | []         | []       | []           | MTU=1500  |
         +--------------------------------------+--------+----------+----------+---------+------------+----------+-------------+------------+

.. rubric:: |postreq|

Continue to :ref:`Create other System Administrators <create-other-system-administrators-97b99bb94430>`.
