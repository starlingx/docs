.. _index-security-kub-81153c1254c3:

.. include:: /_includes/toc-title-security-kub.rest

.. only:: partner

   .. include:: /security/index-security-84d0d8aa401b.rst
      :start-after: kub-begin
      :end-before: kub-end

****************
UEFI Secure Boot
****************

.. toctree::
   :maxdepth: 1

   overview-of-uefi-secure-boot
   use-uefi-secure-boot

*******************
Firewall Management
*******************

.. toctree::
   :maxdepth: 1

   security-default-firewall-rules
   security-firewall-options

**********************
Certificate Management
**********************

.. toctree::
   :maxdepth: 2

   https-access-overview
   utility-script-to-display-certificates
   etcd-certificates-c1fc943e4a9c
   kubernetes-certificates-f4196d7cae9c
   kubernetes-root-ca-certificate
   update-renew-kubernetes-certificates-52b00bd0bdae
   manual-kubernetes-root-ca-certificate-update-8e9df2cd7fb9
   kubernetes-root-ca-certificate-update-cloud-orchestration-a627f9d02d6d
   system-local-ca-issuer-9196c5794834
   local-ldap-certificates-4e1df1e39341
   configure-rest-api-apps-and-web-admin-server-certs-after-inst-6816457ab95f
   configure-docker-registry-certificate-after-installation-c519edbfe90a
   oidc-client-dex-server-certificates-dc174462d51a
   migrate-platform-certificates-to-use-cert-manager-c0b1727e4e5d
   portieris-server-certificate-a0c7054844bd
   vault-server-certificate-8573125eeea6
   dc-admin-endpoint-certificates-8fe7adf3f932
   add-a-trusted-ca
   alarm-expiring-soon-and-expired-certificates-baf5b8f73009

************
Cert Manager
************

.. toctree::
   :maxdepth: 1

   security-cert-manager
   the-cert-manager-bootstrap-process


Cert-Manager Post Installation Setup
====================================

.. toctree::
   :maxdepth: 1

   firewall-port-overrides
   enable-public-use-of-the-cert-manager-acmesolver-image
   enable-use-of-cert-manager-acmesolver-image-in-a-particular-namespace
   enable-the-use-of-cert-manager-apis-by-an-arbitrary-user

***************
User Management
***************

.. toctree::
   :maxdepth: 3

   introduction-to-user-management-6c0b13c6d325

Examples of User Management Common Tasks
========================================

.. toctree::
   :maxdepth: 2

   example-common-tasks-97773f3a82f0
   configure-oidc-ldap-authentication-for-k8s-user-authentication-8cea26362167
   create-first-system-administrator-1775e1b20941
   system-administrator-local-access-using-ssh-linux-shell-and-st-69213db2a936
   create-other-system-administrators-97b99bb94430
   create-end-users-359693b84854
   end-users-local-access-using-ssh-or-k8s-cli-2b88b1235671


Remote Access
-------------

.. toctree::
   :maxdepth: 1

   remote-access-2209661be417
   system-administrator-collect-system-information-for-user-8502c985343d
   system-administrator-access-system-horizon-gui-a4a95fe70ef9
   system-administrator-configure-system-remote-cli-and-7b814d8937df
   system-administrator-access-system-remote-cli-and-k8s-3807c4f96c87
   end-user-configure-k8s-remote-cli-fad235bb7a18
   end-user-access-k8s-remote-cli-7bb5b71ed604


Reference Material
==================

.. toctree::
   :maxdepth: 2

   the-sysadmin-account
   types-of-system-accounts


Linux User Accounts
-------------------

.. toctree::
   :maxdepth: 2

   overview-of-system-accounts
   establish-keystone-credentials-from-a-linux-account
   starlingx-openstack-kubernetes-from-stsadmin-account-login
   kubernetes-cli-from-local-ldap-linux-account-login
   add-ldap-users-to-linux-groups-using-pamcconfiguration-d31d95e255e1


Keystone Accounts
-----------------

.. toctree::
   :maxdepth: 1

   keystone-accounts
   about-keystone-accounts
   keystone-account-authentication
   keystone-account-roles-64098d1abdc1
   manage-keystone-accounts
   configure-the-keystone-token-expiration-time
   keystone-passwd-recovery-ef3b3ce867b7
   keystone-security-compliance-configuration-b149adca6a7f


LDAP Accounts
-------------

Local LDAP Accounts
^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   local-ldap-linux-user-accounts
   create-ldap-linux-accounts
   create-ldap-linux-groups-4c94045f8ee0
   delete-ldap-linux-accounts-7de0782fbafd
   remote-access-for-linux-accounts
   password-recovery-for-linux-user-accounts
   local-ldap-user-password-expiry-mechanism-eba5d34abbd4
   estabilish-credentials-for-linux-user-accounts
   manage-local-ldap-39fe3a85a528

Remote Windows Active Directory accounts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   remote-windows-active-directory-accounts
   sssd-support-5fb6c4b0320b

Selectively Disable SSH for Local LDAP and WAD Users
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   selectively-disable-ssh-for-local-openldap-and-wad-users-e5aaf09e790c

Manage Composite Local LDAP Accounts at Scale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   manage-local-ldap-39fe3a85a528

Kubernetes API User Authentication Using LDAP Server
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   overview-of-ldap-servers
   centralized-vs-distributed-oidc-auth-setup
   configure-kubernetes-for-oidc-token-validation-while-bootstrapping-the-system
   configure-kubernetes-for-oidc-token-validation-after-bootstrapping-the-system
   configure-oidc-auth-applications
   configure-users-groups-and-authorization
   configure-kubernetes-client-access
   deprovision-ldap-server-authentication

Password Rules
--------------

.. toctree::
   :maxdepth: 2

   starlingx-system-accounts-system-account-password-rules
   linux-accounts-password-3dcad436dce4

Access the System
-----------------

.. toctree::
   :maxdepth: 2

   configure-local-cli-access
   configure-remote-cli-access
   security-configure-container-backed-remote-clis-and-clients
   using-container-backed-remote-clis-and-clients
   security-install-kubectl-and-helm-clients-directly-on-a-host
   security-access-the-gui
   configure-http-and-https-ports-for-horizon-using-the-cli
   configure-horizon-user-lockout-on-failed-logins
   install-the-kubernetes-dashboard
   security-rest-api-access
   connect-to-container-registries-through-a-firewall-or-proxy

Private Namespace and Restricted RBAC
-------------------------------------

.. toctree::
   :maxdepth: 1

   private-namespace-and-restricted-rbac

Resource Management
-------------------

.. toctree::
   :maxdepth: 1

   resource-management

Pod Security Admission Controller
---------------------------------

.. toctree::
   :maxdepth: 1

   pod-security-admission-controller-8e9e6994100f


********
Auditing
********

.. toctree::
   :maxdepth: 1

   auditd-support-339a51d8ce16
   operator-login-authentication-logging
   operator-command-logging
   kubernetes-operator-command-logging-663fce5d74e7


.. _portieris-admission-controller-security-index:

************************************************
Container Image Integrity (Signature Validation)
************************************************

.. toctree::
   :maxdepth: 1

   portieris-overview
   install-portieris
   portieris-clusterimagepolicy-and-imagepolicy-configuration
   remove-portieris


**************************
Container AppArmor Profile
**************************

.. toctree::
   :maxdepth: 1

   about-apparmor-ebdab8f1ed87
   enable-disable-apparmor-on-a-host-63a7a184d310
   enable-disable-apparmor-on-a-host-using-horizon-a318ab726396
   install-security-profiles-operator-1b2f9a0f0108
   profile-management-a8df19c86a5d
   apply-a-profile-to-a-pod-c2fa4d958dec
   enable-apparmor-log-bb600560d794
   author-apparmor-profiles-b02de0a22771

***********************
Encrypting Data at Rest
***********************

.. toctree::
   :maxdepth: 1

   partial-disk-transparent-encryption-support-via-software-enc-27a570f3142c
   encrypt-kubernetes-secret-data-at-rest

Vault Secret and Data Management
================================

.. _vault-secret-and-data-management-050a998960d0:
.. _vault-secret-and-data-management-security-index:

.. toctree::
   :maxdepth: 2

   security-vault-overview
   install-vault
   configure-vault
   configure-vault-using-the-cli
   remove-vault



***************************
Software Delivery Integrity
***************************

.. toctree::
   :maxdepth: 1

   authentication-of-software-delivery

***************************
IPsec on Management Network
***************************

.. toctree::
   :maxdepth: 1

   ipsec-overview-680c2dcfbf3b
   ipsec-configuration-and-enabling-f70964bc49d1
   ipsec-certificates-2c0655a2a888
   ipsec-clis-5f38181d077f


***************
CVE Maintenance
***************

.. toctree::
   :maxdepth: 1

   cve-maintenance-723cd9dd54b3


*******************************************************
Security Feature Configuration for Spectre and Meltdown
*******************************************************

.. toctree::
   :maxdepth: 1

   security-feature-configuration-for-spectre-and-meltdown

************************
Deprecated Functionality
************************

.. toctree::
   :maxdepth: 1

   starlingx-rest-api-applications-and-the-web-administration-server-deprecated
   enable-https-access-for-starlingx-rest-and-web-server-endpoints
   install-update-the-starlingx-rest-and-web-server-certificate


***************************************
Appendix: Locally creating certificates
***************************************

.. toctree::
   :maxdepth: 1

   create-certificates-locally-using-openssl
   create-certificates-locally-using-cert-manager-on-the-controller
