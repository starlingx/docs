.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _configure-oidc-ldap-authentication-for-k8s-user-authentication-8cea26362167:

=====================================================================
Configure OIDC/LDAP Authentication for Kubernetes User Authentication
=====================================================================

After installing |prod|, you should configure |OIDC|/|LDAP| authentication
for kubernetes access user authentication.

|OIDC|/|LDAP| authentication can be supported by |prod|'s local |LDAP| server
and/or up to three remote |LDAP| servers (for example, Windows Active Directory).

In this example, |OIDC|/|LDAP| authentication is setup for local |LDAP|.

.. rubric:: |prereq|

-  You must have the credentials for the 'sysadmin' local Linux user account
   used for installation.

.. rubric:: |proc|

#.   Login to the active controller as the 'sysadmin' user.

     Use either a local console or |SSH|.

#.   Setup 'sysadmin' credentials.

     .. code-block::

        $ source /etc/platform/openrc

#.   Configure Kubernetes for |OIDC| token validation.

     Use the default nodePort for the ``oidc-auth-apps`` system application of 30556.

     .. code-block::

        $ OAMIP=$(system oam-show | egrep "(oam_ip|oam_floating_ip)" | awk '{print $4}')

        $ system service-parameter-add kubernetes kube_apiserver oidc-client-id=stx-oidc-client-app

        $ system service-parameter-add kubernetes kube_apiserver oidc-groups-claim=groups

        $ system service-parameter-add kubernetes kube_apiserver oidc-issuer-url=https://${OAMIP}:30556/dex

        $ system service-parameter-add kubernetes kube_apiserver oidc-username-claim=email

        $ system service-parameter-apply kubernetes

#.   Configure and apply the ``oidc-auth-apps`` system application.

     #.   Create the certificate to be used by both the |OIDC| client and the
          |OIDC| identity provider.

          .. code-block::

              $ mkdir /home/sysadmin/oidc

              $ OAMIP=$(system oam-show | egrep "(oam_ip|oam_floating_ip)" |
              awk '{print $4}')

              $ cat <<EOF > /home/sysadmin/oidc/oidc-auth-apps-certificate.yaml
              apiVersion: cert-manager.io/v1
              kind: Certificate
              metadata:
                 name: oidc-auth-apps-certificate
                 namespace: kube-system
              spec:
                 secretName: oidc-auth-apps-certificate
                 duration: 2160h # 90 days
                 renewBefore: 360h # 15 days
                 issuerRef:
                    name: system-local-ca
                    kind: ClusterIssuer
                 commonName: ${OAMIP}
                 subject:
                    organizations:
                       - ABC-Company
                    organizationalUnits:
                       - StarlingX-system-oidc-auth-apps
                 ipAddresses:
                 - ${OAMIP}
              EOF

              $ kubectl apply -f/home/sysadmin/oidc/oidc-auth-apps-certificate.yaml

     #.   Configure the |OIDC|-client with the |OIDC| client certificate and
          |OIDC| identity server certificate (created in the ``Create the
          certificate to be used by both the OIDC client and the OIDC identity
          provider`` step) and the Trusted |CA| that you used to sign these
          certificates (i.e., the system-local-ca).

          .. code-block::

              $ kubectl get secret system-local-ca -n cert-manager -o=jsonpath='{.data.ca\.crt}' | base64 --decode > /home/sysadmin/oidc/dex-ca-cert.crt

              $ kubectl create secret generic dex-ca-cert --from-file=/home/sysadmin/oidc/dex-ca-cert.crt  -n kube-system

              $ cat <<EOF > /home/sysadmin/oidc/oidc-client-overrides.yaml
              tlsName: oidc-auth-apps-certificate
              config:
                 # The |OIDC|-client container mounts the dex-ca-cert secret at /home, therefore
                 # issuer_root_ca: /home/<filename-only-of-generic-secret>
                 issuer_root_ca: /home/dex-ca-cert.crt
                 issuer_root_ca_secret: dex-ca-cert
                 # secret for accessing dex
                 client_secret: stx-oidc-client-p@ssw0rd
              EOF

              $ system helm-override-update oidc-auth-apps oidc-client kube-system --values /home/sysadmin/oidc/oidc-client-overrides.yaml

     #.   Configure the secret observer to track renewals of certificates.

          .. code-block::

              $ cat <<EOF > /home/sysadmin/oidc/secret-observer-overrides.yaml
              cronSchedule: "*/15 * * * *"
              observedSecrets:
                 - secretName: "dex-ca-cert"
                 filename: "dex-ca-cert.crt"
                 deploymentToRestart: "stx-oidc-client"
                 - secretName: "oidc-auth-apps-certificate"
                 filename: "tls.crt"
                 deploymentToRestart: "stx-oidc-client"
                 - secretName: "oidc-auth-apps-certificate"
                 filename: "tls.crt"
                 deploymentToRestart: "oidc-dex"
              EOF

              $ system helm-override-update oidc-auth-apps secret-observer kube-system --values /home/sysadmin/oidc/secret-observer-overrides.yaml

     #.   Create a secret with the certificate of the |CA| that signed the
          certificate of local |LDAP|, i.e., ``system-local-ca``, to be
          used in the ``Specify the configuration for connecting to Local LDAP
          in the user overrides for the oidc-auth-apps application`` step.

          .. code-block::

             $ kubectl get secret system-local-ca -n cert-manager -o=jsonpath='{.data.ca\.crt}' | base64 --decode > /home/sysadmin/oidc/local-ldap-ca-cert.crt

             $ kubectl create secret generic local-ldap-ca-cert --from-file=/home/sysadmin/oidc/local-ldap-ca-cert.crt -n kube-system

     #.   Specify the configuration for connecting to local |LDAP| in the user
          overrides for the ``oidc-auth-apps`` application.

          .. code-block::

             $ OAMIP=$(system oam-show | egrep "(oam_ip|oam_floating_ip)" | awk '{print $4}')

             $ MGMTIP=$(system addrpool-list --nowrap | fgrep management |  awk '{print $14}')

             $ BINDPW=$(keyring get ldap ldapadmin)

             $ cat <<EOF > /home/sysadmin/oidc/dex-overrides.yaml
             config:
                staticClients:
                - id: stx-oidc-client-app
                   name: STX OIDC Client app
                   redirectURIs: ['https://${OAMIP}:30555/callback']
                   secret: stx-oidc-client-p@ssw0rd
                expiry:
                   idTokens: "10h"
                connectors:
                - type: ldap
                   name: LocalLDAP
                   id: localldap-1
                   config:
                      host: ${MGMTIP}:636
                      rootCA: /etc/ssl/certs/adcert/local-ldap-ca-cert.crt
                      insecureNoSSL: false
                      insecureSkipVerify: false
                      bindDN: CN=ldapadmin,DC=cgcs,DC=local
                      bindPW: ${BINDPW}
                      usernamePrompt: Username
                      userSearch:
                         baseDN: ou=People,dc=cgcs,dc=local
                         filter: "(objectClass=posixAccount)"
                         username: uid
                         idAttr: DN
                         emailAttr: uid
                         nameAttr: gecos
                      groupSearch:
                         baseDN: ou=Group,dc=cgcs,dc=local
                         filter: "(objectClass=posixGroup)"
                         userMatchers:
                         - userAttr: uid
                            groupAttr: memberUid
                         nameAttr: cn
             volumeMounts:
             - mountPath: /etc/ssl/certs/adcert
                name: certdir
             - mountPath: /etc/dex/tls
                name: https-tls
             volumes:
             - name: certdir
                secret:
                   secretName: local-ldap-ca-cert
             - name: https-tls
                secret:
                   defaultMode: 420
                   secretName: oidc-auth-apps-certificate
             EOF

             $ system helm-override-update oidc-auth-apps dex kube-system --values /home/sysadmin/oidc/dex-overrides.yaml

     #.   Apply the ``oidc-auth-apps`` system application.

          .. code-block::

             $ system application-apply oidc-auth-apps

     #.   Wait for the ``oidc-auth-apps`` system application to reach the 'applied' status.

          .. code-block::

             $ system application-list

.. rubric:: |postreq|

See :ref:`create-first-system-administrator-1775e1b20941`.
