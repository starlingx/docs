
.. zmz1607701719511
.. _kubernetes-cli-from-local-ldap-linux-account-login:

========================================================
For Kubernetes CLI from a Local LDAP Linux Account Login
========================================================

You can establish credentials for executing Kubernetes |CLIs| (kubectl and
helm) for a Local |LDAP| user, if required; this is not setup by default.

.. rubric:: |context|

For more information about :command:`ldapusersetup`, see :ref:`Creating LDAP
Linux Accounts <create-ldap-linux-accounts>`.

.. rubric:: |prereq|

.. _kubernetes-cli-from-local-ldap-linux-account-login-ul-afg-rcn-ynb:

You must configure the **oidc-auth-apps** |OIDC| Identity Provider (dex) to get
Kubernetes authentication tokens. See :ref:`Set up OIDC Auth Applications
<configure-oidc-auth-applications>` for more information.

.. rubric:: |proc|

#.  Assign Kubernetes permissions to the user. See :ref:`Configure Users,
    Groups, and Authorization <configure-users-groups-and-authorization>` for
    more information.

#.  Configure kubectl access. See :ref:`Configure Kubernetes Client Access
    <configure-kubernetes-client-access>` to setup the Kubernetes configuration
    file and get an authentication token.
