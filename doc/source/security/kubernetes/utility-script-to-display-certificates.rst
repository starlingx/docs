

.. _utility-script-to-display-certificates:

------------------------------------------
Display Certificates Installed on a System
------------------------------------------

The system certificate-list command
-----------------------------------

The ``system certificate-list``  command can be used to list the platform
certificates present on the |prod| system with details such as expiry date,
residual time, subject, issuer and renewal behaviour (manual or automatic).

The :command:`system certificate-list` command has the following options:

-   ``system certificate-list --expired`` lists the expired certificates.

-   ``system certificate-list --soon_to_expiry <N>`` lists the certificates
    expiring in given <N> days.

For example:

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ system certificate-list
    +------------------------------------------------------------+
    admin_conf_client
    +------------------------------------------------------------+
    Residual Time : 359d
    Issue Date    : June 03 09:26:57 2024
    Expiry Date   : June 07 12:17:19 2025
    Issuer        : CN=starlingx
    Subject       : CN=kubernetes-admin,O=system:masters
    Renewal       : Automatic
    File Path     : /etc/kubernetes/admin.conf
    +------------------------------------------------------------+
    apiserver
    +------------------------------------------------------------+
    Residual Time : 359d
    Issue Date    : June 03 09:26:57 2024
    Expiry Date   : June 07 12:17:17 2025
    Issuer        : CN=starlingx
    Subject       : CN=kube-apiserver
    Renewal       : Automatic
    File Path     : /etc/kubernetes/pki/apiserver.crt
    +------------------------------------------------------------+
    ..etc

The command ``system k8s-certificate-list`` can be used to list all k8s
tls/opaque type certificates present on the |prod| system with details such as
expiry date, residual time, subject, issuer and renewal behaviour (manual or
automatic).

The :command:`system k8s-certificate-list` command has the following options:

-   ``system k8s-certificate-list --expired`` lists the expired certificates.

-   ``system k8s-certificate-list --soon_to_expiry <N>`` lists the certificates
    expiring in given <N> days.

For example:

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ system k8s-certificate-list
    +------------------------------------------------------------+
    cm-cert-manager-webhook-ca/ca.crt
    +------------------------------------------------------------+
    Residual Time : 359d
    Issue Date    : June 07 12:22:41 2024
    Expiry Date   : June 07 12:22:41 2025
    Issuer        : CN=cert-manager-webhook-ca
    Subject       : CN=cert-manager-webhook-ca
    Namespace     : cert-manager
    Secret        : cm-cert-manager-webhook-ca
    Renewal       : Automatic
    Secret Type   : Opaque
    +------------------------------------------------------------+
    cm-cert-manager-webhook-ca/tls.crt
    +------------------------------------------------------------+
    Residual Time : 359d
    Issue Date    : June 07 12:22:41 2024
    Expiry Date   : June 07 12:22:41 2025
    Issuer        : CN=cert-manager-webhook-ca
    Subject       : CN=cert-manager-webhook-ca
    Namespace     : cert-manager
    Secret        : cm-cert-manager-webhook-ca
    Renewal       : Automatic
    Secret Type   : Opaque
    +------------------------------------------------------------+

    ..etc


The command ``system certificate-show <certificate name>`` shows the full
details of the certificate, the certificate name can be picked from ``system
certificate-list`` output.

For example:

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ system certificate-show system-restapi-gui-certificate
    Certificate:
        Residual Time: 84d
        Version: v3
        Serial Number: 0xf6de1076c4f523ae530b39730c61a769
        Issuer: CN=starlingx
        Validity:
            Not Before: June 07 12:25:25 2024
            Not After: September 05 12:25:25 2024
        Subject: CN=system-restapi-gui,O=starlingx,L=78269f12243d4b19b1bf5687e2359c5a
        Subject Public Key Info:
            key_size: (2048 bit)
        X509v3 extensions:
            X509v3 Key Usage:
                values: Digital Signature, Key Encipherment
                critical: True
            X509v3 Basic Constraints:
                CA: False
                critical: True
            X509v3 Authority Key Identifier:
                keyid: a783e3e1c720c9b5dc2537b07f90a49b0ecdf744
            X509v3 Subject Alternative Name:
                DNS: ['78269f12243d4b19b1bf5687e2359c5a.starlingx.local']
                IP Address: ['10.10.10.12']
        Signature Algorithm: sha256WithRSAEncryption
        Signature: 02ac2fe1e454ff0116fff400f24bb1845872b84d44ff47a40e3989d2085e0e306c903a1290f8f3bddf2cb320d5387ce64e4b50d8b3269ab70d6dbb760ecbd566f0fbf644c24f258cce4e0f1987e3636fe88a00ce99b416b47a372b28424367ceef9b04d52b279be2305070c6a92c848de6f50dd046876475c1604fdf630e72f47aba051b30983cf046b69f7c7ddb5fe396cf41955d40bc263640e2d85bba5a10602a7962c69397e30a3ebf6080df6aea2fa4ce380aa413ed89b96f3b3f3d54ee8ddaa477f5d41fd3d2f55231ebc35e76ae9f65db39e24fa07d0c2d7b4f5588daa8da92ff3fd03a240a7e7a70e31d619a1a659c709ed293451e111a316703264cf80a023acb0aa7790828c1508cf88aabf2c02b097d5f7afb077a7759b85a204bfc672c33bcbc07b6a16fc6f756721de043917e7a9de6ca38f99d421232a3302f03b762c3a9847d0a2aa25af47b39b2eca469c5f309a99931234551b7c29f9e119215970e30bb55ad4263f4745751fb927fb98c3c463a2f403686c90a6cac6db2f681e3e5c6951f43da1db8b7b2d9121a3bd49429ddbb72740a1f1a108e62e8add857f344d60cadfc47abf1343f166d33d7083331913538d2881dbd9216763427722e9db4f10cc9952fc5d2f0998e4453925d9b84f3c2394c825eeb1bb8c63948bb2bb8c27165f81f585a4d023d139a8413c235dd8d4cd8cae418f9764c66ad0e
        File Path: /etc/ssl/private/server-cert.pem
        Renewal: Automatic
        Namespace: deployment
        Secret: system-restapi-gui-certificate


The command ``system k8s-certificate-show <certficate name>`` shows the full
details of the k8s certificate, the certificate name can be picked from ``system
k8s-certificate-list`` output.

For example:

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ system k8s-certificate-show system-local-ca
    Certificate:
        Residual Time: 3640d
        Version: v3
        Serial Number: 0x3cb901b2b670bf0996d2c4f52c6d809e061d03d4
        Issuer: CN=starlingx
        Validity:
            Not Before: June 03 09:26:57 2024
            Not After: June 01 09:26:57 2034
        Subject: CN=starlingx
        Subject Public Key Info:
            key_size: (4096 bit)
        X509v3 extensions:
            X509v3 Subject Alternative Name:
                DNS: ['starlingx']
            X509v3 Key Usage:
                values: Digital Signature, Key Encipherment
            X509v3 Basic Constraints:
                CA: True
                critical: True
        Signature Algorithm: sha256WithRSAEncryption
        Signature: af88723bb84c1f5b9ebe62e257564990be3b379e156e1c45203cbf10c05c09e03325b2f59c752a61a3289f32b2dbcc580bba16e13bc02137ff18b27c1f6e7ccc2a9e7243dcfaf7ea16ae0764fd30f915d95b428248be5bdcc4a9a8972d538ce2314e29339759b06f739ef06941af54450fc830aef5be9dd214962959aefc6b6dc22dba2ce2f87b5c763817c9d33bcf2c189e59fc7abad455aef3ad18525d9f8f47938b8d47056bcb513d414fe2f7713b6b8ee37e8999ea48c2529043ec7871fa079d7374b894a5410090c5fe8ad3e08b7b3649cac550b9ce732ec0e0739ac3de4c1eca8d83d3c44bf30841a1a469d5bfba5743f5f1d5260e73057027d4c5ad97a955d8a55f72733c941a30402f92454be97725acabba199a92a723a44aa1f32c86e274944641b5665f4d09ecae46d2d8b6ac100e690d763b1172406e9e3db0f6f3070fa66af43de449b4a1e4c34b2e3329c878138b2078c22facb940ade27372d17354f35dabf406ec7925dba4e60c49139fa99e6f4386eb245c3be5827dd6fea4988bdb11bb684da4d5584a87b7a479cded665de87f8b479fb81430f3bb6443e54051f477465343e1e3c77bb590ee6d87ce5b7f39bf417e047f7acc3ac7aff5e389b317bf0ef79c87f37fe951c06bc431afb450c69be8761b90fca5b46d936a521b728320f6073a9811334581ca977b7f75f7c5b02db5091edd220b77060273
        Namespace: cert-manager
        Secret: system-local-ca
        Renewal: Manual
        Secret Type: kubernetes.io/tls


The show-certs.sh script
------------------------

.. note::
    This script is deprecated and no longer maintained.

The ``show-certs.sh`` script is an alternative way that can be used to display
a list of the specific certificates present on a |prod| system with details
such as expiry date, residual time, subject, issuer and renewal behaviour
(manual or automatic).

The :command:`show-certs.sh` command has the following options:

**sudo show-certs.sh [-k] [-e <number-of-days>] [-h]**

where:

By default, :command:`show-certs.sh` command displays the platform-managed
system certificates, and (highlighted in red) certificates requiring manual
renewal, and certificates expiring within 90 days.

options:

``-k`` displays certificates found in any Kubernetes SECRETS; this may include
platform certificates and end-users' certificates.

``-e`` <number-of-days>. Changes to highlight (in red) certificates within
<number-of-days> of expiry.

``-h`` displays help

.. note::

    This command can only be run locally on the active controller, in an SSH
    shell.

For example:

.. code-block:: none

    ~(keystone_admin)]$ sudo show-certs.sh

    registry.local  CERTIFICATE:
    -----------------------------------------------------
    Renewal 	    :  Manual
    Filename	    :  /etc/ssl/private/registry-cert.crt
    Subject         :  /CN=registry.local
    Issuer          :  /CN=registry.local
    Issue Date	    :  Aug 31 01:43:09 2021 GMT
    Expiry Date	    :  Aug 31 01:43:09 2022 GMT
    Residual Time   :  341d
    -----------------------------------------------------

    local-openldap / deployment  /  system-openldap-local-certificate  CERTIFICATE:
    ------------------------------------------
    Renewal        :  Automatic [Managed by Cert-Manager]
    Namespace      :  deployment
    Secret         :  system-openldap-local-certificate
    Subject        :  CN = system-openldap
    Issuer         :  CN = starlingx
    Issue Date     :  Jul 6 16:15:30 2023 GMT
    Expiry Date    :  Oct 4 16:15:30 2023 GMT
    Residual Time  :  89d

    … etc


For scalability reasons, in a Distributed cloud system, the Subcloud ICA
certificates that are present on a SystemController are redirected to a file.
The script displays the path to the file with a note at the end of the
displayed output.

.. code-block:: none

    Subcloud ICA certificates (*-adminep-ca-certificate) are saved to
    /tmp/subcloud-icas-tls-secrets.HqZSBQoUUJ.txt in order to limit the
    size of the output.

For example,

.. code-block:: none

    ~(keystone_admin)]$ cat /tmp/subcloud-icas-tls-secrets.HqZSBQoUUJ.txt

    Renewal                              Namespace  Secret		             Residual Time
    ---------------------------------------------------------------------------------------
    Automatic [Managed by Cert-Manager]   dc-cert   subcloud1-adminep-ca-certificate   364d
    Automatic [Managed by Cert-Manager]   dc-cert   subcloud10-adminep-ca-certificate  364d
    Automatic [Managed by Cert-Manager]   dc-cert   subcloud100-adminep-ca-certificate 364d
    ---------------------------------------------------------------------------------------

