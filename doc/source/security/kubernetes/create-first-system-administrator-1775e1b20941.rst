.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _create-first-system-administrator-1775e1b20941:

=================================
Create First System Administrator
=================================

After installing |prod|, you should create your first unique system
administrator account.

In this example, a local |LDAP| user account and a local Keystone user account
are created for the first system administrator user.

The first system administrator user must have full 'admin' privileges such that
it can create subsequent system administrators and end users.

.. rubric:: |prereq|

-  You must have the credentials for the 'sysadmin' local Linux user
   account used for the installation.

.. rubric:: |proc|

#.   Login to the active controller as the 'sysadmin' user.

     Use either a local console or |SSH|.


#.   Apply source credentials for the 'admin' keystone user.

     .. code-block::

         $ source /etc/platform/openrc


#.   Create a directory for temporary files for setting up users and groups.

     .. code-block::

         $ mkdir /home/sysadmin/users

#.   Create a new local |LDAP| group for system administrators with full privileges.

     .. code-block::

         $ sudo ldapaddgroup Level1SystemAdmin

     #.   Add full Linux authorization privileges to the ``Level1SystemAdmin``
          |LDAP| group members.

          Enable ``pam_group.so`` in ``/etc/pam.d/common-auth``, and update
          ``/etc/security/group.conf`` with |LDAP| group mappings.

          .. note::

             If it is |AIO-DX| controller configuration, add full Linux authorization
             privileges on both controllers.

          .. code-block::

               # Execute this line only once, on each host
               $ sudo sed -i '1i auth required pam_group.so use_first_pass' /etc/pam.d/common-auth

               # Execute this line for each LDAP group being mapped to 1 or more local Linux groups, on each host
               $ sudo sed -i '$ a\*;*;%Level1SystemAdmin;Al0000-2400;sys_protected,root,sudo' /etc/security/group.conf

     #.   Add full kubernetes authorization privileges to the ``Level1SystemAdmin``
          |LDAP| group members.

          Add a kubernetes ``ClusterRoleBinding`` to bind the ``Level1SystemAdmin``
          group to the ``cluster-admin`` role.

          .. code-block::

               $ cat << EOF > /home/sysadmin/users/Level1SystemAdmin-clusterrolebinding.yml
               apiVersion: rbac.authorization.k8s.io/v1
               kind: ClusterRoleBinding
               metadata:
                  name: Level1SystemAdmin
               subjects:
                 - kind: Group
                   name: Level1SystemAdmin
                   apiGroup: rbac.authorization.k8s.io
               roleRef:
                  kind: ClusterRole
                  name: cluster-admin
                  apiGroup: rbac.authorization.k8s.io
               EOF

               $ kubectl apply -f /home/sysadmin/users/Level1SystemAdmin-clusterrolebinding.yml

#.   Create a new local |LDAP| user for the first system administrator.

     .. code-block::

         $ sudo ldapusersetup -u joefulladmin
         Password:
         Successfully added user joefulladmin to LDAP
         Successfully set password for user joefulladmin
         Warning : password is reset, user will be asked to change password at login
         Successfully modified user entry uid=joefulladmin,ou=People,dc=cgcs,dc=local in LDAP
         Updating password expiry to 90 days
         Successfully modified user entry uid=joefulladmin,ou=People,dc=cgcs,dc=local in LDAP
         Updating password expiry to 2 days


#.   Add the first system administrator to the ``Level1SystemAdmin`` group.

     .. code-block::

         $ sudo ldapaddusertogroup joefulladmin Level1SystemAdmin


#.   Add a new local keystone user for the first system administrator using the
     same username.

     Create a keystone user in the 'admin' project. The |prod|
     infrastructure resources are all contained in the 'admin' project.

     .. code-block::

        $ USERNAME="joefulladmin"
        $ USERPASSWORD="<password>"
        $ PROJECTNAME="admin"
        $ PROJECTID=`openstack project list | grep ${PROJECTNAME} | awk '{print $2}'\`
        $ openstack user create --password "${USERPASSWORD}" --project ${PROJECTID} "${USERNAME}"
        $ openstack role add --project ${PROJECTNAME} --user ${USERNAME}_member_

     #.   Add full |prod| authorization privileges to the first system
          administrator's keystone user account.

          .. code-block::

              $ openstack role add --project ${PROJECTNAME} --user ${USERNAME} admin


#.   Logout as 'sysadmin'.

     .. code-block::

       $ exit


.. rubric:: |postreq|

-  Login to the local console or |SSH| with this new first system
   administrator, ``joefulladmin``. See :ref:`system-administrator-local-access-using-ssh-linux-shell-and-st-69213db2a936`

-  Continue to :ref:`create-other-system-administrators-97b99bb94430`


