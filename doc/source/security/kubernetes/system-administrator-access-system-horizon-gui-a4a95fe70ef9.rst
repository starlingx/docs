.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _system-administrator-access-system-horizon-gui-a4a95fe70ef9:

=========================================
System Administrator - Access Horizon GUI
=========================================

Access the |prod| Horizon GUI using your browser.

This procedure should be performed on your system that has a web browser.

.. rubric:: |prereq|

-  A system with a web browser.

-  You need to have the ``stx-remote-access.tar`` file from your system administrator,
   containing system information related to your |prod| system.

.. rubric:: |proc|

#. Update your web browser to 'trust' the |prod| |CA| certificate.

   #.   Extract the files from the ``stx-remote-access-info.tar`` file
        from your |prod| system administrator.

        .. code-block::

           $ cd ~
           $ tar xvf ./stx-remote-access-info.tar

           # The StarlingX CA Certificate is here:
           $ ls ./stx-remote-access-info/stx.ca.crt


   #.   Follow your web browser's instructions to add '~/stx-remote-access-info/stx.ca.crt'
        to the list of trusted CAs for your browser.

#. Open your web browser at address ``https://<OAM-Floating-IP-Address>:8443``

   Login with your keystone account's 'username' and 'password'.
