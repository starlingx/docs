.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _ipsec-certificates-2c0655a2a888:

==================
IPSec Certificates
==================

|prod| uses x509 certificate for IPsec authentication. The following are IPsec
related certificates.

* Certificates in /etc/swanctl/x509/ directory
    Files in this directory are the IPsec certificates for peer
    authentication and SA establishment. They are issued by system-local-ca
    managed by cert-manager.
* Private keys in /etc/swanctl/private/ directory
    Files in this directory are the corresponding private keys of the IPsec
    certificates in /etc/swanctl/x509/ directory. Together with the
    certificates, they are used for IPsec authentication and SA establishment.

IPsec certificates are valid for 3 months by default. They are monitored and
renewed automatically by the Platform. The IPsec certificates are renewed
(along with the corresponding private keys) when the certificates are within
15 days of expiration.

* Certificates in /etc/swanctl/x509ca/ directory
    Files in this directory are the root |CA| and intermediate |CA| certificates.
    These are the CA certificates that sign the IPsec certificates. With these
    |CA| certificates, a full certificate chain is established. They are used
    by IPsec to authenticate peers and SA establishment.

When the system's root |CA| certificate is updated (by user running
``update_platform_certificates.yml`` for example), the certificates for IPsec,
including IPsec certificates, corresponding private keys and CA certificates,
will all be updated accordingly.

