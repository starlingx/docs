.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _add-ldap-users-to-linux-groups-using-pamcconfiguration-d31d95e255e1:

======================================================
Add LDAP Users to Linux Groups Using PAM Configuration
======================================================

The Linux pam_group module enables binding/mapping of |LDAP| users/groups to a
specified list of one or more Linux groups. The mapping allows Linux
capabilities (via the Linux groups) to be assigned to the |LDAP| users/groups.
The mapping will occur after the |SSSD| service has discovered the |LDAP| users
and groups and cached them on the host.

The mapping between the discovered |LDAP| users and their group membership to the
local Linux groups works for all Linux groups, including system groups, such as
``sudo`` and ``root``.

.. note::

    The procedure described in this section applies to all the |LDAP| users,
    both Local |LDAP| and |LDAP| users in the remote Windows Active Directory
    servers.

.. rubric:: |proc|

Perform the following |PAM| configuration on all the hosts after the system is installed:

#. To configure pam_group module, add the following line to the top of the
   ``/etc/pam.d/common-auth`` file after the comments.

   .. code-block::

       auth    required     pam_group.so use_first_pass

#. Update ``/etc/security/group.conf`` with |LDAP| groups mapping to Linux groups.

   For each |LDAP| group mapping to a local Linux group(s), the following line
   needs to be added to the bottom of the ``/etc/security/group.conf`` file
   using the format ``services;ttys;users;times;groups``:

   .. code-block::

       *;*;%<fully qualified wad group name>;Al0000-2400;<list of local Linux groups>

   Where, ``Al0000-2400`` stands for times. It is used to indicate when these
   groups are available to the user. The times format is a logical list
   of day/time-range entries. ``Al`` stands for all seven days of the week.

   Each day/time-range can be prefixed with a `!` to indicate `anything but`.

   The time-range part is two 24-hour times HHMM separated by a hyphen
   that indicates the start and finish time. More information on the line format
   can be found in the file ``/etc/security/group.conf``.

   For example:

   .. code-block::

       *;*;%pvtest@wad.mydomain.com;Al0000-2400;sys_protected,root,sudo

   The example above can be read as: For all services and all ttys, members(%)
   of ``pvtest@wad.mydomain.com`` group, for all days and times (Al0000-2400),
   add these users to the following local Linux groups:  ``sys_protected``,
   ``root``, and ``sudo``.

   .. note::

       The pam_group configuration will enforce the |LDAP| group membership in Linux
       groups, after a |LDAP| mapped group member is successfully authenticated in the
       platform, either with |ssh| or direct login.

After the login of a |LDAP| user that is part of a mapped |LDAP| group, you can
view the new membership to Linux groups. The |LDAP| user memberships and
privileges set with the above example mapping gives a user the following sudo
privileges:

|WAD| user example:

.. code-block::

    Last login: Mon Jul  8 12:53:12 2024 from 10.10.10.1
    pvtest1@wad.mydomain.com@controller-0:~$ source /etc/platform/openrc
    [pvtest1@wad.mydomain.com@controller-0 ~(keystone_admin)]$ sudo su
    Password:
    root@controller-0:/var/home/wad.mydomain.com/pvtest1# groups
    root
    root@controller-0:/var/home/wad.mydomain.com/pvtest1# exit
    exit
    [pvtest1@wad.mydomain.com@controller-0 ~(keystone_admin)]$ groups
    eng@wad.mydomain.com root sudo sys_protected pvtest@wad.mydomain.com

Local |LDAP| user example:

Add the following line in ``/etc/security/group.conf`` to map users of the
Local |LDAP| group ``managers`` to linux groups: ``sys_protected``, ``root``
and ``sudo``.

.. code-block::

    *;*;%managers;Al0000-2400;sys_protected,root,sudo

Log in with user ``johndole`` from ``managers`` group and check the user's
group memberships and privileges.

.. code-block::

    johndole@controller-0:~$ id
    uid=10007(johndole) gid=100(users) groups=100(users),0(root),27(sudo),345(sys_protected),10001(managers)
    johndole@controller-0:~$ source /etc/platform/openrc
    [johndole@controller-0 ~(keystone_admin)]$ kubectl config get-contexts
    CURRENT   NAME                          CLUSTER      AUTHINFO           NAMESPACE
    *         kubernetes-admin@kubernetes   kubernetes   kubernetes-admin

    [johndole@controller-0 ~(keystone_admin)]$ groups
    users root sudo sys_protected managers
    [johndole@controller-0 ~(keystone_admin)]$

    [johndole@controller-0 ~(keystone_admin)]$ sudo -l
    Password:
    Matching Defaults entries for johndole on controller-0:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin,
    lecture=never,
    secure_path=/usr/local/bin\:/usr/bin\:/bin\:/usr/local/sbin\:/usr/sbin\:/sbin,
    lecture=never,
    secure_path=/usr/local/bin\:/usr/bin\:/bin\:/usr/local/sbin\:/usr/sbin\:/sbin,
    passprompt="Password: "

    User johndole may run the following commands on controller-0:
    (ALL : ALL) ALL
    (ALL) ALL
    [johndole@controller-0 ~(keystone_admin)]$













