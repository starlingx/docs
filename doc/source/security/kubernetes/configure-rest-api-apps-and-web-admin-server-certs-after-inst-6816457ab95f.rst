.. _configure-rest-api-applications-and-web-administration-server-certificates-after-installation-6816457ab95f:

=========================================================================
Configure REST API Applications and Web Administration Server certificate
=========================================================================

|prod| provides support for secure HTTPS external connections used for |prod|
REST API application endpoints (Keystone, Barbican and |prod|) and the |prod|
web administration server.

During installation, the Platform Issuer (``system-local-ca``) will
automatically issue a certificate used to secure access to the |prod| REST API
and to the Web Server GUI. This allows the system to have HTTPS access enabled
from the bootstrap to the services. This certificate will be stored in a K8s
|TLS| secret in namespace ``deployment``, named
``system-restapi-gui-certificate``. It will be managed by cert-manager, renewed
upon expiration and the required services restarted automatically.

After bootstrap, this certificate's fields can be updated using the procedure
:ref:`migrate-platform-certificates-to-use-cert-manager-c0b1727e4e5d`. The
certificate will be managed by cert-manager (auto renewed upon expiration).

The certificate will be anchored by ``system-local-ca``'s Root |CA|. For more
information, refer to
:ref:`system-local-ca-issuer-9196c5794834`.