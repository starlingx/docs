.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _create-other-system-administrators-97b99bb94430:

==================================
Create Other System Administrators
==================================

After setting up your first system administrator, use this first
system administrator to configure other system administrators.

In the following example, creating other system administrators consists of:

-  Create system administrator groups with different privilege levels.

   -  The ``Level1SystemAdmin`` group with full privileges (including sudo) has
      already been created, when creating the first system administrator.

   -  Create a ``Level2SystemAdmin`` group with full privileges, with no linux
      ``sudo`` capability.

   -  Create a ``Level3SystemAdmin`` group with read-only privileges.

-  Create one or more new system administrator users in each of the above groups.

   -  For each user, create both:

      -  a local |LDAP| user account.

      -  a keystone user account.

.. rubric:: |prereq|

-  You need to use the first system administrator created to execute this procedure.

.. rubric:: |proc|

#.   Login to the active controller as the first system administrator,
     ``joefulladmin`` in this example.

     Use either a local console or |SSH|.

#.   Use the ``local_starlingxrc`` to setup |prod| environment variables
     and to setup the keystone user's authentication credentials.

     .. code-block::

         $ source local_starlingxrc
         Enter the password to be used with keystone user joefulladmin:
         Created file /home/joefulladmin/joefulladmin-openrc

#.   Use the ``oidc-auth`` to authenticate via |OIDC|/|LDAP| for kubernetes CLI.

     .. code-block::

         $ oidc-auth
         Using "joefulladmin" as username.
         Password:
         Successful authentication.
         Updated /home/joefulladmin/.kube/config .

#.   Set up additional system admin groups with different privileges.

     Create a directory for temporary files for setting up users and
     groups.

     .. code-block:: none

         $ mkdir /home/joefulladmin/users

     #.  Create a new local |LDAP| group with full privilege (but without linux
         ``sudo`` capability) for the system administrator.

         .. code-block:: none

             $ sudo ldapaddgroup Level2SystemAdmin


         #.   Add full Linux authorization privileges (but without linux 'sudo'
              capability) to the ``Level2SystemAdmin`` |LDAP| group members.

              Update the ``/etc/security/group.conf`` with |LDAP| group mappings.

              .. note::

                For a |AIO-DX| controller configuration, this step must be done
                on both controllers.

              .. code-block:: none

                $ sudo sed -i '$ a\*;*;%Level2SystemAdmin;Al0000-2400;sys_protected,root' /etc/security/group.conf
                

         #.   Add restricted kubernetes authorization privileges to the
              ``Level2SystemAdmin`` |LDAP| group members.

              Add a kubernetes ``ClusterRole`` and kubernetes ``ClusterRoleBinding`` to
              bind the ``Level2SystemAdmin`` group to a more restricted set of
              kubernetes capabilities.

              .. code-block:: none

                  $ cat << EOF > /home/joefulladmin/users/Level2SystemAdmin-clusterrolebinding.yml
                  apiVersion: rbac.authorization.k8s.io/v1
                  kind: ClusterRole
                  metadata:
                     name: Level2SystemAdmin
                  rules:
                  # For the core API group (""), allow full access to all resource types
                  # EXCEPT for serviceaccounts and resource policies (limitranges and resourcequotas) only allow read access
                  - 	apiGroups: [""]
                     resources: ["bindings", "configmaps", "endpoints", "events", "persistentvolumeclaims", "pods", "podtemplates", "replicationcontrollers", "secrets", "services"]
                     verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
                  - 	apiGroups: [""]
                     resources: [ "serviceaccounts", "limitranges", "resourcequotas" ]
                     verbs: ["get", "list"]
                  ---
                  apiVersion: rbac.authorization.k8s.io/v1
                  kind: ClusterRoleBinding
                  metadata:
                     name: Level2SystemAdmin
                  subjects:
                  - 	kind: Group
                     name: Level2SystemAdmin
                     apiGroup: rbac.authorization.k8s.io
                  roleRef:
                     kind: ClusterRole
                     name: Level2SystemAdmin
                     apiGroup: rbac.authorization.k8s.io
                  EOF

                  $ kubectl apply -f /home/joefulladmin/users/Level2SystemAdmin-clusterrolebinding.yml

         #. 'admin' StarlingX Authorization Privileges will be given to the
            ``Level2SystemAdmin`` |LDAP| Group members, when they are created in a
            subsequent step.

     #.  Create a new local |LDAP| group for read-only system administrators.

         .. code-block:: none

              $ sudo ldapaddgroup Level3SystemAdmin

         #.  Do not add additional linux authorization privileges to the ``Level3SystemAdmin``
             |LDAP| group members.

             Update ``/etc/security/group.conf`` with |LDAP| group mappings.

             .. note::

                For a |AIO-DX| controller configuration, this step must be done
                on both controllers.

             .. code-block:: none

                 $ sudo sed -i '$ a\*;*;%Level3SystemAdmin;Al0000-2400;users' /etc/security/group.conf

         #.   Add 'reader' Kubernetes authorization privileges to the ``Level3SystemAdmin``
              |LDAP| group members.

              Add a kubernetes ``ClusterRole`` and kubernetes ``ClusterRoleBinding``
              to bind the ``Level3SystemAdmin`` group to a reader only set of kubernetes
              capabilities.

              .. code-block:: none

                  $ cat << EOF > /home/joefulladmin/users/Level3SystemAdmin-clusterrolebinding.yml
                  apiVersion: rbac.authorization.k8s.io/v1
                  kind: ClusterRole
                  metadata:
                     name: Level3SystemAdmin
                  rules:
                  - 	apiGroups: [""] # "" indicates the core API group
                     resources: ["*"]
                     verbs: ["get", "watch", "list"]
                  ---
                  apiVersion: rbac.authorization.k8s.io/v1
                  kind: ClusterRoleBinding
                  metadata:
                     name: Level3SystemAdmin
                  subjects:
                  - 	kind: Group
                     name: Level3SystemAdmin
                     apiGroup: rbac.authorization.k8s.io
                  roleRef:
                     kind: ClusterRole
                     name: Level3SystemAdmin
                     apiGroup: rbac.authorization.k8s.io
                  EOF

                  $ kubectl apply -f /home/joefulladmin/users/Level3SystemAdmin-clusterrolebinding.yml

         #.  The 'reader' |prod| authorization privileges will be given to the
             ``Level3SystemAdmin`` |LDAP| group members, when they are created
             in a subsequent step.


#.   Create system 'admin' users in each of the 3 system admin groups.

     #.  Create one or more users in the ``Level1SystemAdmin`` group and give each
         a keystone user account with an 'admin' role.

         .. code-block::

            $ sudo ldapusersetup -u davefulladmin
            Password:
            Successfully added user davefulladmin to LDAP
            Successfully set password for user davefulladmin
            Warning : password is reset, user will be asked to change password at login
            Successfully modified user entry uid=davefulladmin,ou=People,dc=cgcs,dc=local in LDAP
            Updating password expiry to 90 days
            Successfully modified user entry uid=davefulladmin,ou=People,dc=cgcs,dc=local in LDAP
            Updating password expiry to 2 days

            $ sudo ldapaddusertogroup davefulladmin Level1SystemAdmin

            $ USERNAME="davefulladmin"
            $ USERPASSWORD="<password>"
            $ PROJECTNAME="admin"
            $ PROJECTID=`openstack project list | grep ${PROJECTNAME} | awk '{print $2}'`
            $ openstack user create --password "${USERPASSWORD}" --project ${PROJECTID} "${USERNAME}"
            $ openstack role add --project ${PROJECTNAME} --user ${USERNAME} _member_
            $ openstack role add --project ${PROJECTNAME} --user ${USERNAME} admin

         Repeat this step for additional users required in the ``Level1SystemAdmin``
         group.

     #.  Create one or more users in the ``Level2SystemAdmin`` group and give
         each a keystone user account with an 'admin' role.

         .. code-block::

            $ sudo ldapusersetup -u jimbasicadmin
            Password:
            Successfully added user jimbasicadmin to LDAP
            Successfully set password for user jimbasicadmin
            Warning : password is reset, user will be asked to change password at login
            Successfully modified user entry uid=jimbasicadmin,ou=People,dc=cgcs,dc=local in LDAP
            Updating password expiry to 90 days
            Successfully modified user entry uid=jimbasicadmin,ou=People,dc=cgcs,dc=local in LDAP
            Updating password expiry to 2 days

            $ sudo ldapaddusertogroup jimbasicadmin Level2SystemAdmin

            $ USERNAME="jimbasicadmin"
            $ USERPASSWORD="<password>"
            $ PROJECTNAME="admin"
            $ PROJECTID=`openstack project list | grep ${PROJECTNAME} | awk '{print $2}'`
            $ openstack user create --password "${USERPASSWORD}" --project ${PROJECTID} "${USERNAME}"
            $ openstack role add --project ${PROJECTNAME} --user ${USERNAME} _member_
            $ openstack role add --project ${PROJECTNAME} --user ${USERNAME} admin

         Repeat this step for additional users required in the ``Level2SystemAdmin``
         group.

     #.  Create one or more users in the ``Level3SystemAdmin`` group and give
         each a keystone user account with 'reader' role.

         .. code-block::

            $ sudo ldapusersetup -u billreaderadmin
            Password:
            Successfully added user billreaderadmin to LDAP
            Successfully set password for user billreaderadmin
            Warning : password is reset, user will be asked to change password at login
            Successfully modified user entry uid=billreaderadmin,ou=People,dc=cgcs,dc=local in LDAP
            Updating password expiry to 90 days
            Successfully modified user entry uid=billreaderadmin,ou=People,dc=cgcs,dc=local in LDAP
            Updating password expiry to 2 days

            $ sudo ldapaddusertogroup billreaderadmin Level3SystemAdmin

            $ USERNAME="billreaderadmin"
            $ USERPASSWORD="<password>"
            $ PROJECTNAME="admin"
            $ PROJECTID=`openstack project list | grep ${PROJECTNAME} | awk '{print $2}'`
            $ openstack user create --password "${USERPASSWORD}" --project ${PROJECTID} "${USERNAME}"
            $ openstack role add --project ${PROJECTNAME} --user ${USERNAME} _member_
            $ openstack role add --project ${PROJECTNAME} --user ${USERNAME} reader

         Repeat this step for additional users required in the ``Level3SystemAdmin``
         group.

.. rubric:: |postreq|

Each system administrator user created should now be able to:

-  |SSH| to the system

   -  execute linux commands based on their linux authorizations.

   -  execute |prod| CLI commands based on their |prod| authorizations.

   -  execute kubernetes CLI commands based on their kubernetes |RBAC| role bindings.

-  See section: :ref:`system-administrator-local-access-using-ssh-linux-shell-and-st-69213db2a936`.


