.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _example-common-tasks-97773f3a82f0:

========================================
Examples of User Management Common Tasks
========================================

This section provides a set of common tasks related to the user management of
both system administrations and general end users, to set up unique users for
your system.



