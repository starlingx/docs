.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _introduction-to-user-management-6c0b13c6d325:

===============================
Introduction to User Management
===============================

User Management is the capability to configure unique users for your system,
i.e. both system administrators and general end users. There are multiple user
types and user account types in |prod|.

User Types
==========

-  **'sysadmin' Linux User**

   The 'sysadmin' linux user is a special-case user for initial install only.

-  **System Administrators**

   The system administrator user type is for managing the |prod| system
   infrastructure. A user of this type requires:

   -  A **Keystone user account**

      The Keystone user account is used for access to |prod| services through
      the |prod| GUI, RESTAPIs, local or remote CLIs.

      -  The bulk of the |prod| system infrastructure is managed through
         the |prod| GUI, RESTAPIs, local or remote CLIs.

   -  A **LDAP user account**

      -  The |LDAP| user account is used for |SSH| access to |prod| physical hosts.

         -  |SSH| access is required to access local |prod| Ansible Playbooks
            or |prod| scripts for management of |prod| system infrastructure
            not covered by |prod| GUI, RESTAPIs, CLIs.

      -  The |LDAP| user account is also used for access to kubernetes services
         through the kubernetes CLIs.

         -  Kubernetes CLIs are required for management of |prod| system
            infrastructure not covered by |prod| GUI, RESTAPIs, CLIs,
            |prod| Ansible Playbooks, or |prod| scripts.


-  **End Users**

   The *end user* user type is for managing hosted containerized applications on
   |prod| (for example, a containerized |O-RAN| application). A user of this
   type requires:

   -  A **LDAP User Account**

      -  The |LDAP| user account is used for access to kubernetes services through
         the kubernetes GUI, RESTAPIs, local or remote CLIs.

         -  It is for creating / managing end users kubernetes resources of containerized
            applications hosted by |prod|.

      -  |Optional| the |LDAP| user account can also be used for |SSH| access to
         |prod| physical hosts.

         -  |SSH| access provides access to local Linux services (for example,
            hardware status,metrics) for the purposes of monitoring Linux 
            resources (for example, interfaces) of end users' containerized 
            applications hosted by |prod|.

User Account Types
==================

-  **'sysadmin' Linux User Account**

   -  The 'sysadmin' local Linux user account is created on the initial software
      install. The default initial password is: sysadmin. The installer is forced
      to change the password immediately on the first login as part of the install
      procedure.

   -  The 'sysadmin' user has LINUX 'sudo all' capability and is a member of the
      root group. This user also has Kubernetes ``cluster-admin`` role, which allows
      it to do all operations in kubernetes environment. When executing ``source /etc/platform/openrc``,
      the user becomes the keystone 'admin' user with 'admin' role, which allows
      it to do all operations in |prod| environment.

   -  The 'sysadmin' linux user should only be used by end users for initial installation,
      i.e. do not use this as a shared user account. Do not use this as a shared account
      amongst your set of system administrators. Create unique user accounts (both
      keystone user accounts and |LDAP| user accounts) for each of your system
      administrators, with only the required privileges.

   -  Do not remove the 'sysadmin' linux user. It is used internally by the |prod|
      platform.

-  **Keystone User Accounts**

   -  The Keystone users are required for access to |prod| services through the
      |prod| GUI, RESTAPIs, local or remote CLIs. The Keystone users are
      created / managed locally on the |prod| system.

   -  There is a default 'admin' Keystone user (with 'admin' role) whose
      password is set to the same password as provided by the initial
      password change for the 'sysadmin' Linux user. Do not use this as a
      shared account amongst your set of system administrators. Create unique
      Keystone user accounts for each of your system administrators, with only
      the required privileges.

   -  There are two static keystone roles for |prod| services:

      -  'admin' - can run all commands.

      -  'reader' - has read-only access to |prod| services. The reader cannot
         perform changes to the system, but can read/show/list any data.

-  **LDAP User Accounts**

   -  |LDAP| users are required for |SSH| access to local |prod| ansible
      playbooks / |prod| scripts and/or access to Kubernetes services
      through the Kubernetes CLIs.

   -  There are two types of |LDAP| users/groups supported on |prod|:

      -  Local |LDAP| - where Local |LDAP| users and groups are created locally
         on |prod| system.

      -  Remote |LDAP| (for example, Windows Active Directory) - where |LDAP|
         users and groups are created remotely on an external |LDAP| system. The
         |prod| system accesses external |LDAP| system, according to configured
         access parameters, and discovers the remote |LDAP| users and groups.
         There can be up to 3 remote |LDAP| servers configured.

      -  For both, the Local |LDAP| scenario and the remote |LDAP| scenario, a
         |LDAP| user (or members of a |LDAP| group), can be assigned linux
         privileges via a group/role-binding to a local |prod| linux group,
         specifically one or more of the following groups: 

         -  **sudo group** - provides sudo all capabilities.

         -  **sys_protected group** - provides access to ``collect`` tool
            for collecting system diagnostic info.

            .. note::
               The ``collect`` tool also requires sudo capability.

         -  **root group** - provides read access to log files.

         The Local |LDAP| scenario and the remote |LDAP| scenario, a |LDAP| user
         can also be assigned to Kubernetes privileges through a Kubernetes 
         ClusterRoleBinding/RoleBinding to either an existing Kubernetes
         ClusterRole/Role or a new customer configured Kubernetes ClusterRole/Role.

