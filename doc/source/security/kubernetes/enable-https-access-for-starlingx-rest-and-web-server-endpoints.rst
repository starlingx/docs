
.. byh1570029392020
.. _enable-https-access-for-starlingx-rest-and-web-server-endpoints:

========================================================
HTTPS Access for StarlingX REST and Web Server Endpoints
========================================================

When secure HTTPS connectivity is enabled, HTTP is disabled.

.. rubric:: |context|

.. _enable-https-access-for-starlingx-rest-and-web-server-endpoints-ul-nt1-h5f-3kb:

.. note::
    When you change from HTTP to HTTPS, or from HTTPS to HTTP:

    -   Remote CLI users must re-source the |prod| rc file.

    -   Public endpoints are changed to HTTP or HTTPS, depending on which
        is enabled.

    -   You must change the port portion of the Horizon Web interface URL.

        For HTTPS, use https:<oam-floating-ip-address>:8443

    -   You must logout and re-login into Horizon for the HTTPS Access
        changes to be displayed accurately in Horizon.


Moving forward, the system will have HTTPS enabled by default, as the REST
API & Web Server certificate will be issued from bootstrap using the Platform
Issuer (``system-local-ca``). This certificate will be renewed by cert-manager,
and can be updated using procedure in
:ref:`migrate-platform-certificates-to-use-cert-manager-c0b1727e4e5d`.

.. note::
    
    Disabling HTTPS should be limited to strictly necessary situations, and the
    usage should be considered deprecated. The following commands can be used
    to modify the HTTPS enabled configuration:

    .. code-block:: none

        system modify -p true|false
        system modify --https_enabled true|false 