
.. xsx1607977134022
.. _keystone-accounts:

=================
Keystone Accounts
=================

|prod-long| uses Keystone for authentication and authorization of users of the
StarlingX REST APIs, the |CLI|, the Horizon Web interface and the Local Docker
Registry. |prod|'s Keystone uses the default local SQL Backend.
