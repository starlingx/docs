.. _selectively-disable-ssh-for-local-openldap-and-wad-users-e5aaf09e790c:

====================================================
Selectively Disable SSH for Local LDAP and WAD Users
====================================================

Local LDAP and |WAD| servers are used for K8s API and |SSH| authentication.
In some cases, it may be necessary to disallow |SSH| authentication for selective users or a
group of users.

The Linux group ``denyssh`` is a system created group which is preconfigured in
the |SSHD| configuration such that any member of this group is denied |SSH| access.

--------------------------------
Deny SSH Access Local LDAP Users
--------------------------------

.. rubric:: |proc|

#. Create a local |LDAP| user with the :command:`ldapusersetup` command and add
   the user to Linux group ``denyssh`` during the creation of the |LDAP| user
   account.

   Example:

   .. code-block::

       [sysadmin@controller-0 ~(keystone_admin)]$ sudo ldapusersetup
       Enter username to add to LDAP: test1
       Successfully added user test1 to LDAP
       Successfully set password for user test1
       Warning : password is reset, user will be asked to change password at login
       Add test1 to sudoer list? (yes/NO): yes
       Successfully added sudo access for user test1 to LDAP
       Add test1 to secondary user group? (yes/NO): yes
       Secondary group to add user to? [sys_protected]: denyssh
       Successfully added user test1 to group cn=denyssh,ou=Group,dc=cgcs,dc=local
       Enter days after which user password must be changed [90]:
       Successfully modified user entry uid=test1,ou=People,dc=cgcs,dc=local in LDAP
       Updating password expiry to 90 days
       Enter days before password is to expire that user is warned [2]:
       Successfully modified user entry uid=test1,ou=People,dc=cgcs,dc=local in LDAP
       Updating password expiry to 2 days

#. Verify that the new user is a member of the ``denyssh`` group.

   Example:

   .. code-block::

       [sysadmin@controller-0 ~(keystone_admin)]$ id test1
       uid=10005(test1) gid=100(users) groups=100(users),10000(denyssh)
       [sysadmin@controller-0 ~(keystone_admin)]$ groups test1
       test1 : users denyssh
       sysadmin@controller-0:~$ getent group|grep denyssh
       denyssh:x:10000:test1

#. Ssh as user ``test1``.

   The ssh should be denied.

#. Remove the user from ``denyssh`` group.

   Example:

   .. code-block::

       [sysadmin@controller-0 ~(keystone_admin)]$ sudo ldapdeleteuserfromgroup test1 denyssh
       Password:
       Successfully deleted user test1 from group cn=denyssh,ou=Group,dc=cgcs,dc=local
       [sysadmin@controller-0 ~(keystone_admin)]$ id test1
       uid=10005(test1) gid=100(users) groups=100(users)

#. Ssh as user ``test1``.

   The ssh should be allowed.

-----------------------------
Deny SSH Access for WAD Users
-----------------------------

.. rubric:: |proc|

#. Create a |WAD| group or use an existing |WAD| group for the users that
   should not have access to the platform.

   .. note::

       The |WAD| group used should have a name other than ``denyssh``.

#. Add the |WAD| user to the |WAD| group.

   .. note::

       The |WAD| user you want to deny access to should not be a member of a
       |WAD| group that has allowed access. The allowed user groups are
       configured with the |SSSD| parameter ``ldap_access_filter``. Giving and
       denying access to the user at the same time leads to inconsistent
       authentication results.

#. Map the |WAD| group to the existing Linux group ``denyssh`` following the |PAM|
   group configuration described in :ref:`add-ldap-users-to-linux-groups-using-pamcconfiguration-d31d95e255e1`.

   Example: Add the following line in ``/etc/security/group.conf`` to map the
   |WAD| group to the ``denysssh`` Linux group.

   ``*;*;%disallowed_users@wad.mydomain.com;Al0000-2400;denyssh``

#. Attempt to ssh as the |WAD| user.

   The ssh should be denied.

#. Remove the user from the |WAD| group.

   The user should be able to ssh.



