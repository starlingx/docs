
.. jgr1582125251291
.. _configure-kubernetes-client-access:

==================================
Configure Kubernetes Client Access
==================================

You can configure Kubernetes access for local and remote clients to
authenticate through Windows Active Directory or |LDAP| server using
**oidc-auth-apps** |OIDC| Identity Provider (dex).

.. _configure-kubernetes-local-client-access:

----------------------------------------
Configure Kubernetes Local Client Access
----------------------------------------

.. rubric:: |context|

Use the procedure below to configure Kubernetes access for a user logged in to
the active controller either through SSH or by using the system console.

.. rubric:: |proc|

#.  Execute the commands below to create the Kubernetes configuration file for
    the logged in user. These commands only need to be executed once. The file
    "~/.kube/config" will be created. The user referred in its contents is the
    current logged in user.

    .. code-block:: none

        ~$ kubeconfig-setup
        ~$ source ~/.profile

#.  Run **oidc-auth** script in order to authenticate and update user
    credentials in the Kubernetes configuration file.

    .. code-block:: none

        ~$ oidc-auth

    .. note::
        The **oidc-auth** script has the following optional parameters that may
        need to be specified:

        ``--cacert <path to ca cert file>``: The path provides |CA| certificate
        that validates the server certificate specified by the ``-c`` option.
        By default, the command reads the value of ``OS_CACERT`` environment
        variable. If none is specified, the command accesses the server without
        verifying its certificate.

        ``-c <OIDC_app_IP>``: This is the IP where the OIDC app is running. When
        not provided, it defaults to "oamcontroller", that is an alias to the
        controller floating |OAM| IP. There are two instances where this
        parameter is used: for local client access inside subclouds
        of a centralized setup, where the **oidc-auth-apps** runs only on the
        System Controller, and for remote client access.

        ``-p <password>``: This is the user password. If the user does not enter
        the password, the user is prompted to do so. This parameter is essential
        in non-interactive shells.

        ``-u <username>``: This is the user to be authenticated. When not
        provided, it defaults to the current logged in user. Usually, this
        parameter is needed in remote client access scenarios, where the current
        logged in user is different from the user to be authenticated.

        ``-b <backend_ID>``: This parameter is used to specify the backend used
        for authentication. It is only needed if there is more than one backend
        configured at **oidc-auth-apps** |OIDC| Identity Provider (Dex).

.. _configure-kubernetes-remote-client-access:

-----------------------------------------
Configure Kubernetes Remote Client Access
-----------------------------------------

.. rubric:: |context|

The access to the Kubernetes cluster from outside the controller can be done
using the remote CLI container or using the host directly. Both options are
described below.

.. _configure-kubernetes-remote-client-access-using-container-backed-remote-cli:

Kubernetes Remote Client Access using the Container-backed Remote CLI
=====================================================================

The steps needed to set up the remote Kubernetes access using the
container-backed remote |CLI| are described in :ref:`Configure Container-backed
Remote CLIs and Clients
<security-configure-container-backed-remote-clis-and-clients>` and
:ref:`Use Container-backed Remote CLIs and Clients
<using-container-backed-remote-clis-and-clients>`.

.. _configure-kubernetes-remote-client-access-using-the-host-directly:

Kubernetes Remote Client Access using the Host Directly
=======================================================

.. rubric:: |proc|

#.  Install the :command:`kubectl` client CLI on the host. Follow the
    instructions on `Install and Set Up kubectl on Linux
    <https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/>`__. The
    example below can be used for Ubuntu.

    .. code-block:: none

        % sudo apt-get update
        % sudo apt-get install -y apt-transport-https
        % curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
        % echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
        % sudo apt-get update
        % sudo apt-get install -y kubectl

#.  Optional: Contact your system administrator for the |prod| Kubernetes
    cluster's public root |CA| certificate. Copy this certificate to your system
    as ``k8s-ca.crt``. This step is strongly recommended, but it still possible
    to connect to the Kubernetes cluster without this certificate.

#.  Create an empty Kubernetes configuration file (the default path is
    ``~/.kube/config``). Execute the commands below to update this file. Use the
    |OAM| IP address and the Kubernetes |CA| certificate acquired in the
    previous step. If the |OAM| IP is IPv6, use the IP enclosed in brackets
    (example: "[fd00::a14:803]"). In the example below, the user is
    "admin-user", change it to the name of user you want to authenticate.

    .. code-block:: none

        $ MYUSER="admin-user"
        $ kubectl config set-cluster wrcpcluster --server=https://<OAM_IP>:6443
        $ kubectl config set clusters.wrcpcluster.certificate-authority-data $(base64 -w0 k8s-ca.crt)
        $ kubectl config set-context ${MYUSER}@wrcpcluster --cluster=wrcpcluster --user ${MYUSER}
        $ kubectl config use-context ${MYUSER}@wrcpcluster

    If you don't have the Kubernetes |CA| certificate, execute the following
    commands instead.

    .. code-block:: none

        $ MYUSER="admin-user"
        $ kubectl config set-cluster wrcpcluster --server=https://<OAM_IP>:6443 --insecure-skip-tls-verify
        $ kubectl config set-context ${MYUSER}@wrcpcluster --cluster=wrcpcluster --user ${MYUSER}
        $ kubectl config use-context ${MYUSER}@wrcpcluster

#.  Get a Kubernetes authentication token. There are two options, the first is
    through **oidc-auth** script and the second is through the browser. Both
    options are described below.

    To get the token through **oidc-auth** script, execute the steps below.

    #.  Install "Python Mechanize" module using the following command:

        .. code-block:: none

            $ sudo pip install mechanize

    #.  Install the **oidc-auth** from |dnload-loc|.

    #.  Execute the command below to get the token and update it in the
        Kubernetes configuration file. If the target environment has multiple
        backends configured, you will need to use the parameter
        ``-b <backend_ID>``. If the target environment is a |DC| system with
        a centralized setup, you should use the |OAM| IP of the System
        Controller.

        .. code-block:: none

            $ oidc-auth -u ${MYUSER} -c <OAM_IP>

    To get the token through a browser, execute the steps below.

    #.  Use the following URL to login into **oidc-auth-apps** |OIDC| client:
        ``https://<oam-floating-ip-address>:30555``. If the target environment
        is a |DC| system with a centralized setup, you should use the |OAM|
        IP of the System Controller.

    #.  If the |prod| **oidc-auth-apps** has been configured for multiple
        '**ldap**' connectors, select the Windows Active Directory or the
        |LDAP| server for authentication.

    #.  Enter your Username and Password.

    #.  Click Login. The ID token and Refresh token are displayed as follows:

        .. code-block:: none

            ID Token:

            eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ4ZjZkYjcxNGI4ODQ5ZjZlNmExM2Y2ZTQzODVhMWE1MjM0YzE1NTQifQ.eyJpc3MiOiJodHRwczovLzEyOC4yMjQuMTUxLjE3MDozMDU1Ni9kZXgiLCJzdWIiOiJDZ2R3ZG5SbGMzUXhFZ1JzWkdGdyIsImF1ZCI6InN0eC1vaWRjLWNsaWVudC1hcHAiLCJleHAiOjE1ODI1NzczMTksImlhdCI6MTU4MjU3NzMwOSwiYXRfaGFzaCI6ImhzRG1kdTFIWGFCcXFNLXBpYWoyaXciLCJlbWFpbCI6InB2dGVzdDEiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZSI6InB2dGVzdDEifQ.TEZ-YMd8kavTGCw_FUR4iGQWf16DWsmqxW89ZlKHxaqPzAJUjGnW5NRdRytiDtf1d9iNIxOT6cGSOJI694qiMVcb-nD856OgCvU58o-e3ZkLaLGDbTP2mmoaqqBYW2FDIJNcV0jt-yq5rc9cNQopGtFXbGr6ZV2idysHooa7rA1543EUpg2FNE4qZ297_WXU7x0Qk2yDNRq-ngNQRWkwsERM3INBktwQpRUg2na3eK_jHpC6AMiUxyyMu3o3FurTfvOp3F0eyjSVgLqhC2Rh4xMbK4LgbBTN35pvnMRwOpL7gJPgaZDd0ttC9L5dBnRs9uT-s2g4j2hjV9rh3KciHQ

            Access Token:

            wcgw4mhddrk7jd24whofclgmj

            Claims:

            {
            "iss": "https://128.224.151.170:30556/dex",
            "sub": "CgdwdnRlc3QxEgRsZGFw",
            "aud": "stx-oidc-client-app",
            "exp": 1582577319,
            "iat": 1582577319,
            "at_hash": "hsDmdu1HXaBqqM-piaj2iw",
            "email": "testuser",
            "email_verified": true,
            "groups": [
                "billingDeptGroup",
                "managerGroup"
            ],
            "name": "testuser"
            }

            Refresh Token:

            ChljdmoybDZ0Y3BiYnR0cmp6N2xlejNmd3F5Ehlid290enR5enR1NWw1dWM2Y2V4dnVlcHli

    #.  Use the token ID to set the Kubernetes credentials in kubectl configs:

        .. code-block:: none

            $ TOKEN=<ID_token_string>
            $ kubectl config set-credentials ${MYUSER} --token ${TOKEN}
