.. _local-ldap-user-password-expiry-mechanism-eba5d34abbd4:

=======================================
Local LDAP user password expiry control
=======================================

.. rubric:: |context|

Password expiry control for local openLDAP users is based on a |SSSD| solution.
The solution uses |SSSD| configuration for a shadow password expiry policy
based on openLDAP password policy control schema, see https://www.ietf.org/archive/id/draft-behera-ldap-password-policy-10.html.

-----------------------------------------------------------------
Setting shadow password expiry information in local |LDAP| server
-----------------------------------------------------------------

When creating a local |LDAP| user with :command:`localusersetup` command or
using the |LDAP| playbook, the following input data needs to be provided:

For example:

Enter days after which user password must be changed [90]: 30
Successfully modified user entry uid=usertest1,ou=People,dc=test,dc=local in |LDAP|
Updating password expiry to 30 days.

In the example above this input sets `shadowMax` attribute for the |LDAP| user
"usertest1".

For example:

Enter days before the password is to expire that user is warned [2]: 14
Successfully modified user entry uid=usertest1,ou=People,dc=test,dc=local in LDAP
Updating password expiry to 14 days.

In the example above this input sets `shadowWarning` attribute for the |LDAP| user
"usertest1".

.. important::

    Always set the `shadowMax`` value higher than the `shadowWarning` value.

The |LDAP| user entry can be checked with the following command:

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ /var/home/sysadmin# sudo ldapfinger usertest1

    dn: uid=usertest1,ou=People,dc=test,dc=local
    objectClass: account
    objectClass: posixAccount
    objectClass: shadowAccount
    objectClass: top
    cn: usertest1
    uid: usertest1
    uidNumber: 10002
    gidNumber: 100
    homeDirectory: /home/usertest1
    gecos: usertest1
    description: User account
    loginShell: /bin/bash
    shadowMax: 30
    shadowWarning: 14
    userPassword: <encrypted password>
    shadowLastChange: 19574

.. note::

    |SSSD| has been configured to use the shadow password policy by setting
    parameter ``ldap_pwd_policy=shadow``. As a result, the |SSSD| parameters,
    ``ldap_user_shadow_max`` and ``ldap_user_shadow_warning`` map to the |LDAP|
    user attributes "shadowMax" and "shadowWarning" respectively.

--------------------------------------------
Password Expiry behavior on a running system
--------------------------------------------

Every node where |SSSD| client is running, in either standalone or Distributed
Cloud system configurations, the following behavior will be observed.

-  At login time, including |SSH| connections, the user will be prompted to
   change the password when password has expired according to the expiry period
   set at user account creation (shadowMax).

-  A password expiration warning will appear at login time to notify the user in
   advance, according to the configured warning time set at the user account
   creation (shadowWarning).

-  A warning that the password has expired is logged in "/var/log/auth.log".

Depending on how much time is left until the password expiration, the user will
be notified with the time left until expiry.

-  If there is more than one day until expiry, the user will be notified with
   the number of days left.

-  If there is less than a day till password expiry, the notification will be
   in number of hours.

-  If it is less than one hour until expiry the notification will be in number
   of minutes.

For example,

-  "Your password will expire in 2 day(s)." or

-  "Your password will expire in 45 minute(s)."

.. note::

    The password expiry warning is displayed as a duplicate message at login.

    For example:

    usertest2@<host IP address>'s password:
    Your password will expire in 1 day(s).
    Your password will expire in 1 day(s).

    This is normal behavior because the messages are posted by 2 different sources
    that check password expiry.

    The source of the 2 notifications for password expiry can be found in the
    "/var/log/auth.log". One notification comes from "sshd" and the 2nd from\
    "sudo".

    Example:

    2023-08-09T02:25:29.375 controller-0 sshd[1788048]: info pam_sss(sshd:account):
    User info message: Your password will expire in 21 hour(s).

    2023-08-09T02:25:29.377 controller-0 sshd[1788048]: info Accepted password
    for usertest3 from <ip addr> port <port_number> ssh2

    ...

    2023-08-09T02:27:03.284 controller-0 sudo: info pam_sss(sudo:account): User
    info message: Your password will expire in 21 hour(s).

.. note::

    Password expiration warning gets triggered when the regular |LDAP|
    user is using ssh to connect to the node the user needs access to and its
    password has reached the threshold for a warning before it expires. The first
    warning message may occur with a delay. This is due to time calculation and
    synchronization mechanism with the |LDAP| server.


