
.. vaq1552681912484
.. _create-ldap-linux-accounts:

==========================
Create LDAP Linux Accounts
==========================

|prod| includes a script for creating |LDAP| Linux accounts.

.. rubric:: |context|

.. note::
    For security reasons, it is recommended that ONLY admin level users be
    allowed to |SSH| to the nodes of the |prod|. Non-admin level users should
    strictly use remote CLIs or remote web GUIs.

.. note::

    In a |prod-dc| system configuration, the :command:`ldapusersetup` command
    and other |LDAP| commands that are used to update data on the |LDAP|
    server, are supported only on System Controller. These commands are not
    supported on subclouds. This is because bind with password file is
    supported only from System Controller. On subclouds, only bind anonymously
    to the |LDAP| server is supported, thus, only the commands that read
    information can be executed.

The :command:`ldapusersetup` command provides an interactive method for setting
up |LDAP| Linux user accounts.

Centralized management is implemented using two |LDAP| servers, one running on
each controller node. |LDAP| server synchronization is automatic using the
native |LDAP| content synchronization protocol.

A set of |LDAP| commands is available to operate on |LDAP| user accounts. The
commands are installed in the directory /usr/local/sbin, and are available to
any user account in the sudoers list. Included commands are
:command:`lsldap`, :command:`ldapadduser`, :command:`ldapdeleteuser`, and
several others starting with the prefix :command:`ldap`.

Use the command option --help on any command to display a brief help message,
as illustrated below.

.. code-block:: none

    $ ldapadduser --help
    Usage : /usr/local/sbin/ldapadduser <username> <groupname | gid> [uid]
    $ ldapdeleteuser --help
    Usage : /usr/local/sbin/ldapdeleteuser <username | uid>

.. rubric:: |prereq|

For convenience, identify the user's Keystone account user name in |prod-long|.

.. rubric:: |proc|

#.  Log in as **sysadmin**, and start the :command:`ldapusersetup` script.

    .. code-block:: none

        controller-0: ~$ sudo ldapusersetup

#.  Follow the interactive steps in the script.


    #.  Provide a user name.

        .. code-block:: none

            Enter username to add to LDAP: teamadmin

        .. code-block:: none

            Successfully added user teamadmin to LDAP
            Successfully set password for user teamadmin
            Warning : password is reset, user will be asked to change password at login

    #. Specify whether the user should have sudo capabilities or not.
       Enabling ``sudo`` privileges allows the LDAP users to execute the
       following operations:

       - ``sw_patch`` to unauthenticated endpoint
       - ``docker`` and/or ``crictl`` commands to communicate with the respective daemons
       - Utilities ``show-certs.sh`` and ``license-install`` (recovery only)
       - IP configuration for local network setup
       - Password change of local openldap users
       - Access to restricted files, example: restricted logs
       - Manual reboots

       .. code-block:: none

           Add teamadmin to sudoer list? (yes/NO): yes
           Successfully added sudo access for user teamadmin to LDAP

       .. note::

          There is another procedure to add ``sudo`` capabilities to a local |LDAP|
          linux account. For details, see :ref:`add-ldap-users-to-linux-groups-using-pamcconfiguration-d31d95e255e1`.
          It is recommended to use either of the procedures but not both to
          avoid overlapping.

    #.  Specify a secondary user group for this |LDAP| user. For example, ``sys_protected group``.

        The purpose of having OpenLDAP/WAD users as a part of the ``sys_protected`` group on the |prod|
        platform is to allow them to execute the |prod| system operations via
        ``source/etc/platform/openrc``. The LDAP user in the ``sys_protected`` group will be
        equivalent to the special ``sysadmin`` bootstrap user, and will have the following:

        - Keystone admin/admin identity and credentials
        - Kubernetes ``/etc/kubernetes/admin.conf`` credentials

        .. code-block:: none

            Add teamadmin to secondary user group? (yes/NO): yes
            Secondary group to add user to? [sys_protected]:
            Successfully added user teamadmin to group cn=sys_protected,ou=Group,dc=cgcs,dc=local

        .. note::

            There is another procedure to add ``sys_protected`` capabilities to
            a local |LDAP| linux account. For details, see
            :ref:`add-ldap-users-to-linux-groups-using-pamcconfiguration-d31d95e255e1`.
            It is recommended to use either of the procedures but not both to
            avoid overlapping.

    #.  Change the password duration.

        .. code-block:: none

            Enter days after which user password must be changed [90]:

        .. code-block:: none

            Successfully modified user entry uid=ldapuser1, ou=People, dc=cgcs, dc=local in LDAP
            Updating password expiry to 90 days

    #.  Change the warning period before the password expires.

        .. code-block:: none

            Enter days before password is to expire that user is warned [2]:

        .. code-block:: none

            Successfully modified user entry uid=teamadmin,ou=People,dc=cgcs,dc=local in LDAP
            Updating password expiry to 2 days


On completion of the script, the command prompt is displayed.

.. code-block:: none

    controller-0: ~$


.. rubric:: |result|

The Local |LDAP| account is created. For information about the user login
process, see :ref:`For StarlingX and Platform OpenStack CLIs from a Local LDAP
Linux Account Login <establish-keystone-credentials-from-a-linux-account>`.

For managing composite Local |LDAP| Accounts (i.e. with associated Keystone and
Kubernetes accounts) for a standalone cloud or a distributed cloud, see
:ref:`Manage Composite Local LDAP Accounts at Scale
<manage-local-ldap-39fe3a85a528>`.
