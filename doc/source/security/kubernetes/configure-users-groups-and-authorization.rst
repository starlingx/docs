
.. rzl1582124533847
.. _configure-users-groups-and-authorization:

==========================================
Configure Users, Groups, and Authorization
==========================================

In the examples provided below, Kubernetes permissions will be given to
**testuser** user. Two different ways to do this are presented: in the first
option, **testuser** user is directly bound to a role; in the second option,
**testuser** is indirectly associated to a Kubernetes group that has
permissions.

.. note::
   For bigger environments, like a |DC| with many subclouds, or to minimize
   Kubernetes custom cluster configurations, use the second option, where
   permissions are granted through Kubernetes groups.

.. _configure-users-groups-and-authorization-option-1-b2f-ck4-dlb:

--------------------------------------------------------
Grant Kubernetes permissions through direct role binding
--------------------------------------------------------

#.  Create the following deployment file and deploy the file with
    :command:`kubectl apply -f` <filename>.

    .. code-block:: none

        kind: ClusterRoleBinding
        apiVersion: rbac.authorization.k8s.io/v1
        metadata:
         name: testuser-rolebinding
        roleRef:
         apiGroup: rbac.authorization.k8s.io
         kind: ClusterRole
         name: cluster-admin
        subjects:
        - apiGroup: rbac.authorization.k8s.io
          kind: User
          name: testuser

.. _configure-users-groups-and-authorization-option-2-b2f-dk4-dlb:

-------------------------------------------
Grant Kubernetes permissions through groups
-------------------------------------------

#.  Create the following deployment file and deploy the file with
    :command:`kubectl apply -f` <filename>.

    .. code-block:: none

        apiVersion: rbac.authorization.k8s.io/v1
        kind: ClusterRole
        metadata:
          name: cluster-reader-role
        rules:
        - apiGroups: ["*"]
          resources: ["*"]
          verbs: ["get", "watch", "list"]
        ---
        apiVersion: rbac.authorization.k8s.io/v1
        kind: ClusterRoleBinding
        metadata:
          name: cluster-reader-rolebinding
        subjects:
        - kind: Group
          name: k8s-reader
          apiGroup: rbac.authorization.k8s.io
        roleRef:
          kind: ClusterRole
          name: cluster-reader-role
          apiGroup: rbac.authorization.k8s.io
        ---
        # Note: the ClusterRole "cluster-admin" already exists in the system.
        apiVersion: rbac.authorization.k8s.io/v1
        kind: ClusterRoleBinding
        metadata:
          name: cluster-admin-rolebinding
        subjects:
        - kind: Group
          name: k8s-admin
          apiGroup: rbac.authorization.k8s.io
        roleRef:
          kind: ClusterRole
          name: cluster-admin
          apiGroup: rbac.authorization.k8s.io

#.  Create the groups **k8s-reader** and **k8s-admin** in your Windows Active
    Directory or |LDAP| server. See `Microsoft documentation on Windows Active
    Directory
    <https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/get-started/
    virtual-dc/active-directory-domain-services-overview>`__ for additional
    information on adding users and groups to Windows Active Directory.

#.  To give Kubernetes permissions to **testuser**, add this user in either the
    **k8s-reader** or **k8s-admin** groups in your Windows Active Directory or
    |LDAP| server, depending on the permissions you want to grant. The
    permissions are given because there is a mapping between a Windows Active
    Directory or |LDAP| group and a Kubernetes group with same name. To remove
    Kubernetes permissions from **testuser** user, remove this user from
    **k8s-reader** and **k8s-admin** groups in your Windows Active Directory or
    |LDAP| server.

    .. note::
       The group names **k8s-reader** and **k8s-admin** are arbitrary. As long
       as the Windows Active Directory or LDAP group have the same name as the
       Kubernetes group, the mapping will happen. For example, if a more
       company-specific approach is preferred, the groups **k8s-reader** and
       **k8s-admin** groups could be named after departments, like
       **billingDeptGroup** and **managerGroup**.
