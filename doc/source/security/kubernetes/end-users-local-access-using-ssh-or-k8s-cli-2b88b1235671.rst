.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _end-users-local-access-using-ssh-or-k8s-cli-2b88b1235671:

=========================================================
End Users - Test Local Access using SSH or Kubernetes CLI
=========================================================

After creating end users, test their access to the the Kubernetes commands / resources
and linux access.

.. rubric:: |prereq|

-  You should already have created at least one end user.

-  You need to perform this procedure using an end user.

.. rubric:: |proc|

#.   Login to the active controller as an end user, ``steveenduser`` in this
     example.

     Use either a local console or |SSH|.

#.   Test access to linux commands (admin and non-admin) using the following commands:

     .. code-block::

          # Creating user requires sudo

          $ sudo ldapusersetup -u johnsmith
          steveenduser is not allowed to run sudo on controller-0.  This incident will be reported.

          # Listing IP interfaces does not require admin privileges

          $ ip link show
          1: lo: <LOOPBACK,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
          link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
          2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
          link/ether 08:00:27:39:06:4e brd ff:ff:ff:ff:ff:ff
          3: enp0s8: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
          link/ether 08:00:27:38:8b:7c brd ff:ff:ff:ff:ff:ff
          ...

#. Test access to kubernetes commands / resources using the following steps:

   #.   Use ``kubeconfig-setup`` to setup ``KUBECONFIG`` for local environment.

        .. code-block::

           $ kubeconfig-setup
           $ source ~/.profile

   #.   Use ``oidc-auth`` to authenticate through |OIDC|/|LDAP|.

        .. code-block::

           $ oidc-auth
           Using "steveenduser" as username.
           Password:
           Successful authentication.
           Updated /home/johnsmith/.kube/config .

   #.   Use ``kubectl`` to test access to kubernetes commands / resources
        (admin and non-admin).

        .. code-block::

               # Displaying anything in 'kube-system' namespace requires 'cluster-admin' privileges

               $ kubectl -n kube-system get secrets
               Error from server (Forbidden): secrets is forbidden: User "steveenduser" cannot list resource "secrets" in API group "" in the namespace "kube-system"

               # Should be able to display resources in his own namespace, 'ABC-ns'

               $ kubectl -n abc-ns get all
               NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
               service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   3d9h

#.   Test access to |prod| commands / resources.

     .. code-block::

          $ source local_starlingxrc
          Enter the password to be used with Keystone user steveenduser:
          Created file /home/johnsmith/steveenduser-openrc

          $ system host-list
          Must provide Keystone credentials or user-defined endpoint and token, error was: The request you have made requires authentication. (HTTP 401) (Request-ID: req-0feb292f-d649-4d9f-8e60-f63643265207)

.. rubric:: |postreq|

Setup remote access for any end users requiring remote access. See :ref:`remote-access-2209661be417`.
