.. _linux-accounts-password-3dcad436dce4:

=============================
Linux Accounts Password Rules
=============================

.. rubric:: Check Current Password Expiry Settings

Before making any changes, you may want to check the current password expiry
settings for the user. You can do this by running the :command:`chage -l
<username>` command, replacing ``<username>`` with the name of the user whose
password expiry settings you want to view.

.. code-block:: none

    sudo chage -l <username>

.. rubric:: Change Password Expiry Settings

To change the password expiry period of Linux accounts, run the
:command:`chage` command, as below:

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ sudo chage -M <days_to_expiry> <username>

For example, to set the maximum number of days before the password must be
changed to 60 days for a user named ``sysadmin``, you can use the following
command:

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ sudo chage -M 60 sysadmin


Verify Changes
--------------

After making the changes, verify that the new password expiry settings have
been applied by running the :command:`chage -l <username>` command again.

.. code-block:: none

    chage -l <username>

For the example above of user ``sysadmin`` and expiry period of 60 days, the
output of ``chage -l <username>`` should be as follows:

.. code-block:: none

    [sysadmin@controller-0 ~(keystone_admin)]$ chage -l sysadmin
    Last password change                                : abr 30, 2024
    Password expires                                    : jun 29, 2024
    Password inactive                                   : never
    Account expires                                     : never
    Minimum number of days between password change      : 0
    Maximum number of days between password change      : 60
    Number of days of warning before password expires   : 7