.. _local-ldap-certificates-4e1df1e39341:

=======================
Local LDAP Certificates
=======================

The local |LDAP| server by default serves both HTTPS on port 636 and HTTP on port 389.

The HTTPS server certificate is issued by cert-manager ClusterIssuer
``system-local-ca`` and is managed internally by cert-manager. The certificate
will be automatically renewed when the expiration date approaches. The
certificate is called ``system-openldap-local-certificate`` with its secret
having the same name ``system-openldap-local-certificate`` in the
``deployment`` namespace. The server certificate and private key files are
stored in the ``/etc/ldap/certs/`` system directory.

In |DC| system, the |LDAP| service runs only in the central cloud. Clients in
the subcloud (|SSSD|, |LDAP| client tools) are configured so that they can
access the |LDAP| services in the central cloud using HTTPS. Thus,
``system-local-ca`` ClusterIssuer's certificate is installed in the subcloud as
a trusted |CA| certificate.

The insecure HTTP service is only supported for backward compatibility with
subclouds running older versions of |prod| that supports only HTTP. If no such
subclouds are present, the insecure HTTP service can be disabled by system
service parameter.

Run the following command to disable the insecure service:

.. code-block:: none

    ~(keystone_admin)]$ system service-parameter-add identity local-openldap insecure_service=disabled

If the service parameter already exists, run the following command:

.. code-block:: none

    ~(keystone_admin)]$ system service-parameter-modify identity local-openldap insecure_service=disabled

The insecure service can be enabled if it has been disabled. Run the following
command to enable the insecure service:

.. code-block:: none

    ~(keystone_admin)]$ system service-parameter-modify identity local-openldap insecure_service=enabled

After disabling or enabling the insecure local-openldap service, for the change
to take effect, apply the service parameter by running the following command:

.. code-block:: none

    ~(keystone_admin)]$ system service-parameter-apply identity --section local-openldap
