.. _configure-docker-registry-certificate-after-installation-c519edbfe90a:

=====================================
Configure Docker Registry Certificate
=====================================

During installation, the Platform Issuer (``system-local-ca``) will
automatically issue a certificate used to secure access to the Local Docker
Registry API. After bootstrap, this certificate's fields can be updated using
the procedure
:ref:`migrate-platform-certificates-to-use-cert-manager-c0b1727e4e5d`. The
certificate will be managed by cert-manager (auto renewed upon expiration).

This certificate will be stored in a Kubernetes |TLS| secret in namespace
``deployment``, named ``system-registry-local-certificate``. It will be managed
by cert-manager, renewed upon expiration and the required services restarted
automatically.

The certificate will be anchored by system-local-ca's Root |CA|. For more
information, refer to
:ref:`system-local-ca-issuer-9196c5794834`.