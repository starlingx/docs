.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _create-end-users-359693b84854:

================
Create End Users
================

After setting up your system administrators, use a ``Level1SystemAdmin``
system administrator to configure 'end users'.

In the following example, creating end users consists of:

-  Create a general end user kubernetes ``ClusterRole`` with restricted
   kubernetes capabilities.

-  For one or more specific end user groups:

   -  Create an |LDAP| group;

   -  You can disable the |SSH| access for this |LDAP| group (i.e.,
      restricting these end users to only use remote CLIs / APIs / GUIs);

   -  Create a Kubernetes namespace for the group;

   -  Bind the general end user kubernetes cluster role to the |LDAP| group
      for this kubernetes namespace,

   -  Create one or more |LDAP| users in this |LDAP| group.

.. rubric:: |prereq|

-  You should already have created a system administrator.

-  You need to perform this procedure using the ``Level1SystemAdmin`` system administrator.

.. rubric:: |proc|

#.   Login to the active controller as a ``Level1SystemAdmin`` system administrator,
     ``joefulladmin`` in this example.

     Use either a local console or |SSH|.


#.   Use the ``local_starlingxrc`` to setup |prod| environment variables
     and to setup your keystone user's authentication credentials.

     .. code-block::

        $ source local_starlingxrc

        Enter the password to be used with keystone user joefulladmin:
        Created file /home/joefulladmin/joefulladmin-openrc

#.   Use the ``oidc-auth`` to authenticate via |OIDC|/|LDAP| for kubernetes CLI.

     .. code-block::

        $ oidc-auth
        Using "joefulladmin" as username.
        Password:
        Successful authentication.
        Updated /home/joefulladmin/.kube/config.


#.   Create a directory for temporary files for setting up users and groups.

     .. code-block::

         $ mkdir /home/joefulladmin/users


#.   Create a general end user kubernetes ``ClusterRole`` with restricted
     kubernetes authorization privileges.

     .. code-block::

         $ cat << EOF > /home/joefulladmin/users/GeneralEndUser-ClusterRole.yml
         apiVersion: rbac.authorization.k8s.io/v1
         kind: ClusterRole
         metadata:
            name: GeneralEndUser
         rules:
         # For the core API group (""), allow full access to all resource types
         # EXCEPT for resource policies (limitranges and resourcequotas) only allow read access
         - 	apiGroups: [""]
            resources: ["bindings", "configmaps", "endpoints", "events", "persistentvolumeclaims", "pods", "podtemplates", "replicationcontrollers", "secrets", "serviceaccounts", "services"]
            verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
         - 	apiGroups: [""]
            resources: [ "limitranges", "resourcequotas" ]
            verbs: ["get", "list"]
         # Allow full access to all resource types of the following explicit list of apiGroups.
         # Notable exceptions here are:
         #     ApiGroup                      ResourceTypes
         #     -------                       -------------
         #     policy                        podsecuritypolicies, poddisruptionbudgets
         #     networking.k8s.io             networkpolicies
         #     admissionregistration.k8s.io  mutatingwebhookconfigurations, validatingwebhookconfigurations
         #
         - 	apiGroups: ["apps", "batch", "extensions", "autoscaling", "apiextensions.k8s.io", "rbac.authorization.k8s.io"]
            resources: ["*"]
            verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
         # Cert Manager API access
         - 	apiGroups: ["cert-manager.io", "acme.cert-manager.io"]
            resources: ["*"]
            verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
         EOF

         $ kubectl apply -f /home/joefulladmin/users/GeneralEndUser-ClusterRole.yml

#.   For one or more specific end user groups, create an |LDAP| group, Kubernetes
     namespace and one or more |LDAP| users.

     #.   Create a new local |LDAP| group for the end user group.

          .. code-block::

               $ sudo ldapaddgroup ABC-EndUsers


     #.   |Optional| Disallow this local |LDAP| group from using |SSH|.

          Users of this |LDAP| group can only use the remote kubernetes
          API/CLI/GUI. Update the ``/etc/security/group.conf`` with |LDAP| group
          mappings.

          .. note::

               If it is |AIO-DX| controller configuration, disallow this local
               |LDAP| group on both controllers.

          .. code-block::

               $ sudo sed -i '$ a\*;*;%ABC-EndUsers;Al0000-2400;denyssh' /etc/security/group.conf

     #.   Create a kubernetes namespace for the end user group.

          .. code-block::

               $ kubectl create namespace abc-ns

          #.   Bind the ``GeneralEndUser`` role to this |LDAP| group for this
               kubernetes namespace.

               .. code-block::

                  $ cat << EOF > /home/joefulladmin/users/ABC-EndUsers-rolebinding.yml
                  apiVersion: rbac.authorization.k8s.io/v1
                  kind: RoleBinding
                  metadata:
                     name: ABC-EndUsers
                     namespace: abc-ns
                  subjects:
                  - 	kind: Group
                     name: ABC-EndUsers
                     apiGroup: rbac.authorization.k8s.io
                  roleRef:
                     kind: ClusterRole
                     name: GeneralEndUser
                     apiGroup: rbac.authorization.k8s.io
                  EOF

                  $ kubectl apply -f  /home/joefulladmin/users/ABC-EndUsers-rolebinding.yml

     #.   Create one or more |LDAP| users for the end user group.

          .. code-block::

               $ sudo ldapusersetup -u steveenduser
               Password:
               Successfully added user steveenduser to LDAP
               Successfully set password for user steveenduser
               Warning : password is reset, user will be asked to change password at login
               Successfully modified user entry uid=steveenduser,ou=People,dc=cgcs,dc=local in LDAP
               Updating password expiry to 90 days
               Successfully modified user entry uid=steveenduser,ou=People,dc=cgcs,dc=local in LDAP
               Updating password expiry to 2 days

               $ sudo ldapaddusertogroup steveenduser ABC-EndUsers

     #.   Repeat the ``Create one or more LDAP users for the end user group`` step
          for the next user in this end user group.

#.   Repeat the ``For one or more specific end user groups, create an LDAP group, kubernetes
     namespace and one or more LDAP users`` step for the next end user group.

.. rubric:: |postreq|

The end user created is able to, optionally, use SSH on the system to execute
kubernetes |CLI| commands to manage the hosted containerized application and
execute Linux commands. See section: :ref:`end-users-local-access-using-ssh-or-k8s-cli-2b88b1235671`.

.. note::

   More setup is required for end user to use remote CLIs/GUIs, see section
   :ref:`remote-access-2209661be417`.
