.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _end-user-access-k8s-remote-cli-7bb5b71ed604:

=======================================
End User - Access Kubernetes Remote CLI
=======================================

Access your |prod| system through kubernetes remote |CLI| on your Linux-based
system.

.. rubric:: |prereq|

You need to have a Linux-based system that has configured the Kubernetes remote
|CLI|. See section: :ref:`end-user-configure-k8s-remote-cli-fad235bb7a18`.

.. rubric:: |proc|

#. Update your |OIDC| token in your ``KUBECONFIG``.

   .. code-block::

      $ ./oidc-auth -u <StarlingX-LDAP-Username> -c <OAM-FLOATING-IP>

#. Test kubernetes remote |CLI| commands.

   .. code-block::

      $ kubectl get all
