.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _system-administrator-configure-system-remote-cli-and-7b814d8937df:

==========================================================================
System Administrator - Configure System Remote CLI & Kubernetes Remote CLI
==========================================================================

Configure the |prod| remote |CLI| and kubernetes remote |CLI| on your
Linux-based system so that you can remotely access your |prod| system through
remote CLI commands.

This procedure should be performed on your Linux-based system.

.. rubric:: |prereq|

-  You need to have a Linux-based system with python installed and either with
   Docker installed or 'sudo' capability to install Docker.

-  You need to have the ``stx-remote-access.tar`` file from your system administrator,
   containing system information related to your |prod| system.

.. rubric:: |proc|

#.   Install Docker on your Linux-based system. The following example is for ubuntu.

     .. code-block::

        # Add Docker's official GPG key:
        $ sudo apt-get update
        $ sudo apt-get install ca-certificates curl
        $ sudo install -m 0755 -d /etc/apt/keyrings
        $ sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
        $ sudo chmod a+r /etc/apt/keyrings/docker.asc

        # Add the repository to Apt sources:
        $ echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
        $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
        $ sudo apt-get update

        # Install Docker Packages
        $ sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

        # Verify that the Docker Engine installation is successful by running the hello-world image.
        $ sudo docker run hello-world

        # Manage docker as non-root user
        $ sudo groupadd docker
        $ sudo usermod -aG docker $USER
        $ sudo reboot

#.   Download and extract the |prod| remote CLI tar file from the |prod| site.

     .. code-block::

        $ cd ~
        $ wget https://mirror.starlingx.cengn.ca/mirror/starlingx/master/debian/monolithic/latest_build/outputs/remote-cli/stx-remote-cli-1.0.tgz
        $ tar xvf stx-remote-cli-1.0.tgz

#.   Extract the |prod| system information from the ``stx-remote-access-info.tar``
     file from your |prod| system administrator.

     .. code-block::

        # Files from your StarlingX System Administrator
        $ ls ~/stx-remote-access-info.tar

        $ cd ~/remote_cli
        $ tar xvf ~/stx-remote-access-info.tar

#.   Update the ``starlingxrc`` file.

     .. code-block::

         $ vi ~/remote_cli/stx-remote-access-info/starlingxrc
            // and change YOURUSERNAMEHERE to your StarlingX LDAP Username, everywhere in file

#.   Update the ``KUBECONFIG`` file.

     .. code-block::

         $ vi ~/remote_cli/stx-remote-access-info/kubeconfig
            // and change YOURUSERNAMEHERE to your StarlingX LDAP Username, everywhere in file

#.   Configure the containerized remote CLI clients.

     .. code-block::

        $ ./configure_client.sh -t platform -r ${HOME}/remote_cli/stx-remote-access-info/starlingxrc -k ${HOME}/remote_cli/stx-remote-access-info/kubeconfig -w ${HOME}/remote_cli -p docker.io/starlingx/stx-platformclients:stx.9.0-v1.5.13

.. rubric:: |postreq|

-  Access the |prod| remote CLI and kubernetes remote CLI.

-  See :ref:`system-administrator-access-system-remote-cli-and-k8s-3807c4f96c87`
