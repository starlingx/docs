.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _ipsec-clis-5f38181d077f:

==========
IPsec CLIs
==========

``swanctl`` is strongSwan's configuration, controlling and monitoring command
line interface. It has to be run as root or by sudo.

The command :command:`swanctl --help` will show all the available subcommands.

.. code-block:: none

    ~(keystone_admin)]$ sudo swanctl --list --help 

    usage:
    swanctl --counters         (-C)  list or reset IKE event counters
    swanctl --initiate         (-i)  initiate a connection
    swanctl --terminate        (-t)  terminate a connection
    swanctl --rekey            (-R)  rekey an SA
    swanctl --redirect         (-d)  redirect an IKE_SA
    swanctl --uninstall        (-u)  uninstall a trap or shunt policy
    swanctl --install          (-p)  install a trap or shunt policy
    swanctl --list-sas         (-l)  list currently active IKE_SAs
    swanctl --monitor-sa       (-m)  monitor for IKE_SA and CHILD_SA changes
    swanctl --list-pols        (-P)  list currently installed policies
    swanctl --list-authorities (-B)  list loaded authority configurations
    swanctl --list-conns       (-L)  list loaded configurations
    swanctl --list-certs       (-x)  list stored certificates
    swanctl --list-pools       (-A)  list loaded pool configurations
    swanctl --list-algs        (-g)  show loaded algorithms
    swanctl --flush-certs      (-f)  flush cached certificates
    swanctl --load-all         (-q)  load credentials, authorities, pools and connections
    swanctl --load-authorities (-b)  (re-)load authority configuration
    swanctl --load-conns       (-c)  (re-)load connection configuration
    swanctl --load-creds       (-s)  (re-)load credentials
    swanctl --load-pools       (-a)  (re-)load pool configuration
    swanctl --log              (-T)  trace logging output
    swanctl --version          (-v)  show version information
    swanctl --stats            (-S)  show daemon stats information
    swanctl --reload-settings  (-r)  reload daemon strongswan.conf
    swanctl --help             (-h)  show usage information

``swanctl`` CLIs can be used for checking IPsec status and verifying configurations.
Do not make any changes to IPsec using these commands.

