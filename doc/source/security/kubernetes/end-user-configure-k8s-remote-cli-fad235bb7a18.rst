.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _end-user-configure-k8s-remote-cli-fad235bb7a18:

==========================================
End User - Configure Kubernetes Remote CLI
==========================================

Configure the kubernetes remote |CLI| on your Linux-based system to enable access
to the |prod| system kubernetes remote |CLI| commands.

This procedure should be performed on your Linux-based system.

.. rubric:: |prereq|

-  You need to have a Linux-based system with python installed.

-  You need to have the ``stx-remote-access.tar`` file from your system administrator,
   containing system information related to your |prod| system.

.. rubric:: |proc|

#.   Install the ``kubectl`` client CLI on the host.

     Follow the instructions on `Install and Set Up kubectl on
     Linux <https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/>`.

     The example below can be used for Ubuntu.

     .. code-block::

        $ sudo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
        $ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

#.   Download the ``oidc-auth`` shell script from |prod| site and install
     python mechanize.

     .. code-block::

        $ wget`https://mirror.starlingx.cengn.ca/mirror/starlingx/master/debian/monolithic/latest_build/outputs/remote-cli/ <https://mirror.starlingx.cengn.ca/mirror/starlingx/master/debian/monolithic/latest_build/outputs/remote-cli/stx-remote-cli-1.0.tgz>`__\ oidc-auth
        $ chmod a+rx oidc-auth

        $ sudo apt install python3-pip
        $ sudo pip install mechanize

#.   Extract the |prod| system information from the
     ``stx-remote-access-info.tar`` file from your |prod| system administrator.

     .. code-block::

        # Files from your StarlingX System Administrator
        $ ls ~/stx-remote-access-info.tar

        $ tar xvf ~/stx-remote-access-info.tar

#.   Update the ``KUBECONFIG`` file.

     .. code-block::

         $ mkdir ~/.kube
         $ cp ~/stx-remote-access-info/kubeconfig ~/.kube/config
         $ vi ~/.kube/config
            // and change YOURUSERNAMEHERE to your StarlingX LDAP Username, everywhere in file

         # Add ~/stx-remote-access-info/stx.ca.crt to the list of Trusted CA 
         #       e.g. commands shown for ubuntu below
         $ sudo cp ~/stx-remote-access-info/stx.ca.crt /usr/local/share/ca-certificates
         $ sudo update-ca-certificates

         # Authenticate with OIDC/LDAP on StarlingX ... and token will be put in ~/.kube/config
         $ ./oidc-auth -u <StarlingX-LDAP-Username> -c <OAM-FLOATING-IP>

.. rubric:: |postreq|

Access the kubernetes remote |CLI|.

See :ref:`end-user-access-k8s-remote-cli-7bb5b71ed604`.
