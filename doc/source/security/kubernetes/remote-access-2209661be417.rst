.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _remote-access-2209661be417:

=============
Remote Access
=============

This section provides a procedure for a system administrator to collect system
and user information required for a user to connect remotely to |prod|.
It also provides procedures for system administrators and end users to remotely
connect to |prod| CLIs, kubernetes CLIs and GUIs.
