
.. yda1607977206655
.. _remote-windows-active-directory-accounts:

========================================
Remote Windows Active Directory accounts
========================================

|prod| can optionally be configured to use remote Windows Active Directory
accounts and native Kubernetes |RBAC| policies for authentication and
authorization of users of the Kubernetes API, |CLI|, and Dashboard.

.. _user-authentication-using-windows-active-directory-security-index:

See :ref:`Overview of LDAP Servers <overview-of-ldap-servers>` for more details.

