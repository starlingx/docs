.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _ipsec-configuration-and-enabling-f70964bc49d1:

==========================
Configure and Enable IPsec
==========================

IPsec is configured and enabled on management network for multi node systems
by default during system deployment. For the first controller, it is configured
and enabled by ansible playbook during bootstrap. For the rest of the nodes in
the system, it is configured and enabled at first reboot during the host 
installation.

IPsec status can be verified by ``swanctl`` CLIs, refer to :ref:`ipsec-clis-5f38181d077f` section for useful commands.

The most useful command to check IPsec status is: :command:`swanctl --list-sa` 

This command lists the established IPsec connections and SAs (Security Associations).

An example output is as following:

.. code-block:: none

    ~(keystone_admin)]$ sudo swanctl --list-sa
    Password:
    system-nodes: #162, ESTABLISHED, IKEv2, 7e224579c2034a09_i ad8a74ef1621ebcb_r*
    local  'CN=ipsec-controller-0' @ 192.168.101.2[500]
    remote 'CN=ipsec-controller-1' @ 192.168.101.4[500]
    AES_CBC-128/HMAC_SHA2_256_128/PRF_AES128_XCBC/MODP_3072
    established 1054s ago, rekeying in 1589s, reauth in 9033s
    node: #7, reqid 2, INSTALLED, TRANSPORT, ESP:AES_GCM_16-128
        installed 671s ago, rekeying in 2622s, expires in 3289s
        in  c61b1765, 1107991 bytes, 10275 packets,     0s ago
        out c38189c2, 113928 bytes,  1332 packets,   616s ago
        local  192.168.101.2/32
        remote 192.168.101.4/32
    system-nodes: #161, ESTABLISHED, IKEv2, 7efa2401684f7927_i* d35349b7c7aa2b13_r
    local  'CN=ipsec-controller-0' @ 192.168.101.3[500]
    remote 'CN=ipsec-controller-1' @ 192.168.101.4[500]
    AES_CBC-128/HMAC_SHA2_256_128/PRF_AES128_XCBC/MODP_3072
    established 1254s ago, rekeying in 1825s, reauth in 8141s
    node: #8, reqid 1, INSTALLED, TRANSPORT, ESP:AES_GCM_16-128
        installed 656s ago, rekeying in 2771s, expires in 3304s
        in  c8b40c6d, 3337097 bytes, 58557 packets,     0s ago
        out cf1b0bdd, 76048257 bytes, 83565 packets,     0s ago
        local  192.168.101.3/32
        remote 192.168.101.4/32

The above output shows two IPsec connections between the two controllers of
a |AIO-DX| system. In multi nodes system such as standard or storage systems,
there will be IPsec connections among all hosts.