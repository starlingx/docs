.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _ipsec-overview-680c2dcfbf3b:

==============
IPsec Overview
==============

IPsec is a set of communication rules or protocols for setting up secure
connections over a network. |prod| utilizes IPsec to protect local traffic
on the internal management network of multi-node systems.

|prod| uses strongSwan as the IPsec implementation. strongSwan is an
opensource IPsec solution located at https://strongswan.org/.

For the most part, IPsec on |prod| is transparent to users.

