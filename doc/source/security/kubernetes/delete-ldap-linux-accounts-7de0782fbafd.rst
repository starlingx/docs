.. _delete-ldap-linux-accounts-7de0782fbafd:

==========================
Delete LDAP Linux Accounts
==========================

.. rubric:: |context|

When a |LDAP| user account is created in the |LDAP| server, using
:command:`sudo ldapusersetup` command, a corresponding |LDAP| Linux user is
created on the |prod| by mapping the |LDAP| user attributes to Linux user
attributes. The delete operation of a |LDAP| Linux account involves both the
deletion from the Linux system as well as the deletion of the corresponding
|LDAP| server object.

The home directory for a new |LDAP| Linux user will be created after the first
login, as: ``/home/<username>``. At the same time, the user will be prompted to
change the default password to a secure password based on mandatory format
rules.

.. rubric:: |proc|

The following steps describe the procedure to delete |LDAP| Linux accounts.

#.  |Optional| Logged in as sysadmin, check that the user exists on |prod| using one of
    the commands:

    .. code-block:: none

        id <username>

    .. code-block:: none

        getent passwd <username>

#.  Delete |LDAP| user.

    .. code-block:: none

        ~(keystone_admin)]$ sudo ldapdeleteuser <username>

    This command will remove the |LDAP| user from both the |LDAP| server as
    well as from the Linux platform.

#.  Check that the |LDAP| user was removed from the local |LDAP| server.

    .. code-block:: none

        ~(keystone_admin)]$ sudo ldapsearch -x -LLL -b dc=cgcs,dc=local

    or

    .. code-block:: none

        ~(keystone_admin)]$ sudo ldapfinger <username>

    .. note::

        SSSD service will sync-up |LDAP| linux users from the |LDAP| server,
        and this might take several minutes because is done according to
        ``ldap_enumeration_refresh_timeout`` time interval setting.

#.  Check that the local |LDAP| Linux user was removed from the cloud platform.

    .. code-block:: none

        ~(keystone_admin)]$ id <username>

    or

    .. code-block:: none

        ~(keystone_admin)]$ getent passwd <username>

The |LDAP| Linux user home directory still exists after the user has been
removed.

The Linux home directories of the deleted Linux |LDAP| users will be managed by
the system administrator. The platform will not remove them together with the
removal of the user.

The system administrator can backup (off system) and/or delete the home
directories.