
.. tnr1582059022065
.. _kubernetes-user-tutorials-helm-package-manager:

====================
Helm Package Manager
====================

|prod-long| supports Helm v3 package manager for Kubernetes that can
be used to securely manage the lifecycle of applications within the Kubernetes cluster.

.. rubric:: |context|

Helm packages are defined by Helm charts which contain all of the Kubernetes
resource definitions necessary to run an application inside of a Kubernetes
cluster. In |prod|, you can configure, install, and upgrade your Kubernetes
applications using Helm charts.

The Helm v3 client can be installed on a remote workstation and used to
remotely manage your Kubernetes application on |prod|, see :ref:`Security -
Access the System <index-security-kub-81153c1254c3>` and :ref:`Remote CLI
access <index-usertasks-kub-1291759aa985>`.

For more information on Helm, see the documentation at `https://helm.sh/docs/
<https://helm.sh/docs/>`__.

For more information on how to configure and use Helm both locally and
remotely, see :ref:`Configure Local CLI Access <configure-local-cli-access>`,
and :ref:`Configure Remote CLI Access <configure-remote-cli-access>`.

---------------
Deploy via Helm
---------------

*********************
Use Helm Package File
*********************

You can use the packaged Helm chart from your development environment, and use
the `Helm CLI <https://helm.sh/docs/intro/install/>`__ to install the package.

#.  Install your Helm package on |prod|

    The command below, executed remotely or locally, will deploy the
    application to the StarlingX-managed Kubernetes cluster:

    .. code-block:: shell

        $ helm install [ -f <myvalues.yaml-file> | --set <attribute-name>=<attribute-value> ] <appName> <package>.tgz

    where:

    The helm chart/application will be deployed with a default set of values;
    unless those values are overridden with ``-f`` and/or ``--set`` options.

*******************
Use Helm Repository
*******************

Helm charts are defined with a default set of values that describe the behavior
of the service installed within the Kubernetes cluster.

You can install the helm application from a Helm charts repository
``<https://helm.sh/docs/helm/helm_repo/>``.

#.  Add the URL for the Helm chart repository (containing the helm chart you
    want to install) to the list of Helm chart repositories configured for your
    helm client.

    .. code-block:: shell

        $ helm repo add <name> <helm-charts-repo-url>

    .. code-block:: shell

        $ helm repo update

#.  Install the Helm application with the following command which will search
    for the specified Helm chart in the configured Helm chart repositories, and
    install it.

    .. code-block:: shell

        $ helm install [ -f <myvalues.yaml-file> | --set <attribute-name>=<attribute-value> ] <appName> <name>/<chart-name>

    where:

    The helm chart/application will be deployed with a default set of values;
    unless those values are overridden with ``-f`` and/or ``--set`` options.
