
.. ihj1591797204756
.. _known-limitations:

================================
Known Kata Container Limitations
================================

This section describes the known limitations when using Kata containers.

.. _known-limitations-section-tsh-tl1-zlb:

--------------
SR-IOV Support
--------------

A minimal **kernel** and **rootfs** for Kata containers are shipped with
|prod-long|, and can be found at ``/usr/share/kata-containers/``. To enable
certain kernel features such as :abbr:`IOMMU (I/O memory management unit)`, and
desired network kernel modules, a custom kernel image, and rootfs has to be
built. However, many tests carried out using this solution were unsuccessful.
More details of this solution can be seen in:
https://bugs.launchpad.net/starlingx/+bug/1867927.

.. _known-limitations-section-ngp-ty3-bmb:

-------------------
CPU Manager Support
-------------------

Kata containers currently occupy only the platform cores. There is no
:abbr:`CPU (Central Processing Unit)` manager support.

.. _known-limitations-section-wcd-xy3-bmb:

---------
Hugepages
---------

.. _known-limitations-ul-uhz-xy3-bmb:

-   Similar to the |SRIOV| limitation, hugepage support must be configured in a
    custom Kata kernel.

-   The size and number of hugepages must be written using the
    :command:`io.katacontainers.config.hypervisor.kernel_params` annotation.

-   Creating a ``hugetlbfs`` mount for hugepages in the Kata container is
    specific to the end user application.

.. note::

    This solution does not work in version v3.1.3 of the Kata Containers. A
    pod can be started with hugepages specified, but none are actually
    allocated to the Kata |VM|.