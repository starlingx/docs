
.. vwx1591793382143
.. _starlingx-kubernetes-user-tutorials-overview:

========================
Kata Containers Overview
========================

|prod| uses a **containerd** :abbr:`CRI (Container Runtime Interface)` that
supports both runc and Kata Container runtimes. The default runtime is runc. If
you want to launch a pod that uses the Kata Container runtime, you must declare
it explicitly. You can see more details on how to specify the use of Kata
Container in :ref:`specifying-kata-container-runtime-in-pod-spec`.

For more information about Kata containers, see `https://katacontainers.io/
<https://katacontainers.io/>`__.


This guide describes how to run Kata Containers with Kubernetes on StarlingX.

.. _kata_container:

---------------------------------
Run Kata Containers in Kubernetes
---------------------------------

There are two methods to run Kata Containers in Kubernetes: by runtime class or
by annotation. Runtime class is supported in Kubernetes since v1.12.0 or
higher, and it is the recommended method for running Kata Containers.

To run by runtime class, create a RuntimeClass with ``handler`` set to ``kata``.
Then reference this class in the pod spec, as shown in the following example:

.. code-block:: none

    kind: RuntimeClass
    apiVersion: node.k8s.io/v1
    metadata:
      name: kata-containers
    handler: kata
    ---
    apiVersion: v1
    kind: Pod
    metadata:
      name: busybox-runtime
    spec:
      runtimeClassName: kata-containers
      containers:
      - name: busybox
        command:
          - sleep
          - "3600"
        image: busybox

Alternatively, set the ``io.kubernetes.cri.untrusted-workload`` to ``true``, to
run a pod with Kata Containers.

.. note::

      This method is deprecated and may not be supported in future Kubernetes
      releases. We recommend using the RuntimeClass method.

Example of using annotation:

.. code-block:: none

    apiVersion: v1
    kind: Pod
    metadata:
      name: busybox-untrusted
      annotations:
        io.kubernetes.cri.untrusted-workload: "true"
    spec:
      containers:
      - name: busybox
        command:
          - sleep
          - "3600"
        image: busybox


-------------------------
Check Kata Containers use
-------------------------

You can check whether the pod is running with Kata Containers or not by running
``ps aux`` in the host. A normal container is triggered by
``containerd-shim-runc-v1``, while Kata Containers is triggered by
``containerd-shim-kata-v2``.
