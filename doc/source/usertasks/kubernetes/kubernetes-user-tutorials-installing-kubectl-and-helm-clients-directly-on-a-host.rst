
.. orh1571690363235
.. _kubernetes-user-tutorials-installing-kubectl-and-helm-clients-directly-on-a-host:

===================================================
Install Kubectl and Helm Clients Directly on a Host
===================================================


As an alternative to using the container-backed Remote |CLIs| for kubectl and
helm, you can install these commands directly on your remote host.

.. rubric:: |context|

Kubectl and helm installed directly on the remote host provide the best CLI
behaviour, especially for CLI commands that reference local files or require a
shell.

The following procedure shows you how to configure the :command:`kubectl` and
:command:`kubectl` clients directly on a remote host, for an admin user with
**cluster-admin clusterrole**. If using a non-admin user with only role
privileges within a private namespace, additional configuration is required in
order to use :command:`helm`.

.. rubric:: |prereq|

You need from your |prod| administrator: a |WAD| or Local |LDAP| username and
password to get the Kubernetes authentication token, the |OAM| IP and,
optionally, the Kubernetes |CA| certificate of the target |prod| environment.

You must have the **oidc-auth-apps** |OIDC| Identity Provider (dex) configured
on the target |prod| environment to get Kubernetes authentication tokens.

.. xreflink For a Windows Active Directory user, see,
    |sec-doc|: :ref:`Overview of LDAP Servers <overview-of-ldap-servers>`.

.. rubric:: |proc|

.. _kubernetes-user-tutorials-installing-kubectl-and-helm-clients-directly-on-a-host-steps-f54-qqd-tkb:

#.  On the workstation, install the :command:`kubectl` client, set up the
    Kubernetes configuration and get a token. Follow the steps of section
    `Kubernetes Remote Client Access using the Host Directly` at :ref:`Configure
    Kubernetes Client Access <configure-kubernetes-client-access>`, then test
    the :command:`kubectl` access with the command below.

    .. code-block:: none

        % kubectl get pods -o wide
        NAME            READY STATUS RE-    AGE IP           NODE      NOMINA- READINESS
                                        STARTS                           TED NODE GATES
        nodeinfo-648f.. 1/1  Running   0    62d 172.16.38.83  worker-4 <none> <none>
        nodeinfo-648f.. 1/1  Running   0    62d 172.16.97.207 worker-3 <none> <none>
        nodeinfo-648f.. 1/1  Running   0    62d 172.16.203.14 worker-5 <none> <none>
        tiller-deploy.. 1/1  Running   0    27d 172.16.97.219 worker-3 <none> <none>

#.  On the workstation, install the :command:`helm` client on an Ubuntu host
    by performing the following actions on the remote Ubuntu system.

    #.  Install :command:`helm` client.

        .. code-block:: none

            % wget https://get.helm.sh/helm-v3.14.4-linux-amd64.tar.gz
            % tar xvf helm-v3.14.4-linux-amd64.tar.gz
            % sudo cp linux-amd64/helm /usr/local/bin

        In order to use :command:`helm`, additional configuration is required.
        For more information, see :ref:`Configuring Remote Helm Client
        <configuring-remote-helm-client>`.

**Related information**

.. seealso::
    :ref:`Configuring Container-backed Remote CLIs and Clients
    <kubernetes-user-tutorials-configuring-container-backed-remote-clis-and-clients>`

    :ref:`Using Container-backed Remote CLIs and Clients
    <using-container-based-remote-clis-and-clients>`

    :ref:`Configuring Remote Helm Client <configuring-remote-helm-client>`
