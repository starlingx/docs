.. _kubernetes-user-tutorials-fluxcd-deploy-00d5706c3358:

=================
Deploy via FluxCD
=================

`FluxCD <https://fluxcd.io/>`__ is a continuous delivery tool for Kubernetes.
It is used to keep Kubernetes in sync with configuration sources such as Git
repositories.

|prod-long| does not have FluxCD CLI, but it does have `FluxCD resources
<https://fluxcd.io/flux/components/>`__ which can be used to deploy
applications.

Using FluxCD resources you can deploy your applications and apply GitOps to
them.

Create a Source Controller
**************************

The Source Controller is a FluxCD resource that serves as the pivotal point
for acquiring artifacts necessary for deployment. It is crucial for ensuring
easy access to important elements such as source code, configuration files,
and other essential components for deploying your application. Essentially, it
acts as the gateway facilitating the acquisition process, guaranteeing that
your application has the requisite resources for deployment.

There are five types of `Source Controllers
<https://fluxcd.io/flux/components/source/api/v1/>`__, in this example
GitRepository will be used. The GitRepository is a type of Source Controller
that is used to manage applications defined within Git repositories.

In the active controller, run:

.. code-block:: none

    cat <<EOF > gitrepository.yaml
    apiVersion: source.toolkit.fluxcd.io/v1
    kind: GitRepository
    metadata:
      name: <source-controller-name>
    spec:
      interval: 5m
      url: <git-repository-url>
      ref:
        branch: <branch>

    EOF

.. code-block:: shell

    $ kubectl apply -f gitrepository.yaml


The above command creates a GitRepository resource, you can check by running:

.. code-block:: shell

    $ kubectl get gitrepositories


Create a Helm Controller
************************

For deploying applications via FluxCD, create a `Helm Controller
<https://fluxcd.io/flux/components/helm/>`__ or `Kustomize Controller
<https://fluxcd.io/flux/components/kustomize/>`__, each one has its
specifications.

For deploying via Helm, create a HelmRelease resource.

.. code-block:: none

    cat <<EOF > helmrelease.yaml
    apiVersion: "helm.toolkit.fluxcd.io/v2"
    kind: HelmRelease
    metadata:
      name: <helm-release-name>
    spec:
      releaseName: <helm-release-name>
      interval: 5m
      chart:
        spec:
          chart: <helm-chart-path> # Relative path of the Helm chart inside the repo.
          version: <helm-chart-version>
          sourceRef:
            kind: GitRepository
            name: <source-controller-name>
        values: # Override values for the Helm chart.
          foo:
            bar: value

    EOF

.. code-block:: shell

    $ kubectl apply -f helmrelease.yaml

This command creates a HelmChart and a HelmRelease resources, the HelmChart
stores the Helm chart loaded from the GitRepository, and the HelmRelease is
responsible for applying Helm actions, such as configuring, installing or
upgrading the Helm Chart.

You can check if the resources were created by running:

.. code-block:: shell

    $ kubectl get helmcharts

.. code-block:: shell

    $ kubectl get helmreleases

To check the application deployment:

.. code-block:: shell

    $ kubectl get all

-----------------------------
Modify the Application Values
-----------------------------

To modify values of your HelmRelease, you can edit the ``spec.values`` section
of ``helmrelease.yaml`` file and apply it again:

.. code-block:: shell

    $ kubectl apply -f helmrelease.yaml

This will cause the controller to perform Helm actions on the deployment.

