
.. dyp1581949325894
.. _kubernetes-user-tutorials-configuring-container-backed-remote-clis-and-clients:

======================================
Configure Container-backed Remote CLIs
======================================

The |prod| command lines can be accessed from remote computers running
Linux, MacOS, and Windows.

.. rubric:: |context|

This functionality is made available using a docker container with
pre-installed |CLIs| and clients. The container's image is pulled as required
by the remote CLI/client configuration scripts.


.. rubric:: |prereq|

.. _kubernetes-user-tutorials-configuring-container-backed-remote-clis-and-clients-ul-ev3-bfq-nlb:

-   You must have Docker installed on the remote systems you connect from. For
    more information on installing Docker, see
    `https://docs.docker.com/install/ <https://docs.docker.com/install/>`__.
    For Windows remote workstations, Docker is only supported on Windows 10.

    .. note::
        You must be able to run docker commands using one of the following
        options:

        -   Running the scripts using sudo

        -   Adding the Linux user to the docker group

        For more information, see,
        `https://docs.docker.com/engine/install/linux-postinstall/
        <https://docs.docker.com/engine/install/linux-postinstall/>`__

-   For Windows remote workstations, you must run the following commands from a
    Cygwin terminal. See `https://www.cygwin.com/ <https://www.cygwin.com/>`__
    for more information about the Cygwin project.

-   For Windows remote workstations, you must also have :command:`winpty`
    installed. Download the latest release tarball for Cygwin from
    `https://github.com/rprichard/winpty/releases
    <https://github.com/rprichard/winpty/releases>`__. After downloading the
    tarball, extract it to any location and change the Windows <PATH> variable
    to include its bin folder from the extracted winpty folder.

-   You need from your |prod| administrator: your |WAD| or Local |LDAP| username
    and password to get a Kubernetes authentication token, your Keystone
    username and password to log into Horizon, the |OAM| IP and, optionally, the
    Kubernetes |CA| certificate of the target |prod| environment. If HTTPS has
    been enabled for the |prod| RESTAPI Endpoints on the target |prod| system,
    you need the |CA| certificate that signed the |prod| REST APIs Endpoint
    Certificate.

The following procedure helps you configure the Container-backed remote |CLIs|
and clients for a non-admin user.

.. rubric:: |proc|

.. _kubernetes-user-tutorials-configuring-container-backed-remote-clis-and-clients-steps-fvl-n4d-tkb:

#.  Copy the remote client tarball file from |dnload-loc| to the remote
    workstation, and extract its content.

    -   The tarball is available from the |prod| area on |dnload-loc|.

    -   You can extract the tarball contents anywhere on your client system.

    .. parsed-literal::

        $ cd $HOME
        $ tar xvf |prefix|-remote-clients-<version>.tgz

#.  Download the user/tenant **openrc** file from the Horizon Web interface to
    the remote workstation.

    #.  Log in to Horizon as the user and tenant that you want to configure
        remote access for.

        In this example, we use 'user1' user in the 'tenant1' tenant.

    #.  Navigate to :menuselection:`Project --> API Access --> Download Openstack RC
        file`.

    #.  Select :guilabel:`Openstack RC file`.

        The file ``admin-openrc.sh`` downloads. Copy this file to the location of
        the extracted tarball.

    .. note::

        For a Distributed Cloud system, navigate to  :menuselection:`Project
        --> Central Cloud Regions --> RegionOne` and download the **Openstack
        RC file**.

#.  If HTTPS has been enabled for the |prod| RESTAPI Endpoints on the target
    |prod| system, add the following line to the bottom of ``admin-openrc.sh``:

    .. code-block:: none

        export OS_CACERT=<path_to_ca>

    where ``<path_to_ca>`` is the absolute path of the |CA| certificate that
    signed the |prod| REST APIs Endpoint Certificate provided by your |prod|
    administrator.

#.  Create an empty user-kubeconfig file on the remote workstation. The
    contents will be set later.

    .. code-block:: none

        $ touch user-kubeconfig

#.  On the remote workstation, configure the client access.

    #.  Change to the location of the extracted tarball.

        .. parsed-literal::

            $ cd $HOME/|prefix|-remote-clients-<version>/

    #.  Create a working directory that will be mounted by the container
        implementing the remote |CLIs|.

        See the description of the :command:`configure_client.sh` ``-w``
        option below for more details.

        .. code-block:: none

            $ mkdir -p $HOME/remote_cli_wd

    #.  Run the :command:`configure_client.sh` script.

        .. only:: starlingx

            .. code-block:: none

                $ ./configure_client.sh -t platform -r admin-openrc.sh -k user-kubeconfig -w $HOME/remote_cli_wd

        .. only:: partner

            .. include:: /_includes/kubernetes-user-tutorials-configuring-container-backed-remote-clis-and-clients.rest

        If you specify repositories that require authentication, as shown
        above, you must remember to perform a :command:`docker login` to
        that repository before using remote |CLIs| for the first time.

        The options for configure_client.sh are:

        **-t**
            The type of client configuration. The options are platform (for
            |prod-long| |CLI| and clients) and openstack (for |prod-os|
            application |CLI| and clients).

            The default value is platform.

        **-r**
            The user/tenant RC file to use for :command:`openstack` |CLI|
            commands.

            The default value is admin-openrc.sh.

        **-k**
            The kubernetes configuration file to use for :command:`kubectl` and
            :command:`helm` |CLI| commands.

            The default value is temp-kubeconfig.

        **-o**
            The remote CLI/client RC file generated by this script.

            This RC file needs to be sourced in the shell to set up required
            environment variables and aliases before running any remote |CLI|
            commands.

            For the platform client setup, the default is
            remote_client_platform.sh. For the openstack application client
            setup, the default is remote_client_app.sh.

        .. _kubernetes-user-tutorials-configuring-container-backed-remote-clis-and-clients-w-option:

        **-w**
            The working directory that will be mounted by the container
            implementing the remote |CLIs|. When using the remote |CLIs|, any files
            passed as arguments to the remote |CLI| commands need to be in this
            directory in order for the container to access the files. The
            default value is the directory from which the
            :command:`configure_client.sh` command was run.

        **-p**
            Override the container image for the platform |CLI| and clients.

            By default, the platform |CLIs| and clients container image is
            pulled from docker.io/starlingx/stx-platformclients.

            For example, to use the container images from the WRS AWS ECR:

            .. parsed-literal::

                $ ./configure_client.sh -t platform -r admin-openrc.sh -k user-kubeconfig -w $HOME/remote_cli_wd -p |registry-url|/starlingx/stx-platformclients:|v_starlingx-stx-platformclients|

            If you specify repositories that require authentication, you must
            perform a :command:`docker login` to that repository before using
            remote |CLIs|.

        **-a**
            Override the OpenStack application image.

            By default, the OpenStack |CLIs| and clients container image is
            pulled from docker.io/starlingx/stx-openstackclients.

        The :command:`configure-client.sh` command will generate a
        remote_client_platform.sh RC file. This RC file needs to be sourced
        in the shell to set up required environment variables and aliases
        before any remote |CLI| commands can be run.

#.  Copy the file ``remote_client_platform.sh`` to ``$HOME/remote_cli_wd``

#.  Update the contents in the admin-kubeconfig file using the
    :command:`kubectl` command from the container. Use the |OAM| IP address
    and the Kubernetes |CA| certificate (named ``k8s-ca.crt`` in the commands
    below) got from the |prod| administrator. In the example below, the user is
    called "user1", you should change this to your username. If the |OAM| IP is
    IPv6, use the IP enclosed in brackets (example: "[fd00::a14:803]").

    .. code-block:: none

        $ cd $HOME/remote_cli_wd
        $ source remote_client_platform.sh
        $ kubectl config set-cluster wrcpcluster --server=https://<OAM_IP>:6443
        $ kubectl config set clusters.wrcpcluster.certificate-authority-data $(base64 -w0 k8s-ca.crt)
        $ kubectl config set-context user1@wrcpcluster --cluster=wrcpcluster --user user1
        $ kubectl config use-context user1@wrcpcluster

    If you don't have the Kubernetes |CA| certificate, execute the following
    commands instead.

    .. code-block:: none

        $ cd $HOME/remote_cli_wd
        $ source remote_client_platform.sh
        $ kubectl config set-cluster wrcpcluster --server=https://<OAM_IP>:6443 --insecure-skip-tls-verify
        $ kubectl config set-context user1@wrcpcluster --cluster=wrcpcluster --user user1
        $ kubectl config use-context user1@wrcpcluster

.. rubric:: |postreq|

After configuring the platform's container-backed remote CLIs/clients, the
remote platform |CLIs| can be used in any shell after sourcing the generated
remote CLI/client RC file. This RC file sets up the required environment
variables and aliases for the remote |CLI| commands.

.. note::
    Consider adding this command to your .login or shell rc file, such that
    your shells will automatically be initialized with the environment
    variables and aliases for the remote |CLI| commands.

See :ref:`Using Container-backed Remote CLIs and Clients
<using-container-based-remote-clis-and-clients>` for details.

.. seealso::

   * :ref:`Using Container-backed Remote CLIs and Clients
     <using-container-based-remote-clis-and-clients>`

   * :ref:`Installing Kubectl and Helm Clients Directly on a Host
     <kubernetes-user-tutorials-installing-kubectl-and-helm-clients-directly-on-a-host>`

   * :ref:`Configuring Remote Helm Client <configuring-remote-helm-client>`
