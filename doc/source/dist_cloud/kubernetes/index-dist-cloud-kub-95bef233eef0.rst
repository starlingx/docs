.. _index-dist-cloud-kub-95bef233eef0:


.. include:: /_includes/toc-title-dc-kub.rest

.. only:: partner

   .. include:: /dist_cloud/index-dist-cloud-f5dbeb16b976.rst
      :start-after: kub-begin
      :end-before: kub-end

------------
Introduction
------------

.. toctree::
    :maxdepth: 1

    overview-of-distributed-cloud
    distributed-cloud-architecture
    shared-configurations
    regionone-and-systemcontroller-modes
    alarms-management-for-distributed-cloud

.. _install-a-subcloud:

------------
Installation
------------

.. toctree::
    :maxdepth: 1

    installing-and-provisioning-the-central-cloud
    installing-and-provisioning-a-subcloud
    installing-a-subcloud-using-redfish-platform-management-service
    installing-a-subcloud-without-redfish-platform-management-service
    install-a-subcloud-in-phases-0ce5f6fbf696
    reinstalling-a-subcloud-with-redfish-platform-management-service
    subcloud-deployment-with-local-installation-4982449058d5
    enroll-a-factory-installed-nondc-standalone-system-as-a-s-87b2fbf81be3

--------------------------
Subcloud Groups Management
--------------------------

.. toctree::
    :maxdepth: 1

    managing-subcloud-groups
    create-edit-and-delete-subcloud-groups-using-the-cli
    create-subcloud-groups-using-the-horizon-web-interface-69d357303531
    edit-subcloud-groups-85232c3a7d33
    delete-subcloud-groups-22a7c65e66d7
    orchestration-strategy-using-subcloud-groups


----------
Operations
----------

.. toctree::
    :maxdepth: 1

    monitoring-subclouds-using-horizon
    managing-subclouds-using-the-cli
    switching-to-a-subcloud-from-the-system-controller
    synchronization-monitoring-and-control
    cli-commands-for-dc-alarms-management
    managing-ldap-linux-user-accounts-on-the-system-controller
    changing-the-admin-password-on-distributed-cloud
    updating-docker-registry-credentials-on-a-subcloud
    migrate-an-aiosx-subcloud-to-an-aiodx-subcloud
    backup-a-subcloud-group-of-subclouds-using-dcmanager-cli-f12020a8fc42
    delete-subcloud-backup-data-using-dcmanager-cli-9cabe48bc4fd
    restore-a-subcloud-group-of-subclouds-from-backup-data-using-dcmanager-cli-f10c1b63a95e
    rehoming-a-subcloud
    rehoming-subcloud-with-expired-certificates-00549c4ea6e2
    rename-subcloud-e303565e7192
    prestage-a-subcloud-using-dcmanager-df756866163f
    add-a-horizon-keystone-user-to-distributed-cloud-29655b0f0eb9
    update-a-subcloud-network-parameters-b76377641da4

-------------------------------
Subcloud Prestage Orchestration
-------------------------------

.. toctree::
    :maxdepth: 1

    orchestrate-subcloud-prestage-using-the-cli-eb516473582f
    orchestrate-subcloud-prestage-using-the-horizon-dashboard-bdc02df7a2b2

-----------------------------------------
Software Release Deployment Orchestration
-----------------------------------------

.. toctree::
   :maxdepth: 1

   software-release-deployment-in-distributed-cloud-overview
   review-software-release-status-for-distributed-cloud-using-the-cli
   upload-software-releases-using-the-cli-203af02d6457
   deploy-software-releases-using-the-cli-1ea02eb230e5
   review-software-release-status-for-distributed-cloud-using-horizon-dashboard
   upload-software-releases-using-the-horizon-dashboard-67b418278e55
   deploy-software-releases-using-the-horizon-dashboard-eea2f49efc2c

.. _kubernetes-upversion-orchestration:

----------------------------------
Kubernetes Upversion Orchestration
----------------------------------

.. toctree::
    :maxdepth: 1

    the-kubernetes-distributed-cloud-update-orchestration-process
    configuring-kubernetes-update-orchestration-on-distributed-cloud
    create-a-kubernetes-upgrade-orchestration-using-horizon-16742b62ffb2
    apply-a-kubernetes-upgrade-strategy-using-horizon-2bb24c72e947

---------------------------------------
Kubernetes Root CA Update Orchestration
---------------------------------------

.. toctree::
    :maxdepth: 1

    orchestration-commands-for-dcmanager-4947f9fb9588

--------------------------------------------------------
Deploy, Restore and Manage Subclouds of Previous Release
--------------------------------------------------------

.. toctree::
    :maxdepth: 1

    subclouds-previous-release-management-5e986615cb4b

--------------------------------------------------
Distributed Cloud System Controller GEO Redundancy
--------------------------------------------------

.. toctree::
    :maxdepth: 1

    overview-of-distributed-cloud-geo-redundancy
    configure-distributed-cloud-system-controller-geo-redundancy-e3a31d6bf662

-------------------------------------------
Subcloud Management Network Reconfiguration
-------------------------------------------

.. toctree::
    :maxdepth: 1

    manage-subcloud-management-network-parameters-ffde7da356dc
    manage-management-network-parameters-for-a-standalone-aiosx-18c7aaace64d


--------
Appendix
--------

.. toctree::
    :maxdepth: 1

    distributed-cloud-ports-reference
    certificate-management-for-admin-rest-api-endpoints
    subcloud-geo-redundancy-error-root-cause-correction-action-43449d658aae

**********
Deprecated
**********

FPGA device image update management
-----------------------------------

.. toctree::
   :maxdepth: 1

   device-image-update-orchestration
   create-a-firmware-update-orchestration-strategy-using-horizon-cfecdb67cef2
   apply-the-firmware-update-strategy-using-horizon-e78bf11c7189
