
.. xvn1592596490325
.. _changing-the-admin-password-on-distributed-cloud:

==============================================
Change the Admin Password on Distributed Cloud
==============================================

You can change the keystone admin user password across the entire |prod-dc|
system.

.. rubric:: |prereq|

Ensure that all subclouds are managed and online.

.. rubric:: |proc|

#.  Change the password.


    #.  Do one of the following to change a keystone admin user password on
        System Controller.


        -   From the Horizon Web Interface, select the **RegionOne** mode,
            navigate to the **Identity** dashboard and click on the **Users**
            panel. Select **Change Password** from the **Edit** menu for the
            Admin user. For more information, see :ref:`RegionOne and
            SystemController Modes <regionone-and-systemcontroller-modes>`.

        -   From the |CLI|:

            .. code-block:: none

                ~(keystone_admin)]$ openstack --os-region-name SystemController user password set

            Respond to the prompts to complete the process.

            .. include:: /shared/_includes/quotation-marks-in-keystone-password.rest

    #.  Source the script /etc/platform/openrc if you will continue to use the
        environment from the previous |CLI| command.

        .. code-block:: none

            $ source /etc/platform/openrc
            ~(keystone_admin)]$

        .. note::
            In a subcloud, if the |CLI| command returns an authentication error
            after you source the script ``/etc/platform/openrc``, you can verify
            the password on the subcloud by using the :command:`env \| grep OS_PASSWORD`
            command . If it returns the old password, you will need to run the
            :command:`keyring set CGCS admin` command and provide the new admin
            password.

.. only:: partner

    .. include:: /_includes/dm-credentials-on-keystone-pwds.rest
