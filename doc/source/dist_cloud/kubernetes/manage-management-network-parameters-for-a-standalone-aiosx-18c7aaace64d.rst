.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _manage-management-network-parameters-for-a-standalone-aiosx-18c7aaace64d:

============================================================
Manage Management Network Parameters for a Standalone AIO-SX
============================================================

.. rubric:: |context|

You can change the management IP addresses for a standalone |AIO-SX|.

.. rubric:: |prereq|

Ensure that the host has been backed up, in case a system recovery is required.


Change the Network Parameters of the Management Network for Standalone AIO-SX Only
----------------------------------------------------------------------------------

This task is required only if the management network need to be changed for a
standalone |AIO-SX|.

.. rubric:: |proc|

#.  Lock the controller.

    .. code-block:: none 

        system host-lock controller-0

#.  Obtain the UUID of the management pool.

    .. code-block:: none 

        system addrpool-list

#.  Modify the management address pool.

    For example:

    .. code-block:: none 

        system addrpool-modify $UUID \
          --network              192.168.207.0 \
          --prefix                    24 \
          --floating-address         192.168.207.2 \
          --controller0-address  192.168.207.3 \
          --controller1-address  192.168.207.4 \
          --gateway-address       192.168.207.1 \
          --ranges                192.168.207.1-192.168.207.50

#.  Unlock the controller.

    .. code-block:: none 

        system host-unlock controller-0
