
.. sac1584464416105
.. _distributed-cloud-ports-reference:

=================================
Distributed Cloud Ports Reference
=================================

A number of ports must be available for various |prod-dc| components to
function correctly.

.. _distributed-cloud-ports-reference-table-mxl-qhh-blb:

.. begin-dc-ports-table

.. csv-table:: Table 1. |prod-dc| port requirements
   :file: /shared/FW_PORTS.csv
   :header-rows: 1

In addition to these ports, the iDRAC process uses port 443 on subcloud hosts
for HTTPS communications. This port must be available.

.. end-dc-ports-table

