
.. dma1558616138777
.. _review-software-release-status-for-distributed-cloud-using-horizon-dashboard:

================================================================================
Review Software Release Status for Distributed Cloud Using the Horizon Dashboard
================================================================================

You can use the Horizon Dashboard to review the status of each software release
stored in the central software release repository as well as software
synchronization status across subclouds. If a CLI is preferred, see
:ref:`review-software-release-status-for-distributed-cloud-using-the-cli`.

.. rubric:: |proc|

#.  Select the **SystemController** region.

    .. image:: figures/system-controller-region.png

#.  Select **Distributed Cloud Admin** \> **Software Management**.

#.  On the **Software Management** page, select the **Releases** tab.

    .. image:: figures/software-management-system-controller.png
        :width: 1000px

    .. note::

        The Release State indicates whether the release is available, deploying
        or deployed. Deployed indicates that the update has been installed on
        all hosts of the cloud (Central Cloud in this case).

#.  Identify which subclouds are update-current (in-sync).

    The software sync status is part of the overall sync status of a subcloud.
    To review the synchronization status of subclouds, see
    :ref:`monitoring-subclouds-using-horizon`.

    .. note::

        The sync status is the rolled up sync status of Dc-Cert, Firmware,
        Identity, Kube-Rootca, Kubernetes, Platform, and Software sync status.
        The Software sync status indicates whether the subcloud’s deployed
        releases (both major and patch releases) match that of the Central
        Cloud.

    .. image:: figures/monitor-subclouds-horizon.png