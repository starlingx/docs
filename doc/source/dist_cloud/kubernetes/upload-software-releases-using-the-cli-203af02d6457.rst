.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _upload-software-releases-using-the-cli-203af02d6457:

======================================
Upload Software Releases Using the CLI
======================================

New major or patch software releases must be uploaded to central software
release repository before they can be deployed on the System Controller and all
applicable subclouds.

Upload one or more patch releases
---------------------------------

#.  Log in the System Controller as sysadmin user.

#.  Transfer all patch releases to be uploaded to a directory e.g.
    ``/home/sysadmin/patches``.

#.  Upload all patches in the directory to the central software release
    repository.

    .. code-block:: none

        ~(keystone_admin)]$ software --os-region-name SystemController upload-dir /home/sysadmin/patches

    To upload a single patch release use the following command:

    .. code-block:: none

        ~(keystone_admin)]$ software --os-region-name SystemController upload <patch-file-name>

Upload a major release
----------------------

#.  Log in the System Controller as sysadmin user.

#.  Transfer the iso and signature files to ``/home/sysadmin``.

#.  Upload the boot image iso and signature files to the central software
    release repository.

    .. code-block:: none

        ~(keystone_admin)]$ software upload --local <bootimage.iso> <bootimage.sig>

.. note:: 

    When using the --local option, you must provide the absolute path to the
    release files.


View the uploaded release(s)
----------------------------

.. code-block:: none

    ~(keystone_admin)]$ software --os-region-name SystemController list


.. note::

    You can also view the list of software releases locally to the Central
    Cloud using the ``software list`` or ``software --os-region-name RegionOne
    list`` command. The output should be the same as ``software -os-region-name
    SystemController list`` command output.