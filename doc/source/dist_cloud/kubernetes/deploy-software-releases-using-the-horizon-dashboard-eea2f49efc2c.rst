.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _deploy-software-releases-using-the-horizon-dashboard-eea2f49efc2c:

====================================================
Deploy Software Releases Using the Horizon Dashboard
====================================================

After a software release has been uploaded to the central repository, it can be
deployed first on the System Controller and then on a specific subcloud or
across a group of subclouds through different orchestration methods. If a CLI
is preferred, see :ref:`deploy-software-releases-using-the-cli-1ea02eb230e5`.


Deploy a Software Release on the System Controller
--------------------------------------------------

.. rubric:: |proc|

#.  Change to the RegionOne region (top left drop-down menu).

    .. image:: figures/change_to_regionone.png

#.  Go to **Admin** > **Platform** > **Software Management** and open the
    **Deploy Orchestration** tab.

    .. image:: figures/software-management-deploy-orchestration-tab.png

#.  Select **Create Strategy**. 

#.  Create a Software Deploy strategy by specifying the settings for the
    parameters in the **Create Strategy** dialog box.

    .. image:: figures/create-software-deploy-strategy-pop.png

    .. note::

        Ensure the **Delete** checkbox is selected. If not, the deployment must
        be manually deleted via the CLI using the :command:`software deploy delete`
        command after the strategy is completed. Failure to do so will prevent
        the subclouds' software release sync state from being marked as
        out-of-sync, which will block the orchestration of the software
        deployment on the subclouds.

#.  Click **Create Software Deploy Strategy** to confirm the strategy creation.

    .. note::

        To change the strategy settings, you must delete the strategy and
        create a new one.

#.  Wait for the strategy status of the build phase to reach **success**.

#.  Click on **Apply Strategy** to start deploying the software release.

#.  Wait for the strategy to finish applying.

    .. image:: figures/software-deploy-strategy-deploy-orchestration.png

#.  Once the new software release is applied, click **Delete Strategy** to
    delete the strategy.


Orchestrate the Deployment of a Software Release Across the Subclouds
---------------------------------------------------------------------

After the System Controller has been successfully updated with a new major or
patch software release; distributed software deployment orchestration can be
initiated to bring the managed subclouds to the same software level as the
System Controller. The subclouds must, however, be prestaged with the target
software before orchestrated deployment can take place.

For more information on Prestaging Subcloud Orchestration see
:ref:`orchestrate-subcloud-prestage-using-the-cli-eb516473582f`.

Before orchestrating a major software release deployment across the subclouds,
it is strongly recommended that a backup of each subcloud to be upgraded is
taken. See
:ref:`backup-a-subcloud-group-of-subclouds-using-dcmanager-cli-f12020a8fc42`
for instructions.

.. rubric:: |prereq|

The following conditions must be met for the orchestrated software deployment
across the subclouds to be successful.

-   All target subclouds are managed, online and healthy. The Kubernetes
    cluster on each subcloud is fully operational and there are no management
    affecting alarms.

-   Subcloud certificates are up-to-date or will not expire during the
    deployment of the new software release.

-   All target subclouds have been prestaged with the software release and the
    new container images required for that release. See
    :ref:`orchestrate-subcloud-prestage-using-the-cli-eb516473582f` for
    instructions.

-   For the deployment of major software release, ensure that the controller-0
    is the active controller on each subcloud.

-   The subclouds' software sync status must be out of sync.

.. rubric:: |proc|

#.  Select the SystemController region.

    .. image:: figures/system-controller-region.png

#.  Select Distributed **Cloud Admin** > **Orchestration**.

#.  On the **Orchestration** page, select the **Strategy** tab.

    .. image:: figures/orchestration-page-strategy-tab.png

#.  Create a new strategy.

    On the **Strategy** tab, click Create **Strategy**. In the **Create
    Strategy** dialog box, select the Software Deploy strategy type and adjust
    the settings as needed.

    .. image:: figures/create-strategy-form.png

    **Strategy Type**
	    Software Deploy.

    **Release**
        Release ID.

        This option is available when Strategy Type is set to Sw-Deploy.

    **Apply to**
        Subcloud or Subcloud Group.

    **Subcloud**
        Select the subcloud name or All Subclouds.

    **Subcloud Group**
        Select the subcloud group. Available only if you select the **Apply to:
        Subcloud Group** option.

    **Stop on Failure**
        Default: True.

        Determines whether update orchestration failure for a subcloud prevents
        application to subsequent subclouds.

    **Subcloud Apply Type**
        Default: Parallel.

        Parallel or Serial. Determines whether the subclouds are updated in
        parallel or serially.

    **Maximum Parallel Subclouds**
        Default: 20.

        Defines the maximum number of subclouds on which the software
        deployment can run in parallel. If the **Apply to: Subcloud Group**
        option is selected, it will use the ``max_parallel_subclouds`` value of
        the subcloud group instead.

#.  Click **Create Strategy** to confirm the operation.

#.  Click **Apply Strategy** to start applying the software release update to
    the subclouds.

    As each subcloud is deployed, it moves through the following states:

    **initial**
	    The orchestration has not yet started.

    **sw-deploy pre-check**
        Verify that the subcloud has no existing strategies, that it was
        pre-staged, and that the System Controller already has the specified
        release deployed.

    **sw-deploy install license** (only for major releases)
        Installing release license.

    **create VIM sw-deploy strategy**
        The strategy is being created in the subcloud.

    **apply VIM sw-deploy strategy**
        The strategy is being applied in the subcloud.

    **finish sw-deploy strategy**
        Updates that are no longer required are being deleted.

        Updates that require committing are being committed.

    **complete**
        The software release has been deployed successfully.

#.  Once the strategy state changes to **complete**, click Delete Strategy to
    delete it.

    .. image:: figures/strategy-delete.png


.. _customizing-the-update-configuration-for-distributed-cloud-update-orchestration:

-----------------------------------------------------------------------------
Customize the Update Configuration for Distributed Cloud Update Orchestration
-----------------------------------------------------------------------------

You can adjust how the nodes in each subcloud are updated.

.. rubric:: |context|

The update strategy for |prod-dc| Update Orchestration uses separate
configuration settings to control how the nodes on a given system are updated.
You can adjust the settings used by default for all subclouds, and you can
create custom settings for individual subclouds.

You can change the configuration settings before or after creating an update
strategy for |prod-dc| update orchestration. The settings are maintained
independently.

.. rubric:: |proc|

#.  Select the **SystemController** region.

#.  Select **Distributed Cloud Admin** \> **Orchestration**.

#.  On the **Orchestration** page, select the **Subcloud Strategy
    Configurations** tab.

    .. image:: figures/orchestration-subcloud-strategy-config-tab.png
        :width: 1000px

    Take one of the following actions:


    -   To edit the settings applicable to all subclouds by default, click
        **Edit Configuration** in the **all clouds default** row.

        .. image:: figures/edit-subcloud-strategy-configuration.png

        To save your changes, click **Edit Subcloud Strategy Configurations**.

    -   To create custom settings for an individual subcloud, click **Create
        New Subcloud Strategy Configurations**.

        In the **Subcloud** field, select the subcloud for the custom settings.

        To save your configuration changes, click **Create Subcloud Strategy
        Configurations**. The new configuration is added to the list.

        .. image:: figures/subcloud-strategy-configurations.png

    The following settings are available:

    **Subcloud**
        This specifies the subcloud affected by the configuration. For the
        **all clouds default** configuration, this setting cannot be changed.

    **Storage Apply Type**
        Parallel or Serial — determines whether storage nodes are patched in
        parallel or serially

    **Worker Apply Type**
        Parallel or Serial — determines whether worker nodes are patched in
        parallel or serially

    **Maximum Parallel Worker Hosts**
        This sets the maximum number of worker nodes that can be patched in
        parallel.

    **Default Instance Action**
        .. note::

            This parameter is only applicable to hosted application VMs with
            the |prefix|-openstack application.

        migrate or stop-start — determines whether hosted application VMs are
        migrated or stopped and restarted when a worker host is upgraded

    **Alarm Restrictions**
        Relaxed or Strict — determines whether the orchestration is aborted for
        alarms that are not management-affecting.


.. rubric:: |postreq|

For information about creating and applying a patch strategy, see
:ref:`software-release-deployment-in-distributed-cloud-overview`.