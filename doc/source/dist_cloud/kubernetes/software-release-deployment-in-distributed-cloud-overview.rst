
.. bkh1558616177792
.. _software-release-deployment-in-distributed-cloud-overview:

=========================================================
Software Release Deployment in Distributed Cloud Overview
=========================================================

You can deploy a new software release on the System Controller then orchestrate
the software deployment across managed subclouds from the System Controller.

A central software release repository exists for each major release on the
System Controller. This is also used to store all patch releases for each
respective major release. All software releases must be uploaded to the System
Controller region i.e. by specifying ``--os-region-name SystemController`` in
the software upload command, so that they will be available for patch
application to both the Central Cloud and the managed subclouds.

When a new subcloud is deployed, it will be bootstrapped with the latest patch
that is available in the central software release repository for the specified
release.

Both major and minor software releases can be viewed, uploaded, deployed and
removed using CLI or Horizon dashboard. The same procedure is used to deploy
both types of software releases. After a new software release, major or minor,
has been uploaded to the central repository; it must be deployed first on the
Central Cloud, then prestaged on the subclouds and finally deployed on the
subclouds. However, the deployment of a major software release requires more
preparation which is described in a separate chapter.

.. note::

    For Distributed Cloud, manual updating software of individual subclouds is
    not recommended.