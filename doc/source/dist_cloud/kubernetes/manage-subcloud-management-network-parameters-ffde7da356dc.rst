.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _manage-subcloud-management-network-parameters-ffde7da356dc:

=============================================
Manage Subcloud Management Network Parameters
=============================================

You can change the management IP addresses for an |AIO-SX| subcloud.

.. rubric:: |prereq|

-   Ensure that the subcloud has been backed up, in case a subcloud system
    recovery is required.

-   Ensure that the system time between system controllers and the subclouds
    are synchronized.

    .. code-block:: none

        ~(keystone_admin)]$ date -u

    If the time is not correct either on the system controllers or the
    subclouds, check the ``clock_synchronization`` configuration on the system.

    .. code-block:: none

        ~(keystone_admin)]$ system host-show controller-0

    Check the |NTP| server configuration or |PTP| server configuration sections
    to correct the system time based on the system's ``clock_synchronization``
    configuration (|NTP| or |PTP|).

Change the Network Parameters of the Management Network for AIO-SX Subcloud Only
--------------------------------------------------------------------------------

This task is required only if the management network needs to be changed for an
|AIO-SX| subcloud.

.. rubric:: |proc|

#.  Lock the controller.

    .. code-block:: none

        system host-lock controller-0

#.  Obtain the UUID of the management pool.

    .. code-block:: none

        system addrpool-list

#.  Modify the management address pool.

    For example:

    .. code-block:: none 

        system addrpool-modify <uuid> \
          --network              192.168.103.0 \
          --prefix                    26 \
          --floating-address         192.168.103.2 \
          --controller0-address  192.168.103.3 \
          --controller1-address  192.168.103.4 \
          --gateway-address       192.168.103.1 \
          --ranges                192.168.103.1-192.168.103.50

#.  Unlock the controller.

    .. code-block:: none

        system host-unlock controller-0

#.  Wait for the subcloud to recover and to have all alarms cleared.

#.  On the system controller, perform the following:

    #.  Unmanage the subcloud.

        .. code-block:: none

            ~(keystone_admin)]$ dcmanager subcloud unmanage <subcloud-name>

    #.  Update the subcloud with the new subnet parameters.

        For example:

        .. code-block:: none

            ~(keystone_admin)]$ dcmanager subcloud update --management-subnet 192.168.103.0/26 --management-gateway-ip 192.168.103.1 --management-start-ip 192.168.103.2 --management-end-ip 192.168.103.5 --bootstrap-address 10.33.1.3 <subcloud-name>


    #.  Manage the subcloud.

        .. code-block:: none

            ~(keystone_admin)]$ dcmanager subcloud manage <subcloud-name>
