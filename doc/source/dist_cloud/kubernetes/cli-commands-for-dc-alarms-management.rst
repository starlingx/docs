
.. hmg1558616220923
.. _cli-commands-for-dc-alarms-management:

===================================================
CLI Commands for Distributed Cloud Alarm Management
===================================================

You can use the CLI to review alarm summaries for the |prod-dc|.

.. _cli-commands-for-alarms-management-ul-ncv-m4y-fdb:

-   To show the status of all subclouds, as well as a summary count of alarms
    and warnings for each one, use the :command:`alarm summary` command.

    For example:

    .. code-block:: none

        ~(keystone_admin)$ dcmanager alarm summary
        +------------+-----------------+--------------+--------------+----------+----------+
        | NAME       | CRITICAL_ALARMS | MAJOR_ALARMS | MINOR_ALARMS | WARNINGS | STATUS   |
        +------------+-----------------+--------------+--------------+----------+----------+
        | subcloud-5 |               0 |            2 |            0 |        0 | degraded |
        | subcloud-1 |               0 |            0 |            0 |        0 | OK       |
        +------------+-----------------+--------------+--------------+----------+----------+

    System Controller alarms and warnings are not included.

    The status is one of the following:

    **OK**
        There are no alarms or warnings, or only warnings.

    **degraded**
        There are minor or major alarms.

    **critical**
        There are critical alarms.

-   To show the count of alarms and warnings for the System Controller, use the
    :command:`fm alarm-summary` command.

    For example:

    .. code-block:: none

        ~(keystone_admin)$ fm alarm-summary
        +-----------------+--------------+--------------+----------+
        | Critical Alarms | Major Alarms | Minor Alarms | Warnings |
        +-----------------+--------------+--------------+----------+
        | 0               | 0            | 0            | 0        |
        +-----------------+--------------+--------------+----------+

    The following command is equivalent to the :command:`fm alarm-summary`,
    providing a count of alarms and warnings for the System Controller:

    .. code-block:: none

        fm --os-region-name RegionOne alarm-summary

-   To show the alarm and warning count for a specific subcloud only, add the
    ``--os-region-name`` parameter and supply the region name:

    For example:

    .. code-block:: none

        ~(keystone_admin)$ fm --os-region-name 829881a8077d4a1f997efd30611cf0ab --os-auth-url https://192.168.121.2:5000/v3 --os-endpoint-type public alarm-summary
        +-----------------+--------------+--------------+----------+
        | Critical Alarms | Major Alarms | Minor Alarms | Warnings |
        +-----------------+--------------+--------------+----------+
        | 0               | 0            | 0            | 0        |
        +-----------------+--------------+--------------+----------+

    .. note::

        ``829881a8077d4a1f997efd30611cf0ab`` is the subcloud region name and it
        can be retrieved by using the :command:`dcmanager subcloud show <subcloud_name/subcloud_id>`
        command. ``192.168.121.2`` is the external_oam_floating_address of the
        subcloud.

-   To list the alarms for a subcloud:

    .. code-block:: none

        ~(keystone_admin)$ fm --os-region-name 829881a8077d4a1f997efd30611cf0ab --os-auth-url https://192.168.121.2:5000/v3 --os-endpoint-type public alarm-list
        +----------+--------------------------------------------+-------------------+----------+-------------------+
        | Alarm ID | Reason Text                                | Entity ID         | Severity | Time Stamp        |
        +----------+--------------------------------------------+-------------------+----------+-------------------+
        | 250.001  | controller-0 Configuration is out-of-date. | host=controller-0 | major    | 2018-02-06T21:37: |
        |          |                                            |                   |          | 32.650217         |
        |          |                                            |                   |          |                   |
        | 250.001  | controller-1 Configuration is out-of-date. | host=controller-1 | major    | 2018-02-06T21:37: |
        |          |                                            |                   |          | 29.121674         |
        |          |                                            |                   |          |                   |
        +----------+--------------------------------------------+-------------------+----------+-------------------+
