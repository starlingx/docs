
.. hxt1558615716368
.. _review-software-release-status-for-distributed-cloud-using-the-cli:

==================================================================
Review Software Release Status for Distributed Cloud Using the CLI
==================================================================

You can use CLI to review the status of each software release stored in the
central software release repository as well as software synchronization status
across the subclouds. If a web interface is preferred, see
:ref:`review-software-release-status-for-distributed-cloud-using-horizon-dashboard`.

.. rubric:: |proc|

.. _reviewing-update-status-for-distributed-cloud-using-the-cli-steps-unordered-d53-dlx-fdb:

-   To check the status of software releases in the central software release
    repository, use the software list command on the SystemController region.

    For example:

    .. only:: starlingx

        .. code-block:: none

            ~(keystone_admin)]$ software --os-region-name SystemController list
            +---------------------+-------+----------+
            | Release             | RR    |  State   |
            +---------------------+-------+----------+
            | starlingx-24.09.0   | True  | deployed |
            | starlingx-24.09.100 | False | deployed |
            +---------------------+-------+----------+

    .. only:: partner

        .. include:: /_includes/review-software-release-status-for-distributed-cloud-using-the-cli.rest
            :start-after: status-update-table-begin
            :end-before: status-update-table-end

    The **State** column indicates whether the release is unavailable,
    available, deploying, or deployed. The **deployed** state indicates that
    the release has been installed on all hosts of the cloud (the Central Cloud
    in this case). The **unavailable** state indicates that the release and
    associated patches are available in the central software release
    repository, but they cannot be used for deployment on this cloud.

-   To identify which subclouds are update-current (**in-sync**), use the
    following command:

    .. code-block:: none

        ~(keystone_admin)]$ dcmanager subcloud list
        +----+-----------+--------------+--------------------+-------------+
        | id | name      | management   | availability       | sync        |
        +----+-----------+--------------+--------------------+-------------+
        |  1 | subcloud1 | managed      | online             | in-sync     |
        |  2 | subcloud2 | managed      | online             | in-sync     |
        |  3 | subcloud3 | managed      | online             | out-of-sync |
        +----+-----------+--------------+--------------------+-------------+

    .. note::

        The **sync** status is the rolled up **sync** status of
        ``dc_cert_sync_status``, ``firmware_sync_status``,
        ``identity_sync_status``, ``kubernetes_sync_status``,
        ``kube-rootca_sync_status``, ``platform_sync_status``, and
        ``software_sync_status``. The ``software_sync_status`` indicates
        whether the subcloud's deployed releases (both major and patch
        releases) match those of the Central Cloud.

-   To see synchronization details for a subcloud:

    .. code-block:: none

        ~(keystone_admin)]$ dcmanager subcloud show subcloud1
        +-----------------------------+----------------------------+
        | Field                       | Value                      |
        +-----------------------------+----------------------------+
        | id                          | 1                          |
        | name                        | subcloud1                  |
        | description                 | None                       |
        | location                    | None                       |
        | software_version            | nn.nn                      |
        | management                  | managed                    |
        | availability                | online                     |
        | deploy_status               | complete                   |
        | management_subnet           | fd01:82::0/64              |
        | management_start_ip         | fd01:82::2                 |
        | management_end_ip           | fd01:82::11                |
        | management_gateway_ip       | fd01:82::1                 |
        | systemcontroller_gateway_ip | fd01:81::1                 |
        | group_id                    | 1                          |
        | created_at                  | 2020-07-15 19:23:50.966984 |
        | updated_at                  | 2020-07-17 12:36:28.815655 |
        | backup_status               | None                       |
        | backup_datetime             | None                       |
        | prestage_status             | complete                   |
        | prestage_version            | 22.12,24.09                |
        | dc-cert_sync_status         | in-sync                    |
        | firmware_sync_status        | in-sync                    |
        | identity_sync_status        | in-sync                    |
        | kubernetes_sync_status      | out-of-sync                |
        | kube-rootca_sync_status     | in-sync                    |
        | load_sync_status            | not-available              |
        | patching_sync_status        | not-available              |
        | platform_sync_status        | in-sync                    |
        | software_sync_status        | in-sync                    |
        | region_name                 | subcloud11                 |
        +-----------------------------+----------------------------+

    .. note::

        Prior to software release version |prod-ver|, the major software
        release status was presented by load_sync_status attribute and the patch
        software release status by ``patching_sync_status`` attribute. Starting
        with software release version |prod-ver| and above, both major and minor
        software release statuses are presented by a single
        ``software_sync_status`` attribute.

        The ``patching_sync_status`` is only applicable to subclouds running
        the previous release (i.e. below |prod-ver|). It indicates whether the
        subcloud's deployed patches of the previous release match those of
        the Central Cloud.

        Both ``patching_sync_status`` and ``load_sync_status`` attributes will
        be removed in a future release.