.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _upload-software-releases-using-the-horizon-dashboard-67b418278e55:

====================================================
Upload Software Releases Using the Horizon Dashboard
====================================================

You can also upload software releases to the central software release
repository from the Horizon Web interface.

.. rubric:: |context|

If you prefer, you can use the |CLI|. For more information, see
:ref:`upload-software-releases-using-the-cli-203af02d6457`.

.. rubric:: |proc|

#.  Select the **SystemController** region.

    .. image:: figures/system-controller-region.png

#.  Select **Distributed Cloud Admin** \> **Software Management**.

#.  On the **Software Management** page, select the **Releases** tab.

    .. image:: figures/software-management-system-controller.png
        :width: 1000px


#.  On the **Releases** tab, click **Upload Releases**.

    In the **Upload Releases** dialog box, click **Choose Files** to select
    the software release files for upload.

    .. image:: figures/upload-releases-pop.png

#.  In the dialog, click **Upload Releases**.

    The release is added to the Releases list in the **Available** state.

    .. image:: figures/releases-list-state.png