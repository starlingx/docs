.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _orchestrate-subcloud-prestage-using-the-horizon-dashboard-bdc02df7a2b2:

=========================================================
Orchestrate Subcloud Prestage Using the Horizon Dashboard
=========================================================

This section describes the prestage strategy for a single subcloud, all
subclouds or a specific subcloud group using the Horizon Web Interface. If a
CLI is preferred, see
:ref:`orchestrate-subcloud-prestage-using-the-cli-eb516473582f`.

.. rubric:: |prereq|

There are two types of subcloud prestage:

-   **Prestage for software deployment** occurs when the subcloud is running an
    older software than the system controller during prestaging. This prestage
    transfers software update(s) and container images to the subcloud while
    preparing for new software deployment. The prestaged data is stored in
    various locations on the subcloud. For example, metadata is stored in
    ``/opt/software/<sw-version>`` and new images are added to the local docker
    registry.

-   **Prestage for install** occurs when the subcloud is running the same or older
    software than the system controller during prestaging. This prestage
    transfers the entire platform software and container images of the
    specified release while preparing for the subcloud redeployment. This is
    used if the subcloud needs to be reinstalled and restored to a specific
    release. The prestaged data is stored in the subcloud persistent
    filesystem ``/opt/platform-backup/<sw-version>``.

For more information on prerequisites for prestage upgrade and reinstall, see
:ref:`prestage-a-subcloud-using-dcmanager-df756866163f`.

.. note::

   Any existing strategy must be deleted first as only one type
   of strategy can exist at a time.

   .. only:: partner

      .. include:: /_includes/prestage-subcloud-orchestration-eb516473582f.rest
         :start-after: strategy-begin
         :end-before: strategy-end

.. rubric:: |proc|

#.  Select the SystemController region.

    .. image:: figures/system-controller-region.png

#.  Select **Distributed Cloud Admin** > **Orchestration**.

#.  On the **Orchestration** page, select the **Strategy** tab.

#.  Create a prestage strategy.

    On the **Strategy** tab, click **Create Strategy**. In the **Create
    Strategy** dialog box, select the **Prestage strategy type** and adjust the
    settings as needed.

    Prestage strategy can be created for a single subcloud, all subclouds, or a
    specific subcloud group.

    .. image:: figures/create-strategy-prestage.png

    To create a prestage strategy for a specific subcloud, in the **Apply to**
    dialog box select **Subcloud** and in **Subcloud** dialog box select the
    subcloud to apply the strategy.

    .. image:: figures/apply-to-subcloud-option.png

    To create a prestage strategy for all subclouds, in the **Subcloud** dialog
    box select **All Subclouds**.

    .. image:: figures/apply-to-all-subclouds.png

    To create a prestage strategy for a specific subcloud group, in the **Apply
    to** dialog box select **Subcloud Group** and in the **Subcloud Group**
    dialog box choose the **Subcloud Group** to apply the strategy.

    .. image:: figures/subcloud-group-new.png

    **Strategy Type**
        Prestage.

    **Release**
        Release ID.

        This option is available when Strategy Type is set to Prestage.

    **For Software Deploy**
        Default: False.

        Prestage for software deploy operations. If not specified, prestaging
        is targeted towards full installation.

    **Apply to**
        Subcloud or Subcloud Group.
    
    **Subcloud**
        Select the subcloud name or All Subclouds.

    **Subcloud Group**
        Select the subcloud group. Available only if you select the **Apply to:
        Subcloud Group** option.

    **Stop on Failure**
        Default: True.

        Determines whether update orchestration failure for a subcloud prevents
        application to subsequent subclouds.

    **Subcloud Apply Type**
        Default: Parallel.

        This option is available when Subcloud is set to All Subclouds.

        Parallel or Serial. Determines whether the subclouds are updated in
        parallel or serially.
    
    **Maximum Parallel Subclouds**
        Default: 20.

        This option is available when Subcloud is set to All Subclouds.

        Defines the maximum number of subclouds that the prestage strategy can
        process simultaneously. If the **Apply to: Subcloud Group** option is
        selected, it will use the ``max_parallel_subclouds`` value of the
        subcloud group instead.    

    **Force**
        Default: False.

        Skip checking the subcloud for management affecting alarms.

    **Syadmin Password**
        Sysadmin Password.

    .. note::

        Unlike other types of orchestration, prestage orchestration requires
        sysadmin password as all communications with the subclouds are done
        using ansible over the oam network to avoid disruptions to management
        traffic.

#.  Click **Create Strategy** to confirm the operation.

#.  Click on **Apply Strategy** to start the prestaging.

    The overall status of the strategy can be viewed on the Steps table.

    .. image:: figures/orchestration-strategy-steps-table.png

#.  Delete the strategy.

    Once the strategy status is either **complete**, **aborted** or **failed**,
    the strategy can be deleted clicking on **Delete Strategy**.