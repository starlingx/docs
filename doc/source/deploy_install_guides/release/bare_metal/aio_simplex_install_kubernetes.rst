.. include:: /_vendor/rl-strings.txt

.. include:: /_stx-related-links/installation-prereqs.rln
   :start-after: begin-rls
   :end-before: end-rls

.. _aio_simplex_install_kubernetes_r7:

=================================================
Install Kubernetes Platform on All-in-one Simplex
=================================================

.. begin-content

.. only:: partner

   .. include:: /_includes/install-kubernetes-null-labels.rest

.. contents:: |minitoc|
   :local:
   :depth: 1

--------
Overview
--------

.. _aiosx-installation-prereqs:

.. include:: /shared/_includes/desc_aio_simplex.txt

.. include:: /_includes/automated-install-note--09dff208a9c6.rest

.. _installation-prereqs:

.. _aio_simplex_hardware_r7:

-----------------------------
Minimum hardware requirements
-----------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
            :start-after: begin-min-hw-reqs-sx
            :end-before: end-min-hw-reqs-sx

      .. group-tab:: Virtual

         The following sections describe system requirements and host setup for
         a workstation hosting virtual machine(s) where StarlingX will be
         deployed; i.e., a |VM| for each StarlingX node (controller,
         AIO-controller, worker or storage node).

         .. rubric:: **Hardware requirements**

         The host system should have at least:

         * **Processor:** x86_64 only supported architecture with BIOS enabled
           hardware virtualization extensions

         * **Cores:** 8

         * **Memory:** 32GB RAM

         * **Hard Disk:** 500GB HDD

         * **Network:** One network adapter with active Internet connection

         .. rubric:: **Software requirements**

         The host system should have at least:

         * A workstation computer with Ubuntu 16.04 LTS 64-bit

         All other required packages will be installed by scripts in the
         StarlingX tools repository.

         .. rubric:: **Host setup**

         Set up the host with the following steps:

         #. Update OS:

            ::

             apt-get update

         #. Clone the StarlingX tools repository:

            ::

             apt-get install -y git
             cd $HOME
             git clone https://opendev.org/starlingx/virtual-deployment.git

         #. Install required packages:

            ::

               cd $HOME/virtual-deployment/libvirt
               bash install_packages.sh
               apt install -y apparmor-profiles
               apt-get install -y ufw
               ufw disable
               ufw status


            .. note::

               On Ubuntu 16.04, if apparmor-profile modules were installed as
               shown in the example above, you must reboot the server to fully
               install the apparmor-profile modules.

.. only:: partner

   .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
      :start-after: begin-min-hw-reqs-sx
      :end-before: end-min-hw-reqs-sx

--------------------------
Installation Prerequisites
--------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/installation-prereqs.rest
            :start-after: begin-install-prereqs
            :end-before: end-install-prereqs

      .. group-tab:: Virtual

         Several pre-requisites must be completed prior to starting the |prod|
         installation.

         Before attempting to install |prod|, ensure that you have the the
         |prod| host installer ISO image file.

         Get the latest |prod| ISO from the `StarlingX mirror
         <https://mirror.starlingx.cengn.ca/mirror/starlingx/release/latest_release/debian/monolithic/outputs/iso/>`__.
         Alternately, you can get an older release ISO from `here <https://mirror.starlingx.cengn.ca/mirror/starlingx/release/>`__.


.. only:: partner

   .. include:: /shared/_includes/installation-prereqs.rest
      :start-after: begin-install-prereqs
      :end-before: end-install-prereqs

--------------------------------
Prepare Servers for Installation
--------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
            :start-after: start-prepare-servers-common
            :end-before: end-prepare-servers-common

      .. group-tab:: Virtual

         .. note::

             The following commands for host, virtual environment setup, and
             host power-on use KVM / virsh for virtual machine and VM
             management technology. For an alternative virtualization
             environment, see: :ref:`Install StarlingX in VirtualBox
             <install_virtualbox>`.

         #. Prepare virtual environment.

            Set up the virtual platform networks for virtual deployment:

            ::

              bash setup_network.sh

         #. Prepare virtual servers.

            Create the XML definitions for the virtual servers required by this
            configuration option. This will create the XML virtual server
            definition for:

            * simplex-controller-0

            The following command will start/virtually power on:

            * The 'simplex-controller-0' virtual server
            * The X-based graphical virt-manager application

            ::

              bash setup_configuration.sh -c simplex -i ./bootimage.iso

            If there is no X-server present errors will occur and the X-based
            GUI for the virt-manager application will not start. The
            virt-manager GUI is not absolutely required and you can safely
            ignore errors and continue.

.. only:: partner

   .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
      :start-after: start-prepare-servers-common
      :end-before: end-prepare-servers-common

--------------------------------
Install Software on Controller-0
--------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/inc-install-software-on-controller.rest
            :start-after: incl-install-software-controller-0-aio-start
            :end-before: incl-install-software-controller-0-aio-end

      .. group-tab:: Virtual

         In the last step of :ref:`aio_simplex_environ`, the controller-0
         virtual server 'simplex-controller-0' was started by the
         :command:`setup_configuration.sh` command.

         On the host, attach to the console of virtual controller-0 and select
         the appropriate installer menu options to start the non-interactive
         install of StarlingX software on controller-0.

         .. note::

            When entering the console, it is very easy to miss the first
            installer menu selection. Use ESC to navigate to previous menus, to
            ensure you are at the first installer menu.

         ::

           virsh console simplex-controller-0

         Make the following menu selections in the installer:

         #. First menu: Select 'All-in-one Controller Configuration'
         #. Second menu: Select 'Serial Console'

         Wait for the non-interactive install of software to complete and for
         the server to reboot. This can take 5-10 minutes, depending on the
         performance of the host machine.

.. only:: partner

   .. include:: /shared/_includes/inc-install-software-on-controller.rest
      :start-after: incl-install-software-controller-0-aio-start
      :end-before: incl-install-software-controller-0-aio-end

--------------------------------
Bootstrap system on controller-0
--------------------------------

#. Login using the username / password of "sysadmin" / "sysadmin".
   When logging in for the first time, you will be forced to change the
   password.

   ::

      Login: sysadmin
      Password:
      Changing password for sysadmin.
      (current) UNIX Password: sysadmin
      New Password:
      (repeat) New Password:

#. Verify and/or configure IP connectivity.

   .. only:: starlingx

      .. tabs::

         .. group-tab:: Bare Metal

            .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
               :start-after: begin-aio-sx-install-verify-ip-connectivity
               :end-before: end-aio-sx-install-verify-ip-connectivity

         .. group-tab:: Virtual

            .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
               :start-after: begin-aio-sx-install-verify-ip-connectivity
               :end-before: end-aio-sx-install-verify-ip-connectivity

   .. only:: partner

      .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
         :start-after: begin-aio-sx-install-verify-ip-connectivity
         :end-before: end-aio-sx-install-verify-ip-connectivity

#. Specify user configuration overrides for the Ansible bootstrap playbook.

   Ansible is used to bootstrap StarlingX on controller-0. Key files for
   Ansible configuration are:

   ``/etc/ansible/hosts``
      The default Ansible inventory file. Contains a single host: localhost.

   ``/usr/share/ansible/stx-ansible/playbooks/bootstrap.yml``
      The Ansible bootstrap playbook.

   ``/usr/share/ansible/stx-ansible/playbooks/host_vars/bootstrap/default.yml``
      The default configuration values for the bootstrap playbook.

   ``sysadmin home directory ($HOME)``
      The default location where Ansible looks for and imports user
      configuration override files for hosts. For example:
      ``$HOME/<hostname>.yml``.

   .. only:: starlingx

      .. include:: /shared/_includes/ansible_install_time_only.txt

   Specify the user configuration override file for the Ansible bootstrap
   playbook using one of the following methods:

   .. note::

      This Ansible Overrides file for the Bootstrap Playbook ($HOME/localhost.yml)
      contains security sensitive information, use the
      :command:`ansible-vault create $HOME/localhost.yml` command to create it.
      You will be prompted for a password to protect/encrypt the file.
      Use the :command:`ansible-vault edit $HOME/localhost.yml` command if the
      file needs to be edited after it is created.

   #. Use a copy of the default.yml file listed above to provide your overrides.

      The default.yml file lists all available parameters for bootstrap
      configuration with a brief description for each parameter in the file
      comments.

      To use this method, run the :command:`ansible-vault create $HOME/localhost.yml`
      command and copy the contents of the ``default.yml`` file into the
      ansible-vault editor, and edit the configurable values as required.

   #. Create a minimal user configuration override file.

      To use this method, create your override file with
      the :command:`ansible-vault create $HOME/localhost.yml`
      command and provide the minimum required parameters for the deployment
      configuration as shown in the example below. Use the OAM IP SUBNET and IP
      ADDRESSing applicable to your deployment environment.

      .. include:: /shared/_includes/quotation-marks-in-keystone-password.rest

      .. include:: /_includes/min-bootstrap-overrides-simplex.rest

      .. only:: starlingx

         In either of the above options, the bootstrap playbook's default
         values will pull all container images required for the |prod-p| from
         Docker hub

         If you have setup a private Docker registry to use for bootstrapping
         then you will need to add the following lines in $HOME/localhost.yml:

      .. only:: partner

         .. include:: /_includes/install-kubernetes-bootstrap-playbook.rest
            :start-after: docker-reg-begin
            :end-before: docker-reg-end

      .. code-block::

         docker_registries:
           quay.io:
             url: myprivateregistry.abc.com:9001/quay.io
           docker.elastic.co:
             url: myprivateregistry.abc.com:9001/docker.elastic.co
           gcr.io:
             url: myprivateregistry.abc.com:9001/gcr.io
           ghcr.io:
             url: myprivateregistry.abc.com:9001/ghcr.io
           k8s.gcr.io:
             url: myprivateregistry.abc.com:9001/k8s.gcr.io
           docker.io:
             url: myprivateregistry.abc.com:9001/docker.io
           registry.k8s.io:
             url: myprivateregistry.abc.com:9001/registry.k8s.io
           icr.io:
             url: myprivateregistry.abc.com:9001/icr.io
           defaults:
             type: docker
             username: <your_myprivateregistry.abc.com_username>
             password: <your_myprivateregistry.abc.com_password>

         # Add the CA Certificate that signed myprivateregistry.abc.com’s
         # certificate as a Trusted CA
         ssl_ca_cert: /home/sysadmin/myprivateregistry.abc.com-ca-cert.pem

      See :ref:`Use a Private Docker Registry <use-private-docker-registry-r7>`
      for more information.


      .. only:: starlingx

         If a firewall is blocking access to Docker hub or your private
         registry from your StarlingX deployment, you will need to add the
         following lines in ``$HOME/localhost.yml``  (see |docker_proxy_config|
         for more details about Docker proxy settings):

      .. only:: partner

         .. include:: /_includes/install-kubernetes-bootstrap-playbook.rest
            :start-after: firewall-begin
            :end-before: firewall-end

      .. code-block::

         # Add these lines to configure Docker to use a proxy server
         docker_http_proxy: http://my.proxy.com:1080
         docker_https_proxy: https://my.proxy.com:1443
         docker_no_proxy:
            - 1.2.3.4

      Configure ``system_local_ca_cert``, ``system_local_ca_key`` and
      ``system_root_ca_cert`` to setup a local intermediate |CA| (signed by an
      external Root |CA|) for managing / signing all of the |prod|
      Certificates. See :ref:`ansible_bootstrap_configs_platform_issuer` for
      more details.

      Refer to :ref:`ansible_bootstrap_configs_r7` for information on
      additional Ansible bootstrap configurations for advanced Ansible
      bootstrap scenarios.

#. Run the Ansible bootstrap playbook:

   .. include:: /shared/_includes/ntp-update-note.rest

   ::

      ansible-playbook --ask-vault-pass /usr/share/ansible/stx-ansible/playbooks/bootstrap.yml

   Wait for Ansible bootstrap playbook to complete. This can take 5-10 minutes,
   depending on the performance of the host machine.

----------------------
Configure controller-0
----------------------

The newly installed controller needs to be configured.

#. Acquire admin credentials:

   ::

     source /etc/platform/openrc

#. Configure the |OAM| interface of controller-0 and specify the attached
   network as "oam".

   .. only:: starlingx

      .. tabs::

         .. group-tab:: Bare Metal

            .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
               :start-after: begin-config-controller-0-oam-interface-sx
               :end-before: end-config-controller-0-oam-interface-sx

         .. group-tab:: Virtual

            ::

              OAM_IF=enp7s1
              system host-if-modify controller-0 $OAM_IF -c platform
              system interface-network-assign controller-0 $OAM_IF oam

   .. only:: partner

      .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
         :start-after: begin-config-controller-0-oam-interface-sx
         :end-before: end-config-controller-0-oam-interface-sx

#. Configure |NTP| servers for network time synchronization:

   .. only:: starlingx

      .. tabs::

         .. group-tab:: Bare Metal

            .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
               :start-after: begin-config-controller-0-ntp-interface-sx
               :end-before: end-config-controller-0-ntp-interface-sx

            .. code-block:: none

                  ~(keystone_admin)$ system ntp-modify ntpservers=0.pool.ntp.org,1.pool.ntp.org

            To configure |PTP| instead of |NTP|, see |ptp-server-config-index|.

            .. end-config-controller-0-ntp-interface-sx

         .. group-tab:: Virtual

            .. note::

               In a virtual environment, this can sometimes cause Ceph clock
               skew alarms. Also, the virtual instances clock is synchronized
               with the host clock, so it is not absolutely required to
               configure |NTP| in this step.

            .. code-block:: none

                ~(keystone_admin)$ system ntp-modify ntpservers=0.pool.ntp.org,1.pool.ntp.org

   .. only:: partner

      .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
         :start-after: begin-config-controller-0-ntp-interface-sx
         :end-before: end-config-controller-0-ntp-interface-sx

.. only:: openstack

   *************************************
   OpenStack-specific host configuration
   *************************************

   .. incl-config-controller-0-openstack-specific-aio-simplex-start:

   .. important::

      These steps are required only if the StarlingX OpenStack application
      (|prefix|-openstack) will be installed.

   #. **For OpenStack only:** Assign OpenStack host labels to controller-0 in
      support of installing the |prefix|-openstack manifest and helm-charts later.

      .. only:: starlingx

         .. parsed-literal::

            ~(keystone_admin)$ system host-label-assign controller-0 openstack-control-plane=enabled
            ~(keystone_admin)$ system host-label-assign controller-0 openstack-compute-node=enabled
            ~(keystone_admin)$ system host-label-assign controller-0 |vswitch-label|

         .. note::

            If you have a |NIC| that supports |SRIOV|, then you can enable it by
            using the following:

            .. code-block:: none

               ~(keystone_admin)$ system host-label-assign controller-0 sriov=enabled

      .. only:: partner

         .. include:: /_includes/aio_simplex_install_kubernetes.rest
            :start-after: ref1-begin
            :end-before: ref1-end

   #. **For OpenStack only:** Due to the additional OpenStack services running
      on the |AIO| controller platform cores, additional platform cores may be
      required.

      .. only:: starlingx

         .. tabs::

            .. group-tab:: Bare Metal

              .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
                 :start-after: begin-config-controller-0-OS-add-cores-sx
                 :end-before: end-config-controller-0-OS-add-cores-sx


            .. group-tab:: Virtual

               The |VMs| being used for hosts only have 4 cores; 2 for platform
               and 2 for |VMs|. There are no additional cores available for
               platform in this scenario.


      .. only:: partner

         .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
            :start-after: begin-config-controller-0-OS-add-cores-sx
            :end-before: end-config-controller-0-OS-add-cores-sx

   #. Due to the additional OpenStack services' containers running on the
      controller host, the size of the Docker filesystem needs to be
      increased from the default size of 30G to 60G.

      .. code-block:: bash

          # check existing size of docker fs
          system host-fs-list controller-0
          # check available space (Avail Size (GiB)) in cgts-vg LVG where docker fs is located
          system host-lvg-list controller-0
          # if existing docker fs size + cgts-vg available space is less than
          # 60G, you will need to add a new disk to cgts-vg.

             # Get device path of BOOT DISK
             system host-show controller-0 | fgrep rootfs

             # Get UUID of ROOT DISK by listing disks
             system host-disk-list controller-0

             # Add new disk to 'cgts-vg' local volume group
             system host-pv-add controller-0 cgts-vg <DISK_UUID>
             sleep 10    # wait for disk to be added

             # Confirm the available space and increased number of physical
             # volumes added to the cgts-vg colume group
             system host-lvg-list controller-0

          # Increase docker filesystem to 60G
          system host-fs-modify controller-0 docker=60

   #. **For OpenStack only:** Configure the system setting for the vSwitch.

      .. only:: starlingx

         .. tabs::

            .. group-tab:: Bare Metal

               StarlingX has |OVS| (kernel-based) vSwitch configured as default:

               * Runs in a container; defined within the helm charts of
                 |prefix|-openstack manifest.

               * Shares the core(s) assigned to the platform.

               If you require better performance, |OVS-DPDK| (|OVS| with the
               Data Plane Development Kit, which is supported only on bare
               metal hardware) should be used:

               * Runs directly on the host (it is not containerized). Requires
                 that at least 1 core be assigned/dedicated to the vSwitch
                 function.

               To deploy the default containerized |OVS|:

               ::

                    ~(keystone_admin)$ system modify --vswitch_type none

               This does not run any vSwitch directly on the host, instead, it
               uses the containerized |OVS| defined in the helm charts of
               |prefix|-openstack manifest.

               .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
                  :start-after: begin-config-controller-0-OS-vswitch-sx
                  :end-before: end-config-controller-0-OS-vswitch-sx

            .. group-tab:: Virtual

               The default vSwitch is the containerized |OVS| that is packaged
               with the ``stx-openstack`` manifest/helm-charts. |prod| provides
               the option to use OVS-DPDK on the host, however, in the virtual
               environment OVS-DPDK is not supported, only |OVS| is supported.
               Therefore, simply use the default |OVS| vSwitch here.

      .. only:: partner

         .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
            :start-after: begin-config-controller-0-OS-vswitch-sx
            :end-before: end-config-controller-0-OS-vswitch-sx

   #. **For OpenStack only:** Add an instances filesystem or set up a disk
      based nova-local volume group, which is needed for |prefix|-openstack
      nova ephemeral disks.

      .. only:: starlingx

         .. tabs::

            .. group-tab:: Bare Metal

               .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
                  :start-after: begin-config-controller-0-OS-add-fs-sx
                  :end-before: end-config-controller-0-OS-add-fs-sx

            .. group-tab:: Virtual

               Set up an "instances" filesystem, which is needed for
               stx-openstack nova ephemeral disks.

               .. code-block:: bash

                  ~(keystone_admin)$ export NODE=controller-0
                  ~(keystone_admin)$ system host-fs-add ${NODE} instances=34


      .. only:: partner

         .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
            :start-after: begin-config-controller-0-OS-add-fs-sx
            :end-before: end-config-controller-0-OS-add-fs-sx

   #. **For OpenStack only:** Configure data interfaces for controller-0.
      Data class interfaces are vSwitch interfaces used by vSwitch to provide
      |VM| virtio vNIC connectivity to OpenStack Neutron Tenant Networks on the
      underlying assigned Data Network.

      .. important::

         A compute-labeled |AIO|-controller host **MUST** have at least one
         Data class interface.

      * Configure the data interfaces for controller-0.

        .. only:: starlingx

           .. tabs::

              .. group-tab:: Bare Metal

                 .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
                    :start-after: begin-config-controller-0-OS-data-interface-sx
                    :end-before: end-config-controller-0-OS-data-interface-sx

              .. group-tab:: Virtual

                 .. code-block:: bash

                    ~(keystone_admin)$ DATA0IF=eth1000
                    ~(keystone_admin)$ DATA1IF=eth1001
                    ~(keystone_admin)$ export NODE=controller-0
                    ~(keystone_admin)$ PHYSNET0='physnet0'
                    ~(keystone_admin)$ PHYSNET1='physnet1'
                    ~(keystone_admin)$ SPL=/tmp/tmp-system-port-list
                    ~(keystone_admin)$ SPIL=/tmp/tmp-system-host-if-list
                    ~(keystone_admin)$ system host-port-list ${NODE} --nowrap > ${SPL}
                    ~(keystone_admin)$ system host-if-list -a ${NODE} --nowrap > ${SPIL}
                    ~(keystone_admin)$ DATA0PCIADDR=$(cat $SPL | grep $DATA0IF |awk '{print $8}')
                    ~(keystone_admin)$ DATA1PCIADDR=$(cat $SPL | grep $DATA1IF |awk '{print $8}')
                    ~(keystone_admin)$ DATA0PORTUUID=$(cat $SPL | grep ${DATA0PCIADDR} | awk '{print $2}')
                    ~(keystone_admin)$ DATA1PORTUUID=$(cat $SPL | grep ${DATA1PCIADDR} | awk '{print $2}')
                    ~(keystone_admin)$ DATA0PORTNAME=$(cat $SPL | grep ${DATA0PCIADDR} | awk '{print $4}')
                    ~(keystone_admin)$ DATA1PORTNAME=$(cat  $SPL | grep ${DATA1PCIADDR} | awk '{print $4}')
                    ~(keystone_admin)$ DATA0IFUUID=$(cat $SPIL | awk -v DATA0PORTNAME=$DATA0PORTNAME '($12 ~ DATA0PORTNAME) {print $2}')
                    ~(keystone_admin)$ DATA1IFUUID=$(cat $SPIL | awk -v DATA1PORTNAME=$DATA1PORTNAME '($12 ~ DATA1PORTNAME) {print $2}')

                    ~(keystone_admin)$ system datanetwork-add ${PHYSNET0} vlan
                    ~(keystone_admin)$ system datanetwork-add ${PHYSNET1} vlan

                    ~(keystone_admin)$ system host-if-modify -m 1500 -n data0 -c data ${NODE} ${DATA0IFUUID}
                    ~(keystone_admin)$ system host-if-modify -m 1500 -n data1 -c data ${NODE} ${DATA1IFUUID}
                    ~(keystone_admin)$ system interface-datanetwork-assign ${NODE} ${DATA0IFUUID} ${PHYSNET0}
                    ~(keystone_admin)$ system interface-datanetwork-assign ${NODE} ${DATA1IFUUID} ${PHYSNET1}


      .. only:: partner

         .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
            :start-after: begin-config-controller-0-OS-data-interface-sx
            :end-before: end-config-controller-0-OS-data-interface-sx

*****************************************
Optionally Configure PCI-SRIOV Interfaces
*****************************************

#. **Optionally**, configure |PCI|-SRIOV interfaces for controller-0.

   This step is **optional** for Kubernetes. Do this step if using |SRIOV|
   network attachments in hosted application containers.

   .. only:: openstack

      This step is **optional** for OpenStack. Do this step if using |SRIOV|
      vNICs in hosted application VMs. Note that |PCI|-SRIOV interfaces can
      have the same Data Networks assigned to them as vswitch data interfaces.


   #. Configure the |PCI|-SRIOV interfaces for controller-0.

      .. code-block:: bash

         ~(keystone_admin)$ export NODE=controller-0

         # List inventoried host’s ports and identify ports to be used as ‘pci-sriov’ interfaces,
         # based on displayed linux port name, pci address and device type.
         ~(keystone_admin)$ system host-port-list ${NODE}

         # List host’s auto-configured ‘ethernet’ interfaces,
         # find the interfaces corresponding to the ports identified in previous step, and
         # take note of their UUID
         ~(keystone_admin)$ system host-if-list -a ${NODE}

         # Modify configuration for these interfaces
         # Configuring them as ‘pci-sriov’ class interfaces, MTU of 1500 and named sriov#
         ~(keystone_admin)$ system host-if-modify -m 1500 -n sriov0 -c pci-sriov ${NODE} <sriov0-if-uuid> -N <num_vfs>
         ~(keystone_admin)$ system host-if-modify -m 1500 -n sriov1 -c pci-sriov ${NODE} <sriov1-if-uuid> -N <num_vfs>

         # If not already created, create Data Networks that the 'pci-sriov' interfaces will
         # be connected to
         ~(keystone_admin)$ DATANET0='datanet0'
         ~(keystone_admin)$ DATANET1='datanet1'
         ~(keystone_admin)$ system datanetwork-add ${DATANET0} vlan
         ~(keystone_admin)$ system datanetwork-add ${DATANET1} vlan

         # Assign Data Networks to PCI-SRIOV Interfaces
         ~(keystone_admin)$ system interface-datanetwork-assign ${NODE} <sriov0-if-uuid> ${DATANET0}
         ~(keystone_admin)$ system interface-datanetwork-assign ${NODE} <sriov1-if-uuid> ${DATANET1}


   #. **For Kubernetes Only:** To enable using |SRIOV| network attachments for
      the above interfaces in Kubernetes hosted application containers:

      .. only:: starlingx

         .. tabs::

            .. group-tab:: Bare Metal

               .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
                  :start-after: begin-config-controller-0-OS-k8s-sriov-sx
                  :end-before: end-config-controller-0-OS-k8s-sriov-sx

            .. group-tab:: Virtual

               Configure the Kubernetes |SRIOV| device plugin.

               .. code-block:: none

                  ~(keystone_admin)$ system host-label-assign controller-0 sriovdp=enabled

      .. only:: partner

         .. include:: /shared/_includes/aio_simplex_install_kubernetes.rest
            :start-after: begin-config-controller-0-OS-k8s-sriov-sx
            :end-before: end-config-controller-0-OS-k8s-sriov-sx


************************************************************
Optional - Initialize a Ceph-rook Persistent Storage Backend
************************************************************

A persistent storage backend is required if your application requires
|PVCs|.

.. only:: openstack

   .. important::

      The StarlingX OpenStack application **requires** |PVCs|.

.. note::

   Each deployment model enforces a different structure for the Rook Ceph
   cluster and its integration with the platform.

There are two options for persistent storage backend: the host-based Ceph
solution and the Rook container-based Ceph solution.

.. note::

   Host-based Ceph will be deprecated and removed in an upcoming release.
   Adoption of Rook-Ceph is recommended for new deployments.

For host-based Ceph:

#. Add host-based Ceph backend:

   ::

      ~(keystone_admin)$ system storage-backend-add ceph --confirmed

#. Add an |OSD| on controller-0 for host-based Ceph:

   .. code-block:: bash

      # List host’s disks and identify disks you want to use for CEPH OSDs, taking note of their UUID
      # By default, /dev/sda is being used as system disk and can not be used for OSD.
      ~(keystone_admin)$ system host-disk-list controller-0

      # Add disk as an OSD storage
      ~(keystone_admin)$ system host-stor-add controller-0 osd <disk-uuid>

      # List OSD storage devices
      ~(keystone_admin)$ system host-stor-list controller-0

For Rook-Ceph:

#. Check if the rook-ceph app is uploaded.

   .. code-block:: none

      $ source /etc/platform/openrc
      $ system application-list
      +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
      | application              | version   | manifest name                             | manifest file    | status   | progress  |
      +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
      | cert-manager             | 24.09-76  | cert-manager-fluxcd-manifests             | fluxcd-manifests | applied  | completed |
      | dell-storage             | 24.09-25  | dell-storage-fluxcd-manifests             | fluxcd-manifests | uploaded | completed |
      | deployment-manager       | 24.09-13  | deployment-manager-fluxcd-manifests       | fluxcd-manifests | applied  | completed |
      | nginx-ingress-controller | 24.09-57  | nginx-ingress-controller-fluxcd-manifests | fluxcd-manifests | applied  | completed |
      | oidc-auth-apps           | 24.09-53  | oidc-auth-apps-fluxcd-manifests           | fluxcd-manifests | uploaded | completed |
      | platform-integ-apps      | 24.09-138 | platform-integ-apps-fluxcd-manifests      | fluxcd-manifests | uploaded | completed |
      | rook-ceph                | 24.09-12  | rook-ceph-fluxcd-manifests                | fluxcd-manifests | uploaded | completed |
      +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+

#. Add Storage-Backend with Deployment Model.

   There are three deployment models: Controller, Dedicated, and Open.

   For the simplex and duplex environments you can use the Controller and Open
   configuration.

   Controller (default)
      |OSDs| must only be added to host with controller personality set.

      Replication factor is limited to a maximum of 2.

   Dedicated
      |OSDs| must be added only to hosts with the worker personality.

      The replication factor is limited to a maximum of 3.

      This model aligns with existing Bare-metal Ceph use of dedicated storage
      hosts in groups of 2 or 3.

   Open
      |OSDs| can be added to any host without limitations.

      Replication factor has no limitations.

   Application Strategies for deployment model controller.

   Simplex
      |OSDs|: Added to controller nodes.

      Replication Factor: Default 1, maximum 2.

      MON, MGR, MDS: Configured based on the number of hosts where the
      ``host-fs ceph`` is available.

   .. code-block:: none

      $ system storage-backend-add ceph-rook --deployment controller --confirmed
      $ system storage-backend-list
      +--------------------------------------+-----------------+-----------+----------------------+----------+------------+-------------------------------------------+
      | uuid                                 | name            | backend   | state                | task     | services   | capabilities                              |
      +--------------------------------------+-----------------+-----------+----------------------+----------+------------+-------------------------------------------+
      | a2452e47-4b2b-4a3a-a8f0-fb749d92d9cd | ceph-rook-store | ceph-rook | configuring-with-app | uploaded | block,     | deployment_model: controller replication: |
      |                                      |                 |           |                      |          | filesystem | 1 min_replication: 1                      |
      +--------------------------------------+-----------------+-----------+----------------------+----------+------------+-------------------------------------------+

#. Set up a ``host-fs ceph`` filesystem.

   .. code-block:: none

      $ system host-fs-add controller-0 ceph=20

   .. incl-config-controller-0-openstack-specific-aio-simplex-end:

-------------------
Unlock controller-0
-------------------

.. incl-unlock-controller-0-aio-simplex-start:

Unlock controller-0 to bring it into service:

::

  ~(keystone_admin)$ system host-unlock controller-0

Controller-0 will reboot in order to apply configuration changes and come into
service. This can take 5-10 minutes, depending on the performance of the host
machine.

For Rook-Ceph:

#. List all the disks.

   .. code-block:: none

      $ system host-disk-list controller-0
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      | uuid                                 | device_node | device_num | device_type | size_gib | available_gib | rpm          | serial_id           | device_path                                |
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      | 17408af3-e211-4e2b-8cf1-d2b6687476d5 | /dev/sda    | 2048       | HDD         | 292.968  | 0.0           | Undetermined | VBba52ec56-f68a9f2d | /dev/disk/by-path/pci-0000:00:0d.0-ata-1.0 |
      | cee99187-dac4-4a7b-8e58-f2d5bd48dcaf | /dev/sdb    | 2064       | HDD         | 9.765    | 0.0           | Undetermined | VBf96fa322-597194da | /dev/disk/by-path/pci-0000:00:0d.0-ata-2.0 |
      | 0c6435af-805a-4a62-ad8e-403bf916f5cf | /dev/sdc    | 2080       | HDD         | 9.765    | 9.761         | Undetermined | VBeefed5ad-b4815f0d | /dev/disk/by-path/pci-0000:00:0d.0-ata-3.0 |
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+

#. Choose empty disks and provide hostname and uuid to finish |OSD|
   configuration:

   .. code-block:: none

      $ system host-stor-add controller-0 osd cee99187-dac4-4a7b-8e58-f2d5bd48dcaf

#. Wait for |OSDs| pod to be ready.

   .. code-block:: none

      $ kubectl get pods -n rook-ceph
      NAME                                                     READY   STATUS      RESTARTS      AGE
      ceph-mgr-provision-78xjk                                0/1     Completed   0          4m31s
      csi-cephfsplugin-572jc                                  2/2     Running     0          5m32s
      csi-cephfsplugin-provisioner-5467c6c4f-t8x8d            5/5     Running     0          5m28s
      csi-rbdplugin-2npb6                                     2/2     Running     0          5m32s
      csi-rbdplugin-provisioner-fd84899c-k8wcw                5/5     Running     0          5m32s
      rook-ceph-crashcollector-controller-0-589f5f774-d8sjz   1/1     Running     0          3m24s
      rook-ceph-exporter-controller-0-5fd477bb8-c7nxh         1/1     Running     0          3m21s
      rook-ceph-mds-kube-cephfs-a-cc647757-6p9j5              2/2     Running     0          3m25s
      rook-ceph-mds-kube-cephfs-b-5b5845ff59-xprbb            2/2     Running     0          3m19s
      rook-ceph-mgr-a-746fc4dd54-t8bcw                        2/2     Running     0          4m40s
      rook-ceph-mon-a-b6c95db97-f5fqq                         2/2     Running     0          4m56s
      rook-ceph-operator-69b5674578-27bn4                     1/1     Running     0          6m26s
      rook-ceph-osd-0-7f5cd957b8-ppb99                        2/2     Running     0          3m52s
      rook-ceph-osd-prepare-controller-0-vzq2d                0/1     Completed   0          4m18s
      rook-ceph-provision-zcs89                               0/1     Completed   0          101s
      rook-ceph-tools-7dc9678ccb-v2gps                        1/1     Running     0          6m2s
      stx-ceph-manager-664f8585d8-wzr4v                       1/1     Running     0          4m31s

#. Check ceph cluster health.

   .. code-block:: none

      $ ceph -s
          cluster:
              id:     75c8f017-e7b8-4120-a9c1-06f38e1d1aa3
              health: HEALTH_OK

          services:
              mon: 1 daemons, quorum a (age 32m)
              mgr: a(active, since 30m)
              mds: 1/1 daemons up, 1 hot standby
              osd: 1 osds: 1 up (since 30m), 1 in (since 31m)

          data:
              volumes: 1/1 healthy
              pools:   4 pools, 113 pgs
              objects: 22 objects, 595 KiB
              usage:   27 MiB used, 9.7 GiB / 9.8 GiB avail
              pgs:     113 active+clean

          io:
              client:   852 B/s rd, 1 op/s rd, 0 op/s wr


.. incl-unlock-controller-0-aio-simplex-end:

.. only:: partner

   .. include:: /_includes/72hr-to-license.rest

Complete system configuration by reviewing procedures in:

- |index-security-kub-81153c1254c3|
- |index-sysconf-kub-78f0e1e9ca5a|
- |index-admintasks-kub-ebc55fefc368|

.. end-content