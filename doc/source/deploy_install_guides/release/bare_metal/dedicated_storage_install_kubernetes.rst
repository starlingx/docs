|hideable|

.. include:: /_vendor/rl-strings.txt

.. include:: /_stx-related-links/installation-prereqs.rln 
   :start-after: begin-rls
   :end-before: end-rls

.. _dedicated_storage_install_kubernetes_r7:


==============================================================
Install Kubernetes Platform on Standard with Dedicated Storage
==============================================================

.. begin-content

.. contents::
   :local:
   :depth: 1

.. only:: partner

   .. include:: /_includes/install-kubernetes-null-labels.rest

--------
Overview
--------

.. _ded-installation-prereqs:

.. include:: /shared/_includes/desc_dedicated_storage.txt

.. include:: /_includes/automated-install-note--09dff208a9c6.rest 

-----------------------------
Minimum hardware requirements
-----------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
            :start-after: begin-min-hw-reqs-ded
            :end-before: end-min-hw-reqs-ded

      .. group-tab:: Virtual

         .. include:: /shared/_includes/physical_host_req.txt

.. only:: partner

   .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
      :start-after: begin-min-hw-reqs-ded
      :end-before: end-min-hw-reqs-ded

.. _installation-prereqs-dedicated:

--------------------------
Installation Prerequisites
--------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

        .. include:: /shared/_includes/installation-prereqs.rest
           :start-after: begin-install-prereqs
           :end-before: end-install-prereqs

      .. group-tab:: Virtual

         Several pre-requisites must be completed prior to starting the |prod|
         installation.

         Before attempting to install |prod|, ensure that you have the the
         |prod| host installer ISO image file.

         Get the latest |prod| ISO from the `StarlingX mirror
         <https://mirror.starlingx.cengn.ca/mirror/starlingx/release/latest_release/debian/monolithic/outputs/iso/>`__.
         Alternately, you can get an older release ISO from `here
         <https://mirror.starlingx.cengn.ca/mirror/starlingx/release/>`__.


.. only:: partner

   .. include:: /shared/_includes/installation-prereqs.rest
      :start-after: begin-install-prereqs
      :end-before: end-install-prereqs

.. _dedicated-install-prep-servers:

--------------------------------
Prepare Servers for Installation
--------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
            :start-after: start-prepare-servers-common
            :end-before: end-prepare-servers-common

      .. group-tab:: Virtual

         .. note::
         
             The following commands for host, virtual environment setup, and
             host power-on use KVM / virsh for virtual machine and |VM|
             management technology. For an alternative virtualization
             environment, see: :ref:`Install StarlingX in VirtualBox
             <install_virtualbox>`.
         
         #. Prepare virtual environment.
         
            Set up virtual platform networks for virtual deployment:
         
            ::
         
              bash setup_network.sh
         
         #. Prepare virtual servers.
         
            Create the XML definitions for the virtual servers required by this
            configuration option. This will create the XML virtual server
            definition for:
         
            * dedicatedstorage-controller-0
            * dedicatedstorage-controller-1
            * dedicatedstorage-storage-0
            * dedicatedstorage-storage-1
            * dedicatedstorage-worker-0
            * dedicatedstorage-worker-1
         
            The following command will start/virtually power on:
         
            * The 'dedicatedstorage-controller-0' virtual server
            * The X-based graphical virt-manager application
         
            ::
         
              bash setup_configuration.sh -c dedicatedstorage -i ./bootimage.iso
         
            If there is no X-server present errors will occur and the X-based
            GUI for the virt-manager application will not start. The
            virt-manager GUI is not absolutely required and you can safely
            ignore errors and continue.


.. only:: partner

   .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
      :start-after: start-prepare-servers-common
      :end-before: end-prepare-servers-common

--------------------------------
Install Software on Controller-0
--------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/inc-install-software-on-controller.rest
            :start-after: incl-install-software-controller-0-standard-start
            :end-before: incl-install-software-controller-0-standard-end

      .. group-tab:: Virtual

         In the last step of :ref:`dedicated-install-prep-servers`, the
         controller-0 virtual server 'dedicatedstorage-controller-0' was
         started by the :command:`setup_configuration.sh` command.
         
         On the host, attach to the console of virtual controller-0 and select
         the appropriate installer menu options to start the non-interactive
         install of StarlingX software on controller-0.
         
         .. note::
         
            When entering the console, it is very easy to miss the first
            installer menu selection. Use ESC to navigate to previous menus, to
            ensure you are at the first installer menu.
         
         ::
         
           virsh console dedicatedstorage-controller-0
         
         Make the following menu selections in the installer:
         
         #. First menu: Select 'Standard Controller Configuration'
         #. Second menu: Select 'Serial Console'
         
         Wait for the non-interactive install of software to complete and for
         the server to reboot. This can take 5-10 minutes depending on the
         performance of the host machine.

.. only:: partner

   .. include:: /shared/_includes/inc-install-software-on-controller.rest
      :start-after: incl-install-software-controller-0-standard-start
      :end-before: incl-install-software-controller-0-standard-end

--------------------------------
Bootstrap system on controller-0
--------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/incl-bootstrap-sys-controller-0-standard.rest
            :start-after: incl-bootstrap-sys-controller-0-standard-start
            :end-before: incl-bootstrap-sys-controller-0-standard-end

      .. group-tab:: Virtual

         .. include:: /shared/_includes/incl-bootstrap-controller-0-virt-controller-storage-start.rest
            :start-after: incl-bootstrap-controller-0-virt-controller-storage-start:
            :end-before: incl-bootstrap-controller-0-virt-controller-storage-end:

.. only:: partner

   .. include:: /shared/_includes/incl-bootstrap-sys-controller-0-standard.rest
      :start-after: incl-bootstrap-sys-controller-0-standard-start
      :end-before: incl-bootstrap-sys-controller-0-standard-end

----------------------
Configure controller-0
----------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/incl-config-controller-0-storage.rest 
            :start-after: incl-config-controller-0-storage-start
            :end-before: incl-config-controller-0-storage-end

      .. group-tab:: Virtual

         .. include:: /shared/_includes/incl-config-controller-0-virt-controller-storage.rest
            :start-after: incl-config-controller-0-virt-controller-storage-start:
            :end-before: incl-config-controller-0-virt-controller-storage-end:


.. only:: partner

   .. include:: /shared/_includes/incl-config-controller-0-storage.rest 
      :start-after: incl-config-controller-0-storage-start
      :end-before: incl-config-controller-0-storage-end

-------------------
Unlock controller-0
-------------------

.. important::

  Make sure the Ceph storage backend is configured. If it is
  not configured, you will not be able to configure storage
  nodes.

Unlock controller-0 in order to bring it into service:

::

  system host-unlock controller-0

Controller-0 will reboot in order to apply configuration changes and come into
service. This can take 5-10 minutes, depending on the performance of the host
machine.

-----------------------------------------------------------------
Install software on controller-1, storage nodes, and worker nodes
-----------------------------------------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
            :start-after: begin-install-sw-cont-1-stor-and-wkr-nodes
            :end-before: end-install-sw-cont-1-stor-and-wkr-nodes

      .. group-tab:: Virtual

         #. On the host, power on the controller-1 virtual server,
            'dedicatedstorage-controller-1'. It will automatically attempt to
            network boot over the management network:
         
            ::
         
               virsh start dedicatedstorage-controller-1
         
         #. Attach to the console of virtual controller-1:
         
            ::
         
               virsh console dedicatedstorage-controller-1
         
         #. As controller-1 VM boots, a message appears on its console
            instructing you to configure the personality of the node.
         
         #. On the console of controller-0, list hosts to see newly discovered
            controller-1 host (hostname=None):
         
            ::
         
               system host-list
               +----+--------------+-------------+----------------+-------------+--------------+
               | id | hostname     | personality | administrative | operational | availability |
               +----+--------------+-------------+----------------+-------------+--------------+
               | 1  | controller-0 | controller  | unlocked       | enabled     | available    |
               | 2  | None         | None        | locked         | disabled    | offline      |
               +----+--------------+-------------+----------------+-------------+--------------+
         
         #. Using the host id, set the personality of this host to 'controller':
         
            ::
         
               system host-update 2 personality=controller
         
            This initiates software installation on controller-1. This can take
            5-10 minutes, depending on the performance of the host machine.
         
         #. While waiting on the previous step to complete, start up and set
            the personality for 'dedicatedstorage-storage-0' and
            'dedicatedstorage-storage-1'. Set the personality to 'storage' and
            assign a unique hostname for each.
         
            For example, start 'dedicatedstorage-storage-0' from the host:
         
            ::
         
               virsh start dedicatedstorage-storage-0
         
            Wait for new host (hostname=None) to be discovered by checking
            ``system host-list`` on virtual controller-0:
         
            ::
         
               system host-update 3 personality=storage
         
            Repeat for 'dedicatedstorage-storage-1'. On the host:
         
            ::
         
               virsh start dedicatedstorage-storage-1
         
            And wait for new host (hostname=None) to be discovered by checking
            ``system host-list`` on virtual controller-0:
         
            ::
         
               system host-update 4 personality=storage
         
            This initiates software installation on storage-0 and storage-1.
            This can take 5-10 minutes, depending on the performance of the
            host machine.
         
         #. While waiting on the previous step to complete, start up and set
            the personality for 'dedicatedstorage-worker-0' and
            'dedicatedstorage-worker-1'. Set the personality to 'worker' and
            assign a unique hostname for each.
         
            For example, start 'dedicatedstorage-worker-0' from the host:
         
            ::
         
               virsh start dedicatedstorage-worker-0
         
            Wait for new host (hostname=None) to be discovered by checking
            ``system host-list`` on virtual controller-0:
         
            ::
         
               system host-update 5 personality=worker hostname=worker-0
         
            Repeat for 'dedicatedstorage-worker-1'. On the host:
         
            ::
         
               virsh start dedicatedstorage-worker-1
         
            And wait for new host (hostname=None) to be discovered by checking
            ``system host-list`` on virtual controller-0:
         
            ::
         
               system host-update 6 personality=worker hostname=worker-1
         
            This initiates software installation on worker-0 and worker-1.
         
            .. only:: starlingx
         
              .. Note::
         
                 A node with Edgeworker personality is also available. See
                 |deploy-edgeworker-nodes| for details.
         
         #. Wait for the software installation on controller-1, storage-0,
            storage-1, worker-0, and worker-1 to complete, for all virtual
            servers to reboot, and for all to show as locked/disabled/online in
            'system host-list'.
         
            ::
         
               system host-list
               +----+--------------+-------------+----------------+-------------+--------------+
               | id | hostname     | personality | administrative | operational | availability |
               +----+--------------+-------------+----------------+-------------+--------------+
               | 1  | controller-0 | controller  | unlocked       | enabled     | available    |
               | 2  | controller-1 | controller  | locked         | disabled    | online       |
               | 3  | storage-0    | storage     | locked         | disabled    | online       |
               | 4  | storage-1    | storage     | locked         | disabled    | online       |
               | 5  | worker-0     | worker      | locked         | disabled    | online       |
               | 6  | worker-1     | worker      | locked         | disabled    | online       |
               +----+--------------+-------------+----------------+-------------+--------------+

.. only:: partner

   .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
      :start-after: begin-install-sw-cont-1-stor-and-wkr-nodes
      :end-before: end-install-sw-cont-1-stor-and-wkr-nodes

----------------------
Configure controller-1
----------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/incl-config-controller-1.rest
            :start-after: incl-config-controller-1-start:
            :end-before: incl-config-controller-1-end:

      .. group-tab:: Virtual
   
         .. include:: /shared/_includes/incl-config-controller-1-virt-controller-storage.rest
            :start-after: incl-config-controller-1-virt-controller-storage-start
            :end-before: incl-config-controller-1-virt-controller-storage-end

.. only:: partner

   .. include:: /shared/_includes/incl-config-controller-1.rest
      :start-after: incl-config-controller-1-start:
      :end-before: incl-config-controller-1-end:

-------------------
Unlock controller-1
-------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: controller_storage_install_kubernetes.rst
            :start-after: incl-unlock-controller-1-start:
            :end-before: incl-unlock-controller-1-end:

      .. group-tab:: Virtual

         .. include:: controller_storage_install_kubernetes.rst
            :start-after: incl-unlock-controller-1-virt-controller-storage-start:
            :end-before: incl-unlock-controller-1-virt-controller-storage-end:

.. only:: partner

   .. include:: controller_storage_install_kubernetes.rst
      :start-after: incl-unlock-controller-1-start:
      :end-before: incl-unlock-controller-1-end:

.. include:: /_includes/bootstrapping-and-deploying-starlingx.rest

-----------------------
Configure storage nodes
-----------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
            :start-after: begin-dedicated-config-storage-nodes
            :end-before: end-dedicated-config-storage-nodes


      .. group-tab:: Virtual

         On virtual controller-0:
         
         #. Assign the cluster-host network to the MGMT interface for the
            storage nodes.
         
            Note that the MGMT interfaces are partially set up by the network
            install procedure.
         
            ::
         
               for NODE in storage-0 storage-1; do
                  system interface-network-assign $NODE mgmt0 cluster-host
               done
         
         #. Add |OSDs| to storage-0:
         
            ::
         
             HOST=storage-0
             DISKS=$(system host-disk-list ${HOST})
             TIERS=$(system storage-tier-list ceph_cluster)
             OSDs="/dev/sdb"
             for OSD in $OSDs; do
                system host-stor-add ${HOST} $(echo "$DISKS" | grep "$OSD" | awk '{print $2}') --tier-uuid $(echo "$TIERS" | grep storage | awk '{print $2}')
             done
         
             system host-stor-list $HOST
         
         #. Add |OSDs| to storage-1:
         
            ::
         
               HOST=storage-1
               DISKS=$(system host-disk-list ${HOST})
               TIERS=$(system storage-tier-list ceph_cluster)
               OSDs="/dev/sdb"
               for OSD in $OSDs; do
                   system host-stor-add ${HOST} $(echo "$DISKS" | grep "$OSD" | awk '{print $2}') --tier-uuid $(echo "$TIERS" | grep storage | awk '{print $2}')
               done
         
               system host-stor-list $HOST

.. only:: partner

   .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
      :start-after: begin-dedicated-config-storage-nodes
      :end-before: end-dedicated-config-storage-nodes

--------------------
Unlock storage nodes
--------------------

Unlock storage nodes in order to bring them into service:

.. code-block:: bash

    for STORAGE in storage-0 storage-1; do
       system host-unlock $STORAGE
    done

The storage nodes will reboot in order to apply configuration changes and come
into service. This can take 5-10 minutes, depending on the performance of the
host machine.

----------------------
Configure worker nodes
----------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
            :start-after: begin-dedicated-stor-config-workers
            :end-before: end-dedicated-stor-config-workers

      .. group-tab:: Virtual

         On virtual controller-0:
         
         #. Assign the cluster-host network to the MGMT interface for the
            worker nodes.

            Note that the MGMT interfaces are partially set up automatically by
            the network install procedure.
         
            ::
         
               for NODE in worker-0 worker-1; do
                  system interface-network-assign $NODE mgmt0 cluster-host
               done
         
         #. Configure data interfaces for worker nodes.
         
            .. important::
         
               This step is required only if the StarlingX OpenStack
               application (|prefix|-openstack) will be installed.
         
               1G Huge Pages are not supported in the virtual environment and
               there is no virtual NIC supporting |SRIOV|. For that reason,
               data interfaces are not applicable in the virtual environment
               for the Kubernetes-only scenario.
         
            For OpenStack only:
         
            ::
         
               DATA0IF=eth1000
               DATA1IF=eth1001
               PHYSNET0='physnet0'
               PHYSNET1='physnet1'
               SPL=/tmp/tmp-system-port-list
               SPIL=/tmp/tmp-system-host-if-list
         
            Configure the datanetworks in sysinv, prior to referencing it in
            the :command:`system host-if-modify` command.
         
            ::
         
                 system datanetwork-add ${PHYSNET0} vlan
                 system datanetwork-add ${PHYSNET1} vlan
         
                 for NODE in worker-0 worker-1; do
                   echo "Configuring interface for: $NODE"
                   set -ex
                   system host-port-list ${NODE} --nowrap > ${SPL}
                   system host-if-list -a ${NODE} --nowrap > ${SPIL}
                   DATA0PCIADDR=$(cat $SPL | grep $DATA0IF |awk '{print $8}')
                   DATA1PCIADDR=$(cat $SPL | grep $DATA1IF |awk '{print $8}')
                   DATA0PORTUUID=$(cat $SPL | grep ${DATA0PCIADDR} | awk '{print $2}')
                   DATA1PORTUUID=$(cat $SPL | grep ${DATA1PCIADDR} | awk '{print $2}')
                   DATA0PORTNAME=$(cat $SPL | grep ${DATA0PCIADDR} | awk '{print $4}')
                   DATA1PORTNAME=$(cat $SPL | grep ${DATA1PCIADDR} | awk '{print $4}')
                   DATA0IFUUID=$(cat $SPIL | awk -v DATA0PORTNAME=$DATA0PORTNAME '($12 ~ DATA0PORTNAME) {print $2}')
                   DATA1IFUUID=$(cat $SPIL | awk -v DATA1PORTNAME=$DATA1PORTNAME '($12 ~ DATA1PORTNAME) {print $2}')
                   system host-if-modify -m 1500 -n data0 -c data ${NODE} ${DATA0IFUUID}
                   system host-if-modify -m 1500 -n data1 -c data ${NODE} ${DATA1IFUUID}
                   system interface-datanetwork-assign ${NODE} ${DATA0IFUUID} ${PHYSNET0}
                   system interface-datanetwork-assign ${NODE} ${DATA1IFUUID} ${PHYSNET1}
                   set +ex
                 done

         .. rubric:: OpenStack-specific host configuration

         .. important::
          
             This step is required only if the StarlingX OpenStack application
             (|prefix|-openstack) will be installed.
          
         #. **For OpenStack only:** Assign OpenStack host labels to the worker nodes in
            support of installing the |prefix|-openstack manifest/helm-charts later:
          
            .. parsed-literal::
          
                for NODE in worker-0 worker-1; do
                  system host-label-assign $NODE  openstack-compute-node=enabled
                  kubectl taint nodes $NODE openstack-compute-node:NoSchedule
                  system host-label-assign $NODE  |vswitch-label|
                  system host-label-assign $NODE  sriov=enabled
                done
          
         #. **For OpenStack only:** Set up a 'instances' filesystem,
            which is needed for |prefix|-openstack nova ephemeral disks.
          
            ::
          
               for NODE in worker-0 worker-1; do
                 echo "Configuring 'instances' for Nova ephemeral storage: $NODE"
                 system host-fs-add ${NODE} instances=10
               done


.. only:: partner

   .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
      :start-after: begin-dedicated-stor-config-workers
      :end-before: end-dedicated-stor-config-workers

*****************************************
Optionally Configure PCI-SRIOV Interfaces
*****************************************

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
            :start-after: begin-dedicated-conf-pci-sriov-interfaces
            :end-before: end-dedicated-conf-pci-sriov-interfaces

      .. group-tab:: Virtual

         Not applicable.

.. only:: partner

   .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
      :start-after: begin-dedicated-conf-pci-sriov-interfaces
      :end-before: end-dedicated-conf-pci-sriov-interfaces

-------------------
Unlock worker nodes
-------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
            :start-after: begin-dedicated-unlock-workers
            :end-before: end-dedicated-unlock-workers

      .. group-tab:: Virtual

         .. include:: controller_storage_install_kubernetes.rst
            :start-after: incl-unlock-compute-nodes-virt-controller-storage-start:
            :end-before: incl-unlock-compute-nodes-virt-controller-storage-end:

.. only:: partner

   .. include:: /shared/_includes/dedicated_storage_install_kubernetes.rest
      :start-after: begin-dedicated-unlock-workers
      :end-before: end-dedicated-unlock-workers


.. only:: partner

   .. include:: /_includes/72hr-to-license.rest


Complete system configuration by reviewing procedures in:

- |index-security-kub-81153c1254c3|
- |index-sysconf-kub-78f0e1e9ca5a|
- |index-admintasks-kub-ebc55fefc368|


.. end-content