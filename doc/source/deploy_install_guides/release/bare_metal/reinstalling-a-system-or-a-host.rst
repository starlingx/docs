
.. deo1552927844327
.. _reinstalling-a-system-or-a-host-r7:

============================
Reinstall a System or a Host
============================

You can reinstall individual hosts or the entire system if necessary.
Reinstalling host software or deleting and re-adding a host node may be
required to complete certain configuration changes.

.. rubric:: |context|

For a summary of changes that require system or host reinstallation, see
|node-doc|: :ref:`Configuration Changes Requiring Re-installation
<configuration-changes-requiring-re-installation>`.

To reinstall an entire system, refer to the Installation Guide for your system
type (for example, Standard or All-in-one).

.. note::
    To simplify system reinstallation, you can export and reuse an existing
    system configuration. For more information, see :ref:`Reinstalling a System
    Using an Exported Host Configuration File
    <reinstalling-a-system-using-an-exported-host-configuration-file-r7>`.

To reinstall the software on a host using the Host Inventory controls, see
|node-doc|: :ref:`Host Inventory <hosts-tab>`. In some cases, you must delete
the host instead, and then re-add it using the standard host installation
procedure. This applies if the system inventory record must be corrected to
complete the configuration change (for example, if the |MAC| address of the
management interface has changed).

-   :ref:`Reinstalling a System Using an Exported Host Configuration File
    <reinstalling-a-system-using-an-exported-host-configuration-file-r7>`

-   :ref:`Exporting Host Configurations <exporting-host-configurations-r7>`

