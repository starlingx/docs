|hideable|

.. include:: /_vendor/rl-strings.txt

.. include:: /_stx-related-links/installation-prereqs.rln 
   :start-after: begin-rls
   :end-before: end-rls

.. _controller_storage_install_kubernetes_r7:

===============================================================
Install Kubernetes Platform on Standard with Controller Storage
===============================================================

.. begin-content

.. contents:: |minitoc|
   :local:
   :depth: 1

.. only:: partner

   .. include:: /_includes/install-kubernetes-null-labels.rest

--------
Overview
--------

.. _std-installation-prereqs:

.. include:: /shared/_includes/desc_controller_storage.txt

.. include:: /_includes/automated-install-note--09dff208a9c6.rest 

.. _installation-prereqs-std:

-----------------------------
Minimum hardware requirements
-----------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
            :start-after: begin-min-hw-reqs-std
            :end-before: end-min-hw-reqs-std

      .. group-tab:: Virtual

         .. include:: /shared/_includes/physical_host_req.txt

.. only:: partner

   .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
      :start-after: begin-min-hw-reqs-std
      :end-before: end-min-hw-reqs-std

..   .. figure:: /shared/figures/deploy_install_guides/starlingx-deployment-options-controller-storage.png
..      :width: 800

..      Controller storage deployment configuration

.. _installation-prereqs-standard:

--------------------------
Installation Prerequisites
--------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/installation-prereqs.rest
            :start-after: begin-install-prereqs
            :end-before: end-install-prereqs

      .. group-tab:: Virtual

         Several pre-requisites must be completed prior to starting the |prod|
         installation.

         Before attempting to install |prod|, ensure that you have the the
         |prod| host installer ISO image file.

         Get the latest |prod| ISO from the `StarlingX mirror
         <https://mirror.starlingx.cengn.ca/mirror/starlingx/release/latest_release/debian/monolithic/outputs/iso/>`__.
         Alternately, you can get an older release ISO from `here <https://mirror.starlingx.cengn.ca/mirror/starlingx/release/>`__.

.. only:: partner

   .. include:: /shared/_includes/installation-prereqs.rest
      :start-after: begin-install-prereqs
      :end-before: end-install-prereqs

.. _std-storage-prep-servers:

--------------------------------
Prepare Servers for Installation
--------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
            :start-after: start-prepare-servers-common
            :end-before: end-prepare-servers-common

      .. group-tab:: Virtual

         .. note::

             The following commands for host, virtual environment setup, and
             host power-on use KVM / virsh for virtual machine and |VM|
             management technology. For an alternative virtualization
             environment, see: :ref:`Install StarlingX in VirtualBox
             <install_virtualbox>`.

         #. Prepare virtual environment.

            Set up virtual platform networks for virtual deployment:

            ::

              bash setup_network.sh

         #. Prepare virtual servers.

            Create the XML definitions for the virtual servers required by this
            configuration option. This will create the XML virtual server
            definition for:

            * controllerstorage-controller-0
            * controllerstorage-controller-1
            * controllerstorage-worker-0
            * controllerstorage-worker-1

            The following command will start/virtually power on:

            * The 'controllerstorage-controller-0' virtual server
            * The X-based graphical virt-manager application

            ::

              bash setup_configuration.sh -c controllerstorage -i ./bootimage.iso

            If there is no X-server present errors will occur and the X-based
            GUI for the virt-manager application will not start. The
            virt-manager GUI is not absolutely required and you can safely
            ignore errors and continue.

.. only:: partner

   .. include:: /shared/_includes/prepare-servers-for-installation-91baad307173.rest
      :start-after: start-prepare-servers-common
      :end-before: end-prepare-servers-common


--------------------------------
Install Software on Controller-0
--------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/inc-install-software-on-controller.rest
            :start-after: incl-install-software-controller-0-standard-start
            :end-before: incl-install-software-controller-0-standard-end

      .. group-tab:: Virtual

         In the last step of :ref:`std-storage-prep-servers`, the
         controller-0 virtual server 'controllerstorage-controller-0' was
         started by the :command:`setup_configuration.sh` command.

         On the host, attach to the console of virtual controller-0 and select
         the appropriate installer menu options to start the non-interactive
         install of StarlingX software on controller-0.

         .. note::

            When entering the console, it is very easy to miss the first
            installer menu selection. Use ESC to navigate to previous menus, to
            ensure you are at the first installer menu.

         ::

           virsh console controllerstorage-controller-0

         Make the following menu selections in the installer:

         #. First menu: Select 'Standard Controller Configuration'
         #. Second menu: Select 'Serial Console'

         Wait for the non-interactive install of software to complete and for
         the server to reboot. This can take 5-10 minutes depending on the
         performance of the host machine.

.. only:: partner

   .. include:: /shared/_includes/inc-install-software-on-controller.rest
      :start-after: incl-install-software-controller-0-standard-start
      :end-before: incl-install-software-controller-0-standard-end

--------------------------------
Bootstrap system on controller-0
--------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/incl-bootstrap-sys-controller-0-standard.rest
            :start-after: incl-bootstrap-sys-controller-0-standard-start
            :end-before: incl-bootstrap-sys-controller-0-standard-end

      .. group-tab:: Virtual

         .. include:: /shared/_includes/incl-bootstrap-controller-0-virt-controller-storage-start.rest
            :start-after: incl-bootstrap-controller-0-virt-controller-storage-start:
            :end-before: incl-bootstrap-controller-0-virt-controller-storage-end:

.. only:: partner

   .. include:: /shared/_includes/incl-bootstrap-sys-controller-0-standard.rest
      :start-after: incl-bootstrap-sys-controller-0-standard-start
      :end-before: incl-bootstrap-sys-controller-0-standard-end


----------------------
Configure controller-0
----------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/incl-config-controller-0-storage.rest
            :start-after: incl-config-controller-0-storage-start
            :end-before: incl-config-controller-0-storage-end

      .. group-tab:: Virtual

         .. include:: /shared/_includes/incl-config-controller-0-virt-controller-storage.rest
            :start-after: incl-config-controller-0-virt-controller-storage-start:
            :end-before: incl-config-controller-0-virt-controller-storage-end:

.. only:: partner

   .. include:: /shared/_includes/incl-config-controller-0-storage.rest
      :start-after: incl-config-controller-0-storage-start
      :end-before: incl-config-controller-0-storage-end


.. only:: openstack

   *************************************
   OpenStack-specific host configuration
   *************************************

   .. important::

      These steps are required only if the |prod-os| application
      (|prefix|-openstack) will be installed.

   .. only:: starlingx

      .. tabs::

         .. group-tab:: Bare Metal

            .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
               :start-after: begin-openstack-specific-host-configs-bare-metal
               :end-before: end-openstack-specific-host-configs-bare-metal

         .. group-tab:: Virtual

            .. important::

               This step is required only if the StarlingX OpenStack application
               (|prefix|-openstack) will be installed.

            #. **For OpenStack only:** Assign OpenStack host labels to
               controller-0 in support of installing the |prefix|-openstack
               manifest/helm-charts later:

               ::

                system host-label-assign controller-0 openstack-control-plane=enabled

            #. **For OpenStack only:** A vSwitch is required.

               The default vSwitch is containerized OVS that is packaged with
               the |prefix|-openstack manifest/helm-charts. StarlingX provides
               the option to use OVS-DPDK on the host, however, in the virtual
               environment OVS-DPDK is NOT supported, only OVS is supported.
               Therefore, simply use the default OVS

.. only:: openstack

   .. only:: partner

      .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
         :start-after: begin-openstack-specific-host-configs-bare-metal
         :end-before: end-openstack-specific-host-configs-bare-metal

-------------------
Unlock controller-0
-------------------

Unlock controller-0 in order to bring it into service:

::

  system host-unlock controller-0

Controller-0 will reboot in order to apply configuration changes and come into
service. This can take 5-10 minutes, depending on the performance of the host
machine.

.. only:: openstack

   * **For OpenStack only:** Due to the additional openstack services'
     containers running on the controller host, the size of the docker
     filesystem needs to be increased from the default size of 30G to 60G.

     .. code-block:: bash

          # check existing size of docker fs
          system host-fs-list controller-0
          # check available space (Avail Size (GiB)) in cgts-vg LVG where docker fs is located
          system host-lvg-list controller-0
          # if existing docker fs size + cgts-vg available space is less than
          # 60G, you will need to add a new disk to cgts-vg.

             # Get device path of BOOT DISK
             system host-show controller-0 | fgrep rootfs

             # Get UUID of ROOT DISK by listing disks
             system host-disk-list controller-0

             # Add new disk to 'cgts-vg' local volume group
             system host-pv-add controller-0 cgts-vg <DISK_UUID>
             sleep 10    # wait for disk to be added

             # Confirm the available space and increased number of physical
             # volumes added to the cgts-vg colume group
             system host-lvg-list controller-0

          # Increase docker filesystem to 60G
          system host-fs-modify controller-0 docker=60

-------------------------------------------------
Install software on controller-1 and worker nodes
-------------------------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
            :start-after: start-install-sw-on-controller-0-and-workers-standard-with-storage
            :end-before: end-install-sw-on-controller-0-and-workers-standard-with-storage

      .. group-tab:: Virtual

         #. On the host, power on the controller-1 virtual server,
            'controllerstorage-controller-1'. It will automatically attempt to
            network boot over the management network:

            ::

               virsh start controllerstorage-controller-1

         #. Attach to the console of virtual controller-1:

            ::

               virsh console controllerstorage-controller-1

            As the controller-1 |VM| boots, a message appears on its console
            instructing you to configure the personality of the node.

         #. On console of virtual controller-0, list hosts to see the newly
            discovered controller-1 host (hostname=None):

            ::

               system host-list
               +----+--------------+-------------+----------------+-------------+--------------+
               | id | hostname     | personality | administrative | operational | availability |
               +----+--------------+-------------+----------------+-------------+--------------+
               | 1  | controller-0 | controller  | unlocked       | enabled     | available    |
               | 2  | None         | None        | locked         | disabled    | offline      |
               +----+--------------+-------------+----------------+-------------+--------------+

         #. On virtual controller-0, using the host id, set the personality of
            this host to 'controller':

            ::

               system host-update 2 personality=controller

            This initiates the install of software on controller-1. This can take
            5-10 minutes, depending on the performance of the host machine.

         #. While waiting on the previous step to complete, start up and set the
            personality for 'controllerstorage-worker-0' and
            'controllerstorage-worker-1'. Set the personality to 'worker' and
            assign a unique hostname for each.

            For example, start 'controllerstorage-worker-0' from the host:

            ::

               virsh start controllerstorage-worker-0

            Wait for new host (hostname=None) to be discovered by checking
            ``system host-list`` on virtual controller-0:

            ::

               system host-update 3 personality=worker hostname=worker-0

            Repeat for 'controllerstorage-worker-1'. On the host:

            ::

               virsh start controllerstorage-worker-1

            And wait for new host (hostname=None) to be discovered by checking
            ``system host-list`` on virtual controller-0:

            ::

               system host-update 4 personality=worker hostname=worker-1

            .. only:: starlingx

              .. Note::

                 A node with Edgeworker personality is also available. See
                 |deploy-edgeworker-nodes| for details.

         #. Wait for the software installation on controller-1, worker-0, and
            worker-1 to complete, for all virtual servers to reboot, and for all
            to show as locked/disabled/online in 'system host-list'.

            ::

               system host-list
               +----+--------------+-------------+----------------+-------------+--------------+
               | id | hostname     | personality | administrative | operational | availability |
               +----+--------------+-------------+----------------+-------------+--------------+
               | 1  | controller-0 | controller  | unlocked       | enabled     | available    |
               | 2  | controller-1 | controller  | locked         | disabled    | online       |
               | 3  | worker-0     | worker      | locked         | disabled    | online       |
               | 4  | worker-1     | worker      | locked         | disabled    | online       |
               +----+--------------+-------------+----------------+-------------+--------------+

.. only:: partner

   .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
      :start-after: start-install-sw-on-controller-0-and-workers-standard-with-storage
      :end-before: end-install-sw-on-controller-0-and-workers-standard-with-storage



----------------------
Configure controller-1
----------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/incl-config-controller-1.rest
            :start-after: incl-config-controller-1-start:
            :end-before: incl-config-controller-1-end:


      .. group-tab:: Virtual

         .. include:: /shared/_includes/incl-config-controller-1-virt-controller-storage.rest
            :start-after: incl-config-controller-1-virt-controller-storage-start
            :end-before: incl-config-controller-1-virt-controller-storage-end

.. only:: partner

   .. include:: /shared/_includes/incl-config-controller-1.rest
      :start-after: incl-config-controller-1-start:
      :end-before: incl-config-controller-1-end:

-------------------
Unlock controller-1
-------------------

.. incl-unlock-controller-1-start:
.. incl-unlock-controller-1-virt-controller-storage-start:

Unlock controller-1 in order to bring it into service:

::

  system host-unlock controller-1

Controller-1 will reboot in order to apply configuration changes and come into
service. This can take 5-10 minutes, depending on the performance of the host
machine.

.. only:: openstack

   * **For OpenStack only:** Due to the additional openstack services’ containers
     running on the controller host, the size of the docker filesystem needs to be
     increased from the default size of 30G to 60G.

     .. code-block:: bash

          # check existing size of docker fs
          system host-fs-list controller-1
          # check available space (Avail Size (GiB)) in cgts-vg LVG where docker fs is located
          system host-lvg-list controller-1
          # if existing docker fs size + cgts-vg available space is less than
          # 80G, you will need to add a new disk to cgts-vg.

             # Get device path of BOOT DISK
             system host-show controller-1 | fgrep rootfs

             # Get UUID of ROOT DISK by listing disks
             system host-disk-list controller-1

             # Add new disk to 'cgts-vg' local volume group
             system host-pv-add controller-1 cgts-vg <DISK_UUID>
             sleep 10    # wait for disk to be added

             # Confirm the available space and increased number of physical
             # volumes added to the cgts-vg colume group
             system host-lvg-list controller-1

          # Increase docker filesystem to 60G
          system host-fs-modify controller-1 docker=60

.. incl-unlock-controller-1-end:
.. incl-unlock-controller-1-virt-controller-storage-end:

.. include:: /_includes/bootstrapping-and-deploying-starlingx.rest

----------------------
Configure worker nodes
----------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
            :start-after: start-config-worker-nodes-std-with-storage-bare-metal
            :end-before: end-config-worker-nodes-std-with-storage-bare-metal

      .. group-tab:: Virtual

         On virtual controller-0:

         #. Add the third Ceph monitor to a worker node:

            (The first two Ceph monitors are automatically assigned to
            controller-0 and controller-1.)

            ::

              system ceph-mon-add worker-0

         #. Wait for the worker node monitor to complete configuration:

            ::

              system ceph-mon-list
              +--------------------------------------+-------+--------------+------------+------+
              | uuid                                 | ceph_ | hostname     | state      | task |
              |                                      | mon_g |              |            |      |
              |                                      | ib    |              |            |      |
              +--------------------------------------+-------+--------------+------------+------+
              | 64176b6c-e284-4485-bb2a-115dee215279 | 20    | controller-1 | configured | None |
              | a9ca151b-7f2c-4551-8167-035d49e2df8c | 20    | controller-0 | configured | None |
              | f76bc385-190c-4d9a-aa0f-107346a9907b | 20    | worker-0     | configured | None |
              +--------------------------------------+-------+--------------+------------+------+

         #. Assign the cluster-host network to the MGMT interface for the worker
            nodes:

            (Note that the MGMT interfaces are partially set up automatically by
            the network install procedure.)

            .. code-block:: bash

              for NODE in worker-0 worker-1; do
                 system interface-network-assign $NODE mgmt0 cluster-host
              done


         #. Configure data interfaces for worker nodes.

            .. important::

               This step is required only if the StarlingX OpenStack application
               (|prefix|-openstack) will be installed.

               1G Huge Pages are not supported in the virtual environment and there is no
               virtual NIC supporting SRIOV. For that reason, data interfaces are not
               applicable in the virtual environment for the Kubernetes-only scenario.

            For OpenStack only:

            ::

               DATA0IF=eth1000
               DATA1IF=eth1001
               PHYSNET0='physnet0'
               PHYSNET1='physnet1'
               SPL=/tmp/tmp-system-port-list
               SPIL=/tmp/tmp-system-host-if-list

               # configure the datanetworks in sysinv, prior to referencing it
               # in the ``system host-if-modify`` command'.
               system datanetwork-add ${PHYSNET0} vlan
               system datanetwork-add ${PHYSNET1} vlan

               for NODE in worker-0 worker-1; do
                 echo "Configuring interface for: $NODE"
                 set -ex
                 system host-port-list ${NODE} --nowrap > ${SPL}
                 system host-if-list -a ${NODE} --nowrap > ${SPIL}
                 DATA0PCIADDR=$(cat $SPL | grep $DATA0IF |awk '{print $8}')
                 DATA1PCIADDR=$(cat $SPL | grep $DATA1IF |awk '{print $8}')
                 DATA0PORTUUID=$(cat $SPL | grep ${DATA0PCIADDR} | awk '{print $2}')
                 DATA1PORTUUID=$(cat $SPL | grep ${DATA1PCIADDR} | awk '{print $2}')
                 DATA0PORTNAME=$(cat $SPL | grep ${DATA0PCIADDR} | awk '{print $4}')
                 DATA1PORTNAME=$(cat $SPL | grep ${DATA1PCIADDR} | awk '{print $4}')
                 DATA0IFUUID=$(cat $SPIL | awk -v DATA0PORTNAME=$DATA0PORTNAME '($12 ~ DATA0PORTNAME) {print $2}')
                 DATA1IFUUID=$(cat $SPIL | awk -v DATA1PORTNAME=$DATA1PORTNAME '($12 ~ DATA1PORTNAME) {print $2}')
                 system host-if-modify -m 1500 -n data0 -c data ${NODE} ${DATA0IFUUID}
                 system host-if-modify -m 1500 -n data1 -c data ${NODE} ${DATA1IFUUID}
                 system interface-datanetwork-assign ${NODE} ${DATA0IFUUID} ${PHYSNET0}
                 system interface-datanetwork-assign ${NODE} ${DATA1IFUUID} ${PHYSNET1}
                 set +ex
               done


         .. rubric:: OpenStack-specific host configuration

         .. important::

            This step is required only if the StarlingX OpenStack application
            (|prefix|-openstack) will be installed.

         #. **For OpenStack only:** Assign OpenStack host labels to the worker nodes in
            support of installing the |prefix|-openstack manifest/helm-charts later:

            ::

                for NODE in worker-0 worker-1; do
                  system host-label-assign $NODE  openstack-compute-node=enabled
                  kubectl taint nodes $NODE openstack-compute-node:NoSchedule
                  system host-label-assign $NODE  openvswitch=enabled
                done

            .. note::

               If you have a |NIC| that supports |SRIOV|, then you can enable it by
               using the following:

               .. code-block:: none

                  system host-label-assign controller-0 sriov=enabled

         #. **For OpenStack only:** Set up a 'instances' filesystem,
            which is needed for |prefix|-openstack nova ephemeral disks.

            ::

               for NODE in worker-0 worker-1; do
                 echo "Configuring 'instances' for Nova ephemeral storage: $NODE"
                 system host-fs-add ${NODE} instances=10
               done


.. only:: partner

   .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
      :start-after: start-config-worker-nodes-std-with-storage-bare-metal
      :end-before: end-config-worker-nodes-std-with-storage-bare-metal

*****************************************
Optionally Configure PCI-SRIOV Interfaces
*****************************************

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
            :start-after: start-config-pci-sriov-interfaces-standard-storage
            :end-before: end-config-pci-sriov-interfaces-standard-storage

      .. group-tab:: Virtual

         Not applicable

.. only:: partner

   .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
      :start-after: start-config-pci-sriov-interfaces-standard-storage
      :end-before: end-config-pci-sriov-interfaces-standard-storage

--------------------
Unlock worker nodes
--------------------

Unlock worker nodes in order to bring them into service:

.. incl-unlock-compute-nodes-virt-controller-storage-start:

.. code-block:: bash

  for NODE in worker-0 worker-1; do
     system host-unlock $NODE
  done

The worker nodes will reboot in order to apply configuration changes and come
into service. This can take 5-10 minutes, depending on the performance of the
host machine.

.. incl-unlock-compute-nodes-virt-controller-storage-end:

----------------------------------------------------------------------------
If configuring host based Ceph Storage Backend, Add Ceph OSDs to controllers
----------------------------------------------------------------------------

.. only:: starlingx

   .. tabs::

      .. group-tab:: Bare Metal

         .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
            :start-after: start-add-ceph-osds-to-controllers-std-storage
            :end-before: end-add-ceph-osds-to-controllers-std-storage

      .. group-tab:: Virtual

         On virtual controller-0:

         #. Add |OSDs| to controller-0:

            .. important::

               This step requires a configured Ceph storage backend

            ::

               HOST=controller-0
               DISKS=$(system host-disk-list ${HOST})
               TIERS=$(system storage-tier-list ceph_cluster)
               OSDs="/dev/sdb"
               for OSD in $OSDs; do
                  system host-stor-add ${HOST} $(echo "$DISKS" | grep "$OSD" | awk '{print $2}') --tier-uuid $(echo "$TIERS" | grep storage | awk '{print $2}')
                  while true; do system host-stor-list ${HOST} | grep ${OSD} | grep configuring; if [ $? -ne 0 ]; then break; fi; sleep 1; done
               done

               system host-stor-list $HOST

         #. Add |OSDs| to controller-1:

            .. important::

               This step requires a configured Ceph storage backend

            ::

               HOST=controller-1
               DISKS=$(system host-disk-list ${HOST})
               TIERS=$(system storage-tier-list ceph_cluster)
               OSDs="/dev/sdb"
               for OSD in $OSDs; do
                   system host-stor-add ${HOST} $(echo "$DISKS" | grep "$OSD" | awk '{print $2}') --tier-uuid $(echo "$TIERS" | grep storage | awk '{print $2}')
                   while true; do system host-stor-list ${HOST} | grep ${OSD} | grep configuring; if [ $? -ne 0 ]; then break; fi; sleep 1; done
               done

               system host-stor-list $HOST


.. only:: partner

   .. include:: /shared/_includes/controller_storage_install_kubernetes.rest
      :start-after: start-add-ceph-osds-to-controllers-std-storage
      :end-before: end-add-ceph-osds-to-controllers-std-storage


.. only:: partner

   .. include:: /_includes/72hr-to-license.rest


Complete system configuration by reviewing procedures in:

- |index-security-kub-81153c1254c3|
- |index-sysconf-kub-78f0e1e9ca5a|
- |index-admintasks-kub-ebc55fefc368|


*******************************************************************
If configuring Rook Ceph Storage Backend, configure the environment
*******************************************************************

.. note::

   Each deployment model enforces a different structure for the Rook Ceph
   cluster and its integration with the platform.

#. Check if the rook-ceph app is uploaded.

   .. code-block:: none

      $ source /etc/platform/openrc
      $ system application-list
      +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
      | application              | version   | manifest name                             | manifest file    | status   | progress  |
      +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+
      | cert-manager             | 24.09-76  | cert-manager-fluxcd-manifests             | fluxcd-manifests | applied  | completed |
      | dell-storage             | 24.09-25  | dell-storage-fluxcd-manifests             | fluxcd-manifests | uploaded | completed |
      | deployment-manager       | 24.09-13  | deployment-manager-fluxcd-manifests       | fluxcd-manifests | applied  | completed |
      | nginx-ingress-controller | 24.09-57  | nginx-ingress-controller-fluxcd-manifests | fluxcd-manifests | applied  | completed |
      | oidc-auth-apps           | 24.09-53  | oidc-auth-apps-fluxcd-manifests           | fluxcd-manifests | uploaded | completed |
      | platform-integ-apps      | 24.09-138 | platform-integ-apps-fluxcd-manifests      | fluxcd-manifests | uploaded | completed |
      | rook-ceph                | 24.09-12  | rook-ceph-fluxcd-manifests                | fluxcd-manifests | uploaded | completed |
      +--------------------------+-----------+-------------------------------------------+------------------+----------+-----------+

#. Add Storage-Backend with Deployment Model.

   There are three deployment models: Controller, Dedicated, and Open.

   For the simplex and duplex environments you can use the Controller and Open
   configuration.

   Controller (default)
      |OSDs| must only be added to host with controller personality set.

      Replication factor is limited to a maximum of 2.

      This model aligns with the existing Bare-metal Ceph assignement of OSDs
      to controllers.

   Dedicated
      |OSDs| must be added only to hosts with the worker personality.

      The replication factor is limited to a maximum of 3.

      This model aligns with existing Bare-metal Ceph use of dedicated storage
      hosts in groups of 2 or 3.

   Open
      |OSDs| can be added to any host without limitations.

      Replication factor has no limitations.

   Application Strategies for deployment model controller.

   Duplex, Duplex+ or Standard
      |OSDs|: Added to controller nodes.

      Replication Factor: Default 1, maximum 'Any'.

   .. code-block:: none

      $ system storage-backend-add ceph-rook --deployment open --confirmed
      $ system storage-backend-list
      +--------------------------------------+-----------------+-----------+----------------------+----------+------------------+---------------------------------------------+
      | uuid                                 | name            | backend   | state                | task     | services         | capabilities                                |
      +--------------------------------------+-----------------+-----------+----------------------+----------+------------------+---------------------------------------------+
      | 0dfef1f0-a5a4-4b20-a013-ef76e92bcd42 | ceph-rook-store | ceph-rook | configuring-with-app | uploaded | block,filesystem | deployment_model: open replication: 2       |
      |                                      |                 |           |                      |          |                  | min_replication: 1                          |
      +--------------------------------------+-----------------+-----------+----------------------+----------+------------------+---------------------------------------------+

#. Set up a ``host-fs ceph`` filesystem.

   .. code-block:: none

      $ system host-fs-add controller-0 ceph=20
      $ system host-fs-add controller-1 ceph=20
      $ system host-fs-add compute-0 ceph=20

#. List all the disks.

   .. code-block:: none

      $ system host-disk-list controller-0
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      | uuid                                 | device_node | device_num | device_type | size_gib | available_gib | rpm          | serial_id           | device_path                                |
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      | 7f2b9ff5-b6ee-4eaf-a7eb-cecd3ba438fd | /dev/sda    | 2048       | HDD         | 292.968  | 0.0           | Undetermined | VB3e6c5449-c7224b07 | /dev/disk/by-path/pci-0000:00:0d.0-ata-1.0 |
      | fdaf3f71-a2df-4b40-9e70-335900f953a3 | /dev/sdb    | 2064       | HDD         | 9.765    | 0.0           | Undetermined | VB323207f8-b6b9d531 | /dev/disk/by-path/pci-0000:00:0d.0-ata-2.0 |
      | ced60373-0dbc-4bc7-9d03-657c1f92164a | /dev/sdc    | 2080       | HDD         | 9.765    | 9.761         | Undetermined | VB49833b9d-a22a2455 | /dev/disk/by-path/pci-0000:00:0d.0-ata-3.0 |
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      $ system host-disk-list controller-1
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      | uuid                                 | device_node | device_num | device_type | size_gib | available_gib | rpm          | serial_id           | device_path                                |
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      | 119533a5-bc66-47e0-a448-f0561871989e | /dev/sda    | 2048       | HDD         | 292.968  | 0.0           | Undetermined | VBb1b06a09-6137c63a | /dev/disk/by-path/pci-0000:00:0d.0-ata-1.0 |
      | 03cbb10e-fdc1-4d84-a0d8-6e02c22e3251 | /dev/sdb    | 2064       | HDD         | 9.765    | 0.0           | Undetermined | VB5fcf59a9-7c8a531b | /dev/disk/by-path/pci-0000:00:0d.0-ata-2.0 |
      | 7351013f-8280-4ff3-88bd-76e88f14fa2f | /dev/sdc    | 2080       | HDD         | 9.765    | 9.761         | Undetermined | VB0d1ce946-d0a172c4 | /dev/disk/by-path/pci-0000:00:0d.0-ata-3.0 |
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      $ system host-disk-list compute-0
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      | uuid                                 | device_node | device_num | device_type | size_gib | available_gib | rpm          | serial_id           | device_path                                |
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+
      | 14245695-46df-43e8-b54b-9fb3c22ac359 | /dev/sda    | 2048       | HDD         | 292.     | 0.0           | Undetermined | VB8ac41a93-82275093 | /dev/disk/by-path/pci-0000:00:0d.0-ata-1.0 |
      | 765d8dff-e584-4064-9c95-6ea3aa25473c | /dev/sdb    | 2064       | HDD         | 9.765    | 0.0           | Undetermined | VB569d6dab-9ae3e6af | /dev/disk/by-path/pci-0000:00:0d.0-ata-2.0 |
      | c9b4ed65-da32-4770-b901-60b56fd68c35 | /dev/sdc    | 2080       | HDD         | 9.765    | 9.761         | Undetermined | VBf88762a8-9aa3315c | /dev/disk/by-path/pci-0000:00:0d.0-ata-3.0 |
      +--------------------------------------+-------------+------------+-------------+----------+---------------+--------------+---------------------+--------------------------------------------+

#. Choose empty disks and provide hostname and uuid to finish |OSD|
   configuration:

   .. code-block:: none

      $ system host-stor-add controller-0 osd fdaf3f71-a2df-4b40-9e70-335900f953a3
      $ system host-stor-add controller-1 osd 03cbb10e-fdc1-4d84-a0d8-6e02c22e3251
      $ system host-stor-add compute-0 osd c9b4ed65-da32-4770-b901-60b56fd68c35
#. Apply the rook-ceph application.

   .. code-block:: none

      $ system application-apply rook-ceph

#. Wait for |OSDs| pod to be ready.

   .. code-block:: none

      $ kubectl get pods -n rook-ceph
      NAME                                                     READY   STATUS      RESTARTS      AGE
      ceph-mgr-provision-nh6dl                                 0/1     Completed   0                18h
      csi-cephfsplugin-2nnwf                                   2/2     Running     10 (3h9m ago)    18h
      csi-cephfsplugin-flbll                                   2/2     Running     14 (3h42m ago)   18h
      csi-cephfsplugin-provisioner-5467c6c4f-98fxk             5/5     Running     5 (4h7m ago)     18h
      csi-cephfsplugin-zzskz                                   2/2     Running     17 (168m ago)    18h
      csi-rbdplugin-42ldl                                      2/2     Running     17 (168m ago)    18h
      csi-rbdplugin-8xzxz                                      2/2     Running     14 (3h42m ago)   18h
      csi-rbdplugin-b6dvk                                      2/2     Running     10 (3h9m ago)    18h
      csi-rbdplugin-provisioner-fd84899c-6795x                 5/5     Running     5 (4h7m ago)     18h
      rook-ceph-crashcollector-compute-0-59f554f6fc-5s5cz      1/1     Running     0                4m19s
      rook-ceph-crashcollector-controller-0-589f5f774-b2297    1/1     Running     0                3h2m
      rook-ceph-crashcollector-controller-1-68d66b9bff-njrhg   1/1     Running     1 (4h7m ago)     18h
      rook-ceph-exporter-compute-0-569b65cf6c-xhfjk            1/1     Running     0                4m14s
      rook-ceph-exporter-controller-0-5fd477bb8-rzkqd          1/1     Running     0                3h2m
      rook-ceph-exporter-controller-1-6f5d8695b9-772rb         1/1     Running     1 (4h7m ago)     18h
      rook-ceph-mds-kube-cephfs-a-654c56d89d-mdklw             2/2     Running     11 (166m ago)    18h
      rook-ceph-mds-kube-cephfs-b-6c498f5db4-5hbcj             2/2     Running     2 (166m ago)     3h2m
      rook-ceph-mgr-a-5d6664f544-rgfpn                         3/3     Running     9 (3h42m ago)    18h
      rook-ceph-mgr-b-5c4cb984b9-cl4qq                         3/3     Running     0                168m
      rook-ceph-mgr-c-7d89b6cddb-j9hxp                         3/3     Running     0                3h9m
      rook-ceph-mon-a-6ffbf95cdf-cvw8r                         2/2     Running     0                3h9m
      rook-ceph-mon-b-5558b5ddc7-h7nhz                         2/2     Running     2 (4h7m ago)     18h
      rook-ceph-mon-c-6db9c888cb-mfxfh                         2/2     Running     0                167m
      rook-ceph-operator-69b5674578-k6k4j                      1/1     Running     0                8m10s
      rook-ceph-osd-0-dd94574ff-dvrrs                          2/2     Running     2 (4h7m ago)     18h
      rook-ceph-osd-1-5d7f598f8f-88t2j                         2/2     Running     0                3h9m
      rook-ceph-osd-2-6776d44476-sqnlj                         2/2     Running     0                4m20s
      rook-ceph-osd-prepare-compute-0-ls2xw                    0/1     Completed   0                5m16s
      rook-ceph-osd-prepare-controller-0-jk6bz                 0/1     Completed   0                5m27s
      rook-ceph-osd-prepare-controller-1-d845s                 0/1     Completed   0                5m21s
      rook-ceph-provision-vtvc4                                0/1     Completed   0                17h
      rook-ceph-tools-7dc9678ccb-srnd8                         1/1     Running     1 (4h7m ago)     18h
      stx-ceph-manager-664f8585d8-csl7p                        1/1     Running     1 (4h7m ago)     18h

#. Check ceph cluster health.

   .. code-block:: none

      $ ceph -s
          cluster:
              id:     5b579aca-617f-4f2a-b059-73e7071111dc
              health: HEALTH_OK

          services:
              mon: 3 daemons, quorum a,b,c (age 2h)
              mgr: a(active, since 2h), standbys: c, b
              mds: 1/1 daemons up, 1 hot standby
              osd: 3 osds: 3 up (since 82s), 3 in (since 2m)

          data:
              volumes: 1/1 healthy
              pools:   4 pools, 113 pgs
              objects: 26 objects, 648 KiB
              usage:   129 MiB used, 29 GiB / 29 GiB avail
              pgs:     110 active+clean
                      2   active+clean+scrubbing+deep
                      1   active+clean+scrubbing

          io:
              client:   1.2 KiB/s rd, 2 op/s rd, 0 op/s wr

.. end-content