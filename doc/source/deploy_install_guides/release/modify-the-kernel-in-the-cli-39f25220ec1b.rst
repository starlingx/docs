.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _modify-the-kernel-in-the-cli-39f25220ec1b:

===============================
Modify the Kernel using the CLI
===============================

.. rubric:: |context|

The Kernel can be configured during runtime as [ standard <-> lowlatency ].

.. rubric:: |proc|

#.  Lock the |AIO| Controller or Worker Node.

    .. code-block:: none

        ~(keystone_admin)]$ system host-lock <controller | worker>

#.  Verify if there are no alarms.

    .. code-block:: none

        ~(keystone_admin)]$ fm alarm-list

#.  Modify the Kernel.

    .. code-block:: none

        ~(keystone_admin)]$ system host-kernel-modify <controller | worker> < standard | lowlatency>

    For example:

    .. code-block:: none

        ~(keystone_admin)]$ system host-kernel-modify controller-0 lowlatency
        ~(keystone_admin)]$ system host-kernel-show controller-0
        +--------------------+--------------+
        |Property            | Value        |
        +--------------------+--------------+
        | hostname           | controller-0 |
        | kernel_provisioned | lowlatency   |
        | kernel_running     | standard     |
        +--------------------+--------------+

#.  Unlock the |AIO| Controller or Worker Node.

    .. code-block:: none

        ~(keystone_admin)]$ system host-unlock <controller | worker>

#.  Verify if there are no alarms, and if the Kernel is running.

    .. code-block:: none

        ~(keystone_admin)]$ fm alarm-list
        ~(keystone_admin)]$ system host-kernel-show controller-0
        +--------------------+--------------+
        |Property            | Value        |
        +--------------------+--------------+
        | hostname           | controller-0 |
        | kernel_provisioned | lowlatency   |
        | kernel_running     | lowlatency   |
        +--------------------+--------------+


Modify Kernel using the Ansible Bootstrap yaml
----------------------------------------------

Add the following line to you ``localhost.yaml`` file that is used to bootstrap
an |AIO| controller If the line is missing the default kernel is standard.

.. code-block:: none

    kernel : lowlatency


Modify the Kernel using the Development Manager yaml
----------------------------------------------------

Add the following line to the ``deployment-config.yaml``.

Default Kernel is ``standard``.

.. warning::

    The subfunctions section must contain ``lowlatency``.

.. code-block:: yaml

    kernel: lowlatency
    subfunctions:
    - controller
    - worker
    - lowlatency