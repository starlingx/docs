===========================
Install StarlingX OpenStack
===========================

.. rubric:: |prereq|

These instructions assume that you have completed the following
OpenStack-specific configuration tasks that are required by the underlying
StarlingX Kubernetes platform:

* All nodes have been labeled appropriately for their OpenStack role(s).
* The vSwitch type has been configured.
* The nova-local volume group has been configured on any node's host, if
  running the compute function.

--------------------------------------------
Install application manifest and helm-charts
--------------------------------------------

#. Modify the size of the docker_lv filesystem. By default, the size of the
   docker_lv filesystem is 30G, which is not enough for |prefix|-openstack
   installation. Use the ``host-fs-modify`` CLI to increase the filesystem
   size.

   The syntax is:

   .. code-block:: none

      $ system host-fs-modify <hostname or id> <fs name=size>


   Where:

   * ``hostname or id`` is the location where the file system will be added.
   * ``fs name`` is the file system name.
   * ``size`` is an integer indicating the file system size in Gigabytes.

   For example:

   .. code-block:: none

      $ system host-fs-modify controller-0 docker=60

#. Get the latest |prod-os| application (|prefix|-openstack) manifest
   and helm charts. Use one of the following options:


   * Public download from the `StarlingX mirror
     <https://mirror.starlingx.windriver.com/mirror/starlingx/>`_.

   * After you select a release, helm charts are located in
     ``release/latest_release/debian/openstack/outputs/helm-charts``.

#. Load the |prefix|-openstack application's package into |prod|. The
   tarball package contains |prefix|-openstack's manifest and
   |prefix|-openstack's set of helm charts. For example:

   .. code-block:: none

      $ system application-upload |prefix|-openstack-<version>-debian-stable-versioned.tgz

   This will:

   * Load the manifest and helm charts.
   * Internally manage helm chart override values for each chart.
   * Automatically generate system helm chart overrides for each chart based on
     the current state of the underlying StarlingX Kubernetes platform and the
     recommended StarlingX configuration of OpenStack services.

#. Apply the |prefix|-openstack application in order to bring |prod-os| into
   service. If your environment is preconfigured with a proxy server, then make
   sure HTTPS proxy is set before applying |prefix|-openstack.

   .. note::

      To set the HTTPS proxy at bootstrap time, refer to
      :ref:`ansible_bootstrap_configs_r7`.

      To set the HTTPS proxy after installation, refer to
      :ref:`docker_proxy_config`.

   .. note::

      To install in a subcloud, it is not possible to create all the resources
      automatically. More specifically, a ldap group called *openstack* with
      the gid 1001 should be created in the system controller for the
      installation to proceed properly. To do this, in the system controller:

      .. code-block:: none

         $ ldapaddgroup openstack 1001

   .. code-block:: none

      $ system application-apply |prefix|-openstack

#. Wait for the activation of |prefix|-openstack to complete.

   This can take 5-10 minutes depending on the performance of your host machine.

   Monitor progress:

   .. code-block:: none

      $ watch -n 5 system application-list

.. rubric:: |result|

Your OpenStack cloud is now up and running.

See :doc:`access` for details on how to access |prod-os|.
