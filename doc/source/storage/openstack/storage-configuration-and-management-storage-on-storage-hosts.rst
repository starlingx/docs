
.. include:: /_stx-related-links/storage-configuration-and-management-storage-on-storage-hosts.rln
.. RL strings
.. include:: /_vendor/rl-strings.txt
.. tfu1590592352767
.. _storage-configuration-and-management-storage-on-storage-hosts:

========================
Storage on Storage Hosts
========================

|prod-os| creates default Ceph storage pools for Glance images, Cinder volumes,
Cinder backups, and Nova ephemeral data object data.

For more information, see the |stor-doc|:
|storage-configuration-storage-resources| guide for details on configuring the
internal Ceph cluster on either controller or storage hosts.

