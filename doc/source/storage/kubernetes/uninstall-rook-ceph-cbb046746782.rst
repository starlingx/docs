.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _uninstall-rook-ceph-cbb046746782:

===================
Uninstall Rook Ceph
===================

.. rubric:: |context|

To completely remove Rook Ceph you must remove the app and clear all the
environment configurations to prevent an automatic reinstall.

.. rubric:: |proc|

#.  Remove the application by running the script:

    .. code-block:: none

        source /etc/platform/openrc
        system application-remove rook-ceph --force
        retry_count=1
        retries=200
        while [ $retry_count -le $retries ]; do
            rookstatus=$(system application-list | grep rook-ceph | awk '{print $10}')
            echo $rookstatus
            if [[ "$rookstatus" == "uploaded" ]]; then
                system application-delete rook-ceph
                break
            fi
            echo "Retry #" $retry_count
            let retry_count++
        done

#.  Remove the environment configurations completely.

    #.  Remove |OSDs|.

        #.  Lock the host.

            .. code-block:: none

                ~(keystone_admin)$ system host-lock <hostname>

        #.  List all |OSDs| to get the uuid of each |OSD|.

            .. code-block:: none

                ~(keystone_admin)$ system host-stor-list <hostname>

        #.  Remove each |OSD| using the uuid of all |OSDs|.

            .. code-block:: none

                ~(keystone_admin)$ system host-stor-delete <uuid>

        #.  Unlock the host.

            .. code-block:: none

                ~(keystone_admin)$ system host-unlock <hostname>

    #.  (|AIO-DX| Only) Remove ``controllerfs``.

        #.  To remove a ``controllerfs``, the standby controller must be
            locked and the ``controllerfs`` needs to be in Ready state.

            .. code-block:: none

                ~(keystone_admin)$ system host-lock <hostname>

        #.  Remove the ``controllerfs``.

            .. code-block:: none

                ~(keystone_admin)$ system controllerfs-delete ceph-float

        #.  Unlock the standby controller.

            .. code-block:: none

                ~(keystone_admin)$ system host-unlock <hostname>

    #.  Remove the ``host-fs``.

        .. code-block:: none

            ~(keystone_admin)$ system host-fs-delete <hostname> ceph

    #.  Remove the storage backend ceph-rook.

        .. code-block:: none

            ~(keystone_admin)$ system storage-backend-delete ceph-rook-store --force
