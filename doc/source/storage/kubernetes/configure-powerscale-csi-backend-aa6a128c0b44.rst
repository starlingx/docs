.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _configure-powerscale-csi-backend-aa6a128c0b44:

================================
Configure PowerScale CSI Backend
================================

|CSI| driver for PowerScale is part of the |CSM| open-source suite of
Kubernetes storage enablers for Dell Technology (Dell) products. It is a |CSI|
driver that provides support for provisioning persistent storage using Dell
PowerScale storage array.

.. note::

    Isilon is the legacy name for PowerScale.

.. note::

    By default, the csi-powerscale chart is disabled. It is necessary to enable
    and update user-overrides before applying dell-storage application.

.. contents::
   :local:
   :depth: 2

---------------------------
Enable CSI PowerScale Chart
---------------------------

.. note::

    Disable any other |CSI|/|CSM| if not used/configured, otherwise it could cause the
    application to fail on apply.

    .. code-block::

        (keystone_admin)$ system helm-chart-attribute-modify --enabled false dell-storage <chart_name> dell-storage

#. Enable the chart.

   .. code-block::

       (keystone_admin)$  system helm-chart-attribute-modify --enabled true dell-storage csi-powerscale dell-storage

#. Create overrides to configure the storage connection.

   .. code-block:: none

       :name: powerscaleOverrides.yaml

        isiPath: "/my/isi/path"

        controller:
          replication:
            replicationPrefix: "myCustomReplicationPrefix"

        secret:
          arrays:
          - clusterName: Cluster1
            username: <USERNAME>
            password: <PASSWORD>
            endpoint: https://<cluster1_address>
            endpointPort: 8888
            isDefault: true
            skipCertificateValidation: false
            isiPath: "/my/isi/path"
            isiVolumePathPermissions: "0755"
            ignoreUnresolvableHosts: true
            replicationCertificateID: "dd9c736cc17e6dd5f7d85fe13528cfc20f3b4b0af4f26595d22328c8d1f461af"
            authorizationEndpoint: "https://localhost:9999"

          - clusterName: "Cluster2"
            username: <USERNAME>
            password: <PASSWORD>
            endpoint: https://<cluster2_address>
            isiPath: "/my/isi/path2"

        storageClasses:
        - name: "storageClassDefault"

        proxyAuthzTokens:
          access: "YWJjZGVmCg=="
          refresh: "YWJjZGVmCg=="

#. Apply override.

   .. code-block::

       (keystone_admin)$ system helm-override-update dell-storage csi-powerscale dell-storage --values=powerscaleOverrides.yaml

#. Apply chart.

   .. code-block::

       (keystone_admin)$ system application-apply dell-storage

------------------------
UserOverrides Parameters
------------------------

******************
General Parameters
******************

.. table::
   :widths: auto

   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | Parameter                                | Description                                                        | Required                         | Default                      |
   +==========================================+====================================================================+==================================+==============================+
   | app.starlingx.io/component               | Code affinity.                                                     | No                               | platform                     |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | authorization.rootCertificate            | Authorization proxy server root certificate.                       | Only if authorization is enabled | Not Applicable               |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | certSecret                               | Secret containing the certificate.                                 | No                               | Not Applicable               |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | controller.replication.replicationPrefix | Change replication prefix.                                         | No                               | replication.storage.dell.com |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | controller.snapshot.deletionPolicy       | Specifies what happens when VolumeSnapshot is deleted.             | No                               | Delete                       |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | csm.authorization                        | Enables/disables authorization sidecar container.                  | No                               | false                        |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | csm.replication                          | Enables/disables dell-csi-replicator sidecar container.            | No                               | false                        |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | csm.resiliency                           | Enables/disables podmon sidecar container.                         | No                               | false                        |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | driverName                               | Driver name.                                                       | No                               | csi-isilon.dellemc.com       |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | isiPath                                  | The base path for the volumes to be created on PowerScale cluster. | No                               | /ifs/data/csi                |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | proxyAuthzTokens.access                  | Access token for authorization sidecar container access.           | Only if authorization is enabled | Not Applicable               |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+
   | proxyAuthzTokens.refresh                 | Refresh token for authorization sidecar container access.          | Only if authorization is enabled | Not Applicable               |
   +------------------------------------------+--------------------------------------------------------------------+----------------------------------+------------------------------+

*************************
StorageClasses Parameters
*************************

.. table::
    :widths: auto

    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | Parameter                                             | Description                                                                                                                                                              | Required | Default                       |
    +=======================================================+==========================================================================================================================================================================+==========+===============================+
    | storageClasses                                        | List of storage classes.                                                                                                                                                 | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].name                                | StorageClass name.                                                                                                                                                       | yes      | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].provisioner                         | Driver name.                                                                                                                                                             | no       | "csi-isilon.dellemc.com"      |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].reclaimPolicy                       | PVs that are dynamically created by a StorageClass will have the reclaim policy specified here.                                                                          | no       | Delete                        |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].allowVolumeExpansion                | Allows the users to resize the volume by editing the corresponding |PVC| object.                                                                                         | no       | true                          |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].AccessZone                          | The name of the access zone a volume can be created in.                                                                                                                  | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].IsiPath                             | The base path for the volumes to be created on PowerScale cluster.                                                                                                       | no       | Same as General isiPath       |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].IsiVolumePathPermissions            | The permissions for isi volume directory path.                                                                                                                           | no       | "0777"                        |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].AzServiceIP                         | AccessZone groupnet service IP.                                                                                                                                          | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].RootClientEnabled                   | Determines whether the driver should enable root squashing or not.                                                                                                       | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].ClusterName                         | Name of PowerScale cluster where PV will be provisioned.                                                                                                                 | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].AdvisoryLimit                       | Parameter to set advisory limit to quota.                                                                                                                                | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].SoftLimit                           | Parameter to set soft limit to quota.                                                                                                                                    | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].SoftGracePrd                        | Parameter which must be mentioned along with soft limit.                                                                                                                 | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].fstype                              | Sets the filesystem type which will be used to format the new volume.                                                                                                    | no       | "nfs"                         |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].volumeBindingMode                   | Controls when volume binding and dynamic provisioning should occur.                                                                                                      | no       | Immediate                     |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].allowedTopologies                   | Helps scheduling pods on worker nodes which match all of the below expressions.                                                                                          | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].mountOptions                        | Specifies additional mount options for when a persistent volume is being mounted on a node.                                                                              | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.isReplicationEnabled    | If set to true, will mark this storage class as replication enabled.                                                                                                     | no       | "true"                        |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.remoteStorageClassName  | Name of the remote storage class.                                                                                                                                        | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.remoteClusterID         | ID of a remote cluster.                                                                                                                                                  | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.remoteSystem            | Name of the remote system that should match whatever clusterName you called it in csI-powerscale-creds secret.                                                           | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.rpo                     | Recovery Point Objective value.                                                                                                                                          | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.ignoreNamespaces        | If set to true PowerScale driver, it will ignore what namespace volumes are created and put every volume created using this storage class into a single volume group.    | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.volumeGroupPrefix       | String that would be appended to the volume group name to differentiate the Volume Groups.                                                                               | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.remoteAccessZone        | Name of the access zone a remote volume can be created in.                                                                                                               | no       | Same as General isiAccessZone |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.remoteAzServiceIP       | AccessZone groupnet service IP.                                                                                                                                          | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | storageClasses[0].replication.remoteRootClientEnabled | Determines whether the driver should enable root squashing or not for the remote volume.                                                                                 | no       | Not Applicable                |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-------------------------------+


**************************
SnapshotClasses Parameters
**************************

.. table::
    :widths: auto

    +-----------------------------------+-----------------------------------------------------------------------------------------------+----------+-------------------------+
    | Parameter                         | Description                                                                                   | Required | Default                 |
    +===================================+===============================================================================================+==========+=========================+
    | snapshotClasses                   | List of snapshot classes.                                                                     | no       | Not Applicable          |
    +-----------------------------------+-----------------------------------------------------------------------------------------------+----------+-------------------------+
    | snapshotClasses[0].name           | SnapshotClass name.                                                                           | yes      | Not Applicable          |
    +-----------------------------------+-----------------------------------------------------------------------------------------------+----------+-------------------------+
    | snapshotClasses[0].deletionPolicy | Configure what happens to a VolumeSnapshotContent when the VolumeSnapshot object is deleted.  | yes      | Delete                  |
    +-----------------------------------+-----------------------------------------------------------------------------------------------+----------+-------------------------+
    | snapshotClasses[0].IsiPath        | The base path of the volumes on Isilon cluster for which snapshot is being created.           | no       | Same as General isiPath |
    +-----------------------------------+-----------------------------------------------------------------------------------------------+----------+-------------------------+


******************
Secrets Parameters
******************

.. table::
    :widths: auto

    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | Parameter                                  | Description                                                                                           | Required | Default                 |
    +============================================+=======================================================================================================+==========+=========================+
    | secret.arrays                              | List of PowerStore array.                                                                             | no       | Not Applicable          |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].clusterName               | Logical name of PowerScale Cluster.                                                                   | yes      | Not Applicable          |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].username                  | Username for connecting to PowerScale OneFS API server.                                               | yes      | Not Applicable          |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].password                  | Password for connecting to PowerScale OneFS API server.                                               | yes      | Not Applicable          |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].endpoint                  | HTTPS endpoint of the PowerScale OneFS API server.                                                    | yes      | Not Applicable          |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].endpointPort              | Specifies the HTTPs port number of the PowerScale OneFS API server.                                   | no       | 8080                    |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].isDefault                 | Is this a default cluster?                                                                            | no       | false                   |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].skipCertificateValidation | Specifies whether the PowerScale OneFS API server's certificate chain and host name should be verified| no       | true                    |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].isiPath                   | The base path for the volumes to be created on PowerScale cluster.                                    | no       | Same as General isiPath |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].isiVolumePathPermissions  | The permissions for isi volume directory path.                                                        | no       | "0777"                  |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].ignoreUnresolvableHosts   | Ignore unresolvable hosts on the OneFS.                                                               | no       | false                   |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].replicationCertificateID  | Unique ID if the certificate is used to encrypt replication policy.                                   | no       | ""                      |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+
    | secret.arrays[0].authorizationEndpoint     | HTTPS localhost endpoint that the authorization sidecar will listen on.                               | no       | https://localhost:9400  |
    +--------------------------------------------+-------------------------------------------------------------------------------------------------------+----------+-------------------------+


