.. _create-rbd-volume-snapshot-class-0318eed94b92:

================================
Create RBD Volume Snapshot Class
================================

Volume Snapshot Class for |RBD| provisioner can be created via Helm overrides
to support |PVC| snapshots.

.. rubric:: |context|

A Volume Snapshot Class enables the creation of snapshots for |PVCs|, allowing
efficient backups and data restoration. This functionality ensures data
protection, facilitating point-in-time recovery and minimizing the risk of data
loss in Kubernetes clusters.

The procedure below demonstrates how to create a Volume Snapshot Class and
Volume Snapshot for the |RBD| provisioner.

.. note::

    It is necessary that the |CRDs| and snapshot-controller running pod are
    present in the system to create the Volume Snapshot Class.

    The |CRDs| and snapshot-controller are created by default during
    installation when running the bootstrap playbook.

.. only:: partner

    .. include:: /_includes/create-rbd-volume-snapshot-class-0318eed94b92.rest
        :start-after: fresh-install-begin
        :end-before: fresh-install-end

.. rubric:: |proc|

#.  List installed Helm chart overrides for the ``platform-integ-apps``.

    .. code-block:: none

        ~(keystone_admin)$ system helm-override-list platform-integ-apps
        +--------------------+----------------------+
        | chart name         | overrides namespaces |
        +--------------------+----------------------+
        | ceph-pools-audit   | ['kube-system']      |
        | cephfs-provisioner | ['kube-system']      |
        | rbd-provisioner    | ['kube-system']      |
        +--------------------+----------------------+

#.  Review existing overrides for the rbd-provisioner chart.

    .. code-block:: none

        ~(keystone_admin)$ system helm-override-show platform-integ-apps rbd-provisioner kube-system

#.  Check if the ``provisioner.snapshotter.enabled`` is set to true.

    .. code-block:: none

        ~(keystone_admin)$ system helm-override-show platform-integ-apps rbd-provisioner kube-system
        +--------------------+------------------------------------------------------+
        | Property           | Value                                                |
        +--------------------+------------------------------------------------------+
        | attributes         | enabled: true                                        |
        |                    |                                                      |
        | combined_overrides | ...                                                  |
        |                    | provisioner:                                         |
        |                    |   replicaCount: 1                                    |
        |                    |   snapshotter:                                       |
        |                    |     enabled: true                                    |
        +--------------------+------------------------------------------------------+

    True means that the ``csi-snapshotter`` container is created inside the
    |RBD| provisioner pod and that the |CRDs| and ``snapshot-controller`` with
    the corresponding Kubernetes version are created.

    If the value is false, and the |CRDs| and snapshot controller are present
    in a later version than what is recommended for Kubernetes on your system,
    you can update the value via ``helm-overrides`` and set it to ``true`` and
    continue with the creation of the container as follows:

    #.  Update to ``true`` via ``helm-overrides``.

        .. code-block:: none

            ~(keystone_admin)$ system helm-override-update --reuse-values platform-integ-apps rbd-provisioner kube-system --set provisioner.snapshotter.enabled=true

    #.  Create container.

        .. code-block:: none

            ~(keystone_admin)$ system application-apply platform-integ-apps

    .. important::

        To proceed with the creation of the snapshot class and volume snapshot,
        it is strictly necessary that the ``csi-snapshotter`` container is
        created.

#.  Update ``snapshotClass.create`` to ``true`` via Helm.

    .. code-block:: none

        ~(keystone_admin)$ system helm-override-update --reuse-values platform-integ-apps rbd-provisioner kube-system --set snapshotClass.create=True

#.  Confirm that the new overrides have been applied to the chart.

    .. code-block:: none

        ~(keystone_admin)$ system helm-override-show platform-integ-apps rbd-provisioner kube-system
        +--------------------+------------------------------------------------------+
        | Property           | Value                                                |
        +--------------------+------------------------------------------------------+
        | attributes         | enabled: true                                        |
        |                    |                                                      |
        | combined_overrides | classdefaults:                                       |
        |                    |   adminId: admin                                     |
        |                    |   adminSecretName: ceph-admin                        |
        |                    |   monitors:                                          |
        |                    |   - 192.168.204.2:6789                               |
        |                    |   storageClass: general                              |
        |                    | csiConfig:                                           |
        |                    | - clusterID: c10448eb-6dee-4992-a93c-a1c628b9165e    |
        |                    |   monitors:                                          |
        |                    |   - 192.168.204.2:6789                               |
        |                    | provisioner:                                         |
        |                    |   replicaCount: 1                                    |
        |                    |   snapshotter:                                       |
        |                    |     enabled: true                                    |
        |                    | snapshotClass:                                       |
        |                    |   clusterID: c10448eb-6dee-4992-a93c-a1c628b9165e    |
        |                    |   create: true                                       |
        |                    |   provisionerSecret: ceph-pool-kube-rbd              |
        |                    | storageClasses:                                      |
        |                    | - additionalNamespaces:                              |
        |                    |   - default                                          |
        |                    |   - kube-public                                      |
        |                    |   chunk_size: 64                                     |
        |                    |   clusterID: c10448eb-6dee-4992-a93c-a1c628b9165e    |
        |                    |   controllerExpandSecret: ceph-pool-kube-rbd         |
        |                    |   crush_rule_name: storage_tier_ruleset              |
        |                    |   name: general                                      |
        |                    |   nodeStageSecret: ceph-pool-kube-rbd                |
        |                    |   pool_name: kube-rbd                                |
        |                    |   provisionerSecret: ceph-pool-kube-rbd              |
        |                    |   replication: 1                                     |
        |                    |   userId: ceph-pool-kube-rbd                         |
        |                    |   userSecretName: ceph-pool-kube-rbd                 |
        |                    |                                                      |
        | name               | rbd-provisioner                                      |
        | namespace          | kube-system                                          |
        | system_overrides   | ...                                                  |
        |                    |                                                      |
        | user_overrides     | snapshotClass:                                       |
        |                    |   create: true                                       |
        |                    |                                                      |
        +--------------------+------------------------------------------------------+

#.  Apply the overrides.

    #.  Run the :command:`application-apply` command.

        .. code-block:: none

            ~(keystone_admin)$ system application-apply platform-integ-apps
            +---------------+--------------------------------------+
            | Property      | Value                                |
            +---------------+--------------------------------------+
            | active        | True                                 |
            | app_version   | 1.0-65                               |
            | created_at    | 2024-01-08T18:15:07.178753+00:00     |
            | manifest_file | fluxcd-manifests                     |
            | manifest_name | platform-integ-apps-fluxcd-manifests |
            | name          | platform-integ-apps                  |
            | progress      | None                                 |
            | status        | applying                             |
            | updated_at    | 2024-01-08T18:39:10.251660+00:00     |
            +---------------+--------------------------------------+

    #.  Monitor progress using the application-list command.

        .. code-block:: none

            ~(keystone_admin)$ system application-list
            +--------------------------+---------+-------------------------------------------+------------------+----------+-----------+
            | application              | version | manifest name                             | manifest file    | status   | progress  |
            +--------------------------+---------+-------------------------------------------+------------------+----------+-----------+
            | platform-integ-apps      | 1.0-65  | platform-integ-apps-fluxcd-manifests      | fluxcd-manifests | applied  | completed |
            +--------------------------+---------+-------------------------------------------+------------------+----------+-----------+

#.  Confirm the creation of the Volume Snapshot Class after a few seconds.

    .. code-block:: none

        ~(keystone_admin)$ kubectl get volumesnapshotclasses.snapshot.storage.k8s.io
        NAME              DRIVER                DELETIONPOLICY   AGE
        rbd-snapshot      rbd.csi.ceph.com      Delete           5s

#.  With the |RBD| Volume Snapshot Class created, you can now create |RBD| |PVC|
    snapshots.

    #.  Consider the |RBD| Volume Snapshot yaml example:

        .. code-block:: none

            ~(keystone_admin)$ cat << EOF > ~/rbd-volume-snapshot.yaml
            ---
            apiVersion: snapshot.storage.k8s.io/v1
            kind: VolumeSnapshot
            metadata:
            name: <rbd-pvc-snapshot-name>
            spec:
            volumeSnapshotClassName: rbd-snapshot
            source:
                persistentVolumeClaimName: <rbd-pvc-name>
            EOF

    #.  Replace the values in the ``persistentVolumeClaimName`` and ``name``
        fields.

    #.  Create the Volume Snapshot.

        .. code-block:: none

            ~(keystone_admin)$ kubectl create -f rbd-volume-snapshot.yaml

    #.  Confirm that it was created successfully.

        .. code-block:: none

            ~(keystone_admin)$ kubectl get volumesnapshots.snapshot.storage.k8s.io
            NAME               READYTOUSE   SOURCEPVC   SOURCESNAPSHOTCONTENT   RESTORESIZE   SNAPSHOTCLASS   SNAPSHOTCONTENT                                    CREATIONTIME   AGE
            rbd-pvc-snapshot   true         rbd-pvc                             1Gi           rbd-snapshot    snapcontent-1bb7e2cb-9123-47c4-9e56-7d16f24f973e   13s            17s