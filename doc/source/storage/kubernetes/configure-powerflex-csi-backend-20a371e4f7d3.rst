.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _configure-powerflex-csi-backend-20a371e4f7d3:

===============================
Configure PowerFlex CSI Backend
===============================

|CSI| driver for PowerFlex is a part of the |CSM| open-source suite of
Kubernetes storage enablers for Dell products. It is a |CSI| driver that
provides support for provisioning persistent storage using Dell PowerFlex
storage array.

.. note::

    VxFlexOs is the legacy name for PowerFlex.

.. note::

    By default, the csi-powerflex chart is disabled. It is necessary to enable
    the chart and update user-overrides before applying the dell-storage
    application.

.. contents::
   :local:
   :depth: 2

--------------------------
Enable CSI PowerFlex Chart
--------------------------

.. note::

    Disable any other |CSI|/|CSM| if not used/configured, otherwise it could cause the
    application to fail on apply.

    .. code-block::

        system helm-chart-attribute-modify --enabled false dell-storage <chart_name> dell-storage


#. Enable the chart using the following command:

   .. code-block:: none

       (keystone_admin)$  system helm-chart-attribute-modify --enabled true dell-storage csi-powerflex dell-storage

#. Create overrides to configure the storage connection.

   .. code-block:: none

       :name: powerflexOverrides.yaml
       app.starlingx.io/component: platform

       storageClasses:
       - name: csi-powerflex-nfs
         arrayId: <ARRAY_ID>
         nasServer: nas_2
         provisioner: csi-vxflexos.dellemc.com
         fstype: nfs
         path: /target
         reclaimPolicy: Delete
         allowVolumeExpansion: true
         volumeBindingMode: Immediate
         mountOptions: ["hard"]
         hostIoSize: 8192
         tieringPolicy: 0
         isDataReductionEnabled: "false"
         thinProvisioned: "true"
         storagePool: pool_2
         protocol: NFS

       secret:
         arrays:
         - arrayId: <ARRAYID>
           username: <USERNAME>
           password: <PASSWORD>
           systemID: <SYSTEM_ID>
           allSystemNames: "pflex-1"
           endpoint: https://<POWERFLEX_ADDRESS>/api/rest
           isDefault: true
           skipCertificateValidation: true
           nasName: nas_2

#. Apply overrides using the following command:

   .. code-block::

       (keystone_admin)$ system helm-override-update dell-storage csi-powerflex dell-storage --values=powerflexOverrides.yaml

#. Apply the chart using the following command:

   .. code-block::

       (keystone_admin)$ system application-apply dell-storage

------------------------
UserOverrides Parameters
------------------------

******************
General Parameters
******************

.. table::
    :widths: auto

    +------------------------------------+-----------------------------------------------------------+----------+----------+
    | Parameter                          | Description                                               | Required | Default  |
    +====================================+===========================================================+==========+==========+
    | csm.replication                    | Enables/disables replication sidecar container.           | No       | false    |
    +------------------------------------+-----------------------------------------------------------+----------+----------+
    | csm.resiliency                     | Enables/disables dell-csi-replicator sidecar container.   | No       | false    |
    +------------------------------------+-----------------------------------------------------------+----------+----------+
    | app.starlingx.io/component         | Core affinity.                                            | No       | platform |
    +------------------------------------+-----------------------------------------------------------+----------+----------+
    | controller.snapshot.deletionPolicy | Specifies what happens when VolumeSnapshot is deleted.    | No       | Delete   |
    +------------------------------------+-----------------------------------------------------------+----------+----------+

*************************
StorageClasses Parameters
*************************

.. table::
    :widths: auto

    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | Parameter                                             | Description                                                                                                                                                              | Required | Default                   |
    +=======================================================+==========================================================================================================================================================================+==========+===========================+
    | storageClasses[0].name                                | StorageClass name.                                                                                                                                                       | No       | "csi-powerflex-" + fstype |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].systemID                            | ID of array to be used for volumes. arrayID corresponding to array's globalID specified in secrets.                                                                      | Yes      | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].nasName                             | |NAS| server's name. Important: required for |NFS| protocol.                                                                                                             | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].provisioner                         | Driver name.                                                                                                                                                             | No       | csi-vxflexos.dellemc.com  |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].fstype                              | Filesystem type for mounted volumes.                                                                                                                                     | No       | nfs                       |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].reclaimPolicy                       | PVs that are dynamically created by a StorageClass will have the reclaim policy specified here.                                                                          | No       | Delete                    |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].allowVolumeExpansion                | Allows the users to resize the volume by editing the corresponding |PVC| object.                                                                                         | No       | true                      |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].volumeBindingMode                   | Controls when volume binding and dynamic provisioning should occur.                                                                                                      | No       | Immediate                 |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].protectionDomain                    | Protection domain that storage pool belongs to. Needed if array has two storagepools that share the same name but belong to different protection domains.                | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].iopsLimit                           | Limit the volume |IOPS|.                                                                                                                                                 | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].path                                | The relative path to the root of the associated filesystem.                                                                                                              | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].softLimit                           | Time for soft limit to quota. Specified as percentage.                                                                                                                   | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].gracePeriod                         | Requires the configuration of softLimit. Defines the period in which the soft limit time has reached and can exceed until process ends.                                  | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].bandwithLimitInKbps                 | The value to limit the volume network bandwidth.                                                                                                                         | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].mkfsFormatOption                    | A string value to dictate de fs format options to pass to mkfs.                                                                                                          | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].storagePool                         | Defines storage pool. The value should be picked from the column labeled "CLI ID" of pools in the Unisphere GUI.                                                         | Yes      | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication                         | Used to configure replication sidecar container.                                                                                                                         | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.remoteStorageClassName  | Remote StorageClass name.                                                                                                                                                | No       | csi-powerflex-replication |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.remoteClusterID         | Remote ClusterID.                                                                                                                                                        | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.remoteSystem            | Remote PowerStore system.                                                                                                                                                | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.remoteStoragePool       | Defines storage pool for the replication. The value should be picked from the column labeled "CLI ID" of Pools in the Unisphere GUI.                                     | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.rpo                     | Change to any other |RPOs| supported by PowerStore.                                                                                                                      | No       | Five_Minutes              |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.consistencyGroupName    | Defines the group name in array of a replication.                                                                                                                        | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.volumeGroupPrefix       | Volume group prefix.                                                                                                                                                     | No       | csi-powerflex             |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.protectionDomain        | Protection domain that storage pool of replication belongs to. Needed if array has two storagepools that share the same name but belong to different protection domains. | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.remotePVRetentionPolicy | Reclaim policy for PersistentVolumes. Can accept ``Retain``, ``Recycle``, and ``Delete``.                                                                                | No       | Delete                    |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].replication.remoteRGRetentionPolicy | Reclaim policy for |RG|. Can accept ``Retain``, ``Recycle``, and ``Delete``.                                                                                             | No       | Delete                    |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+
    | storageClasses[0].allowedTopologies                   | This feature lets you specify sub-divisions of a cluster for a volume to be placed in.                                                                                   | No       | Not Applicable            |
    +-------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------------+

*****************
Secret Parameters
*****************

.. table::
    :widths: auto

    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
    | Parameter                                  | Description                                                                             | Required | Default        |
    +============================================+=========================================================================================+==========+================+
    | secret.arrays                              | List of Unity XT.                                                                       | Yes      | Not Applicable |
    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].systemID                  | Array ID of Powerflex.                                                                  | Yes      | Not Applicable |
    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].username                  | Username for accessing PowerFlex system.                                                | Yes      | Not Applicable |
    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].password                  | Password for accessing PowerFlex system.                                                | Yes      | Not Applicable |
    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].endpoint                  | HTTPS localhost endpoint that the authorization sidecar will listen on.                 | Yes      | Not Applicable |
    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].isDefault                 | Treat current array as a default. Important: use at least one of the arrays as default. | No       | false          |
    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].skipCertificateValidation | Indicates if client-side validation of server's certificate can be skipped.             | No       | true           |
    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].AllSystemNames            | Previous names used in secret of Powerflex system.                                      | Yes      | Not Applicable |
    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].nasName                   | What |NAS| should be used for |NFS| volumes. Required for |NFS| protocol.               | No       | Not Applicable |
    +--------------------------------------------+-----------------------------------------------------------------------------------------+----------+----------------+
