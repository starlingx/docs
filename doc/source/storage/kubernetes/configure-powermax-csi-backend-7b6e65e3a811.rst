.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _configure-powermax-csi-backend-7b6e65e3a811:

==============================
Configure PowerMax CSI Backend
==============================

|CSI| driver for PowerMax is a part of the |CSM| open-source suite of
Kubernetes storage enablers for Dell products. It is a |CSI| driver that
provides support for provisioning persistent storage using Dell PowerMax
storage array.

.. note::

    By default, the csi-powermax chart is disabled. It is necessary to enable
    the chart and update user-overrides before applying the dell-storage
    application.

.. contents::
   :local:
   :depth: 2

-------------------------
Enable CSI PowerMax Chart
-------------------------

.. note::

    Disable any other |CSI|/|CSM| if not used/configured, otherwise it could cause the
    application to fail on apply.

    .. code-block:: none

        (keystone_admin)$ system helm-chart-attribute-modify --enabled false dell-storage <chart_name> dell-storage

#. Enable the chart.

   .. code-block:: none

       (keystone_admin)$  system helm-chart-attribute-modify --enabled true dell-storage csi-powermax dell-storage

#. Create overrides to configure the storage connection.

   .. code-block:: none

       :name: powermaxOverrides.yaml
       secret:
         username: <USERNAME>
         password: <PASSWORD>

       authorization:
         configs:
           - username: <USERNAME>
             password: <PASSWORD>
             intendedEndpoint: endpoint1
             systemID: <SYSTEM_ID>
             isDefault: true

       global:
         managementServers:
           - endpoint: https://primary-1.unisphe.re:8443
             credentialsSecret: primary-1-secret
             username: <USERNAME>
             password: <PASSWORD>
             skipCertificateValidation: true

       storageClasses:
       - name: "sc-0"
         SRP: "TEST"
         SYMID: "TEST1"
         ApplicationPrefix: "prefix1"
         HostLimitName: "1000"
         DynamicDistribution: "Optimized

#. Apply overrides.

   .. code-block:: none

       (keystone_admin)$ system helm-override-update dell-storage csi-powermax dell-storage --values=powermaxOverrides.yaml

#. Apply the chart.

   .. code-block:: none

       (keystone_admin)$ system application-apply dell-storage

------------------------
UserOverrides Parameters
------------------------

******************
General Parameters
******************

.. table::
    :widths: auto

    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | Parameter                              | Description                                                                                                                      | Required | Default                      |
    +========================================+==================================================================================================================================+==========+==============================+
    | csm.replication                        | Enables/disables replication sidecar container.                                                                                  | No       | false                        |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | csm.authorization                      | Enables/disables |CSM| authorization and sidecar container.                                                                      | No       | false                        |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | app.starlingx.io/component             | Core affinity.                                                                                                                   | No       | platform                     |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | replication.replicationPrefix          | Change replication prefix.                                                                                                       | No       | replication.storage.dell.com |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | controller.snapshot.deletionPolicy     | Specifies what happens when VolumeSnapshot is deleted.                                                                           | No       | Delete                       |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | defaultFsType                          | Default value for Filesystem type for mounted volumes.                                                                           | No       | ext4                         |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | certSecret                             | Secret containing the certificate.                                                                                               | No       | Not Applicable               |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | global.storageArrays                   | This refers to the list of arrays managed by the driver and Reverse Proxy in StandAlone mode.                                    | Yes      | Not Applicable               |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | global.storageArrays[0].storageArrayId | This refers to PowerMax Symmetrix ID.                                                                                            | Yes      | Not Applicable               |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | global.storageArrays[0].endpoint       | This refers to the URL of the Unisphere server managing storageArrayId.                                                          | Yes      | Not Applicable               |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+
    | global.storageArrays[0].backupEndpoint | This refers to the URL of the backup Unisphere server managing storageArrayId, if Reverse Proxy is installed in StandAlone mode. | Yes      | Not Applicable               |
    +----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------+----------+------------------------------+

*************************
StorageClasses Parameters
*************************

.. table::
    :widths: auto

    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | Parameter                                            | Description                                                                                     | Required | Default                  |
    +======================================================+=================================================================================================+==========+==========================+
    | storageClasses[0].name                               | StorageClass name.                                                                              | Yes      | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].ServiceLevel                       | Name of service level on PowerMax array that should be used for provisioning.                   | No       | Optimized                |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].SRP                                | Serial ID of the array that is used for provisioning.                                           | Yes      | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].SYMID                              | Name of service level on PowerMax array that should be used for provisioning.                   | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].ApplicationPrefix                  | Name of application to be used to group volumes.                                                | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].HostLimitName                      | HostLimitName uniquely identifies given set of limits on a storage class (max of 3 letters).    | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].HostIOLimitMBSec                   | The MBs per Second Host IO limit for the storage class.                                         | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].HostIOLimitIOSec                   | The IOs per Second Host IO limit for the storage class.                                         | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].DynamicDistribution                | Distribution of the Host IO limits for the storage class.                                       | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].nasServer                          | |NAS| server's name. Important: required for |NFS| protocol.                                    | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].provisioner                        | Driver name.                                                                                    | No       | csi-powermax.dellemc.com |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].fstype                             | Filesystem type for mounted volumes.                                                            | No       | $.Values.defaultFsType   |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].reclaimPolicy                      | PVs that are dynamically created by a StorageClass will have the reclaim policy specified here. | No       | Delete                   |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].allowVolumeExpansion               | Allows the users to resize the volume by editing the corresponding |PVC| object.                | No       | true                     |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].volumeBindingMode                  | Controls when volume binding and dynamic provisioning should occur.                             | No       | Immediate                |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].allowRoot                          | Enables or disables root squashing (valid only for |NFS|).                                      | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].mountOptions                       | Specifies additional mount options when a persistent volume is being mounted on a node.         | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].allowedTopologies                  | This feature lets you specify sub-divisions of a cluster for a volume to be placed in.          | Yes      | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication                        | Used to configure replication sidecar container.                                                | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication.remoteStorageClassName | Storage class name on remote cluster to create remote PV.                                       | No       | csi-powermax-replication |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication.remoteClusterID        | ClusterID to which resources will get replicated.                                               | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication.RemoteSRP              | Name of |SRP| on the remote array that should be used for provisioning.                         | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication.RemoteSYMID            | Serial ID of the remote array that is used for replication.                                     | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication.RemoteServiceLevel     | Service Level on remote PowerMax array.                                                         | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication.RdfMode                | SRDF mode of the replication.                                                                   | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication.RdfGroup               | Local |SRDF| group which protects using mode mentioned above.                                   | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication.Bias                   | Configure Bias for Metro mode.                                                                  | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+
    | storageClasses[0].replication.RemoteRDFGroup         | Remote |SRDF| Group which is bound with Local |SRDF| group.                                     | No       | Not Applicable           |
    +------------------------------------------------------+-------------------------------------------------------------------------------------------------+----------+--------------------------+

*****************
Secret Parameters
*****************

.. table::
    :widths: auto

    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | Parameter                                                            | Description                                                                                                | Required | Default                       |
    +======================================================================+============================================================================================================+==========+===============================+
    | global.defaultSecret                                                 | Used to configure default credential secrets values.                                                       | Yes      | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.username                                        | Username for connecting to REST API server.                                                                | Yes      | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.password                                        | Password for connecting to REST API server.                                                                | Yes      | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.chapsecret                                      | Secret that contains chap to authentication.                                                               | No       | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers                               | A list of values to configure all credential secrets used.                                                 | Yes      | []                            |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].credentialsSecret          | Secret name.                                                                                               | No       | $.Release.Name -creds- $index |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].username                   | Username for connecting to REST API server.                                                                | Yes      | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].password                   | Password for connecting to REST API server.                                                                | Yes      | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].chapsecret                 | Secret that contains chap to authentication.                                                               | No       | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].endpoint                   | Endpoint to REST API server.                                                                               | Yes      | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].skipCertificateValidation  | Indicates if client side validation of server's certificate can be skipped.                                | Yes      | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].certSecret                 | Secret containing the certificate.                                                                         | No       | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].limits                     | This refers to various limits for Reverse Proxy.                                                           | No       | Not Applicable                |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].limits.maxActiveRead       | This refers to the maximum concurrent READ request handled by the reverse proxy.                           | No       | 5                             |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].limits.maxActiveWrite      | This refers to the maximum concurrent WRITE request handled by the reverse proxy.                          | No       | 4                             |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].limits.maxOutStandingRead  | This refers to maximum queued READ request when reverse proxy receives more than maxActiveRead requests.   | No       | 50                            |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+
    | global.defaultSecret.managementServers[0].limits.maxOutStandingWrite | This refers to maximum queued WRITE request when reverse proxy receives more than maxActiveWrite requests. | No       | 50                            |
    +----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------+----------+-------------------------------+


************************
Authorization Parameters
************************

.. table::
    :widths: auto

    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | Parameter                                 | Description                                                                         | Required | Default                |
    +===========================================+=====================================================================================+==========+========================+
    | authorization.configs                     | List of authorization configurations.                                                | No       | Not Applicable        |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | authorization.configs[0].username         | Username for connecting to the backend storage array. This parameter is ignored.    | No       | Not Applicable         |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | authorization.configs[0].password         | Password for connecting to the backend storage array. This parameter is ignored.    | No       | Not Applicable         |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | authorization.configs[0].intendedEndpoint | HTTPS REST API endpoint of the backend storage array.                               | No       | Not Applicable         |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | authorization.configs[0].endpoint         | HTTPS localhost endpoint that the authorization sidecar will listen on.             | No       | https://localhost:9400 |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | authorization.configs[0].SystemID         | System ID of the backend storage array.                                             | No       | " "                    |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | authorization.configs[0].isDefault        | If the configuration is default or not.                                             | No       | false                  |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | skipCertificateValidation                 | If certificate is used or not.                                                      | No       | true                   |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | proxyAuthzTokens                          | |JWT| information                                                                   | No       | Not Applicable         |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | proxyAuthzTokens.access                   | Access token for authorization sidecar container access.                            | No       | Not Applicable         |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+
    | proxyAuthzTokens.refresh                  | Refresh token for authorization sidecar container access.                           | No       | Not Applicable         |
    +-------------------------------------------+-------------------------------------------------------------------------------------+----------+------------------------+

