.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _configure-powerstore-csi-backend-02af0aa983b0:

================================
Configure Powerstore CSI Backend
================================

|CSI| driver for PowerStore is a part of the |CSM| open-source suite of
Kubernetes storage enablers for Dell products. It is a |CSI| driver that
provides support for provisioning persistent storage using Dell PowerStore
storage array.

.. contents::
   :local:
   :depth: 2

---------------------------
Enable CSI PowerStore Chart
---------------------------

.. note::

    By default, the csi-powerstore chart is enabled. Disable any other
    |CSI|/|CSM| if not used/configured, otherwise it could cause the
    application to fail on apply.

    .. code-block::

        (keystone_admin)$ system helm-chart-attribute-modify --enabled false dell-storage <chart_name> dell-storage

#. Enable the chart.

   .. code-block:: none

       (keystone_admin)$  system helm-chart-attribute-modify --enabled true dell-storage csi-powerstore dell-storage

#. Create overrides to configure the storage connection.

   **ISCSI Protocol**

   .. code-block:: none

       :name: powerstoreOverrides.yaml
       storageClasses:
       - name: csi-powerstore-iscsi
         arrayID: <ARRAY_ID>
         fstype: ext4

       secret:
         arrays:

         - globalID: <GLOBAL_ID>
           username: <USERNAME>
           password: <PASSWORD>
           endpoint: https://<POWERSTORE_ADDRESS>/api/rest
           isDefault: true
           blockProtocol: "ISCSI"

   **NFS Protocol**

   .. code-block:: none

       :name: powerstoreOverrides.yaml
       storageClasses:
       - name: csi-powerstore-nfs
         arrayID: <ARRAY_ID>
         fstype: ext4
         nasName: NAS1

       secret:
         arrays:

         - globalID: <GLOBAL_ID>
           username: <USERNAME>
           password: <PASSWORD>
           endpoint: https://<POWERSTORE_ADDRESS>/api/rest
           isDefault: true
           blockProtocol: None
           nasName: NAS1

#. Apply overrides.

   .. code-block:: none

       (keystone_admin)$ system helm-override-update dell-storage csi-powerstore dell-storage --values=powerstoreOverrides.yaml

#. Apply the chart.

   .. code-block:: none

       (keystone_admin)$ system application-apply dell-storage

------------------------
UserOverrides Parameters
------------------------

******************
General Parameters
******************

.. table::
    :widths: auto

    +------------------------------------------+-------------------------------------------------------+----------+------------------------------+
    | Parameter                                | Description                                           | Required | Default                      |
    +==========================================+=======================================================+==========+==============================+
    | csm.replication                          | Enables/disables replication sidecar container.       | No       | false                        |
    +------------------------------------------+-------------------------------------------------------+----------+------------------------------+
    | csm.resiliency                           | Enables/disables podmon sidecar container.            | No       | false                        |
    +------------------------------------------+-------------------------------------------------------+----------+------------------------------+
    | app.starlingx.io/component               | Core Affinity.                                        | No       | platform                     |
    +------------------------------------------+-------------------------------------------------------+----------+------------------------------+
    | controller.replication.replicationPrefix | Change replication prefix.                            | No       | replication.storage.dell.com |
    +------------------------------------------+-------------------------------------------------------+----------+------------------------------+
    | controller.snapshot.deletionPolicy       | Specifies what happens when VolumeSnapshot is deleted.| No       | Delete                       |
    +------------------------------------------+-------------------------------------------------------+----------+------------------------------+

*************************
StorageClasses Parameters
*************************

.. table::
    :widths: auto

    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | Parameter                                            | Description                                                                                               | Required | Default                    |
    +======================================================+===========================================================================================================+==========+============================+
    | storageClasses[0].name                               | StorageClass name.                                                                                        | Yes      | "csi-powerstore-" + fstype |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].arrayID                            | ID of array to be used for volumes. arrayID corresponding to array's globalID specified in secrets.       | Yes      | Not Applicable             |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].nasName                            | |NAS| server's name. Important: required for |NFS| protocol.                                              | No       | Not Applicable             |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].provisioner                        | Driver name.                                                                                              | No       | csi-powerstore.dellemc.com |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].fstype                             | Filesystem type for mounted volumes.                                                                      | No       | nfs                        |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].reclaimPolicy                      | PVs that are dynamically created by a StorageClass will have the reclaim policy specified here.           | No       | Delete                     |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].allowVolumeExpansion               | Allows the users to resize the volume by editing the corresponding |PVC| object.                          | No       | false                      |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].volumeBindingMode                  | Controls when volume binding and dynamic provisioning should occur.                                       | No       | Immediate                  |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].allowRoot                          | Enables or disables root squashing (valid only for |NFS|).                                                | No       | false                      |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].nfsAcls                            | Enables setting permissions on |NFS| mount directory.                                                     | No       | 0777                       |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].allowedTopologies                  | This feature lets you specify sub-divisions of a cluster for a volume to be placed in.                    | Yes      | Not Applicable             |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].replication                        | Used to configure replication sidecar container.                                                          | No       | Not Applicable             |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].replication.remoteStorageClassName | Remote StorageClass name.                                                                                 | No       | csi-powerstore-replication |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].replication.remoteClusterID        | Remote ClusterID.                                                                                         | No       | Not Applicable             |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].replication.remoteSystem           | Remote PowerStore system.                                                                                 | No       | Not Applicable             |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].replication.rpo                    | Change to any other |RPOs| supported by PowerStore.                                                       | No       | Five_Minutes               |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].replication.ignoreNamespaces       | Set to true if you want to ignore namespaces and if false creates separate volume group per namespace.    | No       | false                      |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+
    | storageClasses[0].replication.volumeGroupPrefix      | Volume group prefix.                                                                                      | No       | csi-powerstore             |
    +------------------------------------------------------+-----------------------------------------------------------------------------------------------------------+----------+----------------------------+

*****************
Secret Parameters
*****************

.. table::
    :widths: auto

    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | Parameter                                  | Description                                                                                       | Required | Default        |
    +============================================+===================================================================================================+==========+================+
    | secret.arrays                              | List of PowerStore array.                                                                         | Yes      | Not Applicable |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].globalID                  | Unique ID of the PowerStore array. globalID corresponding to arrayID specified in storageClasses. | Yes      | Not Applicable |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].username                  | Username for connecting to REST API server.                                                       | Yes      | Not Applicable |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].password                  | Password for connecting to REST API server.                                                       | Yes      | Not Applicable |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].endpoint                  | HTTPS endpoint of the REST API server.                                                            | Yes      | Not Applicable |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].isDefault                 | Treat current array as a default. Important: requires one array as default.                       | No       | false          |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].skipCertificateValidation | Indicates if client side validation of server's certificate can be skipped.                       | No       | true           |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].blockProtocol             | What SCSI transport protocol used on node side (FC, ISCSI, NVMeTCP, NVMeFC, None, or auto).       | No       | auto           |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].nasName                   | What |NAS| should be used for |NFS| volumes. Required for |NFS| protocol.                         | No       | Not Applicable |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].nfsAcls                   | Enables setting permissions on |NFS| mount directory.                                             | No       | 0777           |
    +--------------------------------------------+---------------------------------------------------------------------------------------------------+----------+----------------+

