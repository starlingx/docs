.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _dell-storage-backend-28f5771e6d9b:

====================
Dell Storage Backend
====================

The |CSI| drivers by Dell implement an interface between |CSI| (|CSI| spec
v1.6) enabled |CO| and Dell Storage Arrays. It is a plug-in that is installed
in Kubernetes to provide persistent storage using the Dell storage system.

For details about Dell |CSIs|, see `<https://dell.github.io/csm-docs/docs/csidriver/>`__.

The Dell Technologies (Dell) |CSM| enables simple and consistent integration,
and automation experiences, extending enterprise storage capabilities to
Kubernetes for cloud-native stateful applications.

For details about Dell |CSMs|, see `<https://dell.github.io/csm-docs/docs/>`__.

: rubric:: |prereq|

- Check if your Dell storage platform is supported. To check, see `<https://dell.github.io/csm-docs/docs/prerequisites/>`__.
- If using any |CSM|, check if it is supported for your platform. To check, see `<https://dell.github.io/csm-docs/docs/prerequisites/#supported-csm-modules>`__.
- Controller is unlocked.
- The Dell-storage application is on the uploaded state.
- All commands are executed on the active controller.

.. note::

    For every change on user-overrides to be updated on the application, you
    need to reapply the override before applying the Dell-storage application.

.. note::

    csi-powerstore is enabled by default. Disable any other |CSI|/|CSM| if
    not used, otherwise it could cause the application to fail on apply.

    .. code-block:: none

        (keystone_admin)$ system helm-chart-attribute-modify --enabled false dell-storage <chart_name> dell-storage

---------------
Common Commands
---------------

- Get list of enabled/disabled Dell-storage application charts.

  .. code-block:: none

      (keystone_admin)$ system helm-override-list dell-storage --long

- Check |CSM|/|CSI| overrides.

  .. code-block:: none

     (keystone_admin)$ system helm-override-show dell-storage <chart_name> dell-storage

- Enable/disable the chart.

  .. code-block:: none

      (keystone_admin)$  system helm-chart-attribute-modify --enabled <true/false> dell-storage <chart_name> dell-storage

- Apply overrides.

  .. code-block:: none

      (keystone_admin)$ system helm-override-update dell-storage <chart_name> dell-storage --values=<override_file>

- Get the Dell-storage pods list.

  .. code-block:: none

      (keystone_admin)$  kubectl get pods -n dell-storage

- Apply/abort/remove the Dell-storage application.

  .. code-block:: none

      (keystone_admin)$  system application-<apply/abort/remove> dell-storage

--------------
Related Topics
--------------

-   :ref:`configure-powermax-csi-backend-7b6e65e3a811`
-   :ref:`configure-powerflex-csi-backend-20a371e4f7d3`
-   :ref:`configure-powerscale-csi-backend-aa6a128c0b44`
-   :ref:`configure-powerstore-csi-backend-02af0aa983b0`
-   :ref:`configure-dell-storage-csms-937563b7bb36`
-   :ref:`configure-unity-xt-csi-backend-c1abbbd371dc`
-   :ref:`dell-storage-snapshots-46916ac34bf0`
-   :ref:`dell-storage-backend-28f5771e6d9b`


