.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _dell-storage-snapshots-46916ac34bf0:

================
Create Snapshots
================

.. rubric:: |context|

A Volume Snapshot Class enables the creation of snapshots for |PVCs|, allowing
efficient backups and data restoration. This functionality ensures data
protection, facilitating point-in-time recovery, and minimizing the risk of data
loss in Kubernetes clusters.

The procedure below demonstrates how to create a Volume Snapshot Class and
Volume Snapshot for the Dell |CSI| provisioner.

.. note::

    It is necessary to have the |CSI| controller running to be able to create
    and mount the |PVC|.

.. note::

    The Dell-storage |CSIs| charts already create default snapshotclasses. You
    can list them by using the following command:

    .. code-block:: none

        ~(keystone_admin)$ kubectl get volumesnapshotclasses.snapshot.storage.k8s.io

.. rubric:: |proc|

|CSI|-Powerstore is used in this example.

#. Check if the Dell-storage application is successfully applied.

   .. code-block:: none

       ~(keystone_admin)$ system application-list
       +--------------------------+-----------+-------------------------------------------+------------------+--------------+-----------+
       | application              | version   | manifest name                             | manifest file    | status       | progress  |
       +--------------------------+-----------+-------------------------------------------+------------------+--------------+-----------+
       | dell-storage             | 24.03-36  | dell-storage-fluxcd-manifests             | fluxcd-manifests | applied      | completed |
       +--------------------------+-----------+-------------------------------------------+------------------+--------------+-----------+

#. Check if the |PVC| is created and bound.

   .. code-block:: none

       ~(keystone_admin)$ kubectl get -n dell-storage pvc
       NAME    STATUS   VOLUME                          CAPACITY   ACCESS MODES   STORAGECLASS         AGE
       pvol0   Bound    csi-powerstore-vol-0fca78f7dd   8Gi        RWO            csi-powerstore-nfs   5m

#. Check the available snapshot classes.

   .. code-block:: none

       ~(keystone_admin)$ kubectl get volumesnapshotclasses.snapshot.storage.k8s.io
       NAME                           DRIVER                       DELETIONPOLICY   AGE
       csi-powerstore-snapshot        csi-powerstore.dellemc.com   Delete           5m

#. Create the |PVC| snapshot file and apply it.

   .. code-block:: none

       ~(keystone_admin)$ cat << EOF > ~/powerstore-volume-snapshot.yaml
       apiVersion: snapshot.storage.k8s.io/v1
       kind: VolumeSnapshot
       metadata:
       name: csi-powerstore-pvc-snapshot
       namespace: dell-storage
       spec:
       volumeSnapshotClassName: csi-powerstore-snapshot
       source:
          persistentVolumeClaimName: pvol0
       EOF

       ~(keystone_admin)$ kubectl apply -f ~/powerstore-volume-snapshot.yaml
       volumesnapshot.snapshot.storage.k8s.io/csi-powerstore-pvc-snapshot created

#. Confirm that the |PVC| snapshot file was created successfully.

   .. code-block:: none

       ~(keystone_admin)$ kubectl get volumesnapshots.snapshot.storage.k8s.io
       NAME                         READYTOUSE  SOURCEPVC  SOURCESNAPSHOTCONTENT RESTORESIZE SNAPSHOTCLASS             SNAPSHOTCONTENT             CREATIONTIME  AGE
       csi-powerstore-pvc-snapshot  true        pvol0                            8Gi         csi-powerstore-snapshot   snapcontent-dcfdbb32-       13s           21s
                                                                                                                        c936-469e-aa01-abe59f574fd1



