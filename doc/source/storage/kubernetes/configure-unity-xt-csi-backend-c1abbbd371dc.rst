.. WARNING: Add no lines of text between the label immediately following
.. and the title.

.. _configure-unity-xt-csi-backend-c1abbbd371dc:

==============================
Configure Unity XT CSI Backend
==============================

|CSI| driver for Unity XT is a part of the |CSM| open-source suite of
Kubernetes storage enablers for Dell products. It is a CSI driver that provides
support for provisioning persistent storage using Dell Unity XT storage array.

.. note::

    By default, the csi-unity chart is disabled. It is necessary to enable the
    chart and update user-overrides before applying the dell-storage application.

.. contents::
   :local:
   :depth: 2

----------------------
Enable CSI Unity Chart
----------------------

.. note::

    Disable any other |CSI|/|CSM| if not used/configured, otherwise it could
    cause the application to fail on apply.

    .. code-block::

        (keystone_admin)$ system helm-chart-attribute-modify --enabled false dell-storage <chart_name> dell-storage

#. Enable the chart.

   .. code-block:: none

       (keystone_admin)$  system helm-chart-attribute-modify --enabled true dell-storage csi-unity dell-storage

#. Create overrides to configure the storage connection.

   .. code-block:: none

       :name: unityOverrides.yaml

        csm:
          resiliency: false
        app.starlingx.io/component: platform

        storageClasses:
        - name: unity-virt2307pjxdbt-nfs
          arrayId: <ARRAY_ID>
          nasServer: nas_2
          provisioner: csi-unity.dellemc.com
          fstype: nfs
          reclaimPolicy: Delete
          allowVolumeExpansion: true
          volumeBindingMode: Immediate
          mountOptions: ["hard"]
          hostIoSize: 8192
          tieringPolicy: 0
          isDataReductionEnabled: "false"
          thinProvisioned: "true"
          storagePool: pool_2
          protocol: NFS
          allowedTopologies:
            - matchLabelExpressions:
              - key: "csi-unity.dellemc.com/virt2307pjxdbt-nfs"
                values:
                  - "true"

        secret:
          arrays:
          - arrayId: <ARRAY_ID>
            username: <USER>
            password: <PASSWORD>
            endpoint: https://<UNITY_ADDRESS>
            isDefault: true
            skipCertificateValidation: true

#. Apply overrides.

   .. code-block:: none

       (keystone_admin)$ system helm-override-update dell-storage csi-unity dell-storage --values=unityOverrides.yaml

#. Apply the chart.

   .. code-block:: none

       (keystone_admin)$ system application-apply dell-storage

------------------------
UserOverrides Parameters
------------------------

******************
General Parameters
******************

.. table::
    :widths: auto

    +----------------------------+--------------------------------------------+----------+----------+
    | Parameter                  | Description                                | Required | Default  |
    +============================+============================================+==========+==========+
    | csm.resiliency             | Enables/disables podmon sidecar container. | No       | false    |
    +----------------------------+--------------------------------------------+----------+----------+
    | app.starlingx.io/component | Core Affinity.                             | No       | platform |
    +----------------------------+--------------------------------------------+----------+----------+


*************************
StorageClasses Parameters
*************************

.. table::
    :widths: auto

    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | Parameter                                | Description                                                                                                      | Required | Default                             |
    +==========================================+==================================================================================================================+==========+=====================================+
    | storageClasses[0].hostIoSize             | Inserts Host IO size that is to be set for the filesystem.                                                       | No       | 8192                                |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].name                   | StorageClass name.                                                                                               | No       | "unity-" + arrayId + "-" + protocol |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].mountOptions           | Defines mount input values.                                                                                      | No       | []                                  |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].provisioner            | Driver name.                                                                                                     | No       | csi-unity.dellemc.com               |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].reclaimPolicy          | PVs that are dynamically created by a StorageClass will have the reclaim policy specified here.                  | No       | Delete                              |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].volumeBindingMode      | Controls when volume binding and dynamic provisioning should occur.                                              | No       | Immediate                           |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].fstype                 | Filesystem type for mounted volumes.                                                                             | No       | nfs                                 |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].protocol               | Defines FC or FIBRE for FibreChannel, ISCSI, |NFS|, or "" for auto selection.                                    | No       | |NFS|                               |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].allowVolumeExpansion   | Allows the users to resize the volume by editing the corresponding |PVC| object.                                 | No       | true                                |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].isDataReductionEnabled | Defines Boolean to choose value of is DataReductionEnabled while creating a new volume. Type: string.            | No       | true                                |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].thinProvisioned        | Defines Boolean to choose value of thinProvisioned while creating a new volume. Type: string.                    | No       | true                                |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].arrayId                | ID of array to be used for volumes. arrayID corresponding to array's globalID specified in secrets.              | Yes      | Not Applicable                      |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].nasServer              | |NAS| server's name. Important: required for |NFS| protocol.                                                     | No       | Not Applicable                      |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].hostIOLimitName        | Insert Host IO Limit Name that is to be used for provisioning here.                                              | No       | Not Applicable                      |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].tieringPolicy          | Tiering policy to be used during provisioning. Requires FAST VP license.                                         | No       | Not Applicable                      |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].storagePool            | Defines storage pool. Value should be picked from the column labeled "CLI ID" of pools in the Unisphere GUI.     | Yes      | Not Applicable                      |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+
    | storageClasses[0].allowedTopologies      | This feature lets you specify sub-divisions of a cluster for a volume to be placed in.                           | No       | Not Applicable                      |
    +------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------+-------------------------------------+

*****************
Secret Parameters
*****************

.. table::

    +--------------------------------------------+----------------------------------------------------------------------------------------+----------+----------------+
    | Parameter                                  | Description                                                                            | Required | Default        |
    +============================================+========================================================================================+==========+================+
    | secret.arrays                              | List of Unity XT.                                                                      | Yes      | Not Applicable |
    +--------------------------------------------+----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].arrayId                   | Array ID of Unity XT.                                                                  | Yes      | Not Applicable |
    +--------------------------------------------+----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].username                  | Username for connecting to Unity XT Unisphere REST API server.                         | Yes      | Not Applicable |
    +--------------------------------------------+----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].password                  | Password for connecting to Unity XT Unisphere REST API server.                         | Yes      | Not Applicable |
    +--------------------------------------------+----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].endpoint                  | HTTPS endpoint of the Unity XT Unisphere REST API server.                              | Yes      | Not Applicable |
    +--------------------------------------------+----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].isDefault                 | Treats current array as a default. Use at least one of the arrays as default.          | No       | false          |
    +--------------------------------------------+----------------------------------------------------------------------------------------+----------+----------------+
    | secret.arrays[0].skipCertificateValidation | Indicates if client side validation of server's certificate can be skipped.            | No       | true           |
    +--------------------------------------------+----------------------------------------------------------------------------------------+----------+----------------+
