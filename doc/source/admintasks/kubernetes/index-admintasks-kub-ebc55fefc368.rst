.. _index-admintasks-kub-ebc55fefc368:

.. include:: /_includes/toc-title-admintasks-kub.rest

.. only:: partner

   .. include:: /admintasks/index-admintasks-768a6e9aaeff.rst
      :start-after: kub-begin
      :end-before: kub-end

.. toctree::
   :maxdepth: 1

   about-the-admin-tutorials

----------------------
Application management
----------------------

.. toctree::
   :maxdepth: 1

   kubernetes-admin-tutorials-helm-package-manager
   kubernetes-admin-tutorials-starlingx-application-package-manager
   admin-application-commands-and-helm-overrides

---------------------
Local Docker registry
---------------------

.. toctree::
   :maxdepth: 1

   local-docker-registry
   kubernetes-admin-tutorials-authentication-and-authorization
   installing-updating-the-docker-registry-certificate
   setting-up-a-public-repository
   freeing-space-in-the-local-docker-registry

--------------------------------
Optimize application performance
--------------------------------

.. toctree::
   :maxdepth: 1

   kubernetes-cpu-manager-policies
   isolating-cpu-cores-to-enhance-application-performance
   kubernetes-topology-manager-policies
   kubernetes-memory-manager-policies-3de9d87855bc


--------------
Metrics Server
--------------

.. toctree::
   :maxdepth: 1

   kubernetes-admin-tutorials-metrics-server

-----------------
PTP Notifications
-----------------

.. toctree::
   :maxdepth: 1

   install-ptp-notifications
   remove-ptp-notifications

------------------
O-RAN O2 Interface
------------------

.. toctree::
   :maxdepth: 1

   oran-o2-application-b50a0c899e66

-------------------------
Harbor Container Registry
-------------------------

.. toctree::
   :maxdepth: 1

   harbor-as-system-app-1d1e3ec59823

--------------------------------------
Technology Preview - Istio Application
--------------------------------------

.. toctree::
   :maxdepth: 1

   istio-service-mesh-application-eee5ebb3d3c4
