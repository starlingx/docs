
.. njh1572366777737
.. _about-the-admin-tutorials:

=========================
About the Admin Tutorials
=========================

The |prod-long| Kubernetes administration tutorials provide working examples
of common administrative tasks.

.. xreflink For details on accessing the system, see :ref:`|prod| Access the System <configuring-local-cli-access>`.

Common administrative tasks covered in this document include:

.. only:: partner

    .. include:: /_includes/about-the-admin-tutorial.rest
        :start-after: about-the-admin-tutorial-begin
        :end-before: about-the-admin-tutorial-end

-   application management

-   local Docker registries

-   Kubernetes CPU resource management
