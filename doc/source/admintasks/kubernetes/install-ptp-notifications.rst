
.. xqd1614091832213
.. _install-ptp-notifications:

=========================
Install PTP Notifications
=========================

|PTP| notification is packaged as a system application and is managed
using the :command:`system application` and :command:`system helm-override`
commands.

The application monitors time related services on a host and provides an API
for subscribing to asynchronous status notifications as well as the ability to
pull the state of each service on demand.

.. note::

    Changes to a node's |PTP| configuration, applied using the
    :command:`system ptp-instance-apply`, requires the ``ptp-notification``
    application to be removed and reapplied, using the
    :command:`system application-remove ptp-notification`, and
    :command:`system application-apply ptp-notification` commands.
    This allows the containers to reload the updated configuration files and
    monitor the services correctly.

**v1 API**

The legacy functionality of ``ptp-notification`` remains available and is
accessible through the v1 API; v1 is only capable of reporting status changes
for the |PTP| Sync State on a system.

**Limitations**

The v1 API only supports monitoring a single ``ptp4l + phc2sys`` instance.
Ensure the system is not configured with multiple instances when using the v1
API.

**v2 API**

The API conforms to O-RAN.WG6.O-Cloud Notification API-v02.01. Using the v2
API, multiple ``ptp4l`` instances can be tracked for independent |PTP| Sync
State and |PTP| Clock Class notifications.

The application monitors the following services:

-  PTP Sync State

-  PTP Clock Class

-  OS Clock Sync State

-  GNSS Sync State

-  Overall System Sync State


.. rubric:: |context|

|prod-long| provides the capability for application(s) to subscribe to
asynchronous |PTP| status notifications and pull for the |PTP| state on demand.

You must provide Helm override values indicating the ``ptp4l`` and ``phc2sys``
instances that you want tracked by your ``ptp-notification`` application.

Since multiple ``ptp4l`` instances can be supported on a node, you must specify
the ``ServiceName`` of the instance that the ``ptp-notification`` application
should track.

For example, follow the steps below:

.. rubric:: |proc|

#.  Apply labels to nodes that will be running the ``ptp-notification``.

    #.  Apply the registration label to the controller nodes.

        .. code-block::

            ~(keystone_admin)]$ system host-label-assign controller-0 ptp-registration=true

    #.  Apply the notification label to each node that is configured for PTP
        clock synchronization.

        .. code-block::

            ~(keystone_admin)]$ system host-label-assign controller-0 ptp-notification=true
            ~(keystone_admin)]$ system host-label-assign compute-0 ptp-notification=true

    #.  Verify the labels.

        .. code-block::

            ~(keystone_admin)]$ system host-label-list <node name>

#.  Locate the application tarball on the system controller.

    .. code-block::

        ~(keystone_admin)]$ ls /usr/local/share/applications/helm/ptp-notification-<version>.tgz

#.  Upload the ``ptp-notification`` application using the command below.

    .. code-block::

        ~(keystone_admin)]$ system application-upload <path to application>

#.  Verify if the application is in the uploaded state.

    .. code-block::

        ~(keystone_admin)]$ system application-list

#.  Apply Helm overrides as required. Create a yaml file and update the fields
    that require Helm overrides.

    .. code-block::

        ~(keystone_admin)]$ system helm-override-update ptp-notification ptp-notification notification --values notification-override.yaml

    .. note::

        You can override the default values for the ``ptp-notification``
        application either by creating separate override sections for v1
        and v2 APIs or by including v1 and v2 APIs in a single file as
        shown in the example below.

    .. code-block:: none

        ~(keystone_admin)]$ cat notification-override.yaml
        ptptracking:
          ptp4lServiceName: ptp4l-legacy
          phc2sysServiceName: phc2sys-legacy
          logging_level: INFO
          ptp4lClockClassLockedList: "6,7,135"
          device:
            holdover_seconds: 15
            poll_freq_seconds: 2
        ptptrackingv2:
          ptp4lServiceName: True
          phc2sysServiceName: True
          ts2phcServiceName: True
          ptp4lClockClassLockedList: "6,7,135"
          phc2sysToleranceThreshold: 1000
          log_level: INFO
          control_timeout: 2
          device:
            holdover_seconds: 15
          osclock:
            holdover_seconds: 15
          overall:
            holdover_seconds: 15

    #.  To configure the ``ptp-notification`` v1 API in a seperate section,
        include the following in the ``notification-override.yaml`` file.
        Ensure that values are updated to match the configured instance
        names on your system.

        .. code-block:: none

            ptptracking:
              enabled: True
              ptp4lSocket: /var/run/ptp4l-instancename
              ptp4lServiceName: ptp4l-instancename
              phc2sysServiceName: phc2sys-instancename
              logging_level: INFO
              ptp4lClockClassLockedList: "6,7,135"
              device:
                holdover_seconds: 15
                poll_freq_seconds: 2

        ``ptptracking``

        where the values are:

        ``ptp4lSocket``
            Update this value to include the correct instance name of your
            configured ptp4l instance.

        ``ptp4lServiceName``
            Update this value to the instance name of your configured ptp4l
            instance.

        ``phc2sysServiceName``
            Update this value to the instance name of your configure phc2sys
            instance.

        ``logging_level: INFO``
            Set the logging level. DEBUG can be used for additional logs.

        ``notification_format: standard``
            Set the notification output format to standard formatting. The
            value can be set to "legacy" for compatibility with older client
            versions.        

        ``ptp4lClockClassLockedList``
            Set the list of clock classes that will allow ``ptp-notification``
            to report **Locked**. The clockClass for a monitored ptp4l instance
            is read via the |PMC|. If the instance clockClass matches one of
            the ``ptp4lClockClassLockedList`` values, then ``ptp-notification``
            will report **Locked** for that instance.

            The default values are "6,7,135", which means that
            ``ptp-notification`` will report locked when reading a clockClass
            of 6, 7 or 135 from the configured ptp4l instance. These values are
            recommended for nodes operating as Boundary Clock (BC).

            For nodes operating as |GM|, it is recommended to set the value to
            "6", so that only clockClass 6 is reported as locked.

        ``holdover_seconds``
            ``holdover_seconds`` configures how long each service will stay in
            the HOLDOVER state before transitioning to FREERUN. The holdover
            value used by the application equates to: holdover_seconds -
            (poll_freq_seconds * 2).

            This is done in order to account for time between the monitor
            polling cycles. The ``holdover_seconds`` value should be configured
            to match the validated holdover time provided by the device
            manufacturer.

        ``poll_freq_seconds``
            ``poll_freq_seconds`` sets how frequently, in seconds the services
            are checked.

    #.  To configure the ``ptp-notification`` v2 API in a separate section,
        include the following in the ``notification-override.yaml`` file.
        Ensure that values are updated to match the configured instance
        names on your system.

        .. code-block:: none

            ptptrackingv2:
              ptp4lServiceName: True
              phc2sysServiceName: True
              ts2phcServiceName: True
              log_level: INFO
              ptp4lClockClassLockedList: "6,7,135"
              phc2sysToleranceThreshold: 1000
              control_timeout: 2
              device:
                holdover_seconds: 15
              osclock:
                holdover_seconds: 15
              overall:
                holdover_seconds: 15

        ``ptptrackingv2``

        where the values are:

        ``ptp4lServiceName: True``

        ``phc2sysServiceName: True``

        ``ts2phcServiceName: True``
            -   The ServiceName fields are defaulted to "True" in the
                application and do not need to be altered.

            -   A service can be set to "False" in order to disable tracking
                for that type. However, if a service type is not configured on
                the node (i.e. node does not use ts2phc), then the application
                automatically determines this and does not attempt to monitor
                the node.

            -   Use these fields if there is a service that is configured on
                the node that you do NOT want to track.

        ``log_level: INFO``
            Set the logging level. DEBUG can be used for additional logs.

        ``ptp4lClockClassLockedList``
            Set the list of clock classes that will allow ``ptp-notification``
            to report **Locked**. The clockClass for a monitored ptp4l instance
            is read via the |PMC|. If the instance clockClass matches one of
            the ``ptp4lClockClassLockedList`` values, then ``ptp-notification``
            will report **Locked** for that instance.

            The default values are "6,7,135", which means that
            ``ptp-notification`` will report locked when reading a clockClass
            of 6, 7 or 135 from the configured ptp4l instance. These values are
            recommended for nodes operating as Boundary Clock (BC).

            For nodes operating as |GM|, it is recommended to set the value to
            "6", so that only clockClass 6 is reported as locked.

        ``phc2sysToleranceThreshold``
            Default value: 1000

            Set the skew threshold in nanoseconds at which ``ptp-notification``
            will report that the system clock is no longer considered
            **Locked**.

            The ``ptp-notification`` application compares the time of the
            system clock to the configured source PHC. If the delta between the
            system clock and the |PHC| is greater than the
            ``phc2sysToleranceThreshold``, a notification will be generated
            that the system clock is not locked.

        ``control_timeout: 2``
           control_timeout sets how frequently, in seconds the services are checked.
           Value applies to all service types.

        ``device``
            ``device`` refers to ptp4l monitoring

            -  ``holdover_seconds: 15``

            -  ``poll_freq_seconds: 2``

        ``osclock``
            holdover_seconds: 15

        ``overall``
            holdover_seconds: 15
                ``holdover_seconds`` configures how long each service will stay
                in the HOLDOVER state before transitioning to FREERUN. The
                holdover value used by the application equates to:
                holdover_seconds - (control_timeout * 2).

                This is done in order to account for time between the monitor
                polling cycles. The ``holdover_seconds`` value should be
                configured to match the validated holdover time provided by the
                device manufacturer.

    #.  View existing values.

        .. code-block:: none

            ~(keystone_admin)]$ system helm-override-show ptp-notification ptp-notification notification

    #.  Update and apply the values.

        Application values can be added by the user and applied, using the following commands.

        .. note::

            Changes to the ``ptp-notification`` override values require the
            application to be removed and re-applied in order to re-create the
            application containers.

        .. code-block:: none

            ~(keystone_admin)]$ system application-remove ptp-notification

            ~(keystone_admin)]$ system helm-override-update ptp-notification ptp-notification notification -–values <notification-override.yaml>

            ~(keystone_admin)]$ system application-apply ptp-notification

#.  Verify the Helm overrides.

    .. code-block::

        ~(keystone_admin)]$ system helm-override-show ptp-notification ptp-notification notification

#.  Apply ``ptp-notification`` using the command below.

    .. code-block::

        ~(keystone_admin)]$ system application-apply ptp-notification


#.  Verify application status and pod status using the following commands:

    #.  Application Status

        .. code-block::

            ~(keystone_admin)]$ system application-list

    #.  Pod Status

        .. code-block::

            ~(keystone_admin)]$ kubectl get pods -n notification -o wide

.. _configure-liveness-probes:

---------------------------------------------------
Configure Liveness Probes for PTP Notification Pods
---------------------------------------------------

Helm overrides can be used to configure liveness probes for the following
``ptp-notification`` containers:

- locationservice-base

- notificationservice-base

- notificationservice-base-v2

To enable the liveness probe for each container, include the following in the
notification-override.yaml file:

.. code-block::

    location:
      endpoint:
        port: 8080
        liveness: True
        livenessDelaySeconds: 60
        livenessPeriodSeconds: 3
        livenessFailureThreshold: 3
        livenessTimeoutSeconds: 3
    ptptracking:
      endpoint:
        port: 8081
        liveness: True
        livenessDelaySeconds: 60
        livenessPeriodSeconds: 3
        livenessFailureThreshold: 3
        livenessTimeoutSeconds: 3
    ptptrackingv2:
      endpoint:
        port: 8082
        liveness: True
        livenessDelaySeconds: 60
        livenessPeriodSeconds: 3
        livenessFailureThreshold: 3
        livenessTimeoutSeconds: 3

Apply the override values using the following commands:

.. code-block:: none

    ~(keystone_admin)]$ system helm-override-update ptp-notification ptp-notification notification --values <notification-override.yaml>
    ~(keystone_admin)]$ system application-apply ptp-notification

.. rubric:: |postreq|

|prod-long| supports applications that rely on PTP for synchronization.
These applications are able to receive PTP status notifications from |prod-long|
hosting the application. For more information see:

-  :ref:`PTP Notifications Overview <ptp-notifications-overview>`

-  `API PTP Notifications <https://docs.starlingx.io/api-ref/ptp-notification-armada-app/api_ptp_notifications_definition_v1.html>`__

.. only:: partner

    .. include:: /_includes/install-ptp-notifications-3a94b1ea1ae3.rest

---------------------------------------
Disable PTP Notification v1/v2 API Pods
---------------------------------------

When installing and configuring ``ptp-notification-v2``, it is recommended to
disable the ``ptp-notification-v1`` API pod to reduce configuration complexity
resulting from running the v1 and v2 API pods at the same time.

.. rubric:: |proc|

#. Set the ``enabled`` flag to False in the Helm override file for either
   ``ptptracking`` or ``ptptrackingv2``.

   .. code-block:: none

       ptptracking:
        enabled: False

   .. code-block:: none

       ptptrackingv2:
        enabled: False

#. Update Helm override.

   .. code-block:: none

       ~(keystone_admin)]$ system helm-override-update ptp-notification ptp-notification notification --values notification-override-2212.yaml

#. Apply ``ptp-notification``.

   .. code-block:: none

       ~(keystone_admin)]$ system application-apply ptp-notification




