============================
Distributed Cloud Management
============================

This is a stub page for the topic: |prod-dc| Management. You can help
StarlingX by expanding the content.

See the story for additional information about what is needed:
`Add Distributed Cloud Management Guide <https://storyboard.openstack.org/#!/story/2006879>`_

For information about contributing to the StarlingX documentation, see the
:doc:`/contributor/doc_contribute_guide`.

.. contents::
   :local:
   :depth: 1