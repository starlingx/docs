
declare RED='\033[0;31m'
declare OG="\033[93m"
declare GR='\033[0;32m'
declare NC='\033[0m'

# Output functions. Colorize various types of messages. 
message () { echo -e "$@" 1>&2; }
confirmation () { message $GR$@$NC; }
warn () { message $OG$@$NC; }
error () { message $RED$0:$?: $@$NC; exit 1; }

declare meta_ver=":MM(.)mm:"

# Check for and exit if file dependancies are not met. Takes a list of full or
# relative paths.
check_file_deps () {
  for filereq in $@
  do
    if [ ! -f "${filereq}" ] && [ ! -L "${filereq}" ]; then error "${filereq}  not found. Quiting."; exit 1; fi
  done
  return 0
}

# Check for and exit if command dependancies are not met. Takes a list of 
# executables.
check_util_deps () {
  for dep in $@
  do
    if ! hash $dep 2>/dev/null; then
      error >&2 "... $dep dependency not met. Please install."
      exit 1
    fi
  done
}

# Creates an rST  title over/underscore string of the same length
# as the argument. Section strings are not supported. Returned output
# is a sequence of equal signs (=).
make_strike () {
  local _title="$1"
  local _strike
  _strike=$(for ((i=1; i<=${#_title}; i++)); do
    printf '=%.0s' "$i"
  done)
  echo $_strike
}

# Trim leading and trailing whitespaces from string.
trimspaces () {
  local _s=$1

  _s="${_s#"${_s%%[![:space:]]*}"}"
  _s="${_s#"${_s%%[![:space:]]*}"}"

  echo $_s
}

# Sets a global hash of acronyms and definitions from rst :abbr: defs. Also
# sets an array of hash keys to facilitate sorting.
#
# Takes path to the file to parse. Optional "1" flag as second option 
# suppresses plural forms such as "PVCs". 
get_abbrs () {

  local ABBREVS
  declare -a -g acro_keys
  declare -A -g acro_keyvals
  local regex=":abbr:\`([A-Za-z]+)\s+\((.*)\)\`"

  [[ ! -z ${1+x} ]] && [[ -e $1 ]] && ABBREVS="$1" \
     || error "Can't find abbrevs file $1"

  [[ ! -z $2{+x} ]] && [[ ${2} == "1" ]] \
     && local strip_plurals=$2

  while IFS= read -r line
  do
    if [[ $line =~ $regex ]]; then 
  
      if [[ ${strip_plurals} -eq 1 ]] && [[ ${BASH_REMATCH[1]:0-1} == "s" ]]; then
        message " Skipping pluralization \"${BASH_REMATCH[1]}\""
        continue
      fi
      acro_keys+=("${BASH_REMATCH[1]}")
      acro_keyvals["${BASH_REMATCH[1]}"]="${BASH_REMATCH[2]}"
    fi
  done < "$ABBREVS" || error "Cannot read $ABBREVS"

}


# Report duplicate :abbr: anchor strings. (Duplicate placeholders cause 
# Sphinx warnings.)
#
# Takes an array of anchor strings. Echos duplicates and returns a duplicate
# count
check_abbr_dups () {

   local -a _anchors=("$@")
   declare -a dups; declare -i _dup_count=0
   IFS=$'\n'; dups=($(sort -f <<<"${_anchors[*]}")); unset IFS
   
   message "... Checking for duplicate anchor strings"
   
   for ((i=0; i < ${#dups[@]}; i++)); do
     if [[ ${dups[$i]} == ${dups[$i-1]} ]]; then 
       warn " Duplicate anchor string \"${dups[$i]}\" found"
       ((_dup_count=$_dup_count+1))
     fi
   done

   echo $_dup_count

}

# Return this product version
#
# Finds and echos out this-ver value from _this.txt
# Calling context is responsible for parsing major and
# minor numbers into vars (g_YY and g_MM by convention)
get_this_version() {

  local this_file="_this.txt"
    local ver_file=$(find doc* -name $this_file)
    [[ ! -f ${ver_file} ]] && \
       error "$this_file not found. Quiting."

  local _ver
  local regex_ver="\.\.\s+\|this-ver\|\s+replace::\s+([0-9]{2}\.[0-9]{2})\s*"

  local _this=$(grep this-ver $ver_file)
  
  if [[ "${_this}" =~ $regex_ver ]]
  then
     _ver="${BASH_REMATCH[1]}"
  else
     error "Can't find local product version"
  fi
  echo $_ver

}


# List hash of separator characters used in ":MM<char>mm:" style 
# placeholders
#
# Takes path to the file to scan.
# Sets global hash 'spacers'. Note that BRE special characters will be escaped.

get_version_spacers() {

   local _sep
   check_file_deps $1 && \
     local _f=$1

   spacers=()
   while read l; do
      _sep=''
      if [[ $l =~ ${meta_ver} ]]; then
        [[ ${BASH_REMATCH[1]} == [\$\*\.\\\[\]] ]] && \
           _sep="\\${BASH_REMATCH[1]}" || \
           _sep="${BASH_REMATCH[1]}"
        spacers["${_sep}"]=0
      fi
   done < $_f

}

# Replace ":MM<char>mm:" placeholders in a target file.
#
# Takes path to file to manipulate and overwrites on disk
# Returns 0 on success
# Note: global hash 'spacers' must be set; see get_version_spacers

replace_version_placeholders() {

   check_file_deps $1 && \
     local _f=$1
     local _rs

   for sp in "${!spacers[@]}"; do
     [[ $sp == '^' ]] && _rs="" || _rs=$sp
     sed -i "s/:MM${sp}mm:/${g_YY}$_rs${g_MM}/g" $_f
   done

   return 0

}

# Return the name of the current default branch
get_this_branch() {

  local _regex_br="defaultbranch=(.*)\s*$"
  local _config="$PWD/.gitreview"

  if [[ $(cat $_config) =~ $_regex_br ]]; then
     echo "${BASH_REMATCH[1]}"
  else
     error "$PWD is not a branch"
  fi
}

declare utils_loaded=1
