import pandas as pd
import argparse
import re

from _p_columns import columns, port_index

def export_to_csv(input_file, output_file, columns, filters, sort_orders):
    # Load the Excel file
    df = pd.read_excel(input_file)

    # Filter columns
    df = df[columns]

    # Apply filters
    for column, value in filters.items():
        if isinstance(value, list):
            df = df[df[column].isin(value)]
        else:
            df = df[df[column] == value]

    # Apply sort orders
    sort_columns = [col for col, order in sort_orders.items()]
    sort_ascending = [order == 'asc' for order in sort_orders.values()]
    df = df.sort_values(by=sort_columns, ascending=sort_ascending)

    # Drop filter-only columns that begin with an underscore
    pattern = re.compile("^_[a-zA-Z]+$")
    for c in columns:
        if pattern.match(c):
            df.pop(c)

    # Export to CSV
    df.to_csv(output_file, index=False)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Export a CSV list of ports from Excel with specified columns, filters, and sort orders.")
    parser.add_argument("input_file", help="Path to the input Excel file. Positioned BEFORE options.")
    parser.add_argument("output_file", help="Path to the output CSV file. Positioned BEFORE options.")
    parser.add_argument("--columns", nargs='+', required=True, help="Space separated list of columns to include in the CSV file")
    parser.add_argument("--filters", nargs='*', required=True, action='append', help="Column filters in the format column=value or column=[value1,value2,...]")
    parser.add_argument("--sort_orders", nargs='*', required=True, action='append', help="Sort orders in the format column=asc/desc")

    args = parser.parse_args()

    # Process filters argument
    filters = {}
    for filt in args.filters:
        for f in filt:
            column, value = f.split('=')
            if value.startswith('[') and value.endswith(']'):
                value = value.strip('[]').split(',')
            filters[column] = value

    # Process sort orders argument
    sort_orders = {}
    for sort in args.sort_orders:
        for s in sort:
            column, order = s.split('=')
            sort_orders[column] = order

    export_to_csv(args.input_file, args.output_file, args.columns, filters, sort_orders)

# Note that positional args are first. Hidden filter columns must be listed in --columns
# e.g: python3.10 xlst_2_csv.py FW_PORTS.xlsx FW_PORTS.csv --columns Source Port Protocol Network Desc HTTPS Note _pl --sort_orders Port=asc --filters _pl=y
